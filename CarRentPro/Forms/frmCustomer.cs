﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using System.IO;
using MySql.Data.MySqlClient;

namespace CarRentPro.Forms
{ 
    public partial class frmCustomer : DevExpress.XtraEditors.XtraForm
    {
        MySqlCommand comm;
        MySqlConnection con;
        public frmCustomer()
        {
            InitializeComponent();
        }
        public frmCustomer(string CstNic)
        {
            InitializeComponent();
            txtNic.Text = CstNic;
        }
        public void CRPrint()
        {
            try
            {

               

                MySqlConnection con = null;

                MySqlCommand cmd = null;

                rptCustomerNICLIC rpt = new rptCustomerNICLIC();


                //The report  created.
                cmd = new MySqlCommand();
                MySqlDataAdapter myDA = new MySqlDataAdapter();

                DataSet myDS = new DataSet();
                //The DataSet created.

                con = new MySqlConnection(AppSett.CS);
                cmd.Connection = con;

                cmd.CommandText =  "SELECT `picID`, `picDLic`, `NIC` FROM  `carrentpro`.`tblcustomer` WHERE `NIC` = '" + txtNic.Text+"'";
                cmd.CommandType = CommandType.Text;
                myDA.SelectCommand = cmd;
                myDA.Fill(myDS, "tblcustomer");

                rpt.SetDataSource(myDS);

                //frmRecord frm = new frmRecord();

                //frm.crystalReportViewer1.ReportSource = rpt;

                // ************** quick Print CRPrint Report without wizad*************

               // rpt.PrintToPrinter(1, true, 0, 0);

                //******************************
                //frm.Visible = true;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void clrCustDetails()
        {
            lblCustID.Text = "";
            txtNic.Text = "";
            txtName.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtLicNo.Text = "";
            dtpLicExp.Text = "";
            txtContact1.Text = "";
            txtContact2.Text = "";
            txtContact3.Text = "";
            picCustomer.Image = CarRentPro.Properties.Resources.user;
            chkblacklist.Checked = false;
            picNIC.Image = CarRentPro.Properties.Resources.nic;
            picDLic.Image = CarRentPro.Properties.Resources.drlicno;
        }

        public void loadCustDetails(string CustNic)
        {
            this.clrCustDetails();
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `ID`,`NIC`,`FName`,`Address1`,`Address2`,`LicNo`,`ExDate`,`Contact1`,`Contact2`,`Contact3`,`Pic`,`BlackLIsted`,`picID`,`picDLic` FROM `tblcustomer` WHERE `NIC` = '" + CustNic + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        lblCustID.Text = rdr[0].ToString();
                        txtNic.Text = rdr[1].ToString();
                        txtName.Text = rdr[2].ToString();
                        txtAddress1.Text = rdr[3].ToString();
                        txtAddress2.Text = rdr[4].ToString();
                        txtLicNo.Text = rdr[5].ToString();
                        dtpLicExp.Text = rdr[6].ToString();
                        txtContact1.Text = rdr[7].ToString();
                        txtContact2.Text = rdr[8].ToString();
                        txtContact3.Text = rdr[9].ToString();
                        picCustomer.Image = this.ByteArray2Image((byte[])rdr[10]);
                        chkblacklist.Checked = (bool)rdr[11];
                        if (rdr[12].ToString()!="")
                        {
                            picNIC.Image = this.ByteArray2Image((byte[])rdr[12]);
                            
                        }
                        if (rdr[12].ToString()!="")
                        {
                            picDLic.Image = this.ByteArray2Image((byte[])rdr[13]);
                            
                        }
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }

            }
        }
        public string GetCustID(string NIC)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                string cstID = "";
                try
                {
                    string Sql = "SELECT `ID` FROM `tblcustomer` WHERE `NIC`='" + NIC + "'";
                    comm = new MySqlCommand(Sql, con);
                    con.Open();
                    MySqlDataReader rdr = comm.ExecuteReader();

                    while (rdr.Read())
                    {
                        cstID = rdr[0].ToString();
                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
                return cstID;

            }
        }
        public void loadCustList()
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    lstCustList.Items.Clear();
                    string Sql = "SELECT `NIC`,`FName`,`Contact1` FROM `tblcustomer`";
                    comm = new MySqlCommand(Sql, con);
                    con.Open();
                    MySqlDataReader rdr = comm.ExecuteReader();

                    while (rdr.Read())
                    {
                        ListViewItem lstItem = new ListViewItem(Convert.ToString(rdr[0]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[1]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[2]));
                        lstCustList.Items.Add(lstItem);
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        public Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            return newImage;
        }
        public void UpdateID(string Item, int NewID)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                int intnewid = NewID;
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tbllastid` SET " + Item.Trim() + "='" + intnewid + "' WHERE clmnID=1";
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                comm.Dispose();
            }
        }
        private Image LoadPic(string Loc)
        {
              return this.ScaleImage(new Bitmap(Loc), 800, 800);
        }
        public int GetLastID(string Item)
        {
            int lastID = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT " + Item + " FROM tbllastid";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    lastID = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return lastID + 1;
        }
        public byte[] imageToByte(Image img)
        {
            using (var ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }
        public Image ByteArray2Image(byte[] Barry)
        {
            MemoryStream ms = new MemoryStream(Barry);
            Image retImage = Image.FromStream(ms);
            return retImage;
        }
        private void pictureEdit1_DoubleClick(object sender, EventArgs e)
        {
            Forms.XtraForm2 frm = new XtraForm2();
            frm.Show();
        }
        private void pictureEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }
        private void frmCustomer_Load(object sender, EventArgs e)
        {
            this.loadCustList();
            
        }
        public void frmCustomer_Activated(object sender, EventArgs e)
        {
           // pictureEdit1.EditValue = Class1.capturedImage;
            if (txtNic.Text == "")
            {
                txtNic.Focus();

            }
            else
            {
                txtName.Focus();
            }
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            bool IsBlackListed = false;
            if (chkblacklist.Checked)
            {
                IsBlackListed = true;
            }
            string NewID = string.Format("CUS{0}", this.GetLastID("`tblcst`").ToString());
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    string sql = "INSERT INTO tblcustomer (ID,NIC,FName,Address1,Address2,LicNo,ExDate,Contact1,Contact2,Contact3,Pic,BlackLIsted) VALUES(@ID,@NIC,@FName,@Address1,@Address2,@LicNo,@ExDate,@Contact1,@Contact2,@Contact3,@Pic,@BlackLIsted)";
                    comm = new MySqlCommand(sql, con);
                    comm.Parameters.AddWithValue("@ID", NewID);
                    comm.Parameters.AddWithValue("@NIC", txtNic.Text.Trim());
                    comm.Parameters.AddWithValue("@FName", txtName.Text.Trim());
                    comm.Parameters.AddWithValue("@Address1", txtAddress1.Text.Trim());
                    comm.Parameters.AddWithValue("@Address2", txtAddress2.Text.Trim());
                    comm.Parameters.AddWithValue("@LicNo", txtLicNo.Text.Trim());
                    comm.Parameters.AddWithValue("@ExDate", dtpLicExp.Value.Date.ToShortDateString());
                    comm.Parameters.AddWithValue("@Contact1", txtContact1.Text.Trim());
                    comm.Parameters.AddWithValue("@Contact2", txtContact2.Text.Trim());
                    comm.Parameters.AddWithValue("@Contact3", txtContact3.Text.Trim());
                    comm.Parameters.AddWithValue("@Pic", this.imageToByte(picCustomer.Image));
                    comm.Parameters.AddWithValue("@BlackLIsted", IsBlackListed);
                    comm.ExecuteNonQuery();
                    this.loadCustList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                this.UpdateID("tblcst", this.GetLastID("`tblcst`"));
                lblCustID.Text = this.GetCustID(txtNic.Text.Trim());
            }
        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            ofd.Filter = ofd.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            if (ofd.ShowDialog()==DialogResult.OK)
            {
                picNIC.Image = this.LoadPic(ofd.FileName);

                try
                {
                    using (con = new MySqlConnection(AppSett.CS))
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = "UPDATE `tblcustomer` SET `picID`=@picID WHERE `ID`='" + lblCustID.Text + "'";
                        comm.Parameters.AddWithValue("@picID", this.imageToByte(picNIC.Image));
                        comm.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                comm.Dispose();
            }
            
        }
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            ofd.Filter = ofd.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                picDLic.Image = this.LoadPic(ofd.FileName);

                try
                {
                    using (con = new MySqlConnection(AppSett.CS))
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = "UPDATE `tblcustomer` SET `picDLic`=@picDLic WHERE `ID`='" + lblCustID.Text + "'";
                        comm.Parameters.AddWithValue("@picDLic", this.imageToByte(picDLic.Image));
                        comm.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                comm.Dispose();
            }
        }
        private void lstCustList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstCustList.SelectedItems.Count==1)
            {
                this.loadCustDetails(lstCustList.SelectedItems[0].Text);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            bool IsBlackListed = false;
            if (chkblacklist.Checked)
            {
                IsBlackListed = true;
            }
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE tblcustomer SET NIC=@NIC,FName=@FName,Address1=@Address1,Address2=@Address2,LicNo=@LicNo,ExDate=@ExDate,Contact1=@Contact1,Contact2=@Contact2,Contact3=@Contact3,Pic=@Pic,BlackLIsted=@BlackLIsted WHERE ID=@ID";
                    comm.Parameters.AddWithValue("@ID", lblCustID.Text);
                    comm.Parameters.AddWithValue("@NIC", txtNic.Text.Trim());
                    comm.Parameters.AddWithValue("@FName", txtName.Text.Trim());
                    comm.Parameters.AddWithValue("@Address1", txtAddress1.Text.Trim());
                    comm.Parameters.AddWithValue("@Address2", txtAddress2.Text.Trim());
                    comm.Parameters.AddWithValue("@LicNo", txtLicNo.Text.Trim());
                    comm.Parameters.AddWithValue("@ExDate", dtpLicExp.Value.Date.ToShortDateString());
                    comm.Parameters.AddWithValue("@Contact1", txtContact1.Text.Trim());
                    comm.Parameters.AddWithValue("@Contact2", txtContact2.Text.Trim());
                    comm.Parameters.AddWithValue("@Contact3", txtContact3.Text.Trim());
                    comm.Parameters.AddWithValue("@Pic", this.imageToByte(picCustomer.Image));
                    comm.Parameters.AddWithValue("@BlackLIsted", IsBlackListed);
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                this.loadCustList();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblCustID.Text.Length>3)
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = "DELETE FROM `tblcustomer` WHERE `ID`='" + lblCustID.Text + "'";
                        comm.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                this.clrCustDetails();
                this.loadCustList();
                
            }
            else
            {
                MessageBox.Show("Select Customer");
            }
            
        }

        private void btnDeleteNIC_Click(object sender, EventArgs e)
        {
            picNIC.Image = CarRentPro.Properties.Resources.nic;
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tblcustomer` SET `picID`=@picID WHERE `ID`='" + lblCustID.Text + "'";
                    comm.Parameters.AddWithValue("@picID", this.imageToByte(picNIC.Image));
                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            comm.Dispose();
        }

        private void btnDeleteDLIc_Click(object sender, EventArgs e)
        {
            picDLic.Image = CarRentPro.Properties.Resources.drlicno;
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tblcustomer` SET `picDLic`=@picDLic WHERE `ID`='" + lblCustID.Text + "'";
                    comm.Parameters.AddWithValue("@picDLic", this.imageToByte(picDLic.Image));
                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            comm.Dispose();
        }

        private void btnClr_Click(object sender, EventArgs e)
        {
            this.clrCustDetails();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            CRPrint();
        }

    }
}