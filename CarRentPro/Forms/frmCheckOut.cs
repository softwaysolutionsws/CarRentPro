﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.IO;
using System.Globalization;

namespace CarRentPro.Forms
{
    public partial class frmCheckOut : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand comm;
        bool IsNewBill = true;
        bool isrent = true;
        string Billno;
        double BilTot;
        double thisTot;
        double TotExeptThis = 0;
        int AppoinmentID;
        string BookID;
        string ResvID;
        
        public frmCheckOut()
        {
            InitializeComponent();
        }
        public frmCheckOut(bool isRent, string bilID, string Bk_or_Res_ID,int apid)
        {
            
            IsNewBill = false;
            Billno = bilID;
            AppoinmentID = apid;

            InitializeComponent();
            isrent = isRent;
            if (isrent)
            {
                radioGroup1.SelectedIndex = 0;
                txtFuelAmount.Enabled = false;
                txtFuelLevel.Enabled = false;
                trkFuel.Enabled = false;
                trkFuel.Value = 0;
                this.fillBookingDetails(Bk_or_Res_ID);
                BookID = Bk_or_Res_ID;
            }
            else
            {
                radioGroup1.SelectedIndex = 1;
                txtFuelAmount.Enabled = false;
                txtFuelLevel.Enabled = false;
                trkFuel.Enabled = false;
                trkFuel.Value = 0;
                this.fillResDetails(Bk_or_Res_ID);
                ResvID = Bk_or_Res_ID;
            }
            this.fillBillDetails(bilID);
            TotExeptThis = BilTot-thisTot;
        }
        private void fillBillDetails(string Bilno)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT billTotal,billDue,billPayed FROM tblbills WHERE billID=@BillID";
                comm = new MySqlCommand(Qry, con);
                comm.Parameters.AddWithValue("@BillID",Bilno);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();
                while (rdr.Read())
                {
                    lblBillTotal.Text = AppSett.FormatCurrency(double.Parse(rdr[0].ToString()).ToString());
                    BilTot = double.Parse(rdr[0].ToString());
                    lblPaid.Text = AppSett.FormatCurrency(double.Parse(rdr[2].ToString()).ToString());
                    lblDue.Text = AppSett.FormatCurrency(double.Parse(rdr[1].ToString()).ToString());
                }
                comm.Dispose();
            }
        }
        private void fillResDetails(string resID)
        {
            string CstNIC = "";
            string VhclNo = "";
            double decorCharge = 0;
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    string Qry = @"SELECT   resCstNIC,resVhclNo,resStart,
                                            resEnd,resPick,resDrop,
                                            resTrip,resDecor,resTotal,
                                            resNote,resTotal 
                                    FROM 
                                        `tblreservation` 
                                    WHERE
                                        `resID`=@resID";
                    comm = new MySqlCommand(Qry, con);
                    comm.Parameters.AddWithValue("@resID", resID);
                    con.Open();
                    MySqlDataReader rdr = comm.ExecuteReader();

                    while (rdr.Read())
                    {
                        CstNIC = rdr[0].ToString();
                        VhclNo = rdr[1].ToString();
                        dtpStart.Value = DateTime.Parse(rdr[2].ToString()).Date;
                        tpStart.Text = DateTime.Parse(rdr[2].ToString()).ToShortTimeString();
                        dtpEnd.Value = DateTime.Parse(rdr[3].ToString()).Date;
                        tpEnd.Text = DateTime.Parse(rdr[3].ToString()).ToShortTimeString();
                        txtPick.Text = rdr[4].ToString();
                        txtDrop.Text = rdr[5].ToString();
                        txtTrip.Text = rdr[6].ToString();
                        decorCharge = double.Parse(rdr[7].ToString());
                        lblGrandTotal.Text = AppSett.FormatCurrency(rdr[8].ToString());
                        thisTot = double.Parse(rdr[8].ToString());
                        txtNote.Text = rdr[9].ToString();
                        lblHire.Text = AppSett.FormatCurrency(rdr[10].ToString());
                    }
                    comm.Dispose();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            this.loadCustDetails(CstNIC);
            cmbVehicleList.Text = VhclNo;
            this.loadVhclDetails(VhclNo);
            if (decorCharge == 0)
            {
                chkDecor.Checked = false;
                txtExCharges.Enabled = false;
            }
            else
            {
                chkDecor.Checked = true;
                lblDecor.Text = AppSett.FormatCurrency(decorCharge.ToString());
            }
        }
        private void fillBookingDetails(string bkID)
        {
            string CstNIC="";
            string VhclNo="";
            double dayRate = 0;
            double Drate = 0;
            double Dcharge = 0;
            bool withd= false;
            double decorCharge = 0;
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    string Qry = @"SELECT bkCstNIC,bkVhclNo,bkStart,
                                    bkEnd,bkDays,bkRate,
                                    bkDrRate,bkHrs,bkDecorCharges,
                                    bkDriverCharges,bkRental,bkTotalCharge,
                                    bkWithDriver,bkNote 
                                FROM 
                                    `tblbooking` 
                                WHERE
                                    `bkID`=@bkID";
                    comm = new MySqlCommand(Qry, con);
                    comm.Parameters.AddWithValue("@bkID", bkID);
                    con.Open();
                    MySqlDataReader rdr = comm.ExecuteReader();

                    while (rdr.Read())
                    {
                        CstNIC = rdr[0].ToString();
                        VhclNo = rdr[1].ToString();
                        dtpStart.Value = DateTime.Parse(rdr[2].ToString()).Date;
                        tpStart.Text = DateTime.Parse(rdr[2].ToString()).ToShortTimeString();
                        dtpEnd.Value = DateTime.Parse(rdr[3].ToString()).Date;
                        tpEnd.Text = DateTime.Parse(rdr[3].ToString()).ToShortTimeString();
                        txtDays.Text = rdr[4].ToString();
                        dayRate = double.Parse(rdr[5].ToString());
                        Drate = double.Parse(rdr[6].ToString());
                        txtHrs.Text = rdr[7].ToString();
                        decorCharge = double.Parse(rdr[8].ToString());
                        Dcharge = double.Parse(rdr[9].ToString());
                        lblRent.Text = AppSett.FormatCurrency(rdr[10].ToString());
                        lblGrandTotal.Text = AppSett.FormatCurrency(rdr[11].ToString());
                        thisTot = double.Parse(rdr[11].ToString());
                        withd = bool.Parse(rdr[12].ToString());
                        txtNote.Text = rdr[13].ToString();
                    }
                    comm.Dispose();
                }
                this.loadCustDetails(CstNIC);
                cmbVehicleList.Text = VhclNo;
                this.loadVhclDetails(VhclNo);
                txtDayRate.Text = AppSett.FormatCurrency(dayRate.ToString());
                if (withd)
                {
                    chkDriver.Checked = true;
                    txtDrate.Text = AppSett.FormatCurrency(Drate.ToString());
                    lblDriverCharge.Text = AppSett.FormatCurrency(Dcharge.ToString());
                }
                else
                {
                    chkDriver.Checked = false;
                    txtDrate.Enabled = false;
                    cmbDriverList.Enabled = false;
                }
                if (decorCharge==0)
                {
                    chkDecor.Checked = false;
                    txtExCharges.Enabled = false;
                }
                else
                {
                    chkDecor.Checked = true;
                    lblDecor.Text = AppSett.FormatCurrency(decorCharge.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        public int GetResourceID(string Vno)
        {
            int id = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT `Id` FROM `resources` WHERE `Description`='" + Vno + "'";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    id = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return id;
        }
        public void UpdateID(string Item, int NewID)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                int intnewid = NewID;
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tbllastid` SET " + Item.Trim() + "='" + intnewid + "' WHERE clmnID=1";
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                comm.Dispose();
            }
        }
        public int GetLastID(string Item)
        {
            int lastID = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT " + Item + " FROM tbllastid";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    lastID = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return lastID + 1;
        }
        public double CalGtot()
        {
            double rent = double.Parse(lblRent.Text);
            double drRent = double.Parse(lblDriverCharge.Text);
            double decor = double.Parse(lblDecor.Text);
            double fuel = double.Parse(lblFuelCharge.Text);
            double hire = double.Parse(lblHire.Text);
            double gtot = 0;

            if (radioGroup1.SelectedIndex==0)
            {
                gtot = rent + drRent + decor + fuel;
            }
            else if (radioGroup1.SelectedIndex==1)
            {
                gtot = hire+ decor + fuel;
            }
            return gtot;
        }
        public TimeSpan CountTime()
        {
            DateTime strt = dtpStart.Value.AddHours(DateTime.Parse(tpStart.Text.Trim()).Hour).AddMinutes(DateTime.Parse(tpStart.Text.Trim()).Minute);
            DateTime end = dtpEnd.Value.AddHours(DateTime.Parse(tpEnd.Text.Trim()).Hour).AddMinutes(DateTime.Parse(tpEnd.Text.Trim()).Minute);
            TimeSpan time = end - strt;
            return time;

        }
        public void loadDriverDetails(string name)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `NIC`,`Name`,`Address`,`Cantact`,`DrivingLICNO`,`LICExDate`,`RarePerDay` FROM `tbldrivers` WHERE `Name` = '" + name + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        txtDName.Text = rdr[1].ToString();
                        txtDnic.Text = rdr[0].ToString();
                        txtDaddress.Text = rdr[2].ToString();
                        txtDcontact.Text = rdr[3].ToString();
                        txtDlic.Text = rdr[4].ToString();
                        dtpDlicExp.Value = DateTime.Parse(rdr[5].ToString());
                        txtDrate.Text = rdr[6].ToString();
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }
            }
        }
        private void ClrVhcl()
        {
            txtVhclNo.Text = "";
            txtVhclMake.Text = "";
            txtVhclType.Text = "";
            txtVhclModel.Text = "";
            dtpDelDate.Text = "";
            txtVhclNxtSrvice.Text = "";
            txtVhclInsNo.Text = "";
            txtVhclLicNo.Text = "";
            txtVhclMYeay.Text = "";
            txtCurODO.Text = "";
        }
        private void ClrRent()
        {
            txtDays.Text = "";
            txtHrs.Text = "";
            txtDayRate.Text = "";
            txtDisLimit.Text = "";
            lblRent.Text = "0.00";
            lblDriverCharge.Text = "0.00";
            chkDriver.Checked = false;
        }
        private void ClrHire()
        {
            txtPick.Text = "";
            txtDrop.Text = "";
            txtTrip.Text = "";
            txtHireCharge.Text = "";
            lblHire.Text = "0.00";
        }
        private void ClrCommon()
        {
            txtNote.Text = "";
            lblDecor.Text = "0.00";
            lblFuelCharge.Text = "0.00";
            chkDecor.Checked = false;
            chkFuelAmt.Checked = false;
        }
        private void ClrMain()
        {
            cmbDriverList.SelectedIndex = -1;
            dateNavigator1.SelectedRanges.Clear();
            tpEnd.Text = "";
            tpStart.Text = "";
            lblGrandTotal.Text = "0.00";
            lblPaid.Text = "0.00";
            lblDue.Text = "0.00";
        }
        private void ClrDriver()
        {
            cmbDriverList.SelectedIndex = -1;
            txtDName.Text = "";
            txtDnic.Text = "";
            txtDaddress.Text = "";
            txtDcontact.Text = "";
            txtDlic.Text = ""; ;
            dtpDlicExp.Text = "";
            txtDrate.Text = "";

        }
        public void loadVhclDetails(string VhclNo)
        {
            #region Load Text Boxes
            this.ClrVhcl();
            string slctID = VhclNo;
            imageSlider1.Images.Clear();
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {

                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = @"SELECT `vhclsupID`,`vhclNo`,
                                        `vhclMake`,`vhclType`,`vchlModel`,
                                        `vhclDelDate`,`vchlOdoReading`,`vchlNextService`,
                                        `vhclInsNo`,`vhclInsExpDate`,`vhclLicNo`,
                                        `vchlLicExpDate`,`vhclMadeYear`,`vchlId`,
                                        `Image1`, `Image2`, `Image3`,`Image4`,
                                        `vhclKmLimit`,`vhclDayRate`
                                    FROM 
                                        `tblvehicle`
                                    WHERE 
                                        `vhclNo` = '" + slctID + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        lblVhclID.Text = rdr[13].ToString();
                        txtVhclNo.Text = rdr[1].ToString();
                        txtVhclMake.Text = rdr[2].ToString();
                        txtVhclType.Text = rdr[3].ToString();
                        txtVhclModel.Text = rdr[4].ToString();
                        dtpDelDate.Text = rdr[5].ToString();
                        txtCurODO.Text = rdr[6].ToString();
                        txtVhclNxtSrvice.Text = rdr[7].ToString();
                        txtVhclInsNo.Text = rdr[8].ToString();
                        dtpInsExpDate.Text = rdr[9].ToString();
                        txtVhclLicNo.Text = rdr[10].ToString();
                        dtpLicExpDate.Text = rdr[11].ToString();
                        txtVhclMYeay.Text = rdr[12].ToString();

                        txtDisLimit.Text = rdr[18].ToString();
                        txtDayRate.Text = rdr[19].ToString();
                        txtCurODO.Text = rdr[6].ToString();

                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[14]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[15]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[16]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[17]));



                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }
            }
            #endregion
        }
        public bool loadCustDetails(string CustNic)
        {
            bool available = false;
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `ID`,`NIC`,`FName`,`Address1`,`Address2`,`LicNo`,`ExDate`,`Contact1`,`Contact2`,`Contact3`,`Pic`,`BlackLIsted`,`picID`,`picDLic` FROM `tblcustomer` WHERE `NIC` = '" + CustNic + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        lblCstID.Text = rdr[0].ToString();
                        txtNIC.Text = rdr[1].ToString();
                        txtFName.Text = rdr[2].ToString();
                        txtAddress1.Text = rdr[3].ToString();
                        txtAddress2.Text = rdr[4].ToString();
                        txtLIcNo.Text = rdr[5].ToString();
                        dtpLicExp.Text = rdr[6].ToString();
                        txtContact1.Text = rdr[7].ToString();
                        txtContact2.Text = rdr[8].ToString();
                        txtContact3.Text = rdr[9].ToString();
                        PicCustomer.Image = this.ByteArray2Image((byte[])rdr[10]);
                        if ((bool)rdr[11])
                        {
                            lblIsBlackListed.Text = "Black Listed";
                        }
                        else
                        {
                            lblIsBlackListed.Text = "";
                        }
                        available = true;
                        //if (rdr[12].ToString() != "")
                        //{
                        //    picNIC.Image = this.ByteArray2Image((byte[])rdr[12]);

                        //}
                        //if (rdr[12].ToString() != "")
                        //{
                        //    picDLic.Image = this.ByteArray2Image((byte[])rdr[13]);

                        //}
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }

            }
            return available;
        }
        public Image ByteArray2Image(byte[] Barry)
        {
            MemoryStream ms = new MemoryStream(Barry);
            Image retImage = Image.FromStream(ms);
            return retImage;
        }
        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioGroup1.EditValue.ToString()=="rent")
            {
                tabType.SelectedTabPageIndex = 0;
                this.ClrCommon();
                this.ClrDriver();
                this.ClrHire();
                this.ClrMain();
                this.ClrRent();
                this.ClrVhcl();
            }
            if (radioGroup1.EditValue.ToString() == "hire")
            {
                tabType.SelectedTabPageIndex = 1;
                this.ClrCommon();
                this.ClrDriver();
                this.ClrHire();
                this.ClrMain();
                this.ClrRent();
                this.ClrVhcl();
            }
        }
        private void txtNIC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!this.loadCustDetails(txtNIC.Text.Trim()))
                {
                    frmCustomer frm = new frmCustomer(txtNIC.Text.Trim());
                    frm.ShowDialog();
                    this.loadCustDetails(txtNIC.Text.Trim());
                }
            }
        }
        private void frmCheckOut_Load(object sender, EventArgs e)
        {
            #region Load Combos
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT vhclNo FROM tblvehicle";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        cmbVehicleList.Properties.Items.Add(rdr[0].ToString());
                    }

                    comm.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT Name FROM tbldrivers";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        cmbDriverList.Properties.Items.Add(rdr[0].ToString());
                        cmbHireDriver.Properties.Items.Add(rdr[0].ToString());
                    }

                    comm.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion

            if (IsNewBill)
            {
                radioGroup1.SelectedIndex = 0;
                txtDrate.Enabled = false;
                cmbDriverList.Enabled = false;
                txtExCharges.Enabled = false;
                txtFuelAmount.Enabled = false;
                txtFuelLevel.Enabled = false;
                trkFuel.Enabled = false;
                trkFuel.Value = 0;
            }
        }
        private void cmbVehicleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbVehicleList.SelectedIndex != -1)
            {
                this.loadVhclDetails(cmbVehicleList.Text.Trim());
                txtCurODO.Focus();
            }
        }
        private void cmbDriverList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDriverList.SelectedIndex != -1)
            {
                this.loadDriverDetails(cmbDriverList.Text.Trim());
                txtDrate.Focus();
            }
        }
        private void dateNavigator1_SelectionChanged(object sender, EventArgs e)
        {
            dtpStart.Value = dateNavigator1.SelectionStart;
            dtpEnd.Value = dateNavigator1.SelectionEnd.AddDays(-1);
            tpStart.Text = "";
            tpEnd.Text = "";
            tpStart.Focus();
        }
        private void tpStart_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    DateTime dateTime = DateTime.ParseExact(tpStart.Text.Trim(), "HHmm", CultureInfo.InvariantCulture);
                    tpStart.Text = dateTime.ToShortTimeString();
                    tpEnd.Text = "";
                    tpEnd.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    tpStart.Text = "";
                    tpStart.Focus();
                }

            }
        }
        private void tpEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radioGroup1.SelectedIndex==0)
                {
                    try
                    {
                        DateTime dateTime = DateTime.ParseExact(tpEnd.Text.Trim(), "HHmm", CultureInfo.InvariantCulture);
                        tpEnd.Text = dateTime.ToShortTimeString();
                        txtDayRate.Focus();

                        txtHrs.Text = this.CountTime().Hours.ToString();
                        txtDays.Text = this.CountTime().Days.ToString();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        tpEnd.Text = "";
                        tpEnd.Focus();
                    }
                }
                else if (radioGroup1.SelectedIndex == 1)
                {
                    try
                    {
                        DateTime dateTime = DateTime.ParseExact(tpEnd.Text.Trim(), "HHmm", CultureInfo.InvariantCulture);
                        tpEnd.Text = dateTime.ToShortTimeString();
                        txtPick.Focus();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }

            }
        }
        private void txtDayRate_KeyDown(object sender, KeyEventArgs e)
        {
            double days;
            double hrs;
            double rate;
            if (tabType.SelectedTabPageIndex==0)
            {
                if (e.KeyCode==Keys.Enter)
                {
                    try
                    {
                        days = double.Parse(txtDays.Text.Trim());
                        hrs = double.Parse(txtHrs.Text.Trim());
                        rate = double.Parse(txtDayRate.Text.Trim());

                        double rent = (rate / 24) * (hrs + (days * 24));
                        lblRent.Text = AppSett.FormatCurrency(rent.ToString());

                        txtDisLimit.Focus();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }
        private void chkDriver_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDriver.Checked)
            {
                txtDrate.Enabled = true;
                cmbDriverList.Enabled = true;
            }
            else
            {
                txtDrate.Enabled = false;
                txtDrate.Text = "";
                cmbDriverList.Enabled = false;
                cmbDriverList.SelectedIndex = -1;
            }
        }
        private void txtDrate_KeyDown(object sender, KeyEventArgs e)
        {
            double days;
            double hrs;
            double Drate;
            if (tabType.SelectedTabPageIndex == 0)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    try
                    {
                        days = double.Parse(txtDays.Text.Trim());
                        hrs = double.Parse(txtHrs.Text.Trim());
                        Drate = double.Parse(txtDrate.Text.Trim());

                        double rent = (Drate / 24) * (hrs + (days * 24));
                        lblDriverCharge.Text = AppSett.FormatCurrency(rent.ToString());

                        txtDisLimit.Focus();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }
        private void chkDecor_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDecor.Checked)
            {
                txtExCharges.Enabled = true;
                txtExCharges.Focus();
            }
            else
            {
                txtExCharges.Enabled = false;
                txtExCharges.Text = "";
            }
        }
        private void chkFuelAmt_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFuelAmt.Checked)
            {
                txtFuelAmount.Enabled = true;
                txtFuelLevel.Enabled = true;
                txtFuelLevel.Focus();
                trkFuel.Enabled = true;
            }
            else
            {
                trkFuel.Enabled = false;
                trkFuel.Value = 0;
                txtFuelAmount.Enabled = false;
                txtFuelAmount.Text = "";
                txtFuelLevel.Enabled = false;
                txtFuelLevel.Text = "";
            }
        }
        private void lblRent_TextChanged(object sender, EventArgs e)
        {
            lblGrandTotal.Text = AppSett.FormatCurrency(this.CalGtot().ToString());
        }
        private void lblDriverCharge_TextChanged(object sender, EventArgs e)
        {
            lblGrandTotal.Text = AppSett.FormatCurrency(this.CalGtot().ToString());
        }
        private void txtExCharges_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                double decor;
                if (double.TryParse(txtExCharges.Text.Trim(),out decor))
                {
                    lblDecor.Text = AppSett.FormatCurrency(decor.ToString());
                }
                else
                {
                    MessageBox.Show("Enter Valid Amount","Invalid Data");
                    txtExCharges.Text = "";
                    txtExCharges.Focus();
                }
            }
        }
        private void txtFuelAmount_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                double fuelCharge;
                if (double.TryParse(txtFuelAmount.Text.Trim(), out fuelCharge))
                {
                    lblFuelCharge.Text = AppSett.FormatCurrency(fuelCharge.ToString());
                }
                else
                {
                    MessageBox.Show("Enter Valid Amount", "Invalid Data");
                    txtFuelAmount.Text = "";
                    txtFuelAmount.Focus();
                }
            }
        }
        private void trkFuel_EditValueChanged(object sender, EventArgs e)
        {
            txtFuelLevel.Text = trkFuel.Value.ToString();
        }
        private void txtFuelLevel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                int level;
                if (int.TryParse(txtFuelLevel.Text,out level)&&(level<=10)&&level>0)
                {
                    trkFuel.Value = level;
                    txtFuelAmount.Focus();
                }
                else
                {
                    txtFuelLevel.Text = "";
                    txtFuelLevel.Focus();
                }
            }
        }
        private void lblDecor_TextChanged(object sender, EventArgs e)
        {
            lblGrandTotal.Text = AppSett.FormatCurrency(this.CalGtot().ToString());
        }
        private void lblFuelCharge_TextChanged(object sender, EventArgs e)
        {
            lblGrandTotal.Text = AppSett.FormatCurrency(this.CalGtot().ToString());
        }
        private void lblHire_TextChanged(object sender, EventArgs e)
        {
            lblGrandTotal.Text = AppSett.FormatCurrency(this.CalGtot().ToString());
        }
        private void txtHireCharge_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                double hire;
                if (double.TryParse(txtHireCharge.Text.Trim(), out hire))
                {
                    lblHire.Text = AppSett.FormatCurrency(hire.ToString());
                }
                else
                {
                    MessageBox.Show("Enter Valid Amount", "Invalid Data");
                    txtHireCharge.Text = "";
                    txtHireCharge.Focus();
                }
            }
        }
        private void txtPick_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                txtDrop.Focus();
            }
        }
        private void txtDrop_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                txtTrip.Focus();
            }
        }
        private void txtTrip_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                txtHireCharge.Focus();
            }
        }
        private void btnProceed_Click(object sender, EventArgs e)
        {
            if (IsNewBill)
            {
                double tot = double.Parse(lblGrandTotal.Text);
                double due = 0;

                SetPayEventArgs.TotAmount = tot;
                SetPayEventArgs.DuePayment = due;

                Billno = string.Format("BIL{0}", this.GetLastID("`tblBills`").ToString());

                frmPay paynow = new frmPay();
                paynow.PayDone += paynow_PayDone;

                string chkID;
                string hireID;
                int apID;

                #region Validate
                string pick="None";
                string drop="None";
                int trip=0;
                double HireCharge=0;
                string VhclNo;
                string vhclType;
                string CstNIC;
                string CstName;
                bool driver=false;
                string DrvID = "None";
                string DrvName ="None";
                double DrvRate=0;
                double DrvCharge = 0; ;
                DateTime start;
                DateTime end;
                int days=0;
                int hrs=0;
                int LimitKm=0;
                double dayRate=0;
                double ratePerExKm=0;
                double Decor=0;
                int FuelLevel=0;
                double FuelAmount=0;
                double Gtotal;
                int CurODO;
                string note = "None";
                double rnt = 0;
                if (lblCstID.Text=="")
                {
                    MessageBox.Show("Please Add Customer");
                    txtNIC.Focus();
                    return;
                }
                else
                {
                    CstNIC = txtNIC.Text.Trim();
                    CstName = txtFName.Text.Trim();
                }
                if (cmbVehicleList.SelectedIndex==-1)
                {
                    MessageBox.Show("Select Vehicle");
                    return;
                }
                else
                {
                    VhclNo = cmbVehicleList.Text;
                    vhclType = txtVhclType.Text;
                }
                try
                {
                    start = dtpStart.Value.Add(DateTime.Parse(tpStart.Text.Trim()).TimeOfDay);
                    end = dtpEnd.Value.Add(DateTime.Parse(tpEnd.Text.Trim()).TimeOfDay);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return;
                }
                

                if (radioGroup1.SelectedIndex == 0)
                {
                    #region ForRent
                    if (!chkDriver.Checked)
                    {
                        DrvID = "None";
                        DrvName = "None";
                        DrvRate = 0;
                        DrvCharge = 0;
                        driver = false;
                    }
                    else if (chkDriver.Checked && cmbDriverList.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select Driver");
                        return;
                    }
                    else
                    {
                        DrvID = txtDnic.Text;
                        DrvName = txtDName.Text;
                        DrvRate = double.Parse(txtDrate.Text);
                        DrvCharge = double.Parse(lblDriverCharge.Text);
                        driver = true;
                    }

                    days = int.Parse(txtDays.Text);
                    hrs = int.Parse(txtHrs.Text);
                    dayRate = double.Parse(txtDayRate.Text);
                    rnt = double.Parse(lblRent.Text);
                    if (int.TryParse(txtDisLimit.Text, out LimitKm))
                    {
                         if (LimitKm <= 0)
                         {
                                if (MessageBox.Show("Continue with Unlimited free Kms...", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    LimitKm = 0;
                                }
                                else
                                {
                                    txtDisLimit.Focus();
                                    return;
                                }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter Valid Distance");
                        txtDisLimit.Focus();
                        return;
                    }

                    if (double.TryParse(txtExKmCharge.Text, out ratePerExKm))
                    {
                        if (ratePerExKm <= 0)
                        {
                            if (MessageBox.Show("Continue with No charge per Extra km", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                ratePerExKm = 0;
                            }
                            else
                            {
                                txtExKmCharge.Focus();
                                return;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter Valid Amount for Extra km Charge");
                        txtExKmCharge.Focus();
                        return;
                    }

                    #endregion
                }
                if (radioGroup1.SelectedIndex==1)
                {
                    #region ForHire
                    pick = txtPick.Text;
                    drop = txtDrop.Text;
                    trip = int.Parse(txtTrip.Text);
                    HireCharge = double.Parse(lblHire.Text);

                    if (cmbHireDriver.SelectedIndex==-1)
                    {
                        MessageBox.Show("Select Driver");
                        return;
                    }
                    else
                    {
                        DrvID = txtDnic.Text;
                        DrvName = txtDName.Text;
                    }
                    #endregion
                }
                if (chkDecor.Checked)
                {
                    Decor = double.Parse(lblDecor.Text);
                }
                else
                {
                    Decor = 0;
                }
                if (chkFuelAmt.Checked)
                {
                    FuelLevel=int.Parse(txtFuelLevel.Text);
                    FuelAmount = double.Parse(lblFuelCharge.Text);
                }
                else
                {
                    FuelLevel = 0;
                    FuelAmount = 0;
                }

                Gtotal = double.Parse(lblGrandTotal.Text);

                if (!int.TryParse(txtCurODO.Text,out CurODO))
                {
                    MessageBox.Show("Enter Valid ODO Reading");
                    txtCurODO.Focus();
                    return;
                }
                note = txtNote.Text;
                #endregion

                if (paynow.ShowDialog()==DialogResult.OK)
                {
                    if (radioGroup1.SelectedIndex == 0)
                    {
                        #region Add to Appointments
                        int resID = this.GetResourceID(VhclNo);
                        apID = int.Parse(this.GetLastID("`tblappointment`").ToString());
                        try
                        {
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = "INSERT INTO `appointments` (`UniqeID`,`Type`,`StartDate`,`EndDate`,`AllDay`,`Subject`,`Location`,`Description`,`Status`,`Lable`,`ResourceID`) VALUES (@UniqeID,@Type,@StartDate,@EndDate,@AllDay,@Subject,@Location,@Description,@Status,@Lable,@ResourceID)";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@UniqeID", apID);
                                comm.Parameters.AddWithValue("@Type", 0);
                                comm.Parameters.AddWithValue("@StartDate", start);
                                comm.Parameters.AddWithValue("@EndDate", end);
                                comm.Parameters.AddWithValue("@AllDay", 0);
                                comm.Parameters.AddWithValue("@Subject", txtFName.Text.Trim());
                                comm.Parameters.AddWithValue("@Location", txtContact1.Text.Trim());
                                comm.Parameters.AddWithValue("@Description", VhclNo);
                                comm.Parameters.AddWithValue("@Status", 0);
                                comm.Parameters.AddWithValue("@Lable", 4);
                                comm.Parameters.AddWithValue("@ResourceID", resID);
                                con.Open();
                                comm.ExecuteNonQuery();
                                this.UpdateID("`tblappointment`", this.GetLastID("`tblappointment`"));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }

                        #endregion
                        #region Add to Checkout
                        try
                        {
                            chkID = string.Format("RENT{0}", this.GetLastID("`tblCheckout`").ToString());
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = @"INSERT INTO `tblchekout` 
                                            (chekoutID,VehicleNo,VehicleType,
                                                CustomerNIC,CustomerName,DriverNIC,Driver,
                                                DriverName,DrivaerRate,DriverCharge,
                                                StartDateTime,EndDatetime,Days,
                                                Hours,LimitKm,RatePerKm,
                                                DecoRate,FuelLevel,FuelAmount,
                                                GrandTotal,apID,curODO,
                                                billID,outNote,RatePerExKm,rentStatus,rent)
                                    VALUES 
                                           (@chekoutID,@VehicleNo,@VehicleType,
                                                @CustomerNIC,@CustomerName,@DriverNIC,@Driver,
                                                @DriverName,@DrivaerRate,@DriverCharge,
                                                @StartDateTime,@EndDatetime,@Days,
                                                @Hours,@LimitKm,@RatePerKm,
                                                @DecoRate,@FuelLevel,@FuelAmount,
                                                @GrandTotal,@apID,@curODO,
                                                @billID,@outNote,@RatePerExKm,@rentStatus,@rent)";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@chekoutID", chkID);
                                comm.Parameters.AddWithValue("@VehicleNo", VhclNo);
                                comm.Parameters.AddWithValue("@VehicleType", vhclType);
                                comm.Parameters.AddWithValue("@CustomerNIC", CstNIC);
                                comm.Parameters.AddWithValue("@CustomerName", CstName);
                                comm.Parameters.AddWithValue("@DriverNIC", DrvID);
                                comm.Parameters.AddWithValue("@Driver", driver);
                                comm.Parameters.AddWithValue("@DriverName", DrvName);
                                comm.Parameters.AddWithValue("@DrivaerRate", DrvRate);
                                comm.Parameters.AddWithValue("@DriverCharge", DrvCharge);
                                comm.Parameters.AddWithValue("@StartDateTime", start);
                                comm.Parameters.AddWithValue("@EndDatetime", end);
                                comm.Parameters.AddWithValue("@Days", days);
                                comm.Parameters.AddWithValue("@Hours", hrs);
                                comm.Parameters.AddWithValue("@LimitKm", LimitKm);
                                comm.Parameters.AddWithValue("@RatePerKm", dayRate);
                                comm.Parameters.AddWithValue("@DecoRate", Decor);
                                comm.Parameters.AddWithValue("@FuelLevel", FuelLevel);
                                comm.Parameters.AddWithValue("@FuelAmount", FuelAmount);
                                comm.Parameters.AddWithValue("@GrandTotal", Gtotal);
                                comm.Parameters.AddWithValue("@apID", apID);
                                comm.Parameters.AddWithValue("@curODO", CurODO);
                                comm.Parameters.AddWithValue("@billID", Billno);
                                comm.Parameters.AddWithValue("@outNote", note);
                                comm.Parameters.AddWithValue("@RatePerExKm", ratePerExKm);
                                comm.Parameters.AddWithValue("@rentStatus", "active");
                                comm.Parameters.AddWithValue("@rent",rnt);



                                con.Open();
                                comm.ExecuteNonQuery();
                                this.UpdateID("`tblCheckout`", this.GetLastID("`tblCheckout`"));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }
                        #endregion
                    }
                    if (radioGroup1.SelectedIndex == 1)
                    {
                        #region Add to Appointments
                        int resID = this.GetResourceID(VhclNo);
                        apID = int.Parse(this.GetLastID("`tblappointment`").ToString());
                        try
                        {
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = "INSERT INTO `appointments` (`UniqeID`,`Type`,`StartDate`,`EndDate`,`AllDay`,`Subject`,`Location`,`Description`,`Status`,`Lable`,`ResourceID`) VALUES (@UniqeID,@Type,@StartDate,@EndDate,@AllDay,@Subject,@Location,@Description,@Status,@Lable,@ResourceID)";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@UniqeID", apID);
                                comm.Parameters.AddWithValue("@Type", 0);
                                comm.Parameters.AddWithValue("@StartDate", start);
                                comm.Parameters.AddWithValue("@EndDate", end);
                                comm.Parameters.AddWithValue("@AllDay", 0);
                                comm.Parameters.AddWithValue("@Subject", txtFName.Text.Trim());
                                comm.Parameters.AddWithValue("@Location", txtContact1.Text.Trim());
                                comm.Parameters.AddWithValue("@Description", VhclNo);
                                comm.Parameters.AddWithValue("@Status", 0);
                                comm.Parameters.AddWithValue("@Lable", 5);
                                comm.Parameters.AddWithValue("@ResourceID", resID);
                                con.Open();
                                comm.ExecuteNonQuery();
                                this.UpdateID("`tblappointment`", this.GetLastID("`tblappointment`"));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }

                        #endregion
                        #region Add to hire
                        try
                        {
                            hireID = string.Format("HIRE{0}", this.GetLastID("`tblHire`").ToString());
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = @"INSERT INTO `tblhire` 
                                            (HireID,VehicleNo,VehicleType,
                                                CustomerNIC,CustomerName,DriverNIC,
                                                DriverName,
                                                StartDateTime,EndDatetime,pickLoc,
                                                dropLoc,trip,hireCharge,
                                                DecoRate,FuelLevel,FuelAmount,
                                                GrandTotal,apID,curODO,
                                                billID,outNote,hireStatus)
                                    VALUES 
                                           (@HireID,@VehicleNo,@VehicleType,
                                                @CustomerNIC,@CustomerName,@DriverNIC,
                                                @DriverName,
                                                @StartDateTime,@EndDatetime,@pickLoc,
                                                @dropLoc,@trip,@hireCharge,
                                                @DecoRate,@FuelLevel,@FuelAmount,
                                                @GrandTotal,@apID,@curODO,
                                                @billID,@outNote,@hireStatus)";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@HireID", hireID);
                                comm.Parameters.AddWithValue("@VehicleNo", VhclNo);
                                comm.Parameters.AddWithValue("@VehicleType", vhclType);
                                comm.Parameters.AddWithValue("@CustomerNIC", CstNIC);
                                comm.Parameters.AddWithValue("@CustomerName", CstName);
                                comm.Parameters.AddWithValue("@DriverNIC", DrvID);
                                comm.Parameters.AddWithValue("@DriverName", DrvName);
                                comm.Parameters.AddWithValue("@StartDateTime", start);
                                comm.Parameters.AddWithValue("@EndDatetime", end);
                                comm.Parameters.AddWithValue("@pickLoc", pick);
                                comm.Parameters.AddWithValue("@dropLoc", drop);
                                comm.Parameters.AddWithValue("@trip", trip);
                                comm.Parameters.AddWithValue("@hireCharge", HireCharge);
                                comm.Parameters.AddWithValue("@DecoRate", Decor);
                                comm.Parameters.AddWithValue("@FuelLevel", FuelLevel);
                                comm.Parameters.AddWithValue("@FuelAmount", FuelAmount);
                                comm.Parameters.AddWithValue("@GrandTotal", Gtotal);
                                comm.Parameters.AddWithValue("@apID", apID);
                                comm.Parameters.AddWithValue("@curODO", CurODO);
                                comm.Parameters.AddWithValue("@billID", Billno);
                                comm.Parameters.AddWithValue("@outNote", note);
                                comm.Parameters.AddWithValue("@hireStatus", "active");

                                con.Open();
                                comm.ExecuteNonQuery();
                                this.UpdateID("`tblHire`", this.GetLastID("`tblHire`"));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }
                        #endregion
                    }
                    this.Dispose();
                    MessageBox.Show("Data Saved");
                }
                this.UpdateID("`tblBills`", this.GetLastID("`tblBills`"));
                this.Dispose();
            }

            if (!IsNewBill)
            {
                double tot = double.Parse(lblBillTotal.Text);
                double due = double.Parse(lblDue.Text);

                SetPayEventArgs.TotAmount = tot;
                SetPayEventArgs.DuePayment = due;

                frmPay paynow = new frmPay();
                paynow.PayDone += paynow_PayDone;

                string chkID;
                string hireID;

                #region Validate
                string pick = "None";
                string drop = "None";
                int trip = 0;
                double HireCharge = 0;
                string VhclNo;
                string vhclType;
                string CstNIC;
                string CstName;
                bool driver = false;
                string DrvID = "None";
                string DrvName = "None";
                double DrvRate = 0;
                double DrvCharge = 0; ;
                DateTime start;
                DateTime end;
                int days = 0;
                int hrs = 0;
                int LimitKm = 0;
                double dayRate = 0;
                double ratePerExKm = 0;
                double Decor = 0;
                int FuelLevel = 0;
                double FuelAmount = 0;
                double Gtotal;
                int CurODO;
                string note = "None";
                double rnt =0;
                if (lblCstID.Text == "")
                {
                    MessageBox.Show("Please Add Customer");
                    txtNIC.Focus();
                    return;
                }
                else
                {
                    CstNIC = txtNIC.Text.Trim();
                    CstName = txtFName.Text.Trim();
                }
                if (cmbVehicleList.SelectedIndex == -1)
                {
                    MessageBox.Show("Select Vehicle");
                    return;
                }
                else
                {
                    VhclNo = cmbVehicleList.Text;
                    vhclType = txtVhclType.Text;
                }
                try
                {
                    start = dtpStart.Value.Add(DateTime.Parse(tpStart.Text.Trim()).TimeOfDay);
                    end = dtpEnd.Value.Add(DateTime.Parse(tpEnd.Text.Trim()).TimeOfDay);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return;
                }


                if (radioGroup1.SelectedIndex == 0)
                {
                    #region ForRent
                    if (!chkDriver.Checked)
                    {
                        DrvID = "None";
                        DrvName = "None";
                        DrvRate = 0;
                        DrvCharge = 0;
                        driver = false;
                    }
                    else if (chkDriver.Checked && cmbDriverList.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select Driver");
                        return;
                    }
                    else
                    {
                        DrvID = txtDnic.Text;
                        DrvName = txtDName.Text;
                        DrvRate = double.Parse(txtDrate.Text);
                        DrvCharge = double.Parse(lblDriverCharge.Text);
                        driver = true;
                    }

                    days = int.Parse(txtDays.Text);
                    hrs = int.Parse(txtHrs.Text);
                    dayRate = double.Parse(txtDayRate.Text);
                    rnt = double.Parse(lblRent.Text);
                    if (int.TryParse(txtDisLimit.Text, out LimitKm))
                    {
                        if (LimitKm <= 0)
                        {
                            if (MessageBox.Show("Continue with Unlimited free Kms...", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                LimitKm = 0;
                            }
                            else
                            {
                                txtDisLimit.Focus();
                                return;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter Valid Distance");
                        txtDisLimit.Focus();
                        return;
                    }

                    if (double.TryParse(txtExKmCharge.Text, out ratePerExKm))
                    {
                        if (ratePerExKm <= 0)
                        {
                            if (MessageBox.Show("Continue with No charge per Extra km", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                ratePerExKm = 0;
                            }
                            else
                            {
                                txtExKmCharge.Focus();
                                return;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter Valid Amount for Extra km Charge");
                        txtExKmCharge.Focus();
                        return;
                    }

                    #endregion
                }
                if (radioGroup1.SelectedIndex == 1)
                {
                    #region ForHire
                    pick = txtPick.Text;
                    drop = txtDrop.Text;
                    trip = int.Parse(txtTrip.Text);
                    HireCharge = double.Parse(lblHire.Text);

                    if (cmbHireDriver.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select Driver");
                        return;
                    }
                    else
                    {
                        DrvID = txtDnic.Text;
                        DrvName = txtDName.Text;
                    }
                    #endregion
                }
                if (chkDecor.Checked)
                {
                    Decor = double.Parse(lblDecor.Text);
                }
                else
                {
                    Decor = 0;
                }
                if (chkFuelAmt.Checked)
                {
                    FuelLevel = int.Parse(txtFuelLevel.Text);
                    FuelAmount = double.Parse(lblFuelCharge.Text);
                }
                else
                {
                    FuelLevel = 0;
                    FuelAmount = 0;
                }

                Gtotal = double.Parse(lblGrandTotal.Text);

                if (!int.TryParse(txtCurODO.Text, out CurODO))
                {
                    MessageBox.Show("Enter Valid ODO Reading");
                    txtCurODO.Focus();
                    return;
                }
                note = txtNote.Text;
                #endregion

                if (paynow.ShowDialog() == DialogResult.OK)
                {
                    if (radioGroup1.SelectedIndex == 0)
                    {
                        #region Update Appointments
                        int resID = this.GetResourceID(VhclNo);
                        try
                        {
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = @"UPDATE `appointments`
                                                                        SET `Type`=@Type,
                                                                            `StartDate`=@StartDate,
                                                                            `EndDate`=@EndDate,
                                                                            `AllDay`=@AllDay,
                                                                            `Subject`=@Subject,
                                                                            `Location`=@Location,
                                                                            `Description`=@Description,
                                                                            `Status`=@Status,
                                                                            `Lable`=@Lable,
                                                                            `ResourceID`=@ResourceID 
                                                                        WHERE
                                                                             UniqeID=@UniqeID";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@Type", 0);
                                comm.Parameters.AddWithValue("@StartDate", start);
                                comm.Parameters.AddWithValue("@EndDate", end);
                                comm.Parameters.AddWithValue("@AllDay", 0);
                                comm.Parameters.AddWithValue("@Subject", txtFName.Text.Trim());
                                comm.Parameters.AddWithValue("@Location", txtContact1.Text.Trim());
                                comm.Parameters.AddWithValue("@Description", VhclNo);
                                comm.Parameters.AddWithValue("@Status", 0);
                                comm.Parameters.AddWithValue("@Lable", 4);
                                comm.Parameters.AddWithValue("@ResourceID", resID);
                                comm.Parameters.AddWithValue("@UniqeID", AppoinmentID);

                                con.Open();
                                comm.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }

                        #endregion
                        #region Add to Checkout
                        try
                        {
                            chkID = string.Format("OUT{0}", this.GetLastID("`tblCheckout`").ToString());
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = @"INSERT INTO `tblchekout` 
                                            (chekoutID,VehicleNo,VehicleType,
                                                CustomerNIC,CustomerName,DriverNIC,Driver,
                                                DriverName,DrivaerRate,DriverCharge,
                                                StartDateTime,EndDatetime,Days,
                                                Hours,LimitKm,RatePerKm,
                                                DecoRate,FuelLevel,FuelAmount,
                                                GrandTotal,apID,curODO,
                                                billID,outNote,RatePerExKm,rentStatus,rent)
                                    VALUES 
                                           (@chekoutID,@VehicleNo,@VehicleType,
                                                @CustomerNIC,@CustomerName,@DriverNIC,@Driver,
                                                @DriverName,@DrivaerRate,@DriverCharge,
                                                @StartDateTime,@EndDatetime,@Days,
                                                @Hours,@LimitKm,@RatePerKm,
                                                @DecoRate,@FuelLevel,@FuelAmount,
                                                @GrandTotal,@apID,@curODO,
                                                @billID,@outNote,@RatePerExKm,@rentStatus,@rent)";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@chekoutID", chkID);
                                comm.Parameters.AddWithValue("@VehicleNo", VhclNo);
                                comm.Parameters.AddWithValue("@VehicleType", vhclType);
                                comm.Parameters.AddWithValue("@CustomerNIC", CstNIC);
                                comm.Parameters.AddWithValue("@CustomerName", CstName);
                                comm.Parameters.AddWithValue("@DriverNIC", DrvID);
                                comm.Parameters.AddWithValue("@Driver", driver);
                                comm.Parameters.AddWithValue("@DriverName", DrvName);
                                comm.Parameters.AddWithValue("@DrivaerRate", DrvRate);
                                comm.Parameters.AddWithValue("@DriverCharge", DrvCharge);
                                comm.Parameters.AddWithValue("@StartDateTime", start);
                                comm.Parameters.AddWithValue("@EndDatetime", end);
                                comm.Parameters.AddWithValue("@Days", days);
                                comm.Parameters.AddWithValue("@Hours", hrs);
                                comm.Parameters.AddWithValue("@LimitKm", LimitKm);
                                comm.Parameters.AddWithValue("@RatePerKm", dayRate);
                                comm.Parameters.AddWithValue("@DecoRate", Decor);
                                comm.Parameters.AddWithValue("@FuelLevel", FuelLevel);
                                comm.Parameters.AddWithValue("@FuelAmount", FuelAmount);
                                comm.Parameters.AddWithValue("@GrandTotal", Gtotal);
                                comm.Parameters.AddWithValue("@apID", AppoinmentID);
                                comm.Parameters.AddWithValue("@curODO", CurODO);
                                comm.Parameters.AddWithValue("@billID", Billno);
                                comm.Parameters.AddWithValue("@outNote", note);
                                comm.Parameters.AddWithValue("@RatePerExKm", ratePerExKm);
                                comm.Parameters.AddWithValue("@rentStatus", "active");
                                comm.Parameters.AddWithValue("@rent",rnt);


                                con.Open();
                                comm.ExecuteNonQuery();
                                this.UpdateID("`tblCheckout`", this.GetLastID("`tblCheckout`"));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }
                        #endregion
                        #region update Booking
                        try
                        {
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = @"UPDATE `tblbooking`
                                                                        SET `bkStatus`=@bkStatus,
                                                                        WHERE
                                                                             bkID=@bkID";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@bkStatus", "Proceeded");
                                comm.Parameters.AddWithValue("@bkID", BookID);

                                con.Open();
                                comm.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }
                        #endregion update Booking
                    }
                    if (radioGroup1.SelectedIndex == 1)
                    {
                        #region Update Appointments
                        int resID = this.GetResourceID(VhclNo);
                        try
                        {
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = @"UPDATE `appointments` 
                                                                        SET `Type`=@Type,
                                                                            `StartDate`=@StartDate,
                                                                            `EndDate`=@EndDate,
                                                                            `AllDay`=@AllDay,
                                                                            `Subject`=@Subject,
                                                                            `Location`=@Location,
                                                                            `Description`=@Description,
                                                                            `Status`=@Status,
                                                                            `Lable`=@Lable,
                                                                            `ResourceID`=@ResourceID 
                                                                        WHERE
                                                                             UniqeID=@UniqeID";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@Type", 0);
                                comm.Parameters.AddWithValue("@StartDate", start);
                                comm.Parameters.AddWithValue("@EndDate", end);
                                comm.Parameters.AddWithValue("@AllDay", 0);
                                comm.Parameters.AddWithValue("@Subject", txtFName.Text.Trim());
                                comm.Parameters.AddWithValue("@Location", txtContact1.Text.Trim());
                                comm.Parameters.AddWithValue("@Description", VhclNo);
                                comm.Parameters.AddWithValue("@Status", 0);
                                comm.Parameters.AddWithValue("@Lable", 5);
                                comm.Parameters.AddWithValue("@ResourceID", resID);
                                comm.Parameters.AddWithValue("@UniqeID", AppoinmentID);

                                con.Open();
                                comm.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }

                        #endregion
                        #region Add to hire
                        try
                        {
                            hireID = string.Format("HIRE{0}", this.GetLastID("`tblHire`").ToString());
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = @"INSERT INTO `tblhire` 
                                            (HireID,VehicleNo,VehicleType,
                                                CustomerNIC,CustomerName,DriverNIC,
                                                DriverName,
                                                StartDateTime,EndDatetime,pickLoc,
                                                dropLoc,trip,hireCharge,
                                                DecoRate,FuelLevel,FuelAmount,
                                                GrandTotal,apID,curODO,
                                                billID,outNote,hireStatus)
                                    VALUES 
                                           (@HireID,@VehicleNo,@VehicleType,
                                                @CustomerNIC,@CustomerName,@DriverNIC,
                                                @DriverName,
                                                @StartDateTime,@EndDatetime,@pickLoc,
                                                @dropLoc,@trip,@hireCharge,
                                                @DecoRate,@FuelLevel,@FuelAmount,
                                                @GrandTotal,@apID,@curODO,
                                                @billID,@outNote,@hireStatus)";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@HireID", hireID);
                                comm.Parameters.AddWithValue("@VehicleNo", VhclNo);
                                comm.Parameters.AddWithValue("@VehicleType", vhclType);
                                comm.Parameters.AddWithValue("@CustomerNIC", CstNIC);
                                comm.Parameters.AddWithValue("@CustomerName", CstName);
                                comm.Parameters.AddWithValue("@DriverNIC", DrvID);
                                comm.Parameters.AddWithValue("@DriverName", DrvName);
                                comm.Parameters.AddWithValue("@StartDateTime", start);
                                comm.Parameters.AddWithValue("@EndDatetime", end);
                                comm.Parameters.AddWithValue("@pickLoc", pick);
                                comm.Parameters.AddWithValue("@dropLoc", drop);
                                comm.Parameters.AddWithValue("@trip", trip);
                                comm.Parameters.AddWithValue("@hireCharge", HireCharge);
                                comm.Parameters.AddWithValue("@DecoRate", Decor);
                                comm.Parameters.AddWithValue("@FuelLevel", FuelLevel);
                                comm.Parameters.AddWithValue("@FuelAmount", FuelAmount);
                                comm.Parameters.AddWithValue("@GrandTotal", Gtotal);
                                comm.Parameters.AddWithValue("@apID", AppoinmentID);
                                comm.Parameters.AddWithValue("@curODO", CurODO);
                                comm.Parameters.AddWithValue("@billID", Billno);
                                comm.Parameters.AddWithValue("@outNote", note);
                                comm.Parameters.AddWithValue("@hireStatus", "active");

                                con.Open();
                                comm.ExecuteNonQuery();
                                this.UpdateID("`tblHire`", this.GetLastID("`tblHire`"));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }
                        #endregion
                        #region update res
                        try
                        {
                            using (con = new MySqlConnection(AppSett.CS))
                            {
                                string sql = @"UPDATE `tblreservation`
                                                                        SET `resStatus`=@resStatus,
                                                                        WHERE
                                                                             resID=@resID";
                                comm = new MySqlCommand(sql, con);
                                comm.Parameters.AddWithValue("@resStatus", "Proceeded");
                                comm.Parameters.AddWithValue("@resID", ResvID);

                                con.Open();
                                comm.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }
                        #endregion update res
                    }
                    this.Dispose();
                    MessageBox.Show("Data Saved");
                    this.Dispose();
                }
            }
        }
        public void AddPayment(string billID, DateTime payTime, string payMeth, string payName, string payRef, string chqDate, double payAmount)
        {
            string payID = string.Format("PAY{0}", this.GetLastID("`tblPayments`").ToString());
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    string sql = "INSERT INTO `tblpayments` (`payID`,`payBillID`,`payDate`,`PayMethod`,`payName`,`payRef`,`payChqDate`,`PayAmount`) VALUES (@payID,@payBillID,@payDate,@PayMethod,@payName,@payRef,@payChqDate,@PayAmount)";
                    comm = new MySqlCommand(sql, con);
                    comm.Parameters.AddWithValue("@payID", payID);
                    comm.Parameters.AddWithValue("@payBillID", billID);
                    comm.Parameters.AddWithValue("@payDate", payTime);
                    comm.Parameters.AddWithValue("@PayMethod", payMeth);
                    comm.Parameters.AddWithValue("@payName", payName);
                    comm.Parameters.AddWithValue("@payRef", payRef);
                    comm.Parameters.AddWithValue("@payChqDate", null);
                    comm.Parameters.AddWithValue("@PayAmount", payAmount);
                    con.Open();
                    comm.ExecuteNonQuery();
                    this.UpdateID("`tblPayments`", this.GetLastID("`tblPayments`"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void paynow_PayDone(object sender, PayDoneEventArgs e)
        {
            this.AddPayment(Billno, e.PayTime, e.PayMethod, e.CardName, e.CardRef, e.ChqDate, e.Payment);
            #region Add 2 Bill
            if (IsNewBill)
            {
                try
                {
                    using (con = new MySqlConnection(AppSett.CS))
                    {
                        string sql = @"INSERT INTO `tblbills` 
                                    (`billID`,`billdate`,`billCstNic`,`billTotal`,`billDue`,`billNote`,`billPayed`)
                                 VALUES  
                                    (@billID,@billdate,@billCstNic,@billTotal,@billDue,@billNote,@billPayed)";
                        comm = new MySqlCommand(sql, con);
                        comm.Parameters.AddWithValue("@billID", Billno);
                        comm.Parameters.AddWithValue("@billCstNic", txtNIC.Text.Trim());
                        comm.Parameters.AddWithValue("@billdate", DateTime.Now);
                        comm.Parameters.AddWithValue("@billTotal", double.Parse(lblBillTotal.Text));
                        comm.Parameters.AddWithValue("@billDue", e.DuePayment);
                        comm.Parameters.AddWithValue("@billNote", null);
                        comm.Parameters.AddWithValue("@billPayed", double.Parse(lblGrandTotal.Text)-e.DuePayment);
                        con.Open();
                        comm.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = "UPDATE `tblbills` SET `billDue`=@billDue,`billNote`=@billNote,`billPayed`=@billPayed WHERE `billID`=@billID";
                        comm.Parameters.AddWithValue("@billID", Billno);
                        comm.Parameters.AddWithValue("@billDue", e.DuePayment);
                        comm.Parameters.AddWithValue("@billPayed", double.Parse(lblGrandTotal.Text) - e.DuePayment);
                        comm.Parameters.AddWithValue("@billNote", null);
                        comm.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            #endregion
        }
        private void lblGrandTotal_TextChanged(object sender, EventArgs e)
        {
            double dt = double.Parse(lblGrandTotal.Text);
            double totbil = dt + TotExeptThis;
            lblBillTotal.Text = AppSett.FormatCurrency(totbil.ToString());
        }
    }
}
