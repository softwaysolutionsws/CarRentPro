﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Threading;


namespace CarRentPro
{
    public partial class frmLoging : Form
    {
       // DBConnetion db = new DBConnetion();
     
        MySqlConnection con;
        MySqlCommand cmd;
        public frmLoging()
        {
            
            InitializeComponent();
           /* Thread t = new Thread(new ThreadStart(StartForm));
            t.Start();
            Thread.Sleep(5000);
            t.Abort();
            Thread.Sleep(1000);
            if (t.IsAlive)
            {
               
                
                // Thread has not finished

            }
            else
            {
                // Finished
                
                Forms.frmActivationkey frm = new Forms.frmActivationkey();
                frm.ShowDialog();

            }*/
            //Hello 
        }
       

        private void UserAccess()
        {
          MySqlConnection  con = new MySqlConnection(AppSett.CS);
          con.Open();

            cmd = new MySqlCommand(  @"SELECT
                                    `UserName`
                                    , `Password`
                                    , `AccesseType`
                                FROM
                                    `carrentpro`.`tblusersloging`where username=@user and password=@pass", con);
           
            cmd.Parameters.AddWithValue("@user", textBox1.Text);
            cmd.Parameters.AddWithValue("@pass", textBox2.Text);
          
            MySqlDataReader myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            if (myReader.Read() == true)
            {
                Forms.frmMain frm = new Forms.frmMain();

                con = new MySqlConnection(AppSett.CS);
                con.Open();

                String Sql = "SELECT `AccesseType` FROM `carrentpro`.`tblusersloging` WHERE `UserName`='" + textBox1.Text + "'";
                cmd = new MySqlCommand(Sql, con);

                string value = cmd.ExecuteScalar().ToString();
                //MessageBox.Show("Done", "Login Done", MessageBoxButtons.OK, MessageBoxIcon.Error);
                switch (value)
                {
                    case "Admin":



                        frm.Show();
                        this.Hide();

                        break;
                    case "cashiyer":


                        break;
                    case "User":


                        break;

                    default:



                        return;

                }


            }


            else
            {
                MessageBox.Show("Incorrect Login Credentials...Try again !", "Login Denied", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
           
        }
        public void StartForm()
        {
            Application.Run(new Forms.frmsplash());
        }
        public void EndForm()
        {
            Application.Run(new Forms.frmActivationkey());
        }
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.facebook.com/swssystem/");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            label7.Text = "+94 71 27 67 843 - udara" ;
            label7.ForeColor = System.Drawing.Color.Red;
        }

        private void label4_MouseHover(object sender, EventArgs e)
        {
            label4.ForeColor = System.Drawing.Color.Red;
        }

        private void label4_MouseLeave(object sender, EventArgs e)
        {
            label4.ForeColor = System.Drawing.Color.White;
        }

        private void label2_MouseHover(object sender, EventArgs e)
        {
            label2.ForeColor = System.Drawing.Color.Red;
        }

        private void label2_MouseLeave(object sender, EventArgs e)
        {
            label2.ForeColor = System.Drawing.Color.White;
        }

        private void pictureBox4_MouseLeave(object sender, EventArgs e)
        {
            label7.Text = "";
            label7.ForeColor = System.Drawing.Color.Red;
        }

        private void label6_MouseHover(object sender, EventArgs e)
        {
            label6.ForeColor = System.Drawing.Color.Red;
        }

        private void label6_MouseLeave(object sender, EventArgs e)
        {
            label6.ForeColor = System.Drawing.Color.White;
        }

        private void textBox1_MouseHover(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox1.Focus();
            textBox1.ForeColor = System.Drawing.Color.Black;
        }

        private void textBox2_MouseMove(object sender, MouseEventArgs e)
        {
           
        }

        private void textBox2_MouseHover(object sender, EventArgs e)
        {
            textBox2.Text = "";
            textBox2.ForeColor = System.Drawing.Color.Black;
            textBox2.Focus();
        }
        private bool validate_login(string user, string pass)
        {
            using (con  = new MySqlConnection(AppSett.CS))
            {
                con.Open();
                cmd.CommandText = "Select * from loging where username=@user and password=@pass";
                cmd.Parameters.AddWithValue("@user", user);
                cmd.Parameters.AddWithValue("@pass", pass);
                cmd.Connection = con;
                MySqlDataReader login = cmd.ExecuteReader();
                if (login.Read())
                {
                    con.Close();
                    return true;
                }
                else
                {
                    con.Close();
                    return false;
                }
            }
          
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {

            UserAccess();
        }

        private void label2_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            label6.ForeColor = System.Drawing.Color.Red;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            label6.ForeColor = System.Drawing.Color.White;
        }

        private void frmLoging_Load(object sender, EventArgs e)
        {
           
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                UserAccess();
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            UserAccess();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                textBox2.Focus();
            }
        }
    }
}
