﻿namespace CarRentPro.Forms
{
    partial class frmCheckIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtagreementNo = new DevExpress.XtraEditors.TextEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.vGridControl1 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkinDataSet = new CarRentPro.CheckinDataSet();
            this.rowbillID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillPayed = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillDue = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillTotal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowchekoutID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDriverName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFuelAmount = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFuelLevel = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDecoRate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRatePerKm = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowHours = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLimitKm = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDays = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDrivaerRate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowEndDatetime = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowStartDateTime = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowNIC = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAddress1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAddress2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLicNo = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclNo = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclMake = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlModel = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlOdoReading = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.lblBilltotal = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtAmountExKM = new DevExpress.XtraEditors.TextEdit();
            this.txtAomuntExfuel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtDriverExCharges = new DevExpress.XtraEditors.TextEdit();
            this.txtfuelwastage = new DevExpress.XtraEditors.TextEdit();
            this.lbldueBlance = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtdiscount = new DevExpress.XtraEditors.TextEdit();
            this.txttotalKM = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtwatingchrgEX = new DevExpress.XtraEditors.TextEdit();
            this.txttotaldays = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.chkExharges = new DevExpress.XtraEditors.CheckEdit();
            this.txtExKM = new DevExpress.XtraEditors.TextEdit();
            this.lblGrandTotal = new DevExpress.XtraEditors.LabelControl();
            this.txtexhours = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtexdays = new DevExpress.XtraEditors.TextEdit();
            this.lblPayed = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.chkFuelAmt = new DevExpress.XtraEditors.CheckEdit();
            this.txtlatechrgEX = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtRateKM = new DevExpress.XtraEditors.TextEdit();
            this.lblExchargeHours = new DevExpress.XtraEditors.LabelControl();
            this.txtamountFule = new DevExpress.XtraEditors.TextEdit();
            this.lblsubTotal = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtNightCharEX = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lblexcharge = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.cGauge1 = new DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge();
            this.arcScaleBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleNeedleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleComponent2 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.dtpDateNow = new System.Windows.Forms.DateTimePicker();
            this.tmtimeNow = new DevExpress.XtraEditors.TimeEdit();
            this.txtodoreading = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.dataTable1TableAdapter = new CarRentPro.CheckinDataSetTableAdapters.DataTable1TableAdapter();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtagreementNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkinDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountExKM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAomuntExfuel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverExCharges.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfuelwastage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdiscount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotalKM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtwatingchrgEX.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotaldays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExharges.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExKM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexhours.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexdays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFuelAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlatechrgEX.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRateKM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtamountFule.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNightCharEX.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmtimeNow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtodoreading.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(6, 9);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(115, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Agreement No :";
            // 
            // txtagreementNo
            // 
            this.txtagreementNo.Location = new System.Drawing.Point(138, 5);
            this.txtagreementNo.Name = "txtagreementNo";
            this.txtagreementNo.Size = new System.Drawing.Size(140, 20);
            this.txtagreementNo.TabIndex = 1;
            this.txtagreementNo.EditValueChanged += new System.EventHandler(this.txtagreementNo_EditValueChanged);
            this.txtagreementNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtagreementNo_KeyDown);
            this.txtagreementNo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtagreementNo_MouseDown);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.vGridControl1);
            this.groupControl4.Location = new System.Drawing.Point(2, 33);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(283, 469);
            this.groupControl4.TabIndex = 22;
            this.groupControl4.Text = "Trancation Deatils";
            // 
            // vGridControl1
            // 
            this.vGridControl1.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridControl1.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridControl1.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridControl1.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridControl1.Appearance.Category.Options.UseBackColor = true;
            this.vGridControl1.Appearance.Category.Options.UseBorderColor = true;
            this.vGridControl1.Appearance.Category.Options.UseFont = true;
            this.vGridControl1.Appearance.Category.Options.UseForeColor = true;
            this.vGridControl1.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridControl1.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridControl1.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridControl1.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridControl1.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridControl1.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridControl1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridControl1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridControl1.Appearance.Empty.Options.UseBackColor = true;
            this.vGridControl1.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridControl1.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridControl1.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridControl1.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridControl1.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridControl1.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridControl1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridControl1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridControl1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridControl1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridControl1.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridControl1.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridControl1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridControl1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridControl1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridControl1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridControl1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridControl1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridControl1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridControl1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridControl1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridControl1.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridControl1.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridControl1.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridControl1.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridControl1.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridControl1.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridControl1.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridControl1.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridControl1.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridControl1.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridControl1.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridControl1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridControl1.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.vGridControl1.DataSource = this.dataTable1BindingSource;
            this.vGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridControl1.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.vGridControl1.Location = new System.Drawing.Point(2, 20);
            this.vGridControl1.Name = "vGridControl1";
            this.vGridControl1.RecordWidth = 96;
            this.vGridControl1.RowHeaderWidth = 104;
            this.vGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowbillID,
            this.rowchekoutID,
            this.rowFName,
            this.rowvhclNo});
            this.vGridControl1.Size = new System.Drawing.Size(279, 447);
            this.vGridControl1.TabIndex = 0;
            // 
            // dataTable1BindingSource
            // 
            this.dataTable1BindingSource.DataMember = "DataTable1";
            this.dataTable1BindingSource.DataSource = this.checkinDataSet;
            // 
            // checkinDataSet
            // 
            this.checkinDataSet.DataSetName = "CheckinDataSet";
            this.checkinDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowbillID
            // 
            this.rowbillID.Appearance.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rowbillID.Appearance.Options.UseFont = true;
            this.rowbillID.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowbillPayed,
            this.rowbillDue,
            this.rowbillTotal});
            this.rowbillID.Name = "rowbillID";
            this.rowbillID.Properties.Caption = "Billing Information";
            this.rowbillID.Properties.FieldName = "billID";
            // 
            // rowbillPayed
            // 
            this.rowbillPayed.Name = "rowbillPayed";
            this.rowbillPayed.Properties.Caption = "bill Payed";
            this.rowbillPayed.Properties.FieldName = "billPayed";
            // 
            // rowbillDue
            // 
            this.rowbillDue.Name = "rowbillDue";
            this.rowbillDue.Properties.Caption = "bill Due";
            this.rowbillDue.Properties.FieldName = "billDue";
            // 
            // rowbillTotal
            // 
            this.rowbillTotal.Name = "rowbillTotal";
            this.rowbillTotal.Properties.Caption = "bill Total";
            this.rowbillTotal.Properties.FieldName = "billTotal";
            // 
            // rowchekoutID
            // 
            this.rowchekoutID.Appearance.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rowchekoutID.Appearance.Options.UseFont = true;
            this.rowchekoutID.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowDriverName,
            this.rowFuelAmount,
            this.rowFuelLevel,
            this.rowDecoRate,
            this.rowRatePerKm,
            this.rowHours,
            this.rowLimitKm,
            this.rowDays,
            this.rowDrivaerRate,
            this.rowEndDatetime,
            this.rowStartDateTime});
            this.rowchekoutID.Height = 16;
            this.rowchekoutID.Name = "rowchekoutID";
            this.rowchekoutID.Properties.Caption = "Rent/Hair Infomation";
            this.rowchekoutID.Properties.FieldName = "chekoutID";
            // 
            // rowDriverName
            // 
            this.rowDriverName.Name = "rowDriverName";
            this.rowDriverName.Properties.Caption = "Driver Name";
            this.rowDriverName.Properties.FieldName = "DriverName";
            // 
            // rowFuelAmount
            // 
            this.rowFuelAmount.Name = "rowFuelAmount";
            this.rowFuelAmount.Properties.Caption = "Fuel Amount";
            this.rowFuelAmount.Properties.FieldName = "FuelAmount";
            // 
            // rowFuelLevel
            // 
            this.rowFuelLevel.Name = "rowFuelLevel";
            this.rowFuelLevel.Properties.Caption = "Fuel Level";
            this.rowFuelLevel.Properties.FieldName = "FuelLevel";
            // 
            // rowDecoRate
            // 
            this.rowDecoRate.Name = "rowDecoRate";
            this.rowDecoRate.Properties.Caption = "Deco Rate";
            this.rowDecoRate.Properties.FieldName = "DecoRate";
            // 
            // rowRatePerKm
            // 
            this.rowRatePerKm.Name = "rowRatePerKm";
            this.rowRatePerKm.Properties.Caption = "Rate Per Km";
            this.rowRatePerKm.Properties.FieldName = "RatePerKm";
            // 
            // rowHours
            // 
            this.rowHours.Name = "rowHours";
            this.rowHours.Properties.Caption = "Hours";
            this.rowHours.Properties.FieldName = "Hours";
            // 
            // rowLimitKm
            // 
            this.rowLimitKm.Name = "rowLimitKm";
            this.rowLimitKm.Properties.Caption = "Limit Km";
            this.rowLimitKm.Properties.FieldName = "LimitKm";
            // 
            // rowDays
            // 
            this.rowDays.Name = "rowDays";
            this.rowDays.Properties.Caption = "Days";
            this.rowDays.Properties.FieldName = "Days";
            // 
            // rowDrivaerRate
            // 
            this.rowDrivaerRate.Name = "rowDrivaerRate";
            this.rowDrivaerRate.Properties.Caption = "Drivaer Rate";
            this.rowDrivaerRate.Properties.FieldName = "DrivaerRate";
            // 
            // rowEndDatetime
            // 
            this.rowEndDatetime.Name = "rowEndDatetime";
            this.rowEndDatetime.Properties.Caption = "End Datetime";
            this.rowEndDatetime.Properties.FieldName = "EndDatetime";
            // 
            // rowStartDateTime
            // 
            this.rowStartDateTime.Name = "rowStartDateTime";
            this.rowStartDateTime.Properties.Caption = "Start Date Time";
            this.rowStartDateTime.Properties.FieldName = "StartDateTime";
            // 
            // rowFName
            // 
            this.rowFName.Appearance.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rowFName.Appearance.Options.UseFont = true;
            this.rowFName.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowNIC,
            this.rowAddress1,
            this.rowAddress2,
            this.rowLicNo,
            this.rowContact1,
            this.rowContact2,
            this.rowContact3});
            this.rowFName.Name = "rowFName";
            this.rowFName.Properties.Caption = "customer Information";
            this.rowFName.Properties.FieldName = "FName";
            // 
            // rowNIC
            // 
            this.rowNIC.Name = "rowNIC";
            this.rowNIC.Properties.Caption = "NIC";
            this.rowNIC.Properties.FieldName = "NIC";
            // 
            // rowAddress1
            // 
            this.rowAddress1.Name = "rowAddress1";
            this.rowAddress1.Properties.Caption = "Address1";
            this.rowAddress1.Properties.FieldName = "Address1";
            // 
            // rowAddress2
            // 
            this.rowAddress2.Name = "rowAddress2";
            this.rowAddress2.Properties.Caption = "Address2";
            this.rowAddress2.Properties.FieldName = "Address2";
            // 
            // rowLicNo
            // 
            this.rowLicNo.Name = "rowLicNo";
            this.rowLicNo.Properties.Caption = "Lic No";
            this.rowLicNo.Properties.FieldName = "LicNo";
            // 
            // rowContact1
            // 
            this.rowContact1.Name = "rowContact1";
            this.rowContact1.Properties.Caption = "Contact1";
            this.rowContact1.Properties.FieldName = "Contact1";
            // 
            // rowContact2
            // 
            this.rowContact2.Name = "rowContact2";
            this.rowContact2.Properties.Caption = "Contact2";
            this.rowContact2.Properties.FieldName = "Contact2";
            // 
            // rowContact3
            // 
            this.rowContact3.Name = "rowContact3";
            this.rowContact3.Properties.Caption = "Contact3";
            this.rowContact3.Properties.FieldName = "Contact3";
            // 
            // rowvhclNo
            // 
            this.rowvhclNo.Appearance.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rowvhclNo.Appearance.Options.UseFont = true;
            this.rowvhclNo.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowvhclMake,
            this.rowvhclType,
            this.rowvchlModel,
            this.rowvchlOdoReading});
            this.rowvhclNo.Name = "rowvhclNo";
            this.rowvhclNo.Properties.Caption = "Vehcile Infomation";
            this.rowvhclNo.Properties.FieldName = "vhclNo";
            // 
            // rowvhclMake
            // 
            this.rowvhclMake.Name = "rowvhclMake";
            this.rowvhclMake.Properties.Caption = "vhcl Make";
            this.rowvhclMake.Properties.FieldName = "vhclMake";
            // 
            // rowvhclType
            // 
            this.rowvhclType.Name = "rowvhclType";
            this.rowvhclType.Properties.Caption = "vhcl Type";
            this.rowvhclType.Properties.FieldName = "vhclType";
            // 
            // rowvchlModel
            // 
            this.rowvchlModel.Name = "rowvchlModel";
            this.rowvchlModel.Properties.Caption = "vchl Model";
            this.rowvchlModel.Properties.FieldName = "vchlModel";
            // 
            // rowvchlOdoReading
            // 
            this.rowvchlOdoReading.Name = "rowvchlOdoReading";
            this.rowvchlOdoReading.Properties.Caption = "vchl Odo Reading";
            this.rowvchlOdoReading.Properties.FieldName = "vchlOdoReading";
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.groupBox2);
            this.groupControl6.Controls.Add(this.groupBox1);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl6.Location = new System.Drawing.Point(2, 2);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(938, 500);
            this.groupControl6.TabIndex = 60;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.xtraTabControl1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(241, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(695, 478);
            this.groupBox2.TabIndex = 63;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Different ";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(3, 17);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(689, 458);
            this.xtraTabControl1.TabIndex = 109;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.lblBilltotal);
            this.xtraTabPage1.Controls.Add(this.labelControl18);
            this.xtraTabPage1.Controls.Add(this.txtAmountExKM);
            this.xtraTabPage1.Controls.Add(this.txtAomuntExfuel);
            this.xtraTabPage1.Controls.Add(this.labelControl22);
            this.xtraTabPage1.Controls.Add(this.labelControl13);
            this.xtraTabPage1.Controls.Add(this.txtDriverExCharges);
            this.xtraTabPage1.Controls.Add(this.txtfuelwastage);
            this.xtraTabPage1.Controls.Add(this.lbldueBlance);
            this.xtraTabPage1.Controls.Add(this.labelControl14);
            this.xtraTabPage1.Controls.Add(this.labelControl2);
            this.xtraTabPage1.Controls.Add(this.labelControl15);
            this.xtraTabPage1.Controls.Add(this.txtdiscount);
            this.xtraTabPage1.Controls.Add(this.txttotalKM);
            this.xtraTabPage1.Controls.Add(this.labelControl7);
            this.xtraTabPage1.Controls.Add(this.txtwatingchrgEX);
            this.xtraTabPage1.Controls.Add(this.txttotaldays);
            this.xtraTabPage1.Controls.Add(this.labelControl11);
            this.xtraTabPage1.Controls.Add(this.chkExharges);
            this.xtraTabPage1.Controls.Add(this.txtExKM);
            this.xtraTabPage1.Controls.Add(this.lblGrandTotal);
            this.xtraTabPage1.Controls.Add(this.txtexhours);
            this.xtraTabPage1.Controls.Add(this.checkEdit1);
            this.xtraTabPage1.Controls.Add(this.labelControl20);
            this.xtraTabPage1.Controls.Add(this.labelControl12);
            this.xtraTabPage1.Controls.Add(this.txtexdays);
            this.xtraTabPage1.Controls.Add(this.lblPayed);
            this.xtraTabPage1.Controls.Add(this.labelControl4);
            this.xtraTabPage1.Controls.Add(this.chkFuelAmt);
            this.xtraTabPage1.Controls.Add(this.txtlatechrgEX);
            this.xtraTabPage1.Controls.Add(this.labelControl21);
            this.xtraTabPage1.Controls.Add(this.txtRateKM);
            this.xtraTabPage1.Controls.Add(this.lblExchargeHours);
            this.xtraTabPage1.Controls.Add(this.txtamountFule);
            this.xtraTabPage1.Controls.Add(this.lblsubTotal);
            this.xtraTabPage1.Controls.Add(this.labelControl5);
            this.xtraTabPage1.Controls.Add(this.txtNightCharEX);
            this.xtraTabPage1.Controls.Add(this.checkEdit2);
            this.xtraTabPage1.Controls.Add(this.labelControl6);
            this.xtraTabPage1.Controls.Add(this.lblexcharge);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(683, 430);
            this.xtraTabPage1.Text = "Rent";
            // 
            // lblBilltotal
            // 
            this.lblBilltotal.Appearance.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBilltotal.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblBilltotal.Appearance.Options.UseFont = true;
            this.lblBilltotal.Appearance.Options.UseForeColor = true;
            this.lblBilltotal.Location = new System.Drawing.Point(551, 385);
            this.lblBilltotal.Name = "lblBilltotal";
            this.lblBilltotal.Size = new System.Drawing.Size(35, 23);
            this.lblBilltotal.TabIndex = 114;
            this.lblBilltotal.Text = "0.00";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Location = new System.Drawing.Point(286, 385);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(91, 23);
            this.labelControl18.TabIndex = 113;
            this.labelControl18.Text = "Bill Total    :";
            // 
            // txtAmountExKM
            // 
            this.txtAmountExKM.EditValue = "0.00";
            this.txtAmountExKM.Location = new System.Drawing.Point(473, 98);
            this.txtAmountExKM.Name = "txtAmountExKM";
            this.txtAmountExKM.Properties.ReadOnly = true;
            this.txtAmountExKM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtAmountExKM.Size = new System.Drawing.Size(105, 20);
            this.txtAmountExKM.TabIndex = 111;
            // 
            // txtAomuntExfuel
            // 
            this.txtAomuntExfuel.EditValue = "0.00";
            this.txtAomuntExfuel.Location = new System.Drawing.Point(473, 123);
            this.txtAomuntExfuel.Name = "txtAomuntExfuel";
            this.txtAomuntExfuel.Properties.ReadOnly = true;
            this.txtAomuntExfuel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtAomuntExfuel.Size = new System.Drawing.Size(105, 20);
            this.txtAomuntExfuel.TabIndex = 112;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Appearance.Options.UseForeColor = true;
            this.labelControl22.Location = new System.Drawing.Point(326, 349);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(72, 19);
            this.labelControl22.TabIndex = 110;
            this.labelControl22.Text = "Discount  :";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(105, 46);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(30, 14);
            this.labelControl13.TabIndex = 77;
            this.labelControl13.Text = "Date :";
            // 
            // txtDriverExCharges
            // 
            this.txtDriverExCharges.EditValue = "0.00";
            this.txtDriverExCharges.Enabled = false;
            this.txtDriverExCharges.Location = new System.Drawing.Point(473, 157);
            this.txtDriverExCharges.Name = "txtDriverExCharges";
            this.txtDriverExCharges.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDriverExCharges.Size = new System.Drawing.Size(105, 20);
            this.txtDriverExCharges.TabIndex = 94;
            this.txtDriverExCharges.EditValueChanged += new System.EventHandler(this.txtDriverExCharges_EditValueChanged);
            this.txtDriverExCharges.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDriverExCharges_KeyDown);
            // 
            // txtfuelwastage
            // 
            this.txtfuelwastage.EditValue = "0";
            this.txtfuelwastage.Location = new System.Drawing.Point(265, 121);
            this.txtfuelwastage.Name = "txtfuelwastage";
            this.txtfuelwastage.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfuelwastage.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtfuelwastage.Properties.Appearance.Options.UseFont = true;
            this.txtfuelwastage.Properties.Appearance.Options.UseForeColor = true;
            this.txtfuelwastage.Size = new System.Drawing.Size(60, 20);
            this.txtfuelwastage.TabIndex = 81;
            // 
            // lbldueBlance
            // 
            this.lbldueBlance.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldueBlance.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbldueBlance.Appearance.Options.UseFont = true;
            this.lbldueBlance.Appearance.Options.UseForeColor = true;
            this.lbldueBlance.Location = new System.Drawing.Point(546, 281);
            this.lbldueBlance.Name = "lbldueBlance";
            this.lbldueBlance.Size = new System.Drawing.Size(25, 18);
            this.lbldueBlance.TabIndex = 106;
            this.lbldueBlance.Text = "0.00";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(105, 126);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(72, 14);
            this.labelControl14.TabIndex = 82;
            this.labelControl14.Text = "Fuel wastage :";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(331, 123);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(7, 14);
            this.labelControl2.TabIndex = 68;
            this.labelControl2.Text = "X";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(276, 24);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(22, 13);
            this.labelControl15.TabIndex = 84;
            this.labelControl15.Text = "Ex  :";
            // 
            // txtdiscount
            // 
            this.txtdiscount.EditValue = "0.00";
            this.txtdiscount.Location = new System.Drawing.Point(491, 350);
            this.txtdiscount.Name = "txtdiscount";
            this.txtdiscount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtdiscount.Size = new System.Drawing.Size(105, 20);
            this.txtdiscount.TabIndex = 109;
            this.txtdiscount.EditValueChanged += new System.EventHandler(this.txtdiscount_EditValueChanged);
            // 
            // txttotalKM
            // 
            this.txttotalKM.EditValue = "0";
            this.txttotalKM.Location = new System.Drawing.Point(191, 95);
            this.txttotalKM.Name = "txttotalKM";
            this.txttotalKM.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalKM.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txttotalKM.Properties.Appearance.Options.UseFont = true;
            this.txttotalKM.Properties.Appearance.Options.UseForeColor = true;
            this.txttotalKM.Size = new System.Drawing.Size(68, 20);
            this.txttotalKM.TabIndex = 79;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(203, 24);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(53, 13);
            this.labelControl7.TabIndex = 91;
            this.labelControl7.Text = "Tota Use  :";
            // 
            // txtwatingchrgEX
            // 
            this.txtwatingchrgEX.EditValue = "0.00";
            this.txtwatingchrgEX.Enabled = false;
            this.txtwatingchrgEX.Location = new System.Drawing.Point(473, 232);
            this.txtwatingchrgEX.Name = "txtwatingchrgEX";
            this.txtwatingchrgEX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtwatingchrgEX.Size = new System.Drawing.Size(105, 20);
            this.txtwatingchrgEX.TabIndex = 100;
            this.txtwatingchrgEX.EditValueChanged += new System.EventHandler(this.txtwatingchrgEX_EditValueChanged);
            this.txtwatingchrgEX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtwatingchrgEX_KeyDown);
            // 
            // txttotaldays
            // 
            this.txttotaldays.EditValue = "0";
            this.txttotaldays.Location = new System.Drawing.Point(191, 43);
            this.txttotaldays.Name = "txttotaldays";
            this.txttotaldays.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotaldays.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txttotaldays.Properties.Appearance.Options.UseFont = true;
            this.txttotaldays.Properties.Appearance.Options.UseForeColor = true;
            this.txttotaldays.Size = new System.Drawing.Size(68, 20);
            this.txttotaldays.TabIndex = 76;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(105, 103);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(89, 14);
            this.labelControl11.TabIndex = 80;
            this.labelControl11.Text = "Totak Km usage  :";
            // 
            // chkExharges
            // 
            this.chkExharges.Location = new System.Drawing.Point(106, 146);
            this.chkExharges.Name = "chkExharges";
            this.chkExharges.Properties.Caption = "Ex : Driver charge";
            this.chkExharges.Size = new System.Drawing.Size(123, 19);
            this.chkExharges.TabIndex = 93;
            this.chkExharges.CheckedChanged += new System.EventHandler(this.chkExharges_CheckedChanged);
            // 
            // txtExKM
            // 
            this.txtExKM.EditValue = "0";
            this.txtExKM.Location = new System.Drawing.Point(265, 95);
            this.txtExKM.Name = "txtExKM";
            this.txtExKM.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExKM.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtExKM.Properties.Appearance.Options.UseFont = true;
            this.txtExKM.Properties.Appearance.Options.UseForeColor = true;
            this.txtExKM.Size = new System.Drawing.Size(60, 20);
            this.txtExKM.TabIndex = 83;
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandTotal.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblGrandTotal.Appearance.Options.UseFont = true;
            this.lblGrandTotal.Appearance.Options.UseForeColor = true;
            this.lblGrandTotal.Location = new System.Drawing.Point(543, 301);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(28, 19);
            this.lblGrandTotal.TabIndex = 108;
            this.lblGrandTotal.Text = "0.00";
            // 
            // txtexhours
            // 
            this.txtexhours.EditValue = "0";
            this.txtexhours.Location = new System.Drawing.Point(265, 69);
            this.txtexhours.Name = "txtexhours";
            this.txtexhours.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexhours.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtexhours.Properties.Appearance.Options.UseFont = true;
            this.txtexhours.Properties.Appearance.Options.UseForeColor = true;
            this.txtexhours.Size = new System.Drawing.Size(60, 20);
            this.txtexhours.TabIndex = 75;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(106, 221);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Waiting Charge";
            this.checkEdit1.Size = new System.Drawing.Size(141, 19);
            this.checkEdit1.TabIndex = 99;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Appearance.Options.UseForeColor = true;
            this.labelControl20.Location = new System.Drawing.Point(305, 301);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(93, 19);
            this.labelControl20.TabIndex = 107;
            this.labelControl20.Text = "Grand Total  :";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(105, 72);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(35, 14);
            this.labelControl12.TabIndex = 78;
            this.labelControl12.Text = "Hours :";
            // 
            // txtexdays
            // 
            this.txtexdays.EditValue = "0";
            this.txtexdays.Location = new System.Drawing.Point(265, 43);
            this.txtexdays.Name = "txtexdays";
            this.txtexdays.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexdays.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtexdays.Properties.Appearance.Options.UseFont = true;
            this.txtexdays.Properties.Appearance.Options.UseForeColor = true;
            this.txtexdays.Size = new System.Drawing.Size(60, 20);
            this.txtexdays.TabIndex = 85;
            this.txtexdays.EditValueChanged += new System.EventHandler(this.txtexdays_EditValueChanged);
            // 
            // lblPayed
            // 
            this.lblPayed.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPayed.Appearance.Options.UseFont = true;
            this.lblPayed.Location = new System.Drawing.Point(546, 326);
            this.lblPayed.Name = "lblPayed";
            this.lblPayed.Size = new System.Drawing.Size(25, 18);
            this.lblPayed.TabIndex = 105;
            this.lblPayed.Text = "0.00";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(331, 262);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(67, 18);
            this.labelControl4.TabIndex = 101;
            this.labelControl4.Text = "Sub Total  :";
            // 
            // chkFuelAmt
            // 
            this.chkFuelAmt.Location = new System.Drawing.Point(106, 171);
            this.chkFuelAmt.Name = "chkFuelAmt";
            this.chkFuelAmt.Properties.Caption = "Night Out Charge";
            this.chkFuelAmt.Size = new System.Drawing.Size(141, 19);
            this.chkFuelAmt.TabIndex = 95;
            this.chkFuelAmt.CheckedChanged += new System.EventHandler(this.chkFuelAmt_CheckedChanged);
            // 
            // txtlatechrgEX
            // 
            this.txtlatechrgEX.EditValue = "0.00";
            this.txtlatechrgEX.Enabled = false;
            this.txtlatechrgEX.Location = new System.Drawing.Point(473, 208);
            this.txtlatechrgEX.Name = "txtlatechrgEX";
            this.txtlatechrgEX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtlatechrgEX.Size = new System.Drawing.Size(105, 20);
            this.txtlatechrgEX.TabIndex = 98;
            this.txtlatechrgEX.EditValueChanged += new System.EventHandler(this.txtlatechrgEX_EditValueChanged);
            this.txtlatechrgEX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtlatechrgEX_KeyDown);
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Location = new System.Drawing.Point(331, 101);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(7, 14);
            this.labelControl21.TabIndex = 108;
            this.labelControl21.Text = "X";
            // 
            // txtRateKM
            // 
            this.txtRateKM.EditValue = " ";
            this.txtRateKM.Location = new System.Drawing.Point(346, 97);
            this.txtRateKM.Name = "txtRateKM";
            this.txtRateKM.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRateKM.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtRateKM.Properties.Appearance.Options.UseFont = true;
            this.txtRateKM.Properties.Appearance.Options.UseForeColor = true;
            this.txtRateKM.Size = new System.Drawing.Size(60, 20);
            this.txtRateKM.TabIndex = 107;
            this.txtRateKM.EditValueChanged += new System.EventHandler(this.txtRateKM_EditValueChanged);
            this.txtRateKM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRateKM_KeyDown);
            // 
            // lblExchargeHours
            // 
            this.lblExchargeHours.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExchargeHours.Appearance.Options.UseFont = true;
            this.lblExchargeHours.Appearance.Options.UseTextOptions = true;
            this.lblExchargeHours.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblExchargeHours.Location = new System.Drawing.Point(551, 73);
            this.lblExchargeHours.Name = "lblExchargeHours";
            this.lblExchargeHours.Size = new System.Drawing.Size(27, 14);
            this.lblExchargeHours.TabIndex = 88;
            this.lblExchargeHours.Text = "00.00";
            this.lblExchargeHours.Visible = false;
            // 
            // txtamountFule
            // 
            this.txtamountFule.Location = new System.Drawing.Point(346, 121);
            this.txtamountFule.Name = "txtamountFule";
            this.txtamountFule.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamountFule.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtamountFule.Properties.Appearance.Options.UseFont = true;
            this.txtamountFule.Properties.Appearance.Options.UseForeColor = true;
            this.txtamountFule.Size = new System.Drawing.Size(60, 20);
            this.txtamountFule.TabIndex = 86;
            this.txtamountFule.EditValueChanged += new System.EventHandler(this.txtamountFule_EditValueChanged);
            // 
            // lblsubTotal
            // 
            this.lblsubTotal.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsubTotal.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblsubTotal.Appearance.Options.UseFont = true;
            this.lblsubTotal.Appearance.Options.UseForeColor = true;
            this.lblsubTotal.Location = new System.Drawing.Point(546, 262);
            this.lblsubTotal.Name = "lblsubTotal";
            this.lblsubTotal.Size = new System.Drawing.Size(25, 18);
            this.lblsubTotal.TabIndex = 104;
            this.lblsubTotal.Text = "0.00";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(291, 326);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(107, 18);
            this.labelControl5.TabIndex = 102;
            this.labelControl5.Text = "Advance Payed   :";
            // 
            // txtNightCharEX
            // 
            this.txtNightCharEX.EditValue = "0.00";
            this.txtNightCharEX.Enabled = false;
            this.txtNightCharEX.Location = new System.Drawing.Point(473, 182);
            this.txtNightCharEX.Name = "txtNightCharEX";
            this.txtNightCharEX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNightCharEX.Size = new System.Drawing.Size(105, 20);
            this.txtNightCharEX.TabIndex = 96;
            this.txtNightCharEX.EditValueChanged += new System.EventHandler(this.txtNightCharEX_EditValueChanged);
            this.txtNightCharEX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNightCharEX_KeyDown);
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(106, 196);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Late  Charges (Rs.)";
            this.checkEdit2.Size = new System.Drawing.Size(123, 19);
            this.checkEdit2.TabIndex = 97;
            this.checkEdit2.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Location = new System.Drawing.Point(309, 281);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(89, 18);
            this.labelControl6.TabIndex = 103;
            this.labelControl6.Text = "Due Balance   :";
            // 
            // lblexcharge
            // 
            this.lblexcharge.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblexcharge.Appearance.Options.UseFont = true;
            this.lblexcharge.Appearance.Options.UseTextOptions = true;
            this.lblexcharge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblexcharge.Location = new System.Drawing.Point(551, 49);
            this.lblexcharge.Name = "lblexcharge";
            this.lblexcharge.Size = new System.Drawing.Size(27, 14);
            this.lblexcharge.TabIndex = 87;
            this.lblexcharge.Text = "00.00";
            this.lblexcharge.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textEdit1);
            this.groupBox1.Controls.Add(this.labelControl8);
            this.groupBox1.Controls.Add(this.gaugeControl1);
            this.groupBox1.Controls.Add(this.dtpDateNow);
            this.groupBox1.Controls.Add(this.tmtimeNow);
            this.groupBox1.Controls.Add(this.txtodoreading);
            this.groupBox1.Controls.Add(this.labelControl10);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.labelControl9);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(2, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(239, 478);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Current Record";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(93, 126);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Size = new System.Drawing.Size(44, 20);
            this.textEdit1.TabIndex = 66;
            this.textEdit1.EditValueChanged += new System.EventHandler(this.textEdit1_EditValueChanged);
            this.textEdit1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEdit1_KeyDown);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(7, 129);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(59, 14);
            this.labelControl8.TabIndex = 67;
            this.labelControl8.Text = "Fuel Level  :";
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.cGauge1});
            this.gaugeControl1.Location = new System.Drawing.Point(59, 196);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(147, 152);
            this.gaugeControl1.TabIndex = 65;
            this.gaugeControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gaugeControl1_MouseMove_1);
            // 
            // cGauge1
            // 
            this.cGauge1.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent[] {
            this.arcScaleBackgroundLayerComponent1});
            this.cGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 135, 140);
            this.cGauge1.Name = "cGauge1";
            this.cGauge1.Needles.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent[] {
            this.arcScaleNeedleComponent1});
            this.cGauge1.Scales.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent[] {
            this.arcScaleComponent1,
            this.arcScaleComponent2});
            // 
            // arcScaleBackgroundLayerComponent1
            // 
            this.arcScaleBackgroundLayerComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleBackgroundLayerComponent1.Name = "bg1";
            this.arcScaleBackgroundLayerComponent1.ScaleCenterPos = new DevExpress.XtraGauges.Core.Base.PointF2D(0.85F, 0.85F);
            this.arcScaleBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularQuarter_Style11Left;
            this.arcScaleBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // arcScaleComponent1
            // 
            this.arcScaleComponent1.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 14F);
            this.arcScaleComponent1.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(212F, 212F);
            this.arcScaleComponent1.EndAngle = -90F;
            this.arcScaleComponent1.MajorTickCount = 3;
            this.arcScaleComponent1.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent1.MajorTickmark.ShapeOffset = -6F;
            this.arcScaleComponent1.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(1.6F, 1.6F);
            this.arcScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_4;
            this.arcScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent1.MaxValue = 10F;
            this.arcScaleComponent1.MinorTickCount = 4;
            this.arcScaleComponent1.MinorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(1.6F, 1.6F);
            this.arcScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_3;
            this.arcScaleComponent1.Name = "scale1";
            this.arcScaleComponent1.RadiusX = 176F;
            this.arcScaleComponent1.RadiusY = 176F;
            this.arcScaleComponent1.StartAngle = -180F;
            this.arcScaleComponent1.Value = 10F;
            // 
            // arcScaleNeedleComponent1
            // 
            this.arcScaleNeedleComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleNeedleComponent1.EndOffset = -12F;
            this.arcScaleNeedleComponent1.Name = "needle1";
            this.arcScaleNeedleComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_Style11;
            this.arcScaleNeedleComponent1.StartOffset = -17.5F;
            this.arcScaleNeedleComponent1.ZOrder = -50;
            // 
            // arcScaleComponent2
            // 
            this.arcScaleComponent2.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.arcScaleComponent2.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponent2.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(212F, 212F);
            this.arcScaleComponent2.EndAngle = -90F;
            this.arcScaleComponent2.MajorTickCount = 4;
            this.arcScaleComponent2.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent2.MajorTickmark.ShapeOffset = -3F;
            this.arcScaleComponent2.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_2;
            this.arcScaleComponent2.MajorTickmark.TextOffset = -20F;
            this.arcScaleComponent2.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent2.MaxValue = 500F;
            this.arcScaleComponent2.MinorTickCount = 4;
            this.arcScaleComponent2.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_1;
            this.arcScaleComponent2.MinValue = 200F;
            this.arcScaleComponent2.Name = "scale2";
            this.arcScaleComponent2.RadiusX = 120F;
            this.arcScaleComponent2.RadiusY = 120F;
            this.arcScaleComponent2.StartAngle = -180F;
            this.arcScaleComponent2.Value = 200F;
            this.arcScaleComponent2.ZOrder = -1;
            // 
            // dtpDateNow
            // 
            this.dtpDateNow.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateNow.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateNow.Location = new System.Drawing.Point(92, 41);
            this.dtpDateNow.Name = "dtpDateNow";
            this.dtpDateNow.Size = new System.Drawing.Size(114, 22);
            this.dtpDateNow.TabIndex = 64;
            // 
            // tmtimeNow
            // 
            this.tmtimeNow.EditValue = new System.DateTime(2018, 1, 9, 0, 0, 0, 0);
            this.tmtimeNow.Location = new System.Drawing.Point(92, 67);
            this.tmtimeNow.Name = "tmtimeNow";
            this.tmtimeNow.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmtimeNow.Properties.Appearance.Options.UseFont = true;
            this.tmtimeNow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tmtimeNow.Size = new System.Drawing.Size(114, 20);
            this.tmtimeNow.TabIndex = 63;
            // 
            // txtodoreading
            // 
            this.txtodoreading.Location = new System.Drawing.Point(92, 95);
            this.txtodoreading.Name = "txtodoreading";
            this.txtodoreading.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtodoreading.Properties.Appearance.Options.UseFont = true;
            this.txtodoreading.Size = new System.Drawing.Size(114, 20);
            this.txtodoreading.TabIndex = 61;
            this.txtodoreading.EditValueChanged += new System.EventHandler(this.txtodoreading_EditValueChanged);
            this.txtodoreading.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtodoreading_KeyDown);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(6, 98);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(76, 14);
            this.labelControl10.TabIndex = 62;
            this.labelControl10.Text = "ODO Reading  :";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(6, 67);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(31, 14);
            this.labelControl3.TabIndex = 60;
            this.labelControl3.Text = "Time :";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(7, 41);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(30, 14);
            this.labelControl9.TabIndex = 60;
            this.labelControl9.Text = "Date :";
            // 
            // dataTable1TableAdapter
            // 
            this.dataTable1TableAdapter.ClearBeforeFill = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtagreementNo);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(283, 30);
            this.panelControl1.TabIndex = 61;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.groupControl4);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(287, 504);
            this.panelControl2.TabIndex = 62;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.groupControl6);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(287, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(942, 504);
            this.panelControl3.TabIndex = 63;
            // 
            // frmCheckIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1229, 504);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Name = "frmCheckIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Check In";
            this.Load += new System.EventHandler(this.frmCheckIn_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtagreementNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkinDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountExKM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAomuntExfuel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverExCharges.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfuelwastage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdiscount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotalKM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtwatingchrgEX.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotaldays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExharges.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExKM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexhours.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexdays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFuelAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlatechrgEX.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRateKM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtamountFule.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNightCharEX.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmtimeNow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtodoreading.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtagreementNo;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit txtodoreading;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private System.Windows.Forms.DateTimePicker dtpDateNow;
        private DevExpress.XtraEditors.TimeEdit tmtimeNow;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lblExchargeHours;
        private DevExpress.XtraEditors.LabelControl lblexcharge;
        private DevExpress.XtraEditors.TextEdit txtamountFule;
        private DevExpress.XtraEditors.TextEdit txtexdays;
        private DevExpress.XtraEditors.TextEdit txtExKM;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtfuelwastage;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txttotalKM;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtexhours;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txttotaldays;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge cGauge1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNightCharEX;
        private DevExpress.XtraEditors.CheckEdit chkFuelAmt;
        private DevExpress.XtraEditors.TextEdit txtDriverExCharges;
        private DevExpress.XtraEditors.CheckEdit chkExharges;
        private DevExpress.XtraEditors.LabelControl lbldueBlance;
        private DevExpress.XtraEditors.LabelControl lblPayed;
        private DevExpress.XtraEditors.LabelControl lblsubTotal;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtwatingchrgEX;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.TextEdit txtlatechrgEX;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.LabelControl lblGrandTotal;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraVerticalGrid.VGridControl vGridControl1;
        private System.Windows.Forms.BindingSource dataTable1BindingSource;
        private CheckinDataSet checkinDataSet;
        private CheckinDataSetTableAdapters.DataTable1TableAdapter dataTable1TableAdapter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillDue;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillPayed;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowchekoutID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDriverName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStartDateTime;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowEndDatetime;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDrivaerRate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDays;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLimitKm;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowHours;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRatePerKm;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDecoRate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFuelLevel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFuelAmount;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNIC;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAddress1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAddress2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLicNo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclNo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclMake;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlModel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlOdoReading;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtRateKM;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit txtdiscount;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.TextEdit txtAmountExKM;
        private DevExpress.XtraEditors.TextEdit txtAomuntExfuel;
        private DevExpress.XtraEditors.LabelControl lblBilltotal;
        private DevExpress.XtraEditors.LabelControl labelControl18;
    }
}