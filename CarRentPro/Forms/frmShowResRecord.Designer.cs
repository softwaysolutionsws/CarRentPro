﻿namespace CarRentPro.Forms
{
    partial class frmShowResRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmShowResRecord));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.vGridCustomer = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords1 = new CarRentPro.DsRecords();
            this.rowNIC = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAddress1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPic = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.vGridVehicle = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords2 = new CarRentPro.DsRecords();
            this.rowvhclNo1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclMake1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclType1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlModel1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlOdoReading1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlNextService1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclInsExpDate1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlLicExpDate1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowImage11 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.vGridRes = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords3 = new CarRentPro.DsRecords();
            this.rowresID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowresDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowresStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowresEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowresPick = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowresDrop = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowresTrip = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowresHire = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowresDecor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowresTotal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.vGridBill = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords4 = new CarRentPro.DsRecords();
            this.rowbillID3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbilldate3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillTotal3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillPayed3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillDue3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnOUT = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridRes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.vGridCustomer);
            this.groupControl1.Location = new System.Drawing.Point(12, 10);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(247, 231);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Customer Details";
            // 
            // vGridCustomer
            // 
            this.vGridCustomer.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.vGridCustomer.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.vGridCustomer.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridCustomer.Appearance.Category.ForeColor = System.Drawing.Color.Black;
            this.vGridCustomer.Appearance.Category.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.Category.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.Category.Options.UseFont = true;
            this.vGridCustomer.Appearance.Category.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.vGridCustomer.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.vGridCustomer.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.vGridCustomer.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.Empty.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(202)))), ((int)(((byte)(252)))));
            this.vGridCustomer.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(202)))), ((int)(((byte)(252)))));
            this.vGridCustomer.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridCustomer.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.vGridCustomer.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.vGridCustomer.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.vGridCustomer.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.vGridCustomer.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.vGridCustomer.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.vGridCustomer.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.vGridCustomer.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.vGridCustomer.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridCustomer.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.vGridCustomer.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.vGridCustomer.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.vGridCustomer.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.vGridCustomer.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridCustomer.DataMember = "DTresData";
            this.vGridCustomer.DataSource = this.dsRecords1;
            this.vGridCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridCustomer.Location = new System.Drawing.Point(2, 20);
            this.vGridCustomer.Name = "vGridCustomer";
            this.vGridCustomer.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowNIC,
            this.rowFName,
            this.rowAddress1,
            this.rowContact1,
            this.rowContact2,
            this.rowContact3,
            this.rowPic});
            this.vGridCustomer.Size = new System.Drawing.Size(243, 209);
            this.vGridCustomer.TabIndex = 0;
            // 
            // dsRecords1
            // 
            this.dsRecords1.DataSetName = "DsRecords";
            this.dsRecords1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowNIC
            // 
            this.rowNIC.Name = "rowNIC";
            this.rowNIC.Properties.Caption = "NIC";
            this.rowNIC.Properties.FieldName = "NIC";
            // 
            // rowFName
            // 
            this.rowFName.Name = "rowFName";
            this.rowFName.Properties.Caption = "Name";
            this.rowFName.Properties.FieldName = "FName";
            // 
            // rowAddress1
            // 
            this.rowAddress1.Height = 41;
            this.rowAddress1.Name = "rowAddress1";
            this.rowAddress1.Properties.Caption = "Address";
            this.rowAddress1.Properties.FieldName = "Address1";
            // 
            // rowContact1
            // 
            this.rowContact1.Name = "rowContact1";
            this.rowContact1.Properties.Caption = "Contact 1";
            this.rowContact1.Properties.FieldName = "Contact1";
            // 
            // rowContact2
            // 
            this.rowContact2.Name = "rowContact2";
            this.rowContact2.Properties.Caption = "Contact 2";
            this.rowContact2.Properties.FieldName = "Contact2";
            // 
            // rowContact3
            // 
            this.rowContact3.Name = "rowContact3";
            this.rowContact3.Properties.Caption = "Contact 3";
            this.rowContact3.Properties.FieldName = "Contact3";
            // 
            // rowPic
            // 
            this.rowPic.Height = 67;
            this.rowPic.Name = "rowPic";
            this.rowPic.Properties.Caption = "photo";
            this.rowPic.Properties.FieldName = "Pic";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.vGridVehicle);
            this.groupControl2.Location = new System.Drawing.Point(265, 10);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(247, 240);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Vehicle Details";
            // 
            // vGridVehicle
            // 
            this.vGridVehicle.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.vGridVehicle.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridVehicle.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridVehicle.Appearance.Category.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicle.Appearance.Category.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.Category.Options.UseBorderColor = true;
            this.vGridVehicle.Appearance.Category.Options.UseFont = true;
            this.vGridVehicle.Appearance.Category.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridVehicle.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridVehicle.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicle.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridVehicle.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.vGridVehicle.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridVehicle.Appearance.Empty.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(139)))));
            this.vGridVehicle.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(139)))));
            this.vGridVehicle.Appearance.ExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicle.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridVehicle.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridVehicle.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicle.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(251)))), ((int)(((byte)(193)))));
            this.vGridVehicle.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(152)))), ((int)(((byte)(49)))));
            this.vGridVehicle.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.vGridVehicle.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridVehicle.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.vGridVehicle.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(176)))), ((int)(((byte)(84)))));
            this.vGridVehicle.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.vGridVehicle.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridVehicle.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.vGridVehicle.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicle.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.vGridVehicle.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.vGridVehicle.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicle.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridVehicle.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridVehicle.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridVehicle.DataMember = "DTresData";
            this.vGridVehicle.DataSource = this.dsRecords2;
            this.vGridVehicle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridVehicle.Location = new System.Drawing.Point(2, 20);
            this.vGridVehicle.Name = "vGridVehicle";
            this.vGridVehicle.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowvhclNo1,
            this.rowvhclMake1,
            this.rowvhclType1,
            this.rowvchlModel1,
            this.rowvchlOdoReading1,
            this.rowvchlNextService1,
            this.rowvhclInsExpDate1,
            this.rowvchlLicExpDate1,
            this.rowImage11});
            this.vGridVehicle.Size = new System.Drawing.Size(243, 218);
            this.vGridVehicle.TabIndex = 0;
            // 
            // dsRecords2
            // 
            this.dsRecords2.DataSetName = "DsRecords";
            this.dsRecords2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowvhclNo1
            // 
            this.rowvhclNo1.Name = "rowvhclNo1";
            this.rowvhclNo1.Properties.Caption = "Vehicle No";
            this.rowvhclNo1.Properties.FieldName = "vhclNo";
            // 
            // rowvhclMake1
            // 
            this.rowvhclMake1.Name = "rowvhclMake1";
            this.rowvhclMake1.Properties.Caption = "Make";
            this.rowvhclMake1.Properties.FieldName = "vhclMake";
            // 
            // rowvhclType1
            // 
            this.rowvhclType1.Name = "rowvhclType1";
            this.rowvhclType1.Properties.Caption = "Type";
            this.rowvhclType1.Properties.FieldName = "vhclType";
            // 
            // rowvchlModel1
            // 
            this.rowvchlModel1.Name = "rowvchlModel1";
            this.rowvchlModel1.Properties.Caption = "Model";
            this.rowvchlModel1.Properties.FieldName = "vchlModel";
            // 
            // rowvchlOdoReading1
            // 
            this.rowvchlOdoReading1.Name = "rowvchlOdoReading1";
            this.rowvchlOdoReading1.Properties.Caption = "ODO Reading";
            this.rowvchlOdoReading1.Properties.FieldName = "vchlOdoReading";
            // 
            // rowvchlNextService1
            // 
            this.rowvchlNextService1.Name = "rowvchlNextService1";
            this.rowvchlNextService1.Properties.Caption = "Next Service";
            this.rowvchlNextService1.Properties.FieldName = "vchlNextService";
            // 
            // rowvhclInsExpDate1
            // 
            this.rowvhclInsExpDate1.Name = "rowvhclInsExpDate1";
            this.rowvhclInsExpDate1.Properties.Caption = "Ins Exp Date";
            this.rowvhclInsExpDate1.Properties.FieldName = "vhclInsExpDate";
            // 
            // rowvchlLicExpDate1
            // 
            this.rowvchlLicExpDate1.Name = "rowvchlLicExpDate1";
            this.rowvchlLicExpDate1.Properties.Caption = "Lic Exp Date";
            this.rowvchlLicExpDate1.Properties.FieldName = "vchlLicExpDate";
            // 
            // rowImage11
            // 
            this.rowImage11.Height = 74;
            this.rowImage11.Name = "rowImage11";
            this.rowImage11.Properties.Caption = "Photo";
            this.rowImage11.Properties.FieldName = "Image1";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.vGridRes);
            this.groupControl3.Location = new System.Drawing.Point(14, 247);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(247, 213);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = "Res Details";
            // 
            // vGridRes
            // 
            this.vGridRes.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.vGridRes.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.vGridRes.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridRes.Appearance.Category.ForeColor = System.Drawing.Color.Black;
            this.vGridRes.Appearance.Category.Options.UseBackColor = true;
            this.vGridRes.Appearance.Category.Options.UseBorderColor = true;
            this.vGridRes.Appearance.Category.Options.UseFont = true;
            this.vGridRes.Appearance.Category.Options.UseForeColor = true;
            this.vGridRes.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.vGridRes.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.vGridRes.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridRes.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridRes.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.vGridRes.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridRes.Appearance.Empty.Options.UseBackColor = true;
            this.vGridRes.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(192)))), ((int)(((byte)(114)))));
            this.vGridRes.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(192)))), ((int)(((byte)(114)))));
            this.vGridRes.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridRes.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridRes.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridRes.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridRes.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridRes.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridRes.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.vGridRes.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridRes.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.vGridRes.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.vGridRes.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridRes.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridRes.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gainsboro;
            this.vGridRes.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.vGridRes.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridRes.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridRes.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.vGridRes.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridRes.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.vGridRes.Appearance.RecordValue.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.vGridRes.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridRes.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridRes.Appearance.RecordValue.Options.UseBorderColor = true;
            this.vGridRes.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridRes.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(136)))));
            this.vGridRes.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.vGridRes.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.vGridRes.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridRes.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridRes.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridRes.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.vGridRes.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridRes.DataMember = "DTresData";
            this.vGridRes.DataSource = this.dsRecords3;
            this.vGridRes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridRes.Location = new System.Drawing.Point(2, 20);
            this.vGridRes.Name = "vGridRes";
            this.vGridRes.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowresID,
            this.rowresDate,
            this.rowresStart,
            this.rowresEnd,
            this.rowresPick,
            this.rowresDrop,
            this.rowresTrip,
            this.rowresHire,
            this.rowresDecor,
            this.rowresTotal});
            this.vGridRes.Size = new System.Drawing.Size(243, 191);
            this.vGridRes.TabIndex = 0;
            // 
            // dsRecords3
            // 
            this.dsRecords3.DataSetName = "DsRecords";
            this.dsRecords3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowresID
            // 
            this.rowresID.Name = "rowresID";
            this.rowresID.Properties.Caption = "Res ID";
            this.rowresID.Properties.FieldName = "resID";
            // 
            // rowresDate
            // 
            this.rowresDate.Name = "rowresDate";
            this.rowresDate.Properties.Caption = "Date";
            this.rowresDate.Properties.FieldName = "resDate";
            // 
            // rowresStart
            // 
            this.rowresStart.Name = "rowresStart";
            this.rowresStart.Properties.Caption = "Start";
            this.rowresStart.Properties.FieldName = "resStart";
            // 
            // rowresEnd
            // 
            this.rowresEnd.Height = 18;
            this.rowresEnd.Name = "rowresEnd";
            this.rowresEnd.Properties.Caption = "End";
            this.rowresEnd.Properties.FieldName = "resEnd";
            // 
            // rowresPick
            // 
            this.rowresPick.Name = "rowresPick";
            this.rowresPick.Properties.Caption = "Pick @";
            this.rowresPick.Properties.FieldName = "resPick";
            // 
            // rowresDrop
            // 
            this.rowresDrop.Height = 16;
            this.rowresDrop.Name = "rowresDrop";
            this.rowresDrop.Properties.Caption = "Drop @";
            this.rowresDrop.Properties.FieldName = "resDrop";
            // 
            // rowresTrip
            // 
            this.rowresTrip.Name = "rowresTrip";
            this.rowresTrip.Properties.Caption = "Trip";
            this.rowresTrip.Properties.FieldName = "resTrip";
            // 
            // rowresHire
            // 
            this.rowresHire.Name = "rowresHire";
            this.rowresHire.Properties.Caption = "Hire";
            this.rowresHire.Properties.FieldName = "resHire";
            // 
            // rowresDecor
            // 
            this.rowresDecor.Name = "rowresDecor";
            this.rowresDecor.Properties.Caption = "Decor Charges";
            this.rowresDecor.Properties.FieldName = "resDecor";
            // 
            // rowresTotal
            // 
            this.rowresTotal.Name = "rowresTotal";
            this.rowresTotal.Properties.Caption = "Total";
            this.rowresTotal.Properties.FieldName = "resTotal";
            // 
            // vGridBill
            // 
            this.vGridBill.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(57)))));
            this.vGridBill.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.vGridBill.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridBill.Appearance.Category.ForeColor = System.Drawing.Color.White;
            this.vGridBill.Appearance.Category.Options.UseBackColor = true;
            this.vGridBill.Appearance.Category.Options.UseBorderColor = true;
            this.vGridBill.Appearance.Category.Options.UseFont = true;
            this.vGridBill.Appearance.Category.Options.UseForeColor = true;
            this.vGridBill.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.vGridBill.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.vGridBill.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.White;
            this.vGridBill.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridBill.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridBill.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridBill.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.vGridBill.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridBill.Appearance.Empty.Options.UseBackColor = true;
            this.vGridBill.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(193)))), ((int)(((byte)(74)))));
            this.vGridBill.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(193)))), ((int)(((byte)(74)))));
            this.vGridBill.Appearance.ExpandButton.ForeColor = System.Drawing.Color.White;
            this.vGridBill.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridBill.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridBill.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridBill.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridBill.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridBill.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridBill.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridBill.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(249)))), ((int)(((byte)(236)))));
            this.vGridBill.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridBill.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(114)))), ((int)(((byte)(50)))));
            this.vGridBill.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(114)))), ((int)(((byte)(50)))));
            this.vGridBill.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridBill.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridBill.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.vGridBill.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridBill.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(158)))), ((int)(((byte)(64)))));
            this.vGridBill.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.vGridBill.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridBill.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridBill.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.vGridBill.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridBill.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(247)))), ((int)(((byte)(230)))));
            this.vGridBill.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridBill.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridBill.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridBill.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.vGridBill.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.vGridBill.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.White;
            this.vGridBill.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridBill.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridBill.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridBill.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.vGridBill.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridBill.DataMember = "DTresData";
            this.vGridBill.DataSource = this.dsRecords4;
            this.vGridBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridBill.Location = new System.Drawing.Point(2, 20);
            this.vGridBill.Name = "vGridBill";
            this.vGridBill.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowbillID3,
            this.rowbilldate3,
            this.rowbillTotal3,
            this.rowbillPayed3,
            this.rowbillDue3});
            this.vGridBill.Size = new System.Drawing.Size(243, 98);
            this.vGridBill.TabIndex = 0;
            // 
            // dsRecords4
            // 
            this.dsRecords4.DataSetName = "DsRecords";
            this.dsRecords4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowbillID3
            // 
            this.rowbillID3.Name = "rowbillID3";
            this.rowbillID3.Properties.Caption = "Bill ID";
            this.rowbillID3.Properties.FieldName = "billID";
            // 
            // rowbilldate3
            // 
            this.rowbilldate3.Height = 18;
            this.rowbilldate3.Name = "rowbilldate3";
            this.rowbilldate3.Properties.Caption = "Bill Date";
            this.rowbilldate3.Properties.FieldName = "billdate";
            // 
            // rowbillTotal3
            // 
            this.rowbillTotal3.Name = "rowbillTotal3";
            this.rowbillTotal3.Properties.Caption = "Bill Total";
            this.rowbillTotal3.Properties.FieldName = "billTotal";
            // 
            // rowbillPayed3
            // 
            this.rowbillPayed3.Name = "rowbillPayed3";
            this.rowbillPayed3.Properties.Caption = "Paid";
            this.rowbillPayed3.Properties.FieldName = "billPayed";
            // 
            // rowbillDue3
            // 
            this.rowbillDue3.Name = "rowbillDue3";
            this.rowbillDue3.Properties.Caption = "Due Balance";
            this.rowbillDue3.Properties.FieldName = "billDue";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.vGridBill);
            this.groupControl4.Location = new System.Drawing.Point(267, 256);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(247, 120);
            this.groupControl4.TabIndex = 1;
            this.groupControl4.Text = "Bill Details";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(269, 382);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(245, 78);
            this.txtNote.TabIndex = 13;
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(538, 77);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(120, 40);
            this.simpleButton2.TabIndex = 17;
            this.simpleButton2.Text = "Pay";
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(538, 141);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(120, 40);
            this.simpleButton1.TabIndex = 16;
            this.simpleButton1.Text = "Cancel";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnOUT
            // 
            this.btnOUT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnOUT.ImageOptions.Image")));
            this.btnOUT.Location = new System.Drawing.Point(538, 12);
            this.btnOUT.Name = "btnOUT";
            this.btnOUT.Size = new System.Drawing.Size(120, 40);
            this.btnOUT.TabIndex = 15;
            this.btnOUT.Text = "Hire OUT";
            this.btnOUT.Click += new System.EventHandler(this.btnOUT_Click);
            // 
            // frmShowResRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 464);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnOUT);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmShowResRecord";
            this.Text = "Res Record";
            this.Load += new System.EventHandler(this.frmShowResRecord_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridRes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraVerticalGrid.VGridControl vGridCustomer;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraVerticalGrid.VGridControl vGridVehicle;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraVerticalGrid.VGridControl vGridRes;
        private DevExpress.XtraVerticalGrid.VGridControl vGridBill;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DsRecords dsRecords1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNIC;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAddress1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPic;
        private DsRecords dsRecords2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclNo1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclMake1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclType1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlModel1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlOdoReading1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlNextService1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclInsExpDate1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlLicExpDate1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowImage11;
        private DsRecords dsRecords3;
        private DsRecords dsRecords4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillID3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbilldate3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillTotal3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillDue3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillPayed3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresEnd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresPick;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresDrop;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresTrip;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresDecor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresHire;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowresTotal;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnOUT;
    }
}