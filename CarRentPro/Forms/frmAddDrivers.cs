﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.IO;

namespace CarRentPro.Forms
{
    public partial class frmAddDrivers : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand cmd;
        public frmAddDrivers()
        {
            InitializeComponent();
        }

        public Image ByteArray2Image(byte[] Barry)
        {
            MemoryStream ms = new MemoryStream(Barry);
            Image retImage = Image.FromStream(ms);
            return retImage;
        }


        public void LoadDrivers()
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                con.Open();
                try
                {
                    lstDrivers.Items.Clear();

                    string Sql = "SELECT  `DriverID` , `Name`,`NIC`,  `Address`, `Cantact`, `DrivingLICNO`,`LICExDate`, `RarePerDay`,`Pic` FROM .`tbldrivers`";
                    cmd = new MySqlCommand(Sql, con);
                    
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        ListViewItem lstItem = new ListViewItem(Convert.ToString(rdr[0]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[1]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[2]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[3]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[4]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[5]));
                        lstItem.SubItems.Add(DateTime.Parse(Convert.ToString(rdr[6])).ToShortDateString());
                        lstItem.SubItems.Add(Convert.ToString(rdr[7]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[8]));

                        lstDrivers.Items.Add(lstItem);
                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        public byte[] imageToByte(Image img)
        {
            using (var ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }
        
        public int GetLastID(string Item)
        {
            int lastID = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT " + Item + " FROM tbllastid";
                cmd = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    lastID = int.Parse(rdr[0].ToString());
                }
                cmd.Dispose();
            }
            return lastID + 1;
        }


        private void simpleButton1_Click(object sender, EventArgs e)
        {
             try
                 {

                   string NewID = string.Format("DRIV{0}", this.GetLastID("`tblDrivers`").ToString());
                   double Rate = double.Parse(txtRate.Text);
                   string Sqlstr = "INSERT INTO tbldrivers(DriverID, NIC, Name, Address, Cantact, DrivingLICNO, LICExDate, RarePerDay,Pic) VALUES('" + NewID + "','" + txtnic.Text + "', '" + txtFName.Text + "','" + txtAddress.Text + "', '" + txtcontant.Text + "','" + txtLICNo.Text + "', '" + dtpEpDateLIC.Value.Date.ToShortDateString() + "','" + Rate + "' ,'" + this.imageToByte(PicDriver.Image) + "')";//, @Pic
  
            
                  using (con = new MySqlConnection(AppSett.CS))
                   {
                      con.Open();
                
                       cmd = new MySqlCommand(Sqlstr, con);
                      cmd.ExecuteNonQuery();
                      cmd.Dispose();
                      con.Close();
                   }
                }
             catch (Exception ex)
                 {

                     MessageBox.Show(ex.Message);
                 }
          }

        

        private void frmAddDrivers_Load(object sender, EventArgs e)
        {
            txtID.Text = string.Format("DRI{0}", this.GetLastID("`tblDrivers`").ToString());
            LoadDrivers();

        }

        private void addAppointment_Click(object sender, EventArgs e)
        {
        
            string fileName;
            openFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            openFileDialog1.ShowDialog();
            string location = openFileDialog1.FileName;
            
                    PicDriver.EditValue = new Bitmap(openFileDialog1.FileName);
                    fileName = openFileDialog1.SafeFileName;
              
         
        }

        private void lstDrivers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstDrivers.SelectedItems.Count > 0)
            {    
                ListViewItem item = lstDrivers.SelectedItems[0];
                txtID.Text = item.SubItems[0].Text;
                txtFName.Text = item.SubItems[1].Text;
                txtnic.Text = item.SubItems[2].Text;
                txtAddress.Text = item.SubItems[3].Text;
                txtcontant.Text = item.SubItems[4].Text;
                txtLICNo.Text = item.SubItems[5].Text;
                dtpEpDateLIC.Text = item.SubItems[6].Text;
                txtRate.Text = item.SubItems[7].Text;

              /*  using (con = new MySqlConnection(AppSett.CS))
                {
                        con.Open();
                        cmd = new MySqlCommand();
                        cmd.Connection = con;
                        cmd.CommandText = "SELECT `Pic` FROM .`tbldrivers` WHERE `DriverID` = '" + item.SubItems[0].Text + "'";
                        MySqlDataReader rdr = cmd.ExecuteReader();
                        while (rdr.Read())
                        {
                            PicDriver.Image = this.ByteArray2Image((byte[])rdr[0]);
                        }
                        
                    }*/

            }
            else
            {
                //
            }
        }
    }
}