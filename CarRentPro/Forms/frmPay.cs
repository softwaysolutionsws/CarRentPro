﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRentPro.Forms
{
    public partial class frmPay : DevExpress.XtraEditors.XtraForm
    {
        public delegate void PayDoneEventHandler(object sender, PayDoneEventArgs e);
        public event PayDoneEventHandler PayDone;
        public frmPay()
        {
            InitializeComponent();
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            radMaster.Checked = true;
        }
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            radVisa.Checked = true;
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            radAmex.Checked = true;
        }
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            radCash.Checked = true;
        }
        private void frmPay_Load(object sender, EventArgs e)
        {
            lblGrndTot.Text = AppSett.FormatCurrency(SetPayEventArgs.TotAmount.ToString());
            lblDuePay.Text = AppSett.FormatCurrency(SetPayEventArgs.DuePayment.ToString());
            radCash.Checked = true;
            txtPayment.Text = "0.00";
        }
        private void radCash_CheckedChanged(object sender, EventArgs e)
        {
            if (radCash.Checked==true)
            {
                txtCardRef.Enabled = false;
                txtPayment.Focus();
            }
            else
            {
                txtCardRef.Enabled = true;
            }
        }
        private void btnPayNow_Click(object sender, EventArgs e)
        {
            string payMeth = "";
            string CrdName = "";
            string CrdRef = "";
            string chqDate = "";

            if (xtraTabControl1.SelectedTabPageIndex==0)
            {
                if (radCash.Checked)
                {
                    payMeth = "Cash";
                    CrdName = "None";
                    CrdRef = "None";
                    chqDate = "None";
                }

                if (radMaster.Checked || radVisa.Checked || radAmex.Checked || radEzCash.Checked || radMcash.Checked)
                {
                    payMeth = "Card";
                    CrdRef = txtCardRef.Text.Trim();
                    if (radMaster.Checked)
                    {
                        CrdName = "Master";
                    }
                    if (radVisa.Checked)
                    {
                        CrdName = "Visa";
                    }
                    if (radAmex.Checked)
                    {
                        CrdName = "American Ex";
                    }
                    if (radEzCash.Checked)
                    {
                        CrdName = "Ez-Cash";
                    }
                    if (radMcash.Checked)
                    {
                        CrdName = "m-Cash";
                    }
                }
            }
            
            if (xtraTabControl1.SelectedTabPageIndex==1)
            {
                payMeth = "Cheque";
                CrdName = txtBank.Text.Trim();
                CrdRef = txtCardRef.Text.Trim();
                chqDate = dtpChqDate.Value.ToShortDateString();
                MessageBox.Show("Test");
                return;
            }
            double Payment = double.Parse(txtPayment.Text.Trim());

            PayDoneEventArgs args = new PayDoneEventArgs(payMeth, CrdName, CrdRef, Payment, DateTime.Now, chqDate, double.Parse(lblDueBal.Text));

            PayDone(this, args);
            this.Dispose();
        }
        private void txtPayment_TextChanged(object sender, EventArgs e)
        {
            double payment = 0;
            if (double.TryParse(txtPayment.Text.Trim(),out payment))
            {
                lblDueBal.Text = AppSett.FormatCurrency((double.Parse(lblDuePay.Text) - payment).ToString());
            }
            else
            {
                lblDueBal.Text = "0.00";
            }
        }
        private void txtPayment_Click(object sender, EventArgs e)
        {
            txtPayment.SelectAll();
        }
        private void pictureBox5_Click(object sender, EventArgs e)
        {
            radMcash.Checked = true;
        }
        private void pictureBox6_Click(object sender, EventArgs e)
        {
            radEzCash.Checked = true;
        }
    }
}
