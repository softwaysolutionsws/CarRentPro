﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;

namespace CarRentPro.Forms
{
    public partial class frmShowBkRecord : DevExpress.XtraEditors.XtraForm
    {
        int apID;
        string bilID;
        string bkID;
        MySqlConnection con;
        MySqlCommand comm;


        public frmShowBkRecord(int apId)
        {
            InitializeComponent();
            apID = apId;
        }
        private void frmShowRecord_Load(object sender, EventArgs e)
        {
            this.dTbkDataTableAdapter.Fill(this.dsRecords1.DTbkData, apID);
            this.dTbkDataTableAdapter.Fill(this.dsRecords2.DTbkData, apID);
            this.dTbkDataTableAdapter.Fill(this.dsRecords3.DTbkData, apID);
            this.dTbkDataTableAdapter.Fill(this.dsRecords4.DTbkData, apID);
            txtNote.Text = this.dsRecords1.DTbkData[0].bkNote.ToString();

            bilID = vGridPaymentdlt.Rows[0].Properties.Value.ToString();
            bkID = vGridBooking.Rows[0].Properties.Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void btnOUT_Click(object sender, EventArgs e)
        {
            frmCheckOut frm = new frmCheckOut(true, bilID, bkID, apID);
            frm.ShowDialog();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete this Appiontment","COnfirm",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = "DELETE FROM `appointments` WHERE `UniqeID`='" + apID + "'";
                        comm.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    this.Dispose();
                }

                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = @"UPDATE `tblbooking` 
                                                        SET 
                                                            bkStatus=@bkStatus
                                                        WHERE
                                                            `bkID`=@bkID";

                        comm.Parameters.AddWithValue("@bkStatus", "Canceled");
                        comm.Parameters.AddWithValue("@bkID", bkID);

                        comm.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            else
            {
                return;
            }
        }
    }
}