﻿namespace CarRentPro.Forms
{
    partial class frmPay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPay));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tbCard = new DevExpress.XtraTab.XtraTabPage();
            this.radEzCash = new System.Windows.Forms.RadioButton();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.radMcash = new System.Windows.Forms.RadioButton();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.radCash = new System.Windows.Forms.RadioButton();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtCardRef = new DevExpress.XtraEditors.TextEdit();
            this.radAmex = new System.Windows.Forms.RadioButton();
            this.radVisa = new System.Windows.Forms.RadioButton();
            this.radMaster = new System.Windows.Forms.RadioButton();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbCheque = new DevExpress.XtraTab.XtraTabPage();
            this.txtBank = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.dtpChqDate = new System.Windows.Forms.DateTimePicker();
            this.txtChqAmount = new DevExpress.XtraEditors.TextEdit();
            this.txtChqNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtPayment = new DevExpress.XtraEditors.TextEdit();
            this.btnPayNow = new DevExpress.XtraEditors.SimpleButton();
            this.lblDuePay = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lblDueBal = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lblGrndTot = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tbCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardRef.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tbCheque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChqAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChqNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayment.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tbCard;
            this.xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl1.Size = new System.Drawing.Size(303, 269);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbCard,
            this.tbCheque});
            // 
            // tbCard
            // 
            this.tbCard.Controls.Add(this.radEzCash);
            this.tbCard.Controls.Add(this.pictureBox6);
            this.tbCard.Controls.Add(this.radMcash);
            this.tbCard.Controls.Add(this.pictureBox5);
            this.tbCard.Controls.Add(this.radCash);
            this.tbCard.Controls.Add(this.pictureBox4);
            this.tbCard.Controls.Add(this.labelControl15);
            this.tbCard.Controls.Add(this.txtCardRef);
            this.tbCard.Controls.Add(this.radAmex);
            this.tbCard.Controls.Add(this.radVisa);
            this.tbCard.Controls.Add(this.radMaster);
            this.tbCard.Controls.Add(this.pictureBox3);
            this.tbCard.Controls.Add(this.pictureBox2);
            this.tbCard.Controls.Add(this.pictureBox1);
            this.tbCard.Image = ((System.Drawing.Image)(resources.GetObject("tbCard.Image")));
            this.tbCard.Name = "tbCard";
            this.tbCard.Size = new System.Drawing.Size(297, 222);
            this.tbCard.Text = "Card";
            // 
            // radEzCash
            // 
            this.radEzCash.AutoSize = true;
            this.radEzCash.Location = new System.Drawing.Point(135, 130);
            this.radEzCash.Name = "radEzCash";
            this.radEzCash.Size = new System.Drawing.Size(14, 13);
            this.radEzCash.TabIndex = 46;
            this.radEzCash.TabStop = true;
            this.radEzCash.UseVisualStyleBackColor = true;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Image = global::CarRentPro.Properties.Resources.ezcash;
            this.pictureBox6.Location = new System.Drawing.Point(102, 82);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(72, 42);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 45;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // radMcash
            // 
            this.radMcash.AutoSize = true;
            this.radMcash.Location = new System.Drawing.Point(51, 130);
            this.radMcash.Name = "radMcash";
            this.radMcash.Size = new System.Drawing.Size(14, 13);
            this.radMcash.TabIndex = 44;
            this.radMcash.TabStop = true;
            this.radMcash.UseVisualStyleBackColor = true;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Image = global::CarRentPro.Properties.Resources.mcash;
            this.pictureBox5.Location = new System.Drawing.Point(18, 82);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(72, 42);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 43;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // radCash
            // 
            this.radCash.AutoSize = true;
            this.radCash.Location = new System.Drawing.Point(216, 130);
            this.radCash.Name = "radCash";
            this.radCash.Size = new System.Drawing.Size(14, 13);
            this.radCash.TabIndex = 42;
            this.radCash.TabStop = true;
            this.radCash.UseVisualStyleBackColor = true;
            this.radCash.CheckedChanged += new System.EventHandler(this.radCash_CheckedChanged);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Image = global::CarRentPro.Properties.Resources.cash;
            this.pictureBox4.Location = new System.Drawing.Point(186, 82);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(72, 42);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 41;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(21, 174);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(128, 26);
            this.labelControl15.TabIndex = 40;
            this.labelControl15.Text = "Card Reference";
            // 
            // txtCardRef
            // 
            this.txtCardRef.Location = new System.Drawing.Point(167, 176);
            this.txtCardRef.Name = "txtCardRef";
            this.txtCardRef.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCardRef.Properties.Appearance.Options.UseFont = true;
            this.txtCardRef.Size = new System.Drawing.Size(97, 26);
            this.txtCardRef.TabIndex = 39;
            // 
            // radAmex
            // 
            this.radAmex.AutoSize = true;
            this.radAmex.Location = new System.Drawing.Point(219, 63);
            this.radAmex.Name = "radAmex";
            this.radAmex.Size = new System.Drawing.Size(14, 13);
            this.radAmex.TabIndex = 38;
            this.radAmex.TabStop = true;
            this.radAmex.UseVisualStyleBackColor = true;
            // 
            // radVisa
            // 
            this.radVisa.AutoSize = true;
            this.radVisa.Location = new System.Drawing.Point(134, 63);
            this.radVisa.Name = "radVisa";
            this.radVisa.Size = new System.Drawing.Size(14, 13);
            this.radVisa.TabIndex = 37;
            this.radVisa.TabStop = true;
            this.radVisa.UseVisualStyleBackColor = true;
            // 
            // radMaster
            // 
            this.radMaster.AutoSize = true;
            this.radMaster.Location = new System.Drawing.Point(49, 63);
            this.radMaster.Name = "radMaster";
            this.radMaster.Size = new System.Drawing.Size(14, 13);
            this.radMaster.TabIndex = 36;
            this.radMaster.TabStop = true;
            this.radMaster.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::CarRentPro.Properties.Resources.AMEX;
            this.pictureBox3.Location = new System.Drawing.Point(185, 15);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(72, 42);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 35;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::CarRentPro.Properties.Resources.visa;
            this.pictureBox2.Location = new System.Drawing.Point(102, 15);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(72, 42);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 34;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::CarRentPro.Properties.Resources.master_card;
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(72, 42);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // tbCheque
            // 
            this.tbCheque.Controls.Add(this.txtBank);
            this.tbCheque.Controls.Add(this.labelControl6);
            this.tbCheque.Controls.Add(this.dtpChqDate);
            this.tbCheque.Controls.Add(this.txtChqAmount);
            this.tbCheque.Controls.Add(this.txtChqNo);
            this.tbCheque.Controls.Add(this.labelControl5);
            this.tbCheque.Controls.Add(this.labelControl3);
            this.tbCheque.Controls.Add(this.labelControl2);
            this.tbCheque.Image = ((System.Drawing.Image)(resources.GetObject("tbCheque.Image")));
            this.tbCheque.Name = "tbCheque";
            this.tbCheque.Size = new System.Drawing.Size(297, 222);
            this.tbCheque.Text = "Cheque";
            // 
            // txtBank
            // 
            this.txtBank.Location = new System.Drawing.Point(120, 25);
            this.txtBank.Name = "txtBank";
            this.txtBank.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBank.Properties.Appearance.Options.UseFont = true;
            this.txtBank.Properties.Appearance.Options.UseTextOptions = true;
            this.txtBank.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtBank.Size = new System.Drawing.Size(162, 24);
            this.txtBank.TabIndex = 44;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(11, 22);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(42, 26);
            this.labelControl6.TabIndex = 45;
            this.labelControl6.Text = "Bank";
            // 
            // dtpChqDate
            // 
            this.dtpChqDate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpChqDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpChqDate.Location = new System.Drawing.Point(120, 127);
            this.dtpChqDate.Name = "dtpChqDate";
            this.dtpChqDate.Size = new System.Drawing.Size(162, 27);
            this.dtpChqDate.TabIndex = 43;
            // 
            // txtChqAmount
            // 
            this.txtChqAmount.Location = new System.Drawing.Point(120, 93);
            this.txtChqAmount.Name = "txtChqAmount";
            this.txtChqAmount.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChqAmount.Properties.Appearance.Options.UseFont = true;
            this.txtChqAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtChqAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtChqAmount.Size = new System.Drawing.Size(162, 24);
            this.txtChqAmount.TabIndex = 42;
            // 
            // txtChqNo
            // 
            this.txtChqNo.Location = new System.Drawing.Point(120, 61);
            this.txtChqNo.Name = "txtChqNo";
            this.txtChqNo.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChqNo.Properties.Appearance.Options.UseFont = true;
            this.txtChqNo.Properties.Appearance.Options.UseTextOptions = true;
            this.txtChqNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtChqNo.Size = new System.Drawing.Size(162, 24);
            this.txtChqNo.TabIndex = 39;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(11, 122);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(40, 26);
            this.labelControl5.TabIndex = 41;
            this.labelControl5.Text = "Date";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(11, 90);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(69, 26);
            this.labelControl3.TabIndex = 40;
            this.labelControl3.Text = "Amount";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(11, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(94, 26);
            this.labelControl2.TabIndex = 39;
            this.labelControl2.Text = "Cheque No";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(19, 339);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(76, 26);
            this.labelControl12.TabIndex = 27;
            this.labelControl12.Text = "Payment";
            // 
            // txtPayment
            // 
            this.txtPayment.Location = new System.Drawing.Point(186, 336);
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPayment.Properties.Appearance.Options.UseFont = true;
            this.txtPayment.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPayment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtPayment.Size = new System.Drawing.Size(97, 32);
            this.txtPayment.TabIndex = 25;
            this.txtPayment.TextChanged += new System.EventHandler(this.txtPayment_TextChanged);
            this.txtPayment.Click += new System.EventHandler(this.txtPayment_Click);
            // 
            // btnPayNow
            // 
            this.btnPayNow.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnPayNow.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPayNow.ImageOptions.Image")));
            this.btnPayNow.Location = new System.Drawing.Point(64, 427);
            this.btnPayNow.Name = "btnPayNow";
            this.btnPayNow.Size = new System.Drawing.Size(101, 44);
            this.btnPayNow.TabIndex = 32;
            this.btnPayNow.Text = "Pay Now";
            this.btnPayNow.Click += new System.EventHandler(this.btnPayNow_Click);
            // 
            // lblDuePay
            // 
            this.lblDuePay.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuePay.Appearance.Options.UseFont = true;
            this.lblDuePay.Appearance.Options.UseTextOptions = true;
            this.lblDuePay.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDuePay.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDuePay.Location = new System.Drawing.Point(171, 304);
            this.lblDuePay.Name = "lblDuePay";
            this.lblDuePay.Size = new System.Drawing.Size(112, 19);
            this.lblDuePay.TabIndex = 38;
            this.lblDuePay.Text = "0.00";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(19, 307);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(115, 26);
            this.labelControl7.TabIndex = 37;
            this.labelControl7.Text = "Due Payment";
            // 
            // lblDueBal
            // 
            this.lblDueBal.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDueBal.Appearance.Options.UseFont = true;
            this.lblDueBal.Appearance.Options.UseTextOptions = true;
            this.lblDueBal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDueBal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDueBal.Location = new System.Drawing.Point(171, 374);
            this.lblDueBal.Name = "lblDueBal";
            this.lblDueBal.Size = new System.Drawing.Size(112, 19);
            this.lblDueBal.TabIndex = 36;
            this.lblDueBal.Text = "0.00";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(19, 374);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(105, 26);
            this.labelControl4.TabIndex = 35;
            this.labelControl4.Text = "Due Balance";
            // 
            // lblGrndTot
            // 
            this.lblGrndTot.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrndTot.Appearance.Options.UseFont = true;
            this.lblGrndTot.Appearance.Options.UseTextOptions = true;
            this.lblGrndTot.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblGrndTot.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGrndTot.Location = new System.Drawing.Point(171, 279);
            this.lblGrndTot.Name = "lblGrndTot";
            this.lblGrndTot.Size = new System.Drawing.Size(112, 19);
            this.lblGrndTot.TabIndex = 34;
            this.lblGrndTot.Text = "0.00";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(19, 279);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(117, 26);
            this.labelControl1.TabIndex = 33;
            this.labelControl1.Text = "Total Amount";
            // 
            // frmPay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 492);
            this.Controls.Add(this.lblDuePay);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.lblDueBal);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.lblGrndTot);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.btnPayNow);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.txtPayment);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmPay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "pay";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmPay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tbCard.ResumeLayout(false);
            this.tbCard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardRef.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tbCheque.ResumeLayout(false);
            this.tbCheque.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChqAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChqNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayment.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tbCard;
        private DevExpress.XtraTab.XtraTabPage tbCheque;
        private DevExpress.XtraEditors.SimpleButton btnPayNow;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtPayment;
        private System.Windows.Forms.RadioButton radAmex;
        private System.Windows.Forms.RadioButton radVisa;
        private System.Windows.Forms.RadioButton radMaster;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtCardRef;
        private System.Windows.Forms.RadioButton radCash;
        private System.Windows.Forms.PictureBox pictureBox4;
        private DevExpress.XtraEditors.LabelControl lblDuePay;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lblDueBal;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lblGrndTot;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.RadioButton radMcash;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.RadioButton radEzCash;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.DateTimePicker dtpChqDate;
        private DevExpress.XtraEditors.TextEdit txtChqAmount;
        private DevExpress.XtraEditors.TextEdit txtChqNo;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtBank;
        private DevExpress.XtraEditors.LabelControl labelControl6;


    }
}