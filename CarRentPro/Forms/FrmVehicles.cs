﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.IO;

namespace CarRentPro.Forms
{
    public partial class FrmVehicles : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand comm;
        int picID;

        public void loadVhclDetails(string VhclNo)
        {
            #region Load Text Boxes
            this.ClrVhcl();
            string slctID = VhclNo;
            imageSlider1.Images.Clear();

                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = "SELECT `vhclsupID`,`vhclNo`,`vhclMake`,`vhclType`,`vchlModel`,`vhclDelDate`,`vchlOdoReading`,`vchlNextService`,`vhclInsNo`,`vhclInsExpDate`,`vhclLicNo`,`vchlLicExpDate`,`vhclMadeYear`,`vchlId`,`Image1`, `Image2`, `Image3`,`Image4`,`vhclKmLimit`,`vhclColour`,`vhclDayRate`,`vhclExChargeKm`,`vhclHireKm`,`vhclNightCrge` FROM `tblvehicle` WHERE `vhclNo` = '" + slctID + "'";
                        MySqlDataReader rdr = comm.ExecuteReader();
                        while (rdr.Read())
                        {
                            lblVhclID.Text = rdr[13].ToString();
                            txtVhclNo.Text = rdr[1].ToString();
                            txtVhclMake.Text = rdr[2].ToString();
                            txtVhclType.Text = rdr[3].ToString();
                            txtVhclModel.Text = rdr[4].ToString();
                            dtpDelDate.Text = rdr[5].ToString();
                            imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[14]));
                            imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[15]));
                            imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[16]));
                            imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[17]));
                            txtVhclCurOdo.Text = rdr[6].ToString();
                            txtVhclNxtSrvice.Text = rdr[7].ToString();
                            txtVhclInsNo.Text = rdr[8].ToString();
                            dtpInsExpDate.Text = rdr[9].ToString();
                            txtVhclLicNo.Text = rdr[10].ToString();
                            dtpLicExpDate.Text = rdr[11].ToString();
                            txtVhclMYeay.Text = rdr[12].ToString();
                            txtDayLimit.Text = rdr[18].ToString();
                            txtVhclColour.Text = rdr[19].ToString();
                            txtPDayRate.Text = rdr[20].ToString();
                            txtExtraChergePerKm.Text = rdr[21].ToString();
                            txtHirePerKm.Text = rdr[22].ToString();
                            txtNightCharge.Text = rdr[23].ToString();

                        } 

                        
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.ToString());
                    }
                }
            
            
            #endregion
        }
        public void clrRates()
        {
            txtPDayRate.Text = "";
            txtNightCharge.Text="";
            txtHirePerKm.Text="";
            txtDayLimit.Text = "";
            txtExtraChergePerKm.Text = "";
        }
        private void LoadPic(int num)
        {
            string fileName;
            openFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            openFileDialog1.ShowDialog();
            string location = openFileDialog1.FileName;
            switch (num)
            {
                case 1:

                    pictureEdit1.EditValue = new Bitmap(openFileDialog1.FileName);
                    textEdit1.Text = location;
                    fileName = openFileDialog1.SafeFileName;
                    break;
                case 2:
                    pictureEdit2.EditValue = new Bitmap(openFileDialog1.FileName);
                    textEdit2.Text = location;
                    fileName = openFileDialog1.SafeFileName;

                    break;
                case 3:
                    pictureEdit3.EditValue = new Bitmap(openFileDialog1.FileName);
                    textEdit3.Text = location;
                    fileName = openFileDialog1.SafeFileName;

                    break;
                case 4:
                    pictureEdit4.EditValue = new Bitmap(openFileDialog1.FileName);
                    textEdit4.Text = location;
                    fileName = openFileDialog1.SafeFileName;

                    break;
            }


        }
        public bool rateConfigAvailabile()
        {
            int count = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `rtID` FROM `carrentpro`.`tblrates` WHERE (`rtVhclID`=@rtVhclID AND `rtType`=@rtType AND `rtDistance`=@rtDistance)";
                    comm.Parameters.AddWithValue("@rtVhclID", lblVhclID.Text.ToString());
                    //comm.Parameters.AddWithValue("@rtType", cmbVhclRateType.Text.Trim());
                    //comm.Parameters.AddWithValue("@rtDistance", cmbVhclRateDLimit.Text.Trim());
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        count++;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }

                if (count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public int CountVhcl4Sup(string SupNIC)
        {
            int count = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `vhclNo` FROM `tblvehicle` WHERE `vhclsupID` = '" + SupNIC + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        count++;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }

                return count;
            }
        }
        public int CountSup(string spNic)
        {
            int count = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `spID`,`spName`,`spNIC`,`spAddress1`,`spAddress2`,`spTel` FROM `tblsupplier` WHERE `spNIC` = '" + spNic + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        count++;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }

                return count;
            }
        }
        public void loadSupDetails(string spNic)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `spID`,`spName`,`spNIC`,`spAddress1`,`spAddress2`,`spTel` FROM `tblsupplier` WHERE `spNIC` = '" + spNic + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        txtName.Text = rdr[1].ToString();
                        txtNIC.Text = rdr[2].ToString();
                        txtAdress1.Text = rdr[3].ToString();
                        txtAdress2.Text = rdr[4].ToString();
                        txtContact.Text = rdr[5].ToString();
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }

            }
        }
        public void loadVhclList()
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    lstVhclFull.Items.Clear();
                    string Sql = "SELECT `vhclNo`,`vhclMake`,`vchlModel`,`vhclDelDate` FROM `tblvehicle`";
                    comm = new MySqlCommand(Sql, con);
                    con.Open();
                    MySqlDataReader rdr = comm.ExecuteReader();

                    while (rdr.Read())
                    {
                        ListViewItem lstItem = new ListViewItem(Convert.ToString(rdr[0]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[1]));
                        lstItem.SubItems.Add(Convert.ToString(rdr[2]));
                        lstItem.SubItems.Add(DateTime.Parse(Convert.ToString(rdr[3])).ToShortDateString());
                        lstVhclFull.Items.Add(lstItem);
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        public void ClrSup()
        {
            txtName.Text = "";
            txtNIC.Text = "";
            txtAdress1.Text = "";
            txtAdress2.Text = "";
            txtContact.Text = "";
        }
        public void ClrVhcl()
        {
            lblVhclID.Text = "";
            txtVhclNo.Text = "";
            txtVhclMake.Text = "";
            txtVhclType.Text = "";
            txtVhclModel.Text = "";
            dtpDelDate.Text = "";
            imageSlider1.Images.Clear();
            txtVhclMYeay.Text = "";
            txtVhclColour.Text = "";
        }
        public void clrAdditional()
        {
            txtVhclCurOdo.Text = "";
            txtVhclNxtSrvice.Text = "";
            txtVhclInsNo.Text = "";
            dtpInsExpDate.Text = "";
            txtVhclLicNo.Text = "";
            dtpLicExpDate.Text = "";
        }
        public FrmVehicles()
        {
            InitializeComponent();
        }
        
               
        public byte[] imageToByte(Image img)
        {
            using (var ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }
        public Image ByteArray2Image(byte[] Barry)
        {
            MemoryStream ms = new MemoryStream(Barry);
            Image retImage = Image.FromStream(ms);
            return retImage;
        }
        public int GetLastID(string Item)
        {
            int lastID = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT " + Item + " FROM tbllastid";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    lastID = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return lastID + 1;
        }
        public string GetSupID(string NIC)
        {
            string SpID = "";

            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT `spID` FROM `tblsupplier` WHERE `spNIC`='" + NIC + "'";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    SpID = rdr[0].ToString();
                }

                comm.Dispose();
            }
            return SpID;
        }
        public void UpdateID(string Item, int NewID)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                int intnewid = NewID;
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tbllastid` SET " + Item.Trim() + "='" + intnewid + "' WHERE clmnID=1";
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                comm.Dispose();
            }
        }
        private void btnSupSave_Click(object sender, EventArgs e)
        {
            string NewID = string.Format("SUP{0}", this.GetLastID("`tblsup`").ToString());
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    string Qry = "INSERT INTO `tblsupplier` (`spID`,`spNIC`,`spName`,`spAddress1`,`spAddress2`,`spTel`) VALUES ('" + NewID + "','" + txtNIC.Text.Trim() + "', '" + txtName.Text.Trim() + "', '" + txtAdress1.Text.Trim() + "', '" + txtAdress2.Text.Trim() + "', '" + txtContact.Text.Trim() + "')";
                    comm = new MySqlCommand(Qry, con);
                    con.Open();
                    comm.ExecuteNonQuery();
                    this.UpdateID("`tblsup`", this.GetLastID("`tblsup`"));
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
            this.loadSupDetails(txtNIC.Text.Trim());
            this.clrRates();
        }
        private void btnVhclSave_Click(object sender, EventArgs e)
        {
            #region Veh Save
            string SqlComm = @"INSERT INTO tblvehicle (`vchlId`,`vhclsupID`,`vhclNo`,`vhclMake`,`vhclType`,`vchlModel`,`vhclMadeYear`,`vhclColour`,`vhclDelDate`,`Image1`,`Image2`,`Image3`,`Image4`,`resID`)
                                               VALUES (@vchlId,@vhclsupID,@vhclNo,@vhclMake,@vhclType,@vchlModel,@vhclMadeYear,@vhclColour,@vhclDelDate,@Image1,@Image2,@Image3,@Image4,@resID)";
            string NewID = string.Format("VHCL{0}", this.GetLastID("`tblvhcl`").ToString());
            string SupID = "Company Own";
            int NewresID = this.GetLastID("`tblresources`");
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    comm = new MySqlCommand(SqlComm, con);
                    comm.Parameters.AddWithValue("@vchlId", NewID);
                    comm.Parameters.AddWithValue("@vhclsupID", SupID);
                    comm.Parameters.AddWithValue("@vhclNo", txtVhclNo.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclMake", txtVhclMake.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclType", txtVhclType.Text.Trim());
                    comm.Parameters.AddWithValue("@vchlModel", txtVhclModel.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclMadeYear", txtVhclMYeay.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclColour", txtVhclColour.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclDelDate", dtpDelDate.Value.Date.ToShortDateString());
                    comm.Parameters.AddWithValue("@Image1", this.imageToByte(pictureEdit1.Image));
                    comm.Parameters.AddWithValue("@Image2", this.imageToByte(pictureEdit2.Image));
                    comm.Parameters.AddWithValue("@Image3", this.imageToByte(pictureEdit3.Image));
                    comm.Parameters.AddWithValue("@Image4", this.imageToByte(pictureEdit4.Image));
                    comm.Parameters.AddWithValue("@resID", NewresID);
                    con.Open();
                    comm.ExecuteNonQuery();
                    con.Close();
                    this.UpdateID("`tblvhcl`", this.GetLastID("`tblvhcl`"));
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            #endregion

                #region toRes
                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        string Qry = "INSERT INTO `resources` (`Description`,`CustomField1`,`Id`) VALUES ('" + txtVhclNo.Text.Trim() + "','" + txtVhclModel.Text + "','" + NewresID + "')";
                        comm = new MySqlCommand(Qry, con);
                        con.Open();
                        comm.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }

                }
                this.UpdateID("`tblresources`", this.GetLastID("`tblresources`"));

                #endregion

                this.loadVhclList();
                this.clrRates();
                this.clrAdditional();
                this.ClrVhcl();
            }
        }
        private void txtNIC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.CountSup(txtNIC.Text.Trim()) > 0)
                {
                    this.loadSupDetails(txtNIC.Text.Trim());
                }

                else
                {
                    txtName.Focus();
                }
                this.clrRates();

            }
        }
        private void lstVhcl_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        private void FrmVehicles_Load(object sender, EventArgs e)
        {
            this.loadVhclList();

           // imageSlider1.Images.Add(Image.FromFile((String)row["img_path"]));
        }
        private void lstVhclFull_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstVhclFull.SelectedItems.Count == 1)
            {
                #region Load Text Boxes
                string slctID = lstVhclFull.SelectedItems[0].Text;
                this.loadVhclDetails(slctID);
                #endregion
                this.clrRates();
            }
        }
        private void btnSupUpdate_Click(object sender, EventArgs e)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE tblsupplier SET spName=@spName,spNIC=@spNIC,spAddress1=@spAddress1,spAddress2=@spAddress2,spTel=@spTel WHERE spID=@spID";
                    comm.Parameters.AddWithValue("@spName", txtName.Text.Trim());
                    comm.Parameters.AddWithValue("@spNIC", txtNIC.Text.Trim());
                    comm.Parameters.AddWithValue("@spAddress1", txtAdress1.Text.Trim());
                    comm.Parameters.AddWithValue("@spAddress2", txtAdress2.Text.Trim());
                    comm.Parameters.AddWithValue("@spTel", txtContact.Text.Trim());
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                this.clrRates();
            }
        }
        private void btnUpdateVhcl_Click(object sender, EventArgs e)
        {
            string SupID = "Company Own";
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = @"UPDATE `tblvehicle`
                                                        SET 
                                                            `vhclsupID`=@vhclsupID,
                                                            `vhclNo`=@vhclNo,
                                                            `vhclMake`=@vhclMake,
                                                            `vhclType`=@vhclType,
                                                            `vchlModel`=@vchlModel,
                                                            `vhclMadeYear`=@vhclMadeYear,
                                                            `vhclColour`=@vhclColour,
                                                            `vhclDelDate`=@vhclDelDate,
                                                            `Image1`=@Image1,
                                                            `Image2`=@Image2,
                                                            `Image3`=@Image3,
                                                            `Image4`=@Image4
                                                         WHERE 
                                                            `vchlId`=@vchlId";
                    comm.Parameters.AddWithValue("@vhclsupID", SupID);
                    comm.Parameters.AddWithValue("@vhclNo", txtVhclNo.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclMake", txtVhclMake.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclType", txtVhclType.Text.Trim());
                    comm.Parameters.AddWithValue("@vchlModel", txtVhclModel.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclMadeYear", txtVhclMYeay.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclColour", txtVhclColour.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclDelDate", dtpDelDate.Value.Date.ToShortDateString());
                    comm.Parameters.AddWithValue("@Image1", this.imageToByte(pictureEdit1.Image));
                    comm.Parameters.AddWithValue("@Image2", this.imageToByte(pictureEdit2.Image));
                    comm.Parameters.AddWithValue("@Image3", this.imageToByte(pictureEdit3.Image));
                    comm.Parameters.AddWithValue("@Image4", this.imageToByte(pictureEdit4.Image));

                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                this.loadVhclList();

                #region LoadSupVcl
               
                #endregion
                this.clrRates();
            }
        }
        private void btnDelVhcl_Click(object sender, EventArgs e)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "DELETE FROM `tblvehicle` WHERE `vchlId`='" + lblVhclID.Text.Trim() + "'";
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            this.loadVhclList();
            this.ClrVhcl();
            this.clrRates();
        }
        private void cmbVhclRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        private void btnRateSave_Click(object sender, EventArgs e)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = @"UPDATE `tblvehicle` 
                                                        SET 
                                                            `vchlOdoReading`=@vchlOdoReading,
                                                            `vchlNextService`=@vchlNextService,
                                                            `vhclInsNo`=@vhclInsNo,
                                                            `vhclInsExpDate`=@vhclInsExpDate,
                                                            `vhclLicNo`=@vhclLicNo,
                                                            `vchlLicExpDate`=@vchlLicExpDate
                                                        WHERE
                                                            `vchlId`=@vchlId";

                    comm.Parameters.AddWithValue("@vchlOdoReading", int.Parse(txtVhclCurOdo.Text.Trim()));
                    comm.Parameters.AddWithValue("@vchlNextService", int.Parse(txtExtraChergePerKm.Text.Trim()));
                    comm.Parameters.AddWithValue("@vhclInsNo", txtVhclInsNo.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclInsExpDate",DateTime.Parse(dtpInsExpDate.Value.ToShortDateString()));
                    comm.Parameters.AddWithValue("@vhclLicNo", txtDayLimit.Text.Trim());
                    comm.Parameters.AddWithValue("@vchlLicExpDate", DateTime.Parse(dtpLicExpDate.Value.ToShortDateString()));

                    comm.Parameters.AddWithValue("@vchlId", lblVhclID.Text.Trim());

                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }
        private void cmbVhclRateDLimit_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        private void txtVhclNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.loadVhclDetails(txtVhclNo.Text.Trim());
                #region loadSupText
                string spID = "";
                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        string str = "SELECT `vhclsupID` FROM `tblvehicle` WHERE `vchlId` = '" + lblVhclID.Text + "'";
                        comm = new MySqlCommand(str, con);
                        con.Open();
                        MySqlDataReader rdr = comm.ExecuteReader();

                        while (rdr.Read())
                        {
                            spID = rdr[0].ToString();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                this.ClrSup();
                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = "SELECT `spID`,`spName`,`spNIC`,`spAddress1`,`spAddress2`,`spTel` FROM `tblsupplier` WHERE `spID` = '" + spID + "'";
                        MySqlDataReader rdr = comm.ExecuteReader();
                        while (rdr.Read())
                        {
                            txtName.Text = rdr[1].ToString();
                            txtNIC.Text = rdr[2].ToString();
                            txtAdress1.Text = rdr[3].ToString();
                            txtAdress2.Text = rdr[4].ToString();
                            txtContact.Text = rdr[5].ToString();
                        }
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.ToString());
                    }
                }

                #endregion

                #region loadSupVeh
                #endregion
                this.clrRates();
            }
        }
        private void btnVhclClear_Click(object sender, EventArgs e)
        {
            this.ClrVhcl();
            this.clrRates();
            this.clrAdditional();

        }

        private void btnAddMtc_Click(object sender, EventArgs e)
        {
            frmVehicleMaintenance frm = new frmVehicleMaintenance();
            frm.Show();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            LoadPic(1);

        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            LoadPic(2);
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            LoadPic(3);
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            LoadPic(4);
        }

        private void txtVhclCurOdo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                
            }
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            using (con=new MySqlConnection(AppSett.CS))
            {
                con.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT `Image1`, `Image2`, `Image3`, `Image4` FROM `tblvehicle` WHERE `vhclNo`='AA-1111'");

                cmd.Connection = con;
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    imageSlider1.Images.Add( this.ByteArray2Image((byte[])rdr[0]));
                    imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[1]));
                    imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[2]));
                    imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[3]));
                }
            }
        }

        

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tblvehicle` SET `vhclDayRate`=@vhclDayRate ,`vhclKmLimit`=@vhclKmLimit,`vhclHireKm`=@vhclHireKm,`vhclNightCrge`=@vhclNightCrge,`vhclExChrgKm`=@vhclExChrgKm WHERE `vchlId`=@vchlId";
                    comm.Parameters.AddWithValue("@vhclDayRate", txtPDayRate.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclExChargeKm", txtExtraChergePerKm.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclHireKm", txtHirePerKm.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclNightCrge", txtNightCharge.Text.Trim());
                    comm.Parameters.AddWithValue("@vhclKmLimit",txtDayLimit.Text.Trim());
                    
                    comm.Parameters.AddWithValue("@vchlId", lblVhclID.Text.Trim());

                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                this.loadVhclList();

                #region LoadSupVcl

                #endregion
                this.clrRates();
            }
        }
    }
}