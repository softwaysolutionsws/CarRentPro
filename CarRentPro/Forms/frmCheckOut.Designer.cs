﻿namespace CarRentPro.Forms
{
    partial class frmCheckOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheckOut));
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel12 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel13 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel14 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel15 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel16 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel17 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel18 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel19 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel20 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel21 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel22 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            this.lblPaid = new DevExpress.XtraEditors.LabelControl();
            this.lblDue = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.lblGrandTotal = new DevExpress.XtraEditors.LabelControl();
            this.btnProceed = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.tabType = new DevExpress.XtraTab.XtraTabControl();
            this.tabRent = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.txtExKmCharge = new DevExpress.XtraEditors.TextEdit();
            this.lblDriverCharge = new DevExpress.XtraEditors.LabelControl();
            this.lblRent = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txtDisLimit = new DevExpress.XtraEditors.TextEdit();
            this.cmbDriverList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtDays = new DevExpress.XtraEditors.TextEdit();
            this.chkDriver = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.txtHrs = new DevExpress.XtraEditors.TextEdit();
            this.txtDrate = new DevExpress.XtraEditors.TextEdit();
            this.txtDayRate = new DevExpress.XtraEditors.TextEdit();
            this.tabHire = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.cmbHireDriver = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblHire = new DevExpress.XtraEditors.LabelControl();
            this.txtHireCharge = new DevExpress.XtraEditors.TextEdit();
            this.txtPick = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.txtTrip = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtDrop = new DevExpress.XtraEditors.TextEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lblFuelCharge = new DevExpress.XtraEditors.LabelControl();
            this.lblDecor = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtFuelAmount = new DevExpress.XtraEditors.TextEdit();
            this.trkFuel = new DevExpress.XtraEditors.TrackBarControl();
            this.txtFuelLevel = new DevExpress.XtraEditors.TextEdit();
            this.chkFuelAmt = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.txtExCharges = new DevExpress.XtraEditors.TextEdit();
            this.chkDecor = new DevExpress.XtraEditors.CheckEdit();
            this.txtCurODO = new DevExpress.XtraEditors.TextEdit();
            this.cmbVehicleList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.tpStart = new DevExpress.XtraEditors.TextEdit();
            this.tpEnd = new DevExpress.XtraEditors.TextEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.AddCst = new DevExpress.XtraEditors.SimpleButton();
            this.PicCustomer = new DevExpress.XtraEditors.PictureEdit();
            this.lblCstID = new DevExpress.XtraEditors.LabelControl();
            this.lblIsBlackListed = new DevExpress.XtraEditors.LabelControl();
            this.dtpLicExp = new System.Windows.Forms.DateTimePicker();
            this.txtContact1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtLIcNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtNIC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtFName = new DevExpress.XtraEditors.MemoEdit();
            this.txtAddress1 = new DevExpress.XtraEditors.MemoEdit();
            this.txtAddress2 = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.dtpLicExpDate = new System.Windows.Forms.DateTimePicker();
            this.dtpInsExpDate = new System.Windows.Forms.DateTimePicker();
            this.txtVhclLicNo = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclInsNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclNxtSrvice = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lblVhclID = new DevExpress.XtraEditors.LabelControl();
            this.imageSlider1 = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.dtpDelDate = new System.Windows.Forms.DateTimePicker();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclMYeay = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclModel = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclMake = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.txtDaddress = new DevExpress.XtraEditors.MemoEdit();
            this.dtpDlicExp = new System.Windows.Forms.DateTimePicker();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.txtDnic = new DevExpress.XtraEditors.TextEdit();
            this.txtDlic = new DevExpress.XtraEditors.TextEdit();
            this.txtDcontact = new DevExpress.XtraEditors.TextEdit();
            this.txtDName = new DevExpress.XtraEditors.TextEdit();
            this.arcScaleComponent2 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleNeedleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.lblBillTotal = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabType)).BeginInit();
            this.tabType.SuspendLayout();
            this.tabRent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtExKmCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDriverList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDriver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHrs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayRate.Properties)).BeginInit();
            this.tabHire.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbHireDriver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHireCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPick.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkFuel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFuelAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExCharges.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDecor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurODO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVehicleList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLIcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclLicNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclInsNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNxtSrvice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDnic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDlic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDcontact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPaid
            // 
            this.lblPaid.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaid.Appearance.Options.UseFont = true;
            this.lblPaid.Appearance.Options.UseTextOptions = true;
            this.lblPaid.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblPaid.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPaid.Location = new System.Drawing.Point(430, 52);
            this.lblPaid.Name = "lblPaid";
            this.lblPaid.Size = new System.Drawing.Size(112, 19);
            this.lblPaid.TabIndex = 56;
            this.lblPaid.Text = "0.00";
            // 
            // lblDue
            // 
            this.lblDue.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDue.Appearance.Options.UseFont = true;
            this.lblDue.Appearance.Options.UseTextOptions = true;
            this.lblDue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDue.Location = new System.Drawing.Point(430, 75);
            this.lblDue.Name = "lblDue";
            this.lblDue.Size = new System.Drawing.Size(112, 19);
            this.lblDue.TabIndex = 55;
            this.lblDue.Text = "0.00";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.Location = new System.Drawing.Point(323, 75);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(109, 19);
            this.labelControl29.TabIndex = 54;
            this.labelControl29.Text = "Due Payment";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.Options.UseFont = true;
            this.labelControl28.Location = new System.Drawing.Point(323, 50);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(36, 19);
            this.labelControl28.TabIndex = 53;
            this.labelControl28.Text = "Paid";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.Location = new System.Drawing.Point(323, 5);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(97, 19);
            this.labelControl25.TabIndex = 52;
            this.labelControl25.Text = "Grand Total";
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandTotal.Appearance.Options.UseFont = true;
            this.lblGrandTotal.Appearance.Options.UseTextOptions = true;
            this.lblGrandTotal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblGrandTotal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGrandTotal.Location = new System.Drawing.Point(430, 5);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(112, 19);
            this.lblGrandTotal.TabIndex = 51;
            this.lblGrandTotal.Text = "0.00";
            this.lblGrandTotal.TextChanged += new System.EventHandler(this.lblGrandTotal_TextChanged);
            // 
            // btnProceed
            // 
            this.btnProceed.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnProceed.ImageOptions.Image")));
            this.btnProceed.Location = new System.Drawing.Point(141, 25);
            this.btnProceed.Name = "btnProceed";
            this.btnProceed.Size = new System.Drawing.Size(101, 44);
            this.btnProceed.TabIndex = 57;
            this.btnProceed.Text = "Proceed";
            this.btnProceed.Click += new System.EventHandler(this.btnProceed_Click);
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.tabType);
            this.groupControl5.Controls.Add(this.groupControl3);
            this.groupControl5.Controls.Add(this.cmbVehicleList);
            this.groupControl5.Controls.Add(this.dtpStart);
            this.groupControl5.Controls.Add(this.dtpEnd);
            this.groupControl5.Controls.Add(this.radioGroup1);
            this.groupControl5.Controls.Add(this.labelControl27);
            this.groupControl5.Controls.Add(this.dateNavigator1);
            this.groupControl5.Controls.Add(this.labelControl32);
            this.groupControl5.Controls.Add(this.tpStart);
            this.groupControl5.Controls.Add(this.tpEnd);
            this.groupControl5.Controls.Add(this.labelControl30);
            this.groupControl5.Location = new System.Drawing.Point(661, 1);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(619, 530);
            this.groupControl5.TabIndex = 50;
            this.groupControl5.Text = "Rates && Calculation";
            // 
            // tabType
            // 
            this.tabType.Location = new System.Drawing.Point(5, 268);
            this.tabType.Name = "tabType";
            this.tabType.SelectedTabPage = this.tabRent;
            this.tabType.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tabType.Size = new System.Drawing.Size(568, 111);
            this.tabType.TabIndex = 136;
            this.tabType.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabRent,
            this.tabHire});
            // 
            // tabRent
            // 
            this.tabRent.Controls.Add(this.labelControl40);
            this.tabRent.Controls.Add(this.labelControl44);
            this.tabRent.Controls.Add(this.txtExKmCharge);
            this.tabRent.Controls.Add(this.lblDriverCharge);
            this.tabRent.Controls.Add(this.lblRent);
            this.tabRent.Controls.Add(this.labelControl31);
            this.tabRent.Controls.Add(this.txtDisLimit);
            this.tabRent.Controls.Add(this.cmbDriverList);
            this.tabRent.Controls.Add(this.labelControl26);
            this.tabRent.Controls.Add(this.labelControl24);
            this.tabRent.Controls.Add(this.txtDays);
            this.tabRent.Controls.Add(this.chkDriver);
            this.tabRent.Controls.Add(this.labelControl45);
            this.tabRent.Controls.Add(this.txtHrs);
            this.tabRent.Controls.Add(this.txtDrate);
            this.tabRent.Controls.Add(this.txtDayRate);
            this.tabRent.Name = "tabRent";
            this.tabRent.Size = new System.Drawing.Size(562, 105);
            this.tabRent.Text = "xtraTabPage1";
            // 
            // labelControl40
            // 
            this.labelControl40.Location = new System.Drawing.Point(141, 52);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(99, 13);
            this.labelControl40.TabIndex = 63;
            this.labelControl40.Text = "Charge per Extra km";
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl44.Appearance.Options.UseFont = true;
            this.labelControl44.Location = new System.Drawing.Point(212, 80);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(49, 14);
            this.labelControl44.TabIndex = 114;
            this.labelControl44.Text = "Rate (Rs.)";
            // 
            // txtExKmCharge
            // 
            this.txtExKmCharge.Location = new System.Drawing.Point(246, 49);
            this.txtExKmCharge.Name = "txtExKmCharge";
            this.txtExKmCharge.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtExKmCharge.Size = new System.Drawing.Size(68, 20);
            this.txtExKmCharge.TabIndex = 62;
            // 
            // lblDriverCharge
            // 
            this.lblDriverCharge.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverCharge.Appearance.Options.UseFont = true;
            this.lblDriverCharge.Appearance.Options.UseTextOptions = true;
            this.lblDriverCharge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDriverCharge.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDriverCharge.Location = new System.Drawing.Point(430, 73);
            this.lblDriverCharge.Name = "lblDriverCharge";
            this.lblDriverCharge.Size = new System.Drawing.Size(112, 19);
            this.lblDriverCharge.TabIndex = 113;
            this.lblDriverCharge.Text = "0.00";
            this.lblDriverCharge.TextChanged += new System.EventHandler(this.lblDriverCharge_TextChanged);
            // 
            // lblRent
            // 
            this.lblRent.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRent.Appearance.Options.UseFont = true;
            this.lblRent.Appearance.Options.UseTextOptions = true;
            this.lblRent.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblRent.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblRent.Location = new System.Drawing.Point(430, 18);
            this.lblRent.Name = "lblRent";
            this.lblRent.Size = new System.Drawing.Size(112, 19);
            this.lblRent.TabIndex = 58;
            this.lblRent.Text = "0.00";
            this.lblRent.TextChanged += new System.EventHandler(this.lblRent_TextChanged);
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl31.Appearance.Options.UseFont = true;
            this.labelControl31.Location = new System.Drawing.Point(14, 52);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(41, 14);
            this.labelControl31.TabIndex = 112;
            this.labelControl31.Text = "Free km";
            // 
            // txtDisLimit
            // 
            this.txtDisLimit.Location = new System.Drawing.Point(61, 49);
            this.txtDisLimit.Name = "txtDisLimit";
            this.txtDisLimit.Size = new System.Drawing.Size(65, 20);
            this.txtDisLimit.TabIndex = 89;
            // 
            // cmbDriverList
            // 
            this.cmbDriverList.Location = new System.Drawing.Point(89, 77);
            this.cmbDriverList.Name = "cmbDriverList";
            this.cmbDriverList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDriverList.Size = new System.Drawing.Size(117, 20);
            this.cmbDriverList.TabIndex = 12;
            this.cmbDriverList.SelectedIndexChanged += new System.EventHandler(this.cmbDriverList_SelectedIndexChanged);
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.Location = new System.Drawing.Point(149, 4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(49, 14);
            this.labelControl26.TabIndex = 111;
            this.labelControl26.Text = "Rate (Rs.)";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Location = new System.Drawing.Point(14, 4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(25, 14);
            this.labelControl24.TabIndex = 102;
            this.labelControl24.Text = "Days";
            // 
            // txtDays
            // 
            this.txtDays.Location = new System.Drawing.Point(14, 20);
            this.txtDays.Name = "txtDays";
            this.txtDays.Size = new System.Drawing.Size(49, 20);
            this.txtDays.TabIndex = 107;
            // 
            // chkDriver
            // 
            this.chkDriver.Location = new System.Drawing.Point(14, 78);
            this.chkDriver.Name = "chkDriver";
            this.chkDriver.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.chkDriver.Properties.Appearance.Options.UseFont = true;
            this.chkDriver.Properties.Caption = "Driver";
            this.chkDriver.Size = new System.Drawing.Size(69, 19);
            this.chkDriver.TabIndex = 94;
            this.chkDriver.CheckedChanged += new System.EventHandler(this.chkDriver_CheckedChanged);
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.Location = new System.Drawing.Point(78, 4);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(29, 14);
            this.labelControl45.TabIndex = 109;
            this.labelControl45.Text = "Hours";
            // 
            // txtHrs
            // 
            this.txtHrs.Location = new System.Drawing.Point(78, 20);
            this.txtHrs.Name = "txtHrs";
            this.txtHrs.Size = new System.Drawing.Size(48, 20);
            this.txtHrs.TabIndex = 108;
            // 
            // txtDrate
            // 
            this.txtDrate.Location = new System.Drawing.Point(267, 77);
            this.txtDrate.Name = "txtDrate";
            this.txtDrate.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDrate.Properties.Appearance.Options.UseFont = true;
            this.txtDrate.Size = new System.Drawing.Size(103, 20);
            this.txtDrate.TabIndex = 56;
            this.txtDrate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDrate_KeyDown);
            // 
            // txtDayRate
            // 
            this.txtDayRate.Location = new System.Drawing.Point(149, 20);
            this.txtDayRate.Name = "txtDayRate";
            this.txtDayRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDayRate.Size = new System.Drawing.Size(75, 20);
            this.txtDayRate.TabIndex = 110;
            this.txtDayRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDayRate_KeyDown);
            // 
            // tabHire
            // 
            this.tabHire.Controls.Add(this.labelControl43);
            this.tabHire.Controls.Add(this.cmbHireDriver);
            this.tabHire.Controls.Add(this.lblHire);
            this.tabHire.Controls.Add(this.txtHireCharge);
            this.tabHire.Controls.Add(this.txtPick);
            this.tabHire.Controls.Add(this.labelControl20);
            this.tabHire.Controls.Add(this.labelControl23);
            this.tabHire.Controls.Add(this.txtTrip);
            this.tabHire.Controls.Add(this.labelControl22);
            this.tabHire.Controls.Add(this.labelControl21);
            this.tabHire.Controls.Add(this.txtDrop);
            this.tabHire.Name = "tabHire";
            this.tabHire.Size = new System.Drawing.Size(562, 105);
            this.tabHire.Text = "xtraTabPage5";
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Appearance.Options.UseFont = true;
            this.labelControl43.Location = new System.Drawing.Point(256, 41);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(37, 14);
            this.labelControl43.TabIndex = 133;
            this.labelControl43.Text = "Driver :";
            // 
            // cmbHireDriver
            // 
            this.cmbHireDriver.Location = new System.Drawing.Point(316, 38);
            this.cmbHireDriver.Name = "cmbHireDriver";
            this.cmbHireDriver.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbHireDriver.Size = new System.Drawing.Size(117, 20);
            this.cmbHireDriver.TabIndex = 13;
            // 
            // lblHire
            // 
            this.lblHire.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHire.Appearance.Options.UseFont = true;
            this.lblHire.Appearance.Options.UseTextOptions = true;
            this.lblHire.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblHire.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblHire.Location = new System.Drawing.Point(430, 75);
            this.lblHire.Name = "lblHire";
            this.lblHire.Size = new System.Drawing.Size(112, 19);
            this.lblHire.TabIndex = 132;
            this.lblHire.Text = "0.00";
            this.lblHire.TextChanged += new System.EventHandler(this.lblHire_TextChanged);
            // 
            // txtHireCharge
            // 
            this.txtHireCharge.Location = new System.Drawing.Point(93, 74);
            this.txtHireCharge.Name = "txtHireCharge";
            this.txtHireCharge.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHireCharge.Properties.Appearance.Options.UseFont = true;
            this.txtHireCharge.Size = new System.Drawing.Size(145, 20);
            this.txtHireCharge.TabIndex = 122;
            this.txtHireCharge.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHireCharge_KeyDown);
            // 
            // txtPick
            // 
            this.txtPick.Location = new System.Drawing.Point(69, 6);
            this.txtPick.Name = "txtPick";
            this.txtPick.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPick.Properties.Appearance.Options.UseFont = true;
            this.txtPick.Size = new System.Drawing.Size(169, 20);
            this.txtPick.TabIndex = 116;
            this.txtPick.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPick_KeyDown);
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Location = new System.Drawing.Point(9, 77);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(60, 14);
            this.labelControl20.TabIndex = 121;
            this.labelControl20.Text = "Charge (Rs.)";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.Location = new System.Drawing.Point(9, 9);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(52, 14);
            this.labelControl23.TabIndex = 115;
            this.labelControl23.Text = "Pick up @:";
            // 
            // txtTrip
            // 
            this.txtTrip.Location = new System.Drawing.Point(88, 38);
            this.txtTrip.Name = "txtTrip";
            this.txtTrip.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrip.Properties.Appearance.Options.UseFont = true;
            this.txtTrip.Size = new System.Drawing.Size(145, 20);
            this.txtTrip.TabIndex = 120;
            this.txtTrip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTrip_KeyDown);
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Location = new System.Drawing.Point(256, 9);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(44, 14);
            this.labelControl22.TabIndex = 117;
            this.labelControl22.Text = "Drop @ :";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Location = new System.Drawing.Point(9, 41);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(49, 14);
            this.labelControl21.TabIndex = 119;
            this.labelControl21.Text = "Distance :";
            // 
            // txtDrop
            // 
            this.txtDrop.Location = new System.Drawing.Point(316, 6);
            this.txtDrop.Name = "txtDrop";
            this.txtDrop.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDrop.Properties.Appearance.Options.UseFont = true;
            this.txtDrop.Size = new System.Drawing.Size(169, 20);
            this.txtDrop.TabIndex = 118;
            this.txtDrop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDrop_KeyDown);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lblFuelCharge);
            this.groupControl3.Controls.Add(this.lblDecor);
            this.groupControl3.Controls.Add(this.labelControl16);
            this.groupControl3.Controls.Add(this.txtFuelAmount);
            this.groupControl3.Controls.Add(this.trkFuel);
            this.groupControl3.Controls.Add(this.txtFuelLevel);
            this.groupControl3.Controls.Add(this.chkFuelAmt);
            this.groupControl3.Controls.Add(this.labelControl33);
            this.groupControl3.Controls.Add(this.labelControl12);
            this.groupControl3.Controls.Add(this.txtNote);
            this.groupControl3.Controls.Add(this.txtExCharges);
            this.groupControl3.Controls.Add(this.chkDecor);
            this.groupControl3.Controls.Add(this.txtCurODO);
            this.groupControl3.Location = new System.Drawing.Point(5, 380);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(568, 146);
            this.groupControl3.TabIndex = 135;
            this.groupControl3.Text = "Additional Details";
            // 
            // lblFuelCharge
            // 
            this.lblFuelCharge.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuelCharge.Appearance.Options.UseFont = true;
            this.lblFuelCharge.Appearance.Options.UseTextOptions = true;
            this.lblFuelCharge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblFuelCharge.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFuelCharge.Location = new System.Drawing.Point(431, 115);
            this.lblFuelCharge.Name = "lblFuelCharge";
            this.lblFuelCharge.Size = new System.Drawing.Size(112, 19);
            this.lblFuelCharge.TabIndex = 131;
            this.lblFuelCharge.Text = "0.00";
            this.lblFuelCharge.TextChanged += new System.EventHandler(this.lblFuelCharge_TextChanged);
            // 
            // lblDecor
            // 
            this.lblDecor.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDecor.Appearance.Options.UseFont = true;
            this.lblDecor.Appearance.Options.UseTextOptions = true;
            this.lblDecor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDecor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDecor.Location = new System.Drawing.Point(431, 32);
            this.lblDecor.Name = "lblDecor";
            this.lblDecor.Size = new System.Drawing.Size(112, 19);
            this.lblDecor.TabIndex = 115;
            this.lblDecor.Text = "0.00";
            this.lblDecor.TextChanged += new System.EventHandler(this.lblDecor_TextChanged);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(248, 119);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(64, 13);
            this.labelControl16.TabIndex = 128;
            this.labelControl16.Text = "Amount (Rs.)";
            // 
            // txtFuelAmount
            // 
            this.txtFuelAmount.Location = new System.Drawing.Point(321, 116);
            this.txtFuelAmount.Name = "txtFuelAmount";
            this.txtFuelAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtFuelAmount.Size = new System.Drawing.Size(90, 20);
            this.txtFuelAmount.TabIndex = 127;
            this.txtFuelAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuelAmount_KeyDown);
            // 
            // trkFuel
            // 
            this.trkFuel.EditValue = null;
            this.trkFuel.Location = new System.Drawing.Point(240, 76);
            this.trkFuel.Name = "trkFuel";
            this.trkFuel.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.trkFuel.Properties.Appearance.Options.UseBackColor = true;
            this.trkFuel.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.trkFuel.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trkFuel.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            trackBarLabel12.Label = "0";
            trackBarLabel13.Label = "1";
            trackBarLabel13.Value = 1;
            trackBarLabel14.Label = "2";
            trackBarLabel14.Value = 2;
            trackBarLabel15.Label = "3";
            trackBarLabel15.Value = 3;
            trackBarLabel16.Label = "4";
            trackBarLabel16.Value = 4;
            trackBarLabel17.Label = "5";
            trackBarLabel17.Value = 5;
            trackBarLabel18.Label = "6";
            trackBarLabel18.Value = 6;
            trackBarLabel19.Label = "7";
            trackBarLabel19.Value = 7;
            trackBarLabel20.Label = "8";
            trackBarLabel20.Value = 8;
            trackBarLabel21.Label = "9";
            trackBarLabel21.Value = 9;
            trackBarLabel22.Label = "10";
            trackBarLabel22.Value = 10;
            this.trkFuel.Properties.Labels.AddRange(new DevExpress.XtraEditors.Repository.TrackBarLabel[] {
            trackBarLabel12,
            trackBarLabel13,
            trackBarLabel14,
            trackBarLabel15,
            trackBarLabel16,
            trackBarLabel17,
            trackBarLabel18,
            trackBarLabel19,
            trackBarLabel20,
            trackBarLabel21,
            trackBarLabel22});
            this.trkFuel.Properties.ShowLabels = true;
            this.trkFuel.Properties.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trkFuel.Size = new System.Drawing.Size(171, 72);
            this.trkFuel.TabIndex = 130;
            this.trkFuel.EditValueChanged += new System.EventHandler(this.trkFuel_EditValueChanged);
            // 
            // txtFuelLevel
            // 
            this.txtFuelLevel.Location = new System.Drawing.Point(321, 55);
            this.txtFuelLevel.Name = "txtFuelLevel";
            this.txtFuelLevel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtFuelLevel.Size = new System.Drawing.Size(35, 20);
            this.txtFuelLevel.TabIndex = 129;
            this.txtFuelLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuelLevel_KeyDown);
            // 
            // chkFuelAmt
            // 
            this.chkFuelAmt.Location = new System.Drawing.Point(248, 55);
            this.chkFuelAmt.Name = "chkFuelAmt";
            this.chkFuelAmt.Properties.Caption = "Fuel Level";
            this.chkFuelAmt.Size = new System.Drawing.Size(67, 19);
            this.chkFuelAmt.TabIndex = 126;
            this.chkFuelAmt.CheckedChanged += new System.EventHandler(this.chkFuelAmt_CheckedChanged);
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(5, 13);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(112, 13);
            this.labelControl33.TabIndex = 125;
            this.labelControl33.Text = "Current ODO Reading :";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(2, 32);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(30, 14);
            this.labelControl12.TabIndex = 123;
            this.labelControl12.Text = "Note :";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(2, 51);
            this.txtNote.Name = "txtNote";
            this.txtNote.Properties.MaxLength = 255;
            this.txtNote.Size = new System.Drawing.Size(223, 94);
            this.txtNote.TabIndex = 113;
            // 
            // txtExCharges
            // 
            this.txtExCharges.Location = new System.Drawing.Point(266, 29);
            this.txtExCharges.Name = "txtExCharges";
            this.txtExCharges.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtExCharges.Size = new System.Drawing.Size(105, 20);
            this.txtExCharges.TabIndex = 99;
            this.txtExCharges.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtExCharges_KeyDown);
            // 
            // chkDecor
            // 
            this.chkDecor.Location = new System.Drawing.Point(248, 11);
            this.chkDecor.Name = "chkDecor";
            this.chkDecor.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.chkDecor.Properties.Appearance.Options.UseFont = true;
            this.chkDecor.Properties.Caption = "Decor Charges (Rs.)";
            this.chkDecor.Size = new System.Drawing.Size(123, 19);
            this.chkDecor.TabIndex = 98;
            this.chkDecor.CheckedChanged += new System.EventHandler(this.chkDecor_CheckedChanged);
            // 
            // txtCurODO
            // 
            this.txtCurODO.Location = new System.Drawing.Point(123, 10);
            this.txtCurODO.Name = "txtCurODO";
            this.txtCurODO.Size = new System.Drawing.Size(105, 20);
            this.txtCurODO.TabIndex = 80;
            // 
            // cmbVehicleList
            // 
            this.cmbVehicleList.Location = new System.Drawing.Point(271, 26);
            this.cmbVehicleList.Name = "cmbVehicleList";
            this.cmbVehicleList.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVehicleList.Properties.Appearance.Options.UseFont = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVehicleList.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbVehicleList.Properties.AutoComplete = false;
            this.cmbVehicleList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbVehicleList.Properties.DropDownRows = 20;
            this.cmbVehicleList.Size = new System.Drawing.Size(181, 30);
            this.cmbVehicleList.TabIndex = 11;
            this.cmbVehicleList.SelectedIndexChanged += new System.EventHandler(this.cmbVehicleList_SelectedIndexChanged);
            // 
            // dtpStart
            // 
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStart.Location = new System.Drawing.Point(450, 91);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(82, 21);
            this.dtpStart.TabIndex = 129;
            // 
            // dtpEnd
            // 
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEnd.Location = new System.Drawing.Point(450, 181);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(77, 21);
            this.dtpEnd.TabIndex = 130;
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(7, 23);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioGroup1.Properties.Appearance.Options.UseFont = true;
            this.radioGroup1.Properties.Columns = 2;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("rent", "Rent"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("hire", "Hire")});
            this.radioGroup1.Size = new System.Drawing.Size(243, 34);
            this.radioGroup1.TabIndex = 133;
            this.radioGroup1.SelectedIndexChanged += new System.EventHandler(this.radioGroup1_SelectedIndexChanged);
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl27.Appearance.Options.UseFont = true;
            this.labelControl27.Location = new System.Drawing.Point(457, 162);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(18, 14);
            this.labelControl27.TabIndex = 128;
            this.labelControl27.Text = "End";
            // 
            // dateNavigator1
            // 
            this.dateNavigator1.CalendarAppearance.DayCellSelected.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.dateNavigator1.CalendarAppearance.DayCellSelected.Options.UseFont = true;
            this.dateNavigator1.CalendarAppearance.DayCellSpecial.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.dateNavigator1.CalendarAppearance.DayCellSpecial.Options.UseFont = true;
            this.dateNavigator1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateNavigator1.Cursor = System.Windows.Forms.Cursors.Default;
            this.dateNavigator1.DateTime = new System.DateTime(2018, 1, 15, 0, 0, 0, 0);
            this.dateNavigator1.EditValue = new System.DateTime(2018, 1, 15, 0, 0, 0, 0);
            this.dateNavigator1.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.dateNavigator1.HighlightHolidays = false;
            this.dateNavigator1.Location = new System.Drawing.Point(7, 63);
            this.dateNavigator1.Name = "dateNavigator1";
            this.dateNavigator1.ShowFooter = false;
            this.dateNavigator1.ShowHeader = false;
            this.dateNavigator1.ShowTodayButton = false;
            this.dateNavigator1.ShowWeekNumbers = false;
            this.dateNavigator1.Size = new System.Drawing.Size(419, 199);
            this.dateNavigator1.TabIndex = 123;
            this.dateNavigator1.SelectionChanged += new System.EventHandler(this.dateNavigator1_SelectionChanged);
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl32.Appearance.Options.UseFont = true;
            this.labelControl32.Location = new System.Drawing.Point(-59, 121);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(59, 14);
            this.labelControl32.TabIndex = 125;
            this.labelControl32.Text = "Vehicle No :";
            // 
            // tpStart
            // 
            this.tpStart.EditValue = "";
            this.tpStart.Location = new System.Drawing.Point(450, 118);
            this.tpStart.Name = "tpStart";
            this.tpStart.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.tpStart.Properties.Mask.EditMask = "t";
            this.tpStart.Properties.MaxLength = 4;
            this.tpStart.Size = new System.Drawing.Size(57, 20);
            this.tpStart.TabIndex = 131;
            this.tpStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tpStart_KeyDown);
            // 
            // tpEnd
            // 
            this.tpEnd.EditValue = "";
            this.tpEnd.Location = new System.Drawing.Point(450, 208);
            this.tpEnd.Name = "tpEnd";
            this.tpEnd.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.tpEnd.Properties.Mask.EditMask = "t";
            this.tpEnd.Properties.MaxLength = 4;
            this.tpEnd.Size = new System.Drawing.Size(57, 20);
            this.tpEnd.TabIndex = 132;
            this.tpEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tpEnd_KeyDown);
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl30.Appearance.Options.UseFont = true;
            this.labelControl30.Location = new System.Drawing.Point(457, 71);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(24, 14);
            this.labelControl30.TabIndex = 127;
            this.labelControl30.Text = "Start";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl35);
            this.groupControl1.Controls.Add(this.txtContact3);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtContact2);
            this.groupControl1.Controls.Add(this.labelControl34);
            this.groupControl1.Controls.Add(this.AddCst);
            this.groupControl1.Controls.Add(this.PicCustomer);
            this.groupControl1.Controls.Add(this.lblCstID);
            this.groupControl1.Controls.Add(this.lblIsBlackListed);
            this.groupControl1.Controls.Add(this.dtpLicExp);
            this.groupControl1.Controls.Add(this.txtContact1);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtLIcNo);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtNIC);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.txtFName);
            this.groupControl1.Controls.Add(this.txtAddress1);
            this.groupControl1.Controls.Add(this.txtAddress2);
            this.groupControl1.Location = new System.Drawing.Point(3, 1);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(659, 324);
            this.groupControl1.TabIndex = 48;
            this.groupControl1.Text = "Customer Deatils";
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(34, 290);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(57, 13);
            this.labelControl35.TabIndex = 10;
            this.labelControl35.Text = "Contact 03:";
            // 
            // txtContact3
            // 
            this.txtContact3.Location = new System.Drawing.Point(98, 287);
            this.txtContact3.Name = "txtContact3";
            this.txtContact3.Size = new System.Drawing.Size(167, 20);
            this.txtContact3.TabIndex = 9;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(271, 214);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(42, 13);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Ex: Date";
            // 
            // txtContact2
            // 
            this.txtContact2.Location = new System.Drawing.Point(98, 261);
            this.txtContact2.Name = "txtContact2";
            this.txtContact2.Size = new System.Drawing.Size(167, 20);
            this.txtContact2.TabIndex = 8;
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(31, 264);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(60, 13);
            this.labelControl34.TabIndex = 8;
            this.labelControl34.Text = "Contact 02 :";
            // 
            // AddCst
            // 
            this.AddCst.Location = new System.Drawing.Point(257, 45);
            this.AddCst.Name = "AddCst";
            this.AddCst.Size = new System.Drawing.Size(87, 23);
            this.AddCst.TabIndex = 7;
            this.AddCst.Text = "Add Customer";
            // 
            // PicCustomer
            // 
            this.PicCustomer.Cursor = System.Windows.Forms.Cursors.Default;
            this.PicCustomer.Location = new System.Drawing.Point(438, 45);
            this.PicCustomer.Name = "PicCustomer";
            this.PicCustomer.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.PicCustomer.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.PicCustomer.Properties.ZoomAccelerationFactor = 1D;
            this.PicCustomer.Size = new System.Drawing.Size(210, 152);
            this.PicCustomer.TabIndex = 6;
            // 
            // lblCstID
            // 
            this.lblCstID.Location = new System.Drawing.Point(352, 50);
            this.lblCstID.Name = "lblCstID";
            this.lblCstID.Size = new System.Drawing.Size(60, 13);
            this.lblCstID.TabIndex = 5;
            this.lblCstID.Text = "Customer ID";
            // 
            // lblIsBlackListed
            // 
            this.lblIsBlackListed.Appearance.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsBlackListed.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblIsBlackListed.Appearance.Options.UseFont = true;
            this.lblIsBlackListed.Appearance.Options.UseForeColor = true;
            this.lblIsBlackListed.Location = new System.Drawing.Point(69, 17);
            this.lblIsBlackListed.Name = "lblIsBlackListed";
            this.lblIsBlackListed.Size = new System.Drawing.Size(140, 29);
            this.lblIsBlackListed.TabIndex = 4;
            this.lblIsBlackListed.Text = " Is Black Listed";
            // 
            // dtpLicExp
            // 
            this.dtpLicExp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLicExp.Location = new System.Drawing.Point(319, 211);
            this.dtpLicExp.Name = "dtpLicExp";
            this.dtpLicExp.Size = new System.Drawing.Size(105, 21);
            this.dtpLicExp.TabIndex = 6;
            // 
            // txtContact1
            // 
            this.txtContact1.Location = new System.Drawing.Point(98, 234);
            this.txtContact1.Name = "txtContact1";
            this.txtContact1.Size = new System.Drawing.Size(167, 20);
            this.txtContact1.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(34, 237);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 13);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Contact 01 :";
            // 
            // txtLIcNo
            // 
            this.txtLIcNo.Location = new System.Drawing.Point(98, 211);
            this.txtLIcNo.Name = "txtLIcNo";
            this.txtLIcNo.Size = new System.Drawing.Size(167, 20);
            this.txtLIcNo.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(17, 210);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(74, 13);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Driving  Lic No :";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(39, 165);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(55, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Address 2 :";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(39, 117);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(55, 13);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Address 1 :";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(60, 74);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(34, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Name :";
            // 
            // txtNIC
            // 
            this.txtNIC.Location = new System.Drawing.Point(98, 47);
            this.txtNIC.Name = "txtNIC";
            this.txtNIC.Size = new System.Drawing.Size(139, 20);
            this.txtNIC.TabIndex = 1;
            this.txtNIC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNIC_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(69, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(25, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "NIC :";
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(98, 71);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(334, 41);
            this.txtFName.TabIndex = 2;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(98, 116);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(334, 42);
            this.txtAddress1.TabIndex = 3;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(98, 163);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(334, 45);
            this.txtAddress2.TabIndex = 4;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.xtraTabControl1.Location = new System.Drawing.Point(5, 331);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(657, 306);
            this.xtraTabControl1.TabIndex = 49;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage4});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.PageClient.BackColor = System.Drawing.Color.White;
            this.xtraTabPage2.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage2.Controls.Add(this.dtpLicExpDate);
            this.xtraTabPage2.Controls.Add(this.dtpInsExpDate);
            this.xtraTabPage2.Controls.Add(this.txtVhclLicNo);
            this.xtraTabPage2.Controls.Add(this.txtVhclInsNo);
            this.xtraTabPage2.Controls.Add(this.labelControl18);
            this.xtraTabPage2.Controls.Add(this.txtVhclNxtSrvice);
            this.xtraTabPage2.Controls.Add(this.labelControl17);
            this.xtraTabPage2.Controls.Add(this.labelControl19);
            this.xtraTabPage2.Controls.Add(this.lblVhclID);
            this.xtraTabPage2.Controls.Add(this.imageSlider1);
            this.xtraTabPage2.Controls.Add(this.labelControl8);
            this.xtraTabPage2.Controls.Add(this.textEdit18);
            this.xtraTabPage2.Controls.Add(this.pictureEdit4);
            this.xtraTabPage2.Controls.Add(this.pictureEdit3);
            this.xtraTabPage2.Controls.Add(this.pictureEdit2);
            this.xtraTabPage2.Controls.Add(this.pictureEdit1);
            this.xtraTabPage2.Controls.Add(this.dtpDelDate);
            this.xtraTabPage2.Controls.Add(this.labelControl9);
            this.xtraTabPage2.Controls.Add(this.labelControl11);
            this.xtraTabPage2.Controls.Add(this.labelControl10);
            this.xtraTabPage2.Controls.Add(this.txtVhclNo);
            this.xtraTabPage2.Controls.Add(this.labelControl13);
            this.xtraTabPage2.Controls.Add(this.labelControl14);
            this.xtraTabPage2.Controls.Add(this.labelControl15);
            this.xtraTabPage2.Controls.Add(this.txtVhclMYeay);
            this.xtraTabPage2.Controls.Add(this.txtVhclModel);
            this.xtraTabPage2.Controls.Add(this.txtVhclMake);
            this.xtraTabPage2.Controls.Add(this.txtVhclType);
            this.xtraTabPage2.Controls.Add(this.simpleButton8);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(569, 300);
            this.xtraTabPage2.Text = "Vehicle Deatils";
            // 
            // dtpLicExpDate
            // 
            this.dtpLicExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLicExpDate.Location = new System.Drawing.Point(292, 264);
            this.dtpLicExpDate.Name = "dtpLicExpDate";
            this.dtpLicExpDate.Size = new System.Drawing.Size(95, 21);
            this.dtpLicExpDate.TabIndex = 88;
            // 
            // dtpInsExpDate
            // 
            this.dtpInsExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInsExpDate.Location = new System.Drawing.Point(292, 233);
            this.dtpInsExpDate.Name = "dtpInsExpDate";
            this.dtpInsExpDate.Size = new System.Drawing.Size(95, 21);
            this.dtpInsExpDate.TabIndex = 85;
            // 
            // txtVhclLicNo
            // 
            this.txtVhclLicNo.Location = new System.Drawing.Point(82, 264);
            this.txtVhclLicNo.Name = "txtVhclLicNo";
            this.txtVhclLicNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclLicNo.TabIndex = 87;
            // 
            // txtVhclInsNo
            // 
            this.txtVhclInsNo.Location = new System.Drawing.Point(82, 234);
            this.txtVhclInsNo.Name = "txtVhclInsNo";
            this.txtVhclInsNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclInsNo.TabIndex = 86;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(18, 270);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(58, 13);
            this.labelControl18.TabIndex = 84;
            this.labelControl18.Text = "License No :";
            // 
            // txtVhclNxtSrvice
            // 
            this.txtVhclNxtSrvice.Location = new System.Drawing.Point(82, 207);
            this.txtVhclNxtSrvice.Name = "txtVhclNxtSrvice";
            this.txtVhclNxtSrvice.Size = new System.Drawing.Size(193, 20);
            this.txtVhclNxtSrvice.TabIndex = 83;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(8, 210);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(68, 13);
            this.labelControl17.TabIndex = 81;
            this.labelControl17.Text = "Next Service :";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(5, 237);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(71, 13);
            this.labelControl19.TabIndex = 82;
            this.labelControl19.Text = "Insurance No :";
            // 
            // lblVhclID
            // 
            this.lblVhclID.Appearance.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVhclID.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblVhclID.Appearance.Options.UseFont = true;
            this.lblVhclID.Appearance.Options.UseForeColor = true;
            this.lblVhclID.Location = new System.Drawing.Point(460, 186);
            this.lblVhclID.Name = "lblVhclID";
            this.lblVhclID.Size = new System.Drawing.Size(54, 15);
            this.lblVhclID.TabIndex = 78;
            this.lblVhclID.Text = "Vehicle ID";
            // 
            // imageSlider1
            // 
            this.imageSlider1.Cursor = System.Windows.Forms.Cursors.Default;
            this.imageSlider1.Location = new System.Drawing.Point(280, 15);
            this.imageSlider1.Name = "imageSlider1";
            this.imageSlider1.Size = new System.Drawing.Size(234, 168);
            this.imageSlider1.TabIndex = 66;
            this.imageSlider1.Text = "imageSlider1";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(38, 145);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(38, 13);
            this.labelControl8.TabIndex = 75;
            this.labelControl8.Text = "Colour :";
            // 
            // textEdit18
            // 
            this.textEdit18.Location = new System.Drawing.Point(82, 142);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit18.Size = new System.Drawing.Size(193, 20);
            this.textEdit18.TabIndex = 76;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit4.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit4.Location = new System.Drawing.Point(354, 58);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit4.Properties.InitialImage")));
            this.pictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit4.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit4.Size = new System.Drawing.Size(127, 104);
            this.pictureEdit4.TabIndex = 74;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit3.Location = new System.Drawing.Point(319, 45);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit3.Properties.InitialImage")));
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit3.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit3.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit3.TabIndex = 73;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit2.Location = new System.Drawing.Point(308, 30);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit2.Properties.InitialImage")));
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit2.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit2.TabIndex = 72;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit1.Location = new System.Drawing.Point(289, 26);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit1.Properties.InitialImage")));
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit1.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit1.TabIndex = 71;
            // 
            // dtpDelDate
            // 
            this.dtpDelDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDelDate.Location = new System.Drawing.Point(181, 165);
            this.dtpDelDate.Name = "dtpDelDate";
            this.dtpDelDate.Size = new System.Drawing.Size(93, 21);
            this.dtpDelDate.TabIndex = 70;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(82, 168);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(78, 13);
            this.labelControl9.TabIndex = 67;
            this.labelControl9.Text = "Delivered Date :";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(45, 71);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(31, 13);
            this.labelControl11.TabIndex = 68;
            this.labelControl11.Text = "Type :";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(56, 18);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(20, 13);
            this.labelControl10.TabIndex = 61;
            this.labelControl10.Text = "No :";
            // 
            // txtVhclNo
            // 
            this.txtVhclNo.Location = new System.Drawing.Point(81, 15);
            this.txtVhclNo.Name = "txtVhclNo";
            this.txtVhclNo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclNo.TabIndex = 65;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(44, 45);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(32, 13);
            this.labelControl13.TabIndex = 60;
            this.labelControl13.Text = "Make :";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(41, 96);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(35, 13);
            this.labelControl14.TabIndex = 59;
            this.labelControl14.Text = "Model :";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(18, 122);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(58, 13);
            this.labelControl15.TabIndex = 58;
            this.labelControl15.Text = "Made Year :";
            // 
            // txtVhclMYeay
            // 
            this.txtVhclMYeay.Location = new System.Drawing.Point(82, 119);
            this.txtVhclMYeay.Name = "txtVhclMYeay";
            this.txtVhclMYeay.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclMYeay.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMYeay.TabIndex = 62;
            // 
            // txtVhclModel
            // 
            this.txtVhclModel.Location = new System.Drawing.Point(82, 93);
            this.txtVhclModel.Name = "txtVhclModel";
            this.txtVhclModel.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclModel.Size = new System.Drawing.Size(193, 20);
            this.txtVhclModel.TabIndex = 63;
            // 
            // txtVhclMake
            // 
            this.txtVhclMake.Location = new System.Drawing.Point(82, 42);
            this.txtVhclMake.Name = "txtVhclMake";
            this.txtVhclMake.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclMake.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMake.TabIndex = 64;
            // 
            // txtVhclType
            // 
            this.txtVhclType.Location = new System.Drawing.Point(82, 68);
            this.txtVhclType.Name = "txtVhclType";
            this.txtVhclType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtVhclType.Properties.Items.AddRange(new object[] {
            "Car",
            "Van",
            "Bike"});
            this.txtVhclType.Size = new System.Drawing.Size(193, 20);
            this.txtVhclType.TabIndex = 69;
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(354, 146);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(62, 23);
            this.simpleButton8.TabIndex = 77;
            this.simpleButton8.Text = "Test";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.txtDaddress);
            this.xtraTabPage4.Controls.Add(this.dtpDlicExp);
            this.xtraTabPage4.Controls.Add(this.labelControl36);
            this.xtraTabPage4.Controls.Add(this.labelControl37);
            this.xtraTabPage4.Controls.Add(this.labelControl38);
            this.xtraTabPage4.Controls.Add(this.labelControl39);
            this.xtraTabPage4.Controls.Add(this.labelControl41);
            this.xtraTabPage4.Controls.Add(this.labelControl42);
            this.xtraTabPage4.Controls.Add(this.txtDnic);
            this.xtraTabPage4.Controls.Add(this.txtDlic);
            this.xtraTabPage4.Controls.Add(this.txtDcontact);
            this.xtraTabPage4.Controls.Add(this.txtDName);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(569, 300);
            this.xtraTabPage4.Text = "Driver Deatils";
            // 
            // txtDaddress
            // 
            this.txtDaddress.Location = new System.Drawing.Point(92, 37);
            this.txtDaddress.Name = "txtDaddress";
            this.txtDaddress.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDaddress.Size = new System.Drawing.Size(304, 46);
            this.txtDaddress.TabIndex = 67;
            // 
            // dtpDlicExp
            // 
            this.dtpDlicExp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDlicExp.Location = new System.Drawing.Point(291, 140);
            this.dtpDlicExp.Name = "dtpDlicExp";
            this.dtpDlicExp.Size = new System.Drawing.Size(105, 21);
            this.dtpDlicExp.TabIndex = 66;
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Appearance.Options.UseFont = true;
            this.labelControl36.Location = new System.Drawing.Point(316, 124);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(42, 14);
            this.labelControl36.TabIndex = 62;
            this.labelControl36.Text = "Ex: Date";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.Location = new System.Drawing.Point(12, 120);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(43, 14);
            this.labelControl37.TabIndex = 63;
            this.labelControl37.Text = "Contact :";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Appearance.Options.UseFont = true;
            this.labelControl38.Location = new System.Drawing.Point(12, 146);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(78, 14);
            this.labelControl38.TabIndex = 64;
            this.labelControl38.Text = "Driving  Lic No :";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Appearance.Options.UseFont = true;
            this.labelControl39.Location = new System.Drawing.Point(12, 96);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(23, 14);
            this.labelControl39.TabIndex = 61;
            this.labelControl39.Text = "NIC :";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Location = new System.Drawing.Point(12, 39);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(45, 14);
            this.labelControl41.TabIndex = 59;
            this.labelControl41.Text = "Address :";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.Location = new System.Drawing.Point(12, 13);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(36, 14);
            this.labelControl42.TabIndex = 58;
            this.labelControl42.Text = "Name :";
            // 
            // txtDnic
            // 
            this.txtDnic.Location = new System.Drawing.Point(92, 89);
            this.txtDnic.Name = "txtDnic";
            this.txtDnic.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDnic.Properties.Appearance.Options.UseFont = true;
            this.txtDnic.Size = new System.Drawing.Size(183, 20);
            this.txtDnic.TabIndex = 57;
            // 
            // txtDlic
            // 
            this.txtDlic.Location = new System.Drawing.Point(92, 143);
            this.txtDlic.Name = "txtDlic";
            this.txtDlic.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDlic.Properties.Appearance.Options.UseFont = true;
            this.txtDlic.Size = new System.Drawing.Size(183, 20);
            this.txtDlic.TabIndex = 55;
            // 
            // txtDcontact
            // 
            this.txtDcontact.Location = new System.Drawing.Point(92, 117);
            this.txtDcontact.Name = "txtDcontact";
            this.txtDcontact.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDcontact.Properties.Appearance.Options.UseFont = true;
            this.txtDcontact.Size = new System.Drawing.Size(183, 20);
            this.txtDcontact.TabIndex = 54;
            // 
            // txtDName
            // 
            this.txtDName.Location = new System.Drawing.Point(92, 10);
            this.txtDName.Name = "txtDName";
            this.txtDName.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDName.Properties.Appearance.Options.UseFont = true;
            this.txtDName.Size = new System.Drawing.Size(304, 20);
            this.txtDName.TabIndex = 53;
            // 
            // arcScaleComponent2
            // 
            this.arcScaleComponent2.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.arcScaleComponent2.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponent2.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(212F, 212F);
            this.arcScaleComponent2.EndAngle = -90F;
            this.arcScaleComponent2.MajorTickCount = 4;
            this.arcScaleComponent2.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent2.MajorTickmark.ShapeOffset = -3F;
            this.arcScaleComponent2.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_2;
            this.arcScaleComponent2.MajorTickmark.TextOffset = -20F;
            this.arcScaleComponent2.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent2.MaxValue = 500F;
            this.arcScaleComponent2.MinorTickCount = 4;
            this.arcScaleComponent2.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_1;
            this.arcScaleComponent2.MinValue = 200F;
            this.arcScaleComponent2.Name = "scale2";
            this.arcScaleComponent2.RadiusX = 120F;
            this.arcScaleComponent2.RadiusY = 120F;
            this.arcScaleComponent2.StartAngle = -180F;
            this.arcScaleComponent2.Value = 200F;
            this.arcScaleComponent2.ZOrder = -1;
            // 
            // arcScaleNeedleComponent1
            // 
            this.arcScaleNeedleComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleNeedleComponent1.EndOffset = -12F;
            this.arcScaleNeedleComponent1.Name = "needle1";
            this.arcScaleNeedleComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_Style11;
            this.arcScaleNeedleComponent1.StartOffset = -17.5F;
            this.arcScaleNeedleComponent1.ZOrder = -50;
            // 
            // arcScaleComponent1
            // 
            this.arcScaleComponent1.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 14F);
            this.arcScaleComponent1.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(212F, 212F);
            this.arcScaleComponent1.EndAngle = -90F;
            this.arcScaleComponent1.MajorTickCount = 3;
            this.arcScaleComponent1.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent1.MajorTickmark.ShapeOffset = -6F;
            this.arcScaleComponent1.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(1.6F, 1.6F);
            this.arcScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_4;
            this.arcScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent1.MaxValue = 10F;
            this.arcScaleComponent1.MinorTickCount = 4;
            this.arcScaleComponent1.MinorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(1.6F, 1.6F);
            this.arcScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_3;
            this.arcScaleComponent1.Name = "scale1";
            this.arcScaleComponent1.RadiusX = 176F;
            this.arcScaleComponent1.RadiusY = 176F;
            this.arcScaleComponent1.StartAngle = -180F;
            this.arcScaleComponent1.Value = 10F;
            // 
            // arcScaleBackgroundLayerComponent1
            // 
            this.arcScaleBackgroundLayerComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleBackgroundLayerComponent1.Name = "bg1";
            this.arcScaleBackgroundLayerComponent1.ScaleCenterPos = new DevExpress.XtraGauges.Core.Base.PointF2D(0.85F, 0.85F);
            this.arcScaleBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularQuarter_Style11Left;
            this.arcScaleBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.lblBillTotal);
            this.groupControl2.Controls.Add(this.labelControl47);
            this.groupControl2.Controls.Add(this.lblPaid);
            this.groupControl2.Controls.Add(this.btnProceed);
            this.groupControl2.Controls.Add(this.labelControl25);
            this.groupControl2.Controls.Add(this.lblDue);
            this.groupControl2.Controls.Add(this.lblGrandTotal);
            this.groupControl2.Controls.Add(this.labelControl28);
            this.groupControl2.Controls.Add(this.labelControl29);
            this.groupControl2.Location = new System.Drawing.Point(667, 537);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.ShowCaption = false;
            this.groupControl2.Size = new System.Drawing.Size(562, 100);
            this.groupControl2.TabIndex = 51;
            this.groupControl2.Text = "groupControl2";
            // 
            // lblBillTotal
            // 
            this.lblBillTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillTotal.Appearance.Options.UseFont = true;
            this.lblBillTotal.Appearance.Options.UseTextOptions = true;
            this.lblBillTotal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblBillTotal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblBillTotal.Location = new System.Drawing.Point(430, 30);
            this.lblBillTotal.Name = "lblBillTotal";
            this.lblBillTotal.Size = new System.Drawing.Size(112, 19);
            this.lblBillTotal.TabIndex = 59;
            this.lblBillTotal.Text = "0.00";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Appearance.Options.UseFont = true;
            this.labelControl47.Location = new System.Drawing.Point(323, 28);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(74, 19);
            this.labelControl47.TabIndex = 58;
            this.labelControl47.Text = "Bill Total";
            // 
            // frmCheckOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1232, 642);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.xtraTabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmCheckOut";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frmCheckOut_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabType)).EndInit();
            this.tabType.ResumeLayout(false);
            this.tabRent.ResumeLayout(false);
            this.tabRent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtExKmCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDriverList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDriver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHrs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayRate.Properties)).EndInit();
            this.tabHire.ResumeLayout(false);
            this.tabHire.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbHireDriver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHireCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPick.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkFuel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFuelAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExCharges.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDecor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurODO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVehicleList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLIcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclLicNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclInsNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNxtSrvice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDnic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDlic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDcontact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.LabelControl lblPaid;
        private DevExpress.XtraEditors.LabelControl lblDue;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl lblGrandTotal;
        private DevExpress.XtraEditors.SimpleButton btnProceed;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit txtContact3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtContact2;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.SimpleButton AddCst;
        public DevExpress.XtraEditors.PictureEdit PicCustomer;
        private DevExpress.XtraEditors.LabelControl lblCstID;
        private DevExpress.XtraEditors.LabelControl lblIsBlackListed;
        private System.Windows.Forms.DateTimePicker dtpLicExp;
        private DevExpress.XtraEditors.TextEdit txtContact1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtLIcNo;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNIC;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private System.Windows.Forms.DateTimePicker dtpDlicExp;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.TextEdit txtDnic;
        private DevExpress.XtraEditors.TextEdit txtDrate;
        private DevExpress.XtraEditors.TextEdit txtDlic;
        private DevExpress.XtraEditors.TextEdit txtDcontact;
        private DevExpress.XtraEditors.TextEdit txtDName;
        private DevExpress.XtraEditors.MemoEdit txtDaddress;
        private DevExpress.XtraEditors.Controls.ImageSlider imageSlider1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.DateTimePicker dtpDelDate;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtVhclNo;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtVhclMYeay;
        private DevExpress.XtraEditors.TextEdit txtVhclModel;
        private DevExpress.XtraEditors.TextEdit txtVhclMake;
        private DevExpress.XtraEditors.ComboBoxEdit txtVhclType;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.LabelControl lblVhclID;
        private System.Windows.Forms.DateTimePicker dtpLicExpDate;
        private System.Windows.Forms.DateTimePicker dtpInsExpDate;
        private DevExpress.XtraEditors.TextEdit txtVhclLicNo;
        private DevExpress.XtraEditors.TextEdit txtVhclInsNo;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtVhclNxtSrvice;
        private DevExpress.XtraEditors.TextEdit txtCurODO;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtDisLimit;
        private DevExpress.XtraEditors.ComboBoxEdit cmbVehicleList;
        private DevExpress.XtraEditors.ComboBoxEdit cmbDriverList;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private DevExpress.XtraEditors.TextEdit txtHireCharge;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtTrip;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtDrop;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit txtPick;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtDays;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit txtHrs;
        private DevExpress.XtraEditors.TextEdit txtDayRate;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.CheckEdit chkDriver;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.TextEdit tpStart;
        private DevExpress.XtraEditors.TextEdit tpEnd;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraTab.XtraTabControl tabType;
        private DevExpress.XtraTab.XtraTabPage tabRent;
        private DevExpress.XtraTab.XtraTabPage tabHire;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraEditors.TextEdit txtExCharges;
        private DevExpress.XtraEditors.CheckEdit chkDecor;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent2;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent1;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.TextEdit txtFuelAmount;
        private DevExpress.XtraEditors.CheckEdit chkFuelAmt;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl lblDriverCharge;
        private DevExpress.XtraEditors.LabelControl lblRent;
        private DevExpress.XtraEditors.TextEdit txtFuelLevel;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.MemoEdit txtFName;
        private DevExpress.XtraEditors.MemoEdit txtAddress1;
        private DevExpress.XtraEditors.MemoEdit txtAddress2;
        private DevExpress.XtraEditors.TrackBarControl trkFuel;
        private DevExpress.XtraEditors.LabelControl lblFuelCharge;
        private DevExpress.XtraEditors.LabelControl lblDecor;
        private DevExpress.XtraEditors.LabelControl lblHire;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit txtExKmCharge;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.ComboBoxEdit cmbHireDriver;
        private DevExpress.XtraEditors.LabelControl lblBillTotal;
        private DevExpress.XtraEditors.LabelControl labelControl47;

    }
}