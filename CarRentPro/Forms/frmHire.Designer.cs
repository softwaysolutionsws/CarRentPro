﻿namespace CarRentPro.Forms
{
    partial class frmHire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHire));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.AddCst = new DevExpress.XtraEditors.SimpleButton();
            this.PicCustomer = new DevExpress.XtraEditors.PictureEdit();
            this.lblCstID = new DevExpress.XtraEditors.LabelControl();
            this.lblIsBlackListed = new DevExpress.XtraEditors.LabelControl();
            this.dtpLicExp = new System.Windows.Forms.DateTimePicker();
            this.txtContact1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtLIcNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtFName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtNIC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.dtpLicExpDate = new System.Windows.Forms.DateTimePicker();
            this.dtpInsExpDate = new System.Windows.Forms.DateTimePicker();
            this.txtVhclLicNo = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclInsNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclNxtSrvice = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclCurOdo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lblVhclID = new DevExpress.XtraEditors.LabelControl();
            this.imageSlider1 = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.dtpDelDate = new System.Windows.Forms.DateTimePicker();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclMYeay = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclModel = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclMake = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.txtDaddress = new DevExpress.XtraEditors.MemoEdit();
            this.dtpDlicExp = new System.Windows.Forms.DateTimePicker();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.txtDnic = new DevExpress.XtraEditors.TextEdit();
            this.txtDrate = new DevExpress.XtraEditors.TextEdit();
            this.txtDlic = new DevExpress.XtraEditors.TextEdit();
            this.txtDcontact = new DevExpress.XtraEditors.TextEdit();
            this.txtDName = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.lstPayments = new System.Windows.Forms.ListView();
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txtCurODO = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.cmbVehicleList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnProceed = new DevExpress.XtraEditors.SimpleButton();
            this.cmbDriverList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chkDriver = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtDayRate = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLIcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclLicNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclInsNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNxtSrvice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclCurOdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDnic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDlic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDcontact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDName.Properties)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurODO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVehicleList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDriverList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDriver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayRate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.labelControl12);
            this.splitContainerControl1.Panel2.Controls.Add(this.textEdit1);
            this.splitContainerControl1.Panel2.Controls.Add(this.labelControl20);
            this.splitContainerControl1.Panel2.Controls.Add(this.comboBoxEdit1);
            this.splitContainerControl1.Panel2.Controls.Add(this.labelControl24);
            this.splitContainerControl1.Panel2.Controls.Add(this.txtDayRate);
            this.splitContainerControl1.Panel2.Controls.Add(this.cmbDriverList);
            this.splitContainerControl1.Panel2.Controls.Add(this.chkDriver);
            this.splitContainerControl1.Panel2.Controls.Add(this.btnProceed);
            this.splitContainerControl1.Panel2.Controls.Add(this.labelControl31);
            this.splitContainerControl1.Panel2.Controls.Add(this.txtCurODO);
            this.splitContainerControl1.Panel2.Controls.Add(this.labelControl32);
            this.splitContainerControl1.Panel2.Controls.Add(this.cmbVehicleList);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1255, 566);
            this.splitContainerControl1.SplitterPosition = 663;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl35);
            this.groupControl1.Controls.Add(this.txtContact3);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtContact2);
            this.groupControl1.Controls.Add(this.labelControl34);
            this.groupControl1.Controls.Add(this.AddCst);
            this.groupControl1.Controls.Add(this.PicCustomer);
            this.groupControl1.Controls.Add(this.lblCstID);
            this.groupControl1.Controls.Add(this.lblIsBlackListed);
            this.groupControl1.Controls.Add(this.dtpLicExp);
            this.groupControl1.Controls.Add(this.txtContact1);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtLIcNo);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.txtAddress2);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txtAddress1);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtFName);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtNIC);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(663, 253);
            this.groupControl1.TabIndex = 49;
            this.groupControl1.Text = "Customer Deatils";
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(34, 226);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(57, 13);
            this.labelControl35.TabIndex = 10;
            this.labelControl35.Text = "Contact 03:";
            // 
            // txtContact3
            // 
            this.txtContact3.Location = new System.Drawing.Point(98, 223);
            this.txtContact3.Name = "txtContact3";
            this.txtContact3.Size = new System.Drawing.Size(167, 20);
            this.txtContact3.TabIndex = 9;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(271, 150);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(42, 13);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Ex: Date";
            // 
            // txtContact2
            // 
            this.txtContact2.Location = new System.Drawing.Point(98, 197);
            this.txtContact2.Name = "txtContact2";
            this.txtContact2.Size = new System.Drawing.Size(167, 20);
            this.txtContact2.TabIndex = 8;
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(31, 200);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(60, 13);
            this.labelControl34.TabIndex = 8;
            this.labelControl34.Text = "Contact 02 :";
            // 
            // AddCst
            // 
            this.AddCst.Location = new System.Drawing.Point(257, 45);
            this.AddCst.Name = "AddCst";
            this.AddCst.Size = new System.Drawing.Size(87, 23);
            this.AddCst.TabIndex = 7;
            this.AddCst.Text = "Add Customer";
            // 
            // PicCustomer
            // 
            this.PicCustomer.Cursor = System.Windows.Forms.Cursors.Default;
            this.PicCustomer.Location = new System.Drawing.Point(438, 45);
            this.PicCustomer.Name = "PicCustomer";
            this.PicCustomer.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.PicCustomer.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.PicCustomer.Properties.ZoomAccelerationFactor = 1D;
            this.PicCustomer.Size = new System.Drawing.Size(210, 152);
            this.PicCustomer.TabIndex = 6;
            // 
            // lblCstID
            // 
            this.lblCstID.Location = new System.Drawing.Point(352, 50);
            this.lblCstID.Name = "lblCstID";
            this.lblCstID.Size = new System.Drawing.Size(60, 13);
            this.lblCstID.TabIndex = 5;
            this.lblCstID.Text = "Customer ID";
            // 
            // lblIsBlackListed
            // 
            this.lblIsBlackListed.Appearance.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsBlackListed.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblIsBlackListed.Appearance.Options.UseFont = true;
            this.lblIsBlackListed.Appearance.Options.UseForeColor = true;
            this.lblIsBlackListed.Location = new System.Drawing.Point(69, 17);
            this.lblIsBlackListed.Name = "lblIsBlackListed";
            this.lblIsBlackListed.Size = new System.Drawing.Size(140, 29);
            this.lblIsBlackListed.TabIndex = 4;
            this.lblIsBlackListed.Text = " Is Black Listed";
            // 
            // dtpLicExp
            // 
            this.dtpLicExp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLicExp.Location = new System.Drawing.Point(319, 147);
            this.dtpLicExp.Name = "dtpLicExp";
            this.dtpLicExp.Size = new System.Drawing.Size(105, 21);
            this.dtpLicExp.TabIndex = 6;
            // 
            // txtContact1
            // 
            this.txtContact1.Location = new System.Drawing.Point(98, 170);
            this.txtContact1.Name = "txtContact1";
            this.txtContact1.Size = new System.Drawing.Size(167, 20);
            this.txtContact1.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(34, 173);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 13);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Contact 01 :";
            // 
            // txtLIcNo
            // 
            this.txtLIcNo.Location = new System.Drawing.Point(98, 147);
            this.txtLIcNo.Name = "txtLIcNo";
            this.txtLIcNo.Size = new System.Drawing.Size(167, 20);
            this.txtLIcNo.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(17, 146);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(74, 13);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Driving  Lic No :";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(98, 121);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(334, 20);
            this.txtAddress2.TabIndex = 4;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(39, 124);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(55, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Address 2 :";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(98, 96);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(334, 20);
            this.txtAddress1.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(39, 99);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(55, 13);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Address 1 :";
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(98, 71);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(334, 20);
            this.txtFName.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(60, 74);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(34, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Name :";
            // 
            // txtNIC
            // 
            this.txtNIC.Location = new System.Drawing.Point(98, 47);
            this.txtNIC.Name = "txtNIC";
            this.txtNIC.Size = new System.Drawing.Size(139, 20);
            this.txtNIC.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(69, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(25, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "NIC :";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 253);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(663, 313);
            this.xtraTabControl1.TabIndex = 50;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage4,
            this.xtraTabPage6,
            this.xtraTabPage3});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.PageClient.BackColor = System.Drawing.Color.White;
            this.xtraTabPage2.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage2.Controls.Add(this.dtpLicExpDate);
            this.xtraTabPage2.Controls.Add(this.dtpInsExpDate);
            this.xtraTabPage2.Controls.Add(this.txtVhclLicNo);
            this.xtraTabPage2.Controls.Add(this.txtVhclInsNo);
            this.xtraTabPage2.Controls.Add(this.labelControl18);
            this.xtraTabPage2.Controls.Add(this.txtVhclNxtSrvice);
            this.xtraTabPage2.Controls.Add(this.labelControl16);
            this.xtraTabPage2.Controls.Add(this.txtVhclCurOdo);
            this.xtraTabPage2.Controls.Add(this.labelControl17);
            this.xtraTabPage2.Controls.Add(this.labelControl19);
            this.xtraTabPage2.Controls.Add(this.lblVhclID);
            this.xtraTabPage2.Controls.Add(this.imageSlider1);
            this.xtraTabPage2.Controls.Add(this.labelControl8);
            this.xtraTabPage2.Controls.Add(this.textEdit18);
            this.xtraTabPage2.Controls.Add(this.pictureEdit4);
            this.xtraTabPage2.Controls.Add(this.pictureEdit3);
            this.xtraTabPage2.Controls.Add(this.pictureEdit2);
            this.xtraTabPage2.Controls.Add(this.pictureEdit1);
            this.xtraTabPage2.Controls.Add(this.dtpDelDate);
            this.xtraTabPage2.Controls.Add(this.labelControl9);
            this.xtraTabPage2.Controls.Add(this.labelControl11);
            this.xtraTabPage2.Controls.Add(this.labelControl10);
            this.xtraTabPage2.Controls.Add(this.txtVhclNo);
            this.xtraTabPage2.Controls.Add(this.labelControl13);
            this.xtraTabPage2.Controls.Add(this.labelControl14);
            this.xtraTabPage2.Controls.Add(this.labelControl15);
            this.xtraTabPage2.Controls.Add(this.txtVhclMYeay);
            this.xtraTabPage2.Controls.Add(this.txtVhclModel);
            this.xtraTabPage2.Controls.Add(this.txtVhclMake);
            this.xtraTabPage2.Controls.Add(this.txtVhclType);
            this.xtraTabPage2.Controls.Add(this.simpleButton8);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(567, 307);
            this.xtraTabPage2.Text = "Vehicle Deatils";
            // 
            // dtpLicExpDate
            // 
            this.dtpLicExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLicExpDate.Location = new System.Drawing.Point(340, 281);
            this.dtpLicExpDate.Name = "dtpLicExpDate";
            this.dtpLicExpDate.Size = new System.Drawing.Size(95, 21);
            this.dtpLicExpDate.TabIndex = 88;
            // 
            // dtpInsExpDate
            // 
            this.dtpInsExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInsExpDate.Location = new System.Drawing.Point(340, 250);
            this.dtpInsExpDate.Name = "dtpInsExpDate";
            this.dtpInsExpDate.Size = new System.Drawing.Size(95, 21);
            this.dtpInsExpDate.TabIndex = 85;
            // 
            // txtVhclLicNo
            // 
            this.txtVhclLicNo.Location = new System.Drawing.Point(130, 281);
            this.txtVhclLicNo.Name = "txtVhclLicNo";
            this.txtVhclLicNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclLicNo.TabIndex = 87;
            // 
            // txtVhclInsNo
            // 
            this.txtVhclInsNo.Location = new System.Drawing.Point(130, 251);
            this.txtVhclInsNo.Name = "txtVhclInsNo";
            this.txtVhclInsNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclInsNo.TabIndex = 86;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(66, 287);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(58, 13);
            this.labelControl18.TabIndex = 84;
            this.labelControl18.Text = "License No :";
            // 
            // txtVhclNxtSrvice
            // 
            this.txtVhclNxtSrvice.Location = new System.Drawing.Point(130, 224);
            this.txtVhclNxtSrvice.Name = "txtVhclNxtSrvice";
            this.txtVhclNxtSrvice.Size = new System.Drawing.Size(193, 20);
            this.txtVhclNxtSrvice.TabIndex = 83;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(12, 199);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(112, 13);
            this.labelControl16.TabIndex = 79;
            this.labelControl16.Text = "Current ODO Reading :";
            // 
            // txtVhclCurOdo
            // 
            this.txtVhclCurOdo.Location = new System.Drawing.Point(130, 198);
            this.txtVhclCurOdo.Name = "txtVhclCurOdo";
            this.txtVhclCurOdo.Size = new System.Drawing.Size(139, 20);
            this.txtVhclCurOdo.TabIndex = 80;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(56, 227);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(68, 13);
            this.labelControl17.TabIndex = 81;
            this.labelControl17.Text = "Next Service :";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(53, 254);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(71, 13);
            this.labelControl19.TabIndex = 82;
            this.labelControl19.Text = "Insurance No :";
            // 
            // lblVhclID
            // 
            this.lblVhclID.Appearance.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVhclID.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblVhclID.Appearance.Options.UseFont = true;
            this.lblVhclID.Appearance.Options.UseForeColor = true;
            this.lblVhclID.Location = new System.Drawing.Point(460, 186);
            this.lblVhclID.Name = "lblVhclID";
            this.lblVhclID.Size = new System.Drawing.Size(54, 15);
            this.lblVhclID.TabIndex = 78;
            this.lblVhclID.Text = "Vehicle ID";
            // 
            // imageSlider1
            // 
            this.imageSlider1.Cursor = System.Windows.Forms.Cursors.Default;
            this.imageSlider1.Location = new System.Drawing.Point(280, 15);
            this.imageSlider1.Name = "imageSlider1";
            this.imageSlider1.Size = new System.Drawing.Size(234, 168);
            this.imageSlider1.TabIndex = 66;
            this.imageSlider1.Text = "imageSlider1";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(38, 145);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(38, 13);
            this.labelControl8.TabIndex = 75;
            this.labelControl8.Text = "Colour :";
            // 
            // textEdit18
            // 
            this.textEdit18.Location = new System.Drawing.Point(82, 142);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit18.Size = new System.Drawing.Size(193, 20);
            this.textEdit18.TabIndex = 76;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit4.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit4.Location = new System.Drawing.Point(354, 58);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit4.Properties.InitialImage")));
            this.pictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit4.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit4.Size = new System.Drawing.Size(127, 104);
            this.pictureEdit4.TabIndex = 74;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit3.Location = new System.Drawing.Point(319, 45);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit3.Properties.InitialImage")));
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit3.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit3.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit3.TabIndex = 73;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit2.Location = new System.Drawing.Point(308, 30);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit2.Properties.InitialImage")));
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit2.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit2.TabIndex = 72;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit1.Location = new System.Drawing.Point(289, 26);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit1.Properties.InitialImage")));
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit1.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit1.TabIndex = 71;
            // 
            // dtpDelDate
            // 
            this.dtpDelDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDelDate.Location = new System.Drawing.Point(181, 165);
            this.dtpDelDate.Name = "dtpDelDate";
            this.dtpDelDate.Size = new System.Drawing.Size(93, 21);
            this.dtpDelDate.TabIndex = 70;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(82, 168);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(78, 13);
            this.labelControl9.TabIndex = 67;
            this.labelControl9.Text = "Delivered Date :";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(45, 71);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(31, 13);
            this.labelControl11.TabIndex = 68;
            this.labelControl11.Text = "Type :";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(56, 18);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(20, 13);
            this.labelControl10.TabIndex = 61;
            this.labelControl10.Text = "No :";
            // 
            // txtVhclNo
            // 
            this.txtVhclNo.Location = new System.Drawing.Point(81, 15);
            this.txtVhclNo.Name = "txtVhclNo";
            this.txtVhclNo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclNo.TabIndex = 65;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(44, 45);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(32, 13);
            this.labelControl13.TabIndex = 60;
            this.labelControl13.Text = "Make :";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(41, 96);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(35, 13);
            this.labelControl14.TabIndex = 59;
            this.labelControl14.Text = "Model :";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(18, 122);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(58, 13);
            this.labelControl15.TabIndex = 58;
            this.labelControl15.Text = "Made Year :";
            // 
            // txtVhclMYeay
            // 
            this.txtVhclMYeay.Location = new System.Drawing.Point(82, 119);
            this.txtVhclMYeay.Name = "txtVhclMYeay";
            this.txtVhclMYeay.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclMYeay.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMYeay.TabIndex = 62;
            // 
            // txtVhclModel
            // 
            this.txtVhclModel.Location = new System.Drawing.Point(82, 93);
            this.txtVhclModel.Name = "txtVhclModel";
            this.txtVhclModel.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclModel.Size = new System.Drawing.Size(193, 20);
            this.txtVhclModel.TabIndex = 63;
            // 
            // txtVhclMake
            // 
            this.txtVhclMake.Location = new System.Drawing.Point(82, 42);
            this.txtVhclMake.Name = "txtVhclMake";
            this.txtVhclMake.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclMake.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMake.TabIndex = 64;
            // 
            // txtVhclType
            // 
            this.txtVhclType.Location = new System.Drawing.Point(82, 68);
            this.txtVhclType.Name = "txtVhclType";
            this.txtVhclType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtVhclType.Properties.Items.AddRange(new object[] {
            "Car",
            "Van",
            "Bike"});
            this.txtVhclType.Size = new System.Drawing.Size(193, 20);
            this.txtVhclType.TabIndex = 69;
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(354, 146);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(62, 23);
            this.simpleButton8.TabIndex = 77;
            this.simpleButton8.Text = "Test";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.txtDaddress);
            this.xtraTabPage4.Controls.Add(this.dtpDlicExp);
            this.xtraTabPage4.Controls.Add(this.labelControl36);
            this.xtraTabPage4.Controls.Add(this.labelControl37);
            this.xtraTabPage4.Controls.Add(this.labelControl38);
            this.xtraTabPage4.Controls.Add(this.labelControl39);
            this.xtraTabPage4.Controls.Add(this.labelControl40);
            this.xtraTabPage4.Controls.Add(this.labelControl41);
            this.xtraTabPage4.Controls.Add(this.labelControl42);
            this.xtraTabPage4.Controls.Add(this.txtDnic);
            this.xtraTabPage4.Controls.Add(this.txtDrate);
            this.xtraTabPage4.Controls.Add(this.txtDlic);
            this.xtraTabPage4.Controls.Add(this.txtDcontact);
            this.xtraTabPage4.Controls.Add(this.txtDName);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(561, 348);
            this.xtraTabPage4.Text = "Driver Deatils";
            // 
            // txtDaddress
            // 
            this.txtDaddress.Location = new System.Drawing.Point(92, 37);
            this.txtDaddress.Name = "txtDaddress";
            this.txtDaddress.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDaddress.Size = new System.Drawing.Size(304, 46);
            this.txtDaddress.TabIndex = 67;
            // 
            // dtpDlicExp
            // 
            this.dtpDlicExp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDlicExp.Location = new System.Drawing.Point(291, 140);
            this.dtpDlicExp.Name = "dtpDlicExp";
            this.dtpDlicExp.Size = new System.Drawing.Size(105, 21);
            this.dtpDlicExp.TabIndex = 66;
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Appearance.Options.UseFont = true;
            this.labelControl36.Location = new System.Drawing.Point(316, 124);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(42, 14);
            this.labelControl36.TabIndex = 62;
            this.labelControl36.Text = "Ex: Date";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.Location = new System.Drawing.Point(12, 120);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(43, 14);
            this.labelControl37.TabIndex = 63;
            this.labelControl37.Text = "Contact :";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Appearance.Options.UseFont = true;
            this.labelControl38.Location = new System.Drawing.Point(12, 146);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(78, 14);
            this.labelControl38.TabIndex = 64;
            this.labelControl38.Text = "Driving  Lic No :";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Appearance.Options.UseFont = true;
            this.labelControl39.Location = new System.Drawing.Point(12, 96);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(23, 14);
            this.labelControl39.TabIndex = 61;
            this.labelControl39.Text = "NIC :";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Appearance.Options.UseFont = true;
            this.labelControl40.Location = new System.Drawing.Point(12, 175);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(70, 14);
            this.labelControl40.TabIndex = 60;
            this.labelControl40.Text = "Rste Per Day :";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Location = new System.Drawing.Point(12, 39);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(45, 14);
            this.labelControl41.TabIndex = 59;
            this.labelControl41.Text = "Address :";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.Location = new System.Drawing.Point(12, 13);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(36, 14);
            this.labelControl42.TabIndex = 58;
            this.labelControl42.Text = "Name :";
            // 
            // txtDnic
            // 
            this.txtDnic.Location = new System.Drawing.Point(92, 89);
            this.txtDnic.Name = "txtDnic";
            this.txtDnic.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDnic.Properties.Appearance.Options.UseFont = true;
            this.txtDnic.Size = new System.Drawing.Size(183, 20);
            this.txtDnic.TabIndex = 57;
            // 
            // txtDrate
            // 
            this.txtDrate.Location = new System.Drawing.Point(92, 172);
            this.txtDrate.Name = "txtDrate";
            this.txtDrate.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDrate.Properties.Appearance.Options.UseFont = true;
            this.txtDrate.Size = new System.Drawing.Size(103, 20);
            this.txtDrate.TabIndex = 56;
            // 
            // txtDlic
            // 
            this.txtDlic.Location = new System.Drawing.Point(92, 143);
            this.txtDlic.Name = "txtDlic";
            this.txtDlic.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDlic.Properties.Appearance.Options.UseFont = true;
            this.txtDlic.Size = new System.Drawing.Size(183, 20);
            this.txtDlic.TabIndex = 55;
            // 
            // txtDcontact
            // 
            this.txtDcontact.Location = new System.Drawing.Point(92, 117);
            this.txtDcontact.Name = "txtDcontact";
            this.txtDcontact.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDcontact.Properties.Appearance.Options.UseFont = true;
            this.txtDcontact.Size = new System.Drawing.Size(183, 20);
            this.txtDcontact.TabIndex = 54;
            // 
            // txtDName
            // 
            this.txtDName.Location = new System.Drawing.Point(92, 10);
            this.txtDName.Name = "txtDName";
            this.txtDName.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDName.Properties.Appearance.Options.UseFont = true;
            this.txtDName.Size = new System.Drawing.Size(304, 20);
            this.txtDName.TabIndex = 53;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.lstPayments);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(561, 348);
            this.xtraTabPage6.Text = "Payment deatils";
            // 
            // lstPayments
            // 
            this.lstPayments.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader16,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader1});
            this.lstPayments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstPayments.FullRowSelect = true;
            this.lstPayments.Location = new System.Drawing.Point(0, 0);
            this.lstPayments.Name = "lstPayments";
            this.lstPayments.Size = new System.Drawing.Size(561, 348);
            this.lstPayments.TabIndex = 23;
            this.lstPayments.UseCompatibleStateImageBehavior = false;
            this.lstPayments.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Date";
            this.columnHeader16.Width = 80;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Pay Method";
            this.columnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader12.Width = 100;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Card / Bank";
            this.columnHeader13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader13.Width = 75;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Ref ID";
            this.columnHeader14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader14.Width = 75;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Chq Date";
            this.columnHeader15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader15.Width = 140;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Amount";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(561, 348);
            this.xtraTabPage3.Text = "Rate Deatils";
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(24, 87);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(112, 13);
            this.labelControl31.TabIndex = 52;
            this.labelControl31.Text = "Current ODO Reading :";
            // 
            // txtCurODO
            // 
            this.txtCurODO.Location = new System.Drawing.Point(142, 84);
            this.txtCurODO.Name = "txtCurODO";
            this.txtCurODO.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCurODO.Size = new System.Drawing.Size(103, 20);
            this.txtCurODO.TabIndex = 51;
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(24, 17);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(56, 13);
            this.labelControl32.TabIndex = 49;
            this.labelControl32.Text = "Vehicle No :";
            // 
            // cmbVehicleList
            // 
            this.cmbVehicleList.Location = new System.Drawing.Point(86, 14);
            this.cmbVehicleList.Name = "cmbVehicleList";
            this.cmbVehicleList.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVehicleList.Properties.Appearance.Options.UseFont = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVehicleList.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cmbVehicleList.Properties.AutoComplete = false;
            this.cmbVehicleList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbVehicleList.Properties.DropDownRows = 20;
            this.cmbVehicleList.Properties.Sorted = true;
            this.cmbVehicleList.Size = new System.Drawing.Size(159, 20);
            this.cmbVehicleList.TabIndex = 50;
            // 
            // btnProceed
            // 
            this.btnProceed.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnProceed.ImageOptions.Image")));
            this.btnProceed.Location = new System.Drawing.Point(359, 455);
            this.btnProceed.Name = "btnProceed";
            this.btnProceed.Size = new System.Drawing.Size(101, 44);
            this.btnProceed.TabIndex = 58;
            this.btnProceed.Text = "Proceed";
            // 
            // cmbDriverList
            // 
            this.cmbDriverList.Location = new System.Drawing.Point(168, 112);
            this.cmbDriverList.Name = "cmbDriverList";
            this.cmbDriverList.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDriverList.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbDriverList.Properties.AutoComplete = false;
            this.cmbDriverList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDriverList.Properties.DropDownRows = 10;
            this.cmbDriverList.Properties.Sorted = true;
            this.cmbDriverList.Size = new System.Drawing.Size(193, 20);
            this.cmbDriverList.TabIndex = 60;
            // 
            // chkDriver
            // 
            this.chkDriver.Location = new System.Drawing.Point(21, 112);
            this.chkDriver.Name = "chkDriver";
            this.chkDriver.Properties.Caption = "Driver/ Charge Rate (Rs.)";
            this.chkDriver.Size = new System.Drawing.Size(145, 19);
            this.chkDriver.TabIndex = 59;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(65, 180);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(112, 13);
            this.labelControl12.TabIndex = 66;
            this.labelControl12.Text = "Current ODO Reading :";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(183, 177);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textEdit1.Size = new System.Drawing.Size(103, 20);
            this.textEdit1.TabIndex = 65;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(65, 151);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(56, 13);
            this.labelControl20.TabIndex = 63;
            this.labelControl20.Text = "Vehicle No :";
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(127, 148);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit1.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.comboBoxEdit1.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.comboBoxEdit1.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.comboBoxEdit1.Properties.AutoComplete = false;
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.DropDownRows = 20;
            this.comboBoxEdit1.Properties.Sorted = true;
            this.comboBoxEdit1.Size = new System.Drawing.Size(159, 20);
            this.comboBoxEdit1.TabIndex = 64;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(313, 233);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(50, 13);
            this.labelControl24.TabIndex = 62;
            this.labelControl24.Text = "Rate (Rs.)";
            // 
            // txtDayRate
            // 
            this.txtDayRate.Location = new System.Drawing.Point(371, 226);
            this.txtDayRate.Name = "txtDayRate";
            this.txtDayRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDayRate.Size = new System.Drawing.Size(124, 20);
            this.txtDayRate.TabIndex = 61;
            // 
            // frmHire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1255, 566);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "frmHire";
            this.Text = "Hire Now";
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLIcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclLicNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclInsNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNxtSrvice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclCurOdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDnic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDlic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDcontact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDName.Properties)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCurODO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVehicleList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDriverList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDriver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayRate.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit txtContact3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtContact2;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.SimpleButton AddCst;
        public DevExpress.XtraEditors.PictureEdit PicCustomer;
        private DevExpress.XtraEditors.LabelControl lblCstID;
        private DevExpress.XtraEditors.LabelControl lblIsBlackListed;
        private System.Windows.Forms.DateTimePicker dtpLicExp;
        private DevExpress.XtraEditors.TextEdit txtContact1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtLIcNo;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtAddress2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtAddress1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtFName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNIC;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private System.Windows.Forms.DateTimePicker dtpLicExpDate;
        private System.Windows.Forms.DateTimePicker dtpInsExpDate;
        private DevExpress.XtraEditors.TextEdit txtVhclLicNo;
        private DevExpress.XtraEditors.TextEdit txtVhclInsNo;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtVhclNxtSrvice;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtVhclCurOdo;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl lblVhclID;
        private DevExpress.XtraEditors.Controls.ImageSlider imageSlider1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.DateTimePicker dtpDelDate;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtVhclNo;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtVhclMYeay;
        private DevExpress.XtraEditors.TextEdit txtVhclModel;
        private DevExpress.XtraEditors.TextEdit txtVhclMake;
        private DevExpress.XtraEditors.ComboBoxEdit txtVhclType;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.MemoEdit txtDaddress;
        private System.Windows.Forms.DateTimePicker dtpDlicExp;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.TextEdit txtDnic;
        private DevExpress.XtraEditors.TextEdit txtDrate;
        private DevExpress.XtraEditors.TextEdit txtDlic;
        private DevExpress.XtraEditors.TextEdit txtDcontact;
        private DevExpress.XtraEditors.TextEdit txtDName;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private System.Windows.Forms.ListView lstPayments;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit txtCurODO;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.ComboBoxEdit cmbVehicleList;
        private DevExpress.XtraEditors.SimpleButton btnProceed;
        private DevExpress.XtraEditors.ComboBoxEdit cmbDriverList;
        private DevExpress.XtraEditors.CheckEdit chkDriver;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtDayRate;
    }
}