﻿namespace CarRentPro.Forms
{
    partial class frmShowRentRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmShowRentRecord));
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.vGridBill = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords4 = new CarRentPro.DsRecords();
            this.rowbillID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbilldate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillTotal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillDue = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillPayed = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.vGridRent = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords3 = new CarRentPro.DsRecords();
            this.rowchekoutID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDriver = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDriverNIC = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDriverName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDrivaerRate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDriverCharge = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowStartDateTime = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowEndDatetime = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDays = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowHours = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLimitKm = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRatePerKm = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDecoRate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFuelLevel = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFuelAmount = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRatePerExKm = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrandTotal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.vGridVehicle = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords2 = new CarRentPro.DsRecords();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.vGridCustomer = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords1 = new CarRentPro.DsRecords();
            this.rowNIC = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAddress1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPic = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.rowcurODO = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclNo = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclMake = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlModel = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlNextService = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclInsExpDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlLicExpDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowImage1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnOUT = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridRent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.vGridBill);
            this.groupControl4.Location = new System.Drawing.Point(265, 345);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(247, 122);
            this.groupControl4.TabIndex = 3;
            this.groupControl4.Text = "Bill Details";
            // 
            // vGridBill
            // 
            this.vGridBill.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(160)))));
            this.vGridBill.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(160)))));
            this.vGridBill.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridBill.Appearance.Category.ForeColor = System.Drawing.Color.Black;
            this.vGridBill.Appearance.Category.Options.UseBackColor = true;
            this.vGridBill.Appearance.Category.Options.UseBorderColor = true;
            this.vGridBill.Appearance.Category.Options.UseFont = true;
            this.vGridBill.Appearance.Category.Options.UseForeColor = true;
            this.vGridBill.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(160)))));
            this.vGridBill.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(160)))));
            this.vGridBill.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridBill.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridBill.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridBill.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridBill.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(180)))));
            this.vGridBill.Appearance.Empty.Options.UseBackColor = true;
            this.vGridBill.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(231)))), ((int)(((byte)(184)))));
            this.vGridBill.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(231)))), ((int)(((byte)(184)))));
            this.vGridBill.Appearance.ExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridBill.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridBill.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridBill.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridBill.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.vGridBill.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridBill.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(50)))));
            this.vGridBill.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Olive;
            this.vGridBill.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridBill.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.vGridBill.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridBill.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridBill.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray;
            this.vGridBill.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.vGridBill.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridBill.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridBill.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(160)))));
            this.vGridBill.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridBill.Appearance.RecordValue.BackColor = System.Drawing.Color.White;
            this.vGridBill.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridBill.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridBill.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridBill.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(65)))));
            this.vGridBill.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.White;
            this.vGridBill.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridBill.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridBill.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(160)))));
            this.vGridBill.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridBill.DataMember = "DTrentData";
            this.vGridBill.DataSource = this.dsRecords4;
            this.vGridBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridBill.Location = new System.Drawing.Point(2, 20);
            this.vGridBill.Name = "vGridBill";
            this.vGridBill.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowbillID,
            this.rowbilldate,
            this.rowbillTotal,
            this.rowbillDue,
            this.rowbillPayed});
            this.vGridBill.Size = new System.Drawing.Size(243, 100);
            this.vGridBill.TabIndex = 0;
            // 
            // dsRecords4
            // 
            this.dsRecords4.DataSetName = "DsRecords";
            this.dsRecords4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowbillID
            // 
            this.rowbillID.Name = "rowbillID";
            this.rowbillID.Properties.Caption = "bill ID";
            this.rowbillID.Properties.FieldName = "billID";
            // 
            // rowbilldate
            // 
            this.rowbilldate.Name = "rowbilldate";
            this.rowbilldate.Properties.Caption = "billdate";
            this.rowbilldate.Properties.FieldName = "billdate";
            // 
            // rowbillTotal
            // 
            this.rowbillTotal.Name = "rowbillTotal";
            this.rowbillTotal.Properties.Caption = "bill Total";
            this.rowbillTotal.Properties.FieldName = "billTotal";
            // 
            // rowbillDue
            // 
            this.rowbillDue.Name = "rowbillDue";
            this.rowbillDue.Properties.Caption = "bill Due";
            this.rowbillDue.Properties.FieldName = "billDue";
            // 
            // rowbillPayed
            // 
            this.rowbillPayed.Name = "rowbillPayed";
            this.rowbillPayed.Properties.Caption = "bill Payed";
            this.rowbillPayed.Properties.FieldName = "billPayed";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.vGridRent);
            this.groupControl3.Location = new System.Drawing.Point(263, 10);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(247, 329);
            this.groupControl3.TabIndex = 4;
            this.groupControl3.Text = "Rent Details";
            // 
            // vGridRent
            // 
            this.vGridRent.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridRent.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridRent.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridRent.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridRent.Appearance.Category.Options.UseBackColor = true;
            this.vGridRent.Appearance.Category.Options.UseBorderColor = true;
            this.vGridRent.Appearance.Category.Options.UseFont = true;
            this.vGridRent.Appearance.Category.Options.UseForeColor = true;
            this.vGridRent.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridRent.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridRent.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridRent.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridRent.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridRent.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridRent.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridRent.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridRent.Appearance.Empty.Options.UseBackColor = true;
            this.vGridRent.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridRent.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridRent.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridRent.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridRent.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridRent.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridRent.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridRent.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridRent.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridRent.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridRent.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridRent.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridRent.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridRent.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridRent.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridRent.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridRent.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridRent.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridRent.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridRent.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridRent.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridRent.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridRent.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridRent.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridRent.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridRent.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridRent.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridRent.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridRent.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridRent.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridRent.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridRent.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridRent.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridRent.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridRent.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.vGridRent.DataMember = "DTrentData";
            this.vGridRent.DataSource = this.dsRecords3;
            this.vGridRent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridRent.Location = new System.Drawing.Point(2, 20);
            this.vGridRent.Name = "vGridRent";
            this.vGridRent.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowchekoutID,
            this.rowStartDateTime,
            this.rowEndDatetime,
            this.rowDays,
            this.rowHours,
            this.rowDriver,
            this.rowDriverNIC,
            this.rowDriverName,
            this.rowDrivaerRate,
            this.rowDriverCharge,
            this.rowLimitKm,
            this.rowRatePerKm,
            this.rowDecoRate,
            this.rowFuelLevel,
            this.rowFuelAmount,
            this.rowRatePerExKm,
            this.rowGrandTotal});
            this.vGridRent.Size = new System.Drawing.Size(243, 307);
            this.vGridRent.TabIndex = 0;
            // 
            // dsRecords3
            // 
            this.dsRecords3.DataSetName = "DsRecords";
            this.dsRecords3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowchekoutID
            // 
            this.rowchekoutID.Name = "rowchekoutID";
            this.rowchekoutID.Properties.Caption = "Out ID";
            this.rowchekoutID.Properties.FieldName = "chekoutID";
            // 
            // rowDriver
            // 
            this.rowDriver.Name = "rowDriver";
            this.rowDriver.Properties.Caption = "Driver";
            this.rowDriver.Properties.FieldName = "Driver";
            // 
            // rowDriverNIC
            // 
            this.rowDriverNIC.Name = "rowDriverNIC";
            this.rowDriverNIC.Properties.Caption = "Driver NIC";
            this.rowDriverNIC.Properties.FieldName = "DriverNIC";
            // 
            // rowDriverName
            // 
            this.rowDriverName.Name = "rowDriverName";
            this.rowDriverName.Properties.Caption = "Driver Name";
            this.rowDriverName.Properties.FieldName = "DriverName";
            // 
            // rowDrivaerRate
            // 
            this.rowDrivaerRate.Name = "rowDrivaerRate";
            this.rowDrivaerRate.Properties.Caption = "Drivaer Rate";
            this.rowDrivaerRate.Properties.FieldName = "DrivaerRate";
            // 
            // rowDriverCharge
            // 
            this.rowDriverCharge.Name = "rowDriverCharge";
            this.rowDriverCharge.Properties.Caption = "Driver Charge";
            this.rowDriverCharge.Properties.FieldName = "DriverCharge";
            // 
            // rowStartDateTime
            // 
            this.rowStartDateTime.Name = "rowStartDateTime";
            this.rowStartDateTime.Properties.Caption = "Start";
            this.rowStartDateTime.Properties.FieldName = "StartDateTime";
            // 
            // rowEndDatetime
            // 
            this.rowEndDatetime.Name = "rowEndDatetime";
            this.rowEndDatetime.Properties.Caption = "End";
            this.rowEndDatetime.Properties.FieldName = "EndDatetime";
            // 
            // rowDays
            // 
            this.rowDays.Name = "rowDays";
            this.rowDays.Properties.Caption = "Days";
            this.rowDays.Properties.FieldName = "Days";
            // 
            // rowHours
            // 
            this.rowHours.Name = "rowHours";
            this.rowHours.Properties.Caption = "Hours";
            this.rowHours.Properties.FieldName = "Hours";
            // 
            // rowLimitKm
            // 
            this.rowLimitKm.Name = "rowLimitKm";
            this.rowLimitKm.Properties.Caption = "Free Km";
            this.rowLimitKm.Properties.FieldName = "LimitKm";
            // 
            // rowRatePerKm
            // 
            this.rowRatePerKm.Name = "rowRatePerKm";
            this.rowRatePerKm.Properties.Caption = "Rate";
            this.rowRatePerKm.Properties.FieldName = "RatePerKm";
            // 
            // rowDecoRate
            // 
            this.rowDecoRate.Name = "rowDecoRate";
            this.rowDecoRate.Properties.Caption = "Decor Charges";
            this.rowDecoRate.Properties.FieldName = "DecoRate";
            // 
            // rowFuelLevel
            // 
            this.rowFuelLevel.Name = "rowFuelLevel";
            this.rowFuelLevel.Properties.Caption = "Fuel Level";
            this.rowFuelLevel.Properties.FieldName = "FuelLevel";
            // 
            // rowFuelAmount
            // 
            this.rowFuelAmount.Name = "rowFuelAmount";
            this.rowFuelAmount.Properties.Caption = "Fuel Amount";
            this.rowFuelAmount.Properties.FieldName = "FuelAmount";
            // 
            // rowRatePerExKm
            // 
            this.rowRatePerExKm.Name = "rowRatePerExKm";
            this.rowRatePerExKm.Properties.Caption = "Rs. Per Ex Km";
            this.rowRatePerExKm.Properties.FieldName = "RatePerExKm";
            // 
            // rowGrandTotal
            // 
            this.rowGrandTotal.Name = "rowGrandTotal";
            this.rowGrandTotal.Properties.Caption = "Grand Total";
            this.rowGrandTotal.Properties.FieldName = "GrandTotal";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.vGridVehicle);
            this.groupControl2.Location = new System.Drawing.Point(10, 286);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(247, 248);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Vehicle Details";
            // 
            // vGridVehicle
            // 
            this.vGridVehicle.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(166)))), ((int)(((byte)(93)))));
            this.vGridVehicle.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(169)))), ((int)(((byte)(107)))));
            this.vGridVehicle.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridVehicle.Appearance.Category.ForeColor = System.Drawing.Color.White;
            this.vGridVehicle.Appearance.Category.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.Category.Options.UseBorderColor = true;
            this.vGridVehicle.Appearance.Category.Options.UseFont = true;
            this.vGridVehicle.Appearance.Category.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.vGridVehicle.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(143)))), ((int)(((byte)(62)))));
            this.vGridVehicle.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridVehicle.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.vGridVehicle.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridVehicle.Appearance.Empty.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(170)))), ((int)(((byte)(111)))));
            this.vGridVehicle.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(170)))), ((int)(((byte)(111)))));
            this.vGridVehicle.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridVehicle.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridVehicle.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicle.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(227)))));
            this.vGridVehicle.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(136)))), ((int)(((byte)(91)))));
            this.vGridVehicle.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridVehicle.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(172)))), ((int)(((byte)(134)))));
            this.vGridVehicle.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(234)))), ((int)(((byte)(216)))));
            this.vGridVehicle.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(169)))), ((int)(((byte)(107)))));
            this.vGridVehicle.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(219)))));
            this.vGridVehicle.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicle.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.vGridVehicle.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(189)))), ((int)(((byte)(125)))));
            this.vGridVehicle.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicle.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridVehicle.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridVehicle.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridVehicle.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(169)))), ((int)(((byte)(107)))));
            this.vGridVehicle.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridVehicle.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.vGridVehicle.DataMember = "DTrentData";
            this.vGridVehicle.DataSource = this.dsRecords2;
            this.vGridVehicle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridVehicle.Location = new System.Drawing.Point(2, 20);
            this.vGridVehicle.Name = "vGridVehicle";
            this.vGridVehicle.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowvhclNo,
            this.rowvhclMake,
            this.rowvhclType,
            this.rowvchlModel,
            this.rowcurODO,
            this.rowvchlNextService,
            this.rowvhclInsExpDate,
            this.rowvchlLicExpDate,
            this.rowImage1});
            this.vGridVehicle.Size = new System.Drawing.Size(243, 226);
            this.vGridVehicle.TabIndex = 0;
            // 
            // dsRecords2
            // 
            this.dsRecords2.DataSetName = "DsRecords";
            this.dsRecords2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.vGridCustomer);
            this.groupControl1.Location = new System.Drawing.Point(12, 11);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(247, 269);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Customer Details";
            // 
            // vGridCustomer
            // 
            this.vGridCustomer.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(57)))));
            this.vGridCustomer.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.vGridCustomer.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridCustomer.Appearance.Category.ForeColor = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.Category.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.Category.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.Category.Options.UseFont = true;
            this.vGridCustomer.Appearance.Category.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.vGridCustomer.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.vGridCustomer.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.vGridCustomer.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.Empty.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(193)))), ((int)(((byte)(74)))));
            this.vGridCustomer.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(193)))), ((int)(((byte)(74)))));
            this.vGridCustomer.Appearance.ExpandButton.ForeColor = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridCustomer.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(249)))), ((int)(((byte)(236)))));
            this.vGridCustomer.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(114)))), ((int)(((byte)(50)))));
            this.vGridCustomer.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(114)))), ((int)(((byte)(50)))));
            this.vGridCustomer.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(158)))), ((int)(((byte)(64)))));
            this.vGridCustomer.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.vGridCustomer.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.vGridCustomer.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(247)))), ((int)(((byte)(230)))));
            this.vGridCustomer.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridCustomer.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.vGridCustomer.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.vGridCustomer.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.White;
            this.vGridCustomer.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridCustomer.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridCustomer.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridCustomer.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.vGridCustomer.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridCustomer.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.vGridCustomer.DataMember = "DTrentData";
            this.vGridCustomer.DataSource = this.dsRecords1;
            this.vGridCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridCustomer.Location = new System.Drawing.Point(2, 20);
            this.vGridCustomer.Name = "vGridCustomer";
            this.vGridCustomer.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowNIC,
            this.rowFName,
            this.rowAddress1,
            this.rowContact1,
            this.rowContact2,
            this.rowContact3,
            this.rowPic});
            this.vGridCustomer.Size = new System.Drawing.Size(243, 247);
            this.vGridCustomer.TabIndex = 0;
            // 
            // dsRecords1
            // 
            this.dsRecords1.DataSetName = "DsRecords";
            this.dsRecords1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowNIC
            // 
            this.rowNIC.Name = "rowNIC";
            this.rowNIC.Properties.Caption = "NIC";
            this.rowNIC.Properties.FieldName = "NIC";
            // 
            // rowFName
            // 
            this.rowFName.Name = "rowFName";
            this.rowFName.Properties.Caption = "Name";
            this.rowFName.Properties.FieldName = "FName";
            // 
            // rowAddress1
            // 
            this.rowAddress1.Height = 50;
            this.rowAddress1.Name = "rowAddress1";
            this.rowAddress1.Properties.Caption = "Address";
            this.rowAddress1.Properties.FieldName = "Address1";
            // 
            // rowContact1
            // 
            this.rowContact1.Name = "rowContact1";
            this.rowContact1.Properties.Caption = "Contact 1";
            this.rowContact1.Properties.FieldName = "Contact1";
            // 
            // rowContact2
            // 
            this.rowContact2.Name = "rowContact2";
            this.rowContact2.Properties.Caption = "Contact 2";
            this.rowContact2.Properties.FieldName = "Contact2";
            // 
            // rowContact3
            // 
            this.rowContact3.Name = "rowContact3";
            this.rowContact3.Properties.Caption = "Contact 3";
            this.rowContact3.Properties.FieldName = "Contact3";
            // 
            // rowPic
            // 
            this.rowPic.Height = 90;
            this.rowPic.Name = "rowPic";
            this.rowPic.Properties.Caption = "Photo";
            this.rowPic.Properties.FieldName = "Pic";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(265, 473);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(245, 61);
            this.txtNote.TabIndex = 13;
            // 
            // rowcurODO
            // 
            this.rowcurODO.Name = "rowcurODO";
            this.rowcurODO.Properties.Caption = "ODO Reading";
            this.rowcurODO.Properties.FieldName = "curODO";
            // 
            // rowvhclNo
            // 
            this.rowvhclNo.Name = "rowvhclNo";
            this.rowvhclNo.Properties.Caption = "Vehicle NO";
            this.rowvhclNo.Properties.FieldName = "vhclNo";
            // 
            // rowvhclMake
            // 
            this.rowvhclMake.Name = "rowvhclMake";
            this.rowvhclMake.Properties.Caption = "Make";
            this.rowvhclMake.Properties.FieldName = "vhclMake";
            // 
            // rowvhclType
            // 
            this.rowvhclType.Name = "rowvhclType";
            this.rowvhclType.Properties.Caption = "Type";
            this.rowvhclType.Properties.FieldName = "vhclType";
            // 
            // rowvchlModel
            // 
            this.rowvchlModel.Name = "rowvchlModel";
            this.rowvchlModel.Properties.Caption = "Model";
            this.rowvchlModel.Properties.FieldName = "vchlModel";
            // 
            // rowvchlNextService
            // 
            this.rowvchlNextService.Name = "rowvchlNextService";
            this.rowvchlNextService.Properties.Caption = "Next Service";
            this.rowvchlNextService.Properties.FieldName = "vchlNextService";
            // 
            // rowvhclInsExpDate
            // 
            this.rowvhclInsExpDate.Name = "rowvhclInsExpDate";
            this.rowvhclInsExpDate.Properties.Caption = "Ins Exp Date";
            this.rowvhclInsExpDate.Properties.FieldName = "vhclInsExpDate";
            // 
            // rowvchlLicExpDate
            // 
            this.rowvchlLicExpDate.Name = "rowvchlLicExpDate";
            this.rowvchlLicExpDate.Properties.Caption = "Lic Exp Date";
            this.rowvchlLicExpDate.Properties.FieldName = "vchlLicExpDate";
            // 
            // rowImage1
            // 
            this.rowImage1.Height = 77;
            this.rowImage1.Name = "rowImage1";
            this.rowImage1.Properties.Caption = "Photo";
            this.rowImage1.Properties.FieldName = "Image1";
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(516, 77);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(120, 40);
            this.simpleButton2.TabIndex = 17;
            this.simpleButton2.Text = "Pay";
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(516, 141);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(120, 40);
            this.simpleButton1.TabIndex = 16;
            this.simpleButton1.Text = "Cancel";
            // 
            // btnOUT
            // 
            this.btnOUT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnOUT.ImageOptions.Image")));
            this.btnOUT.Location = new System.Drawing.Point(516, 12);
            this.btnOUT.Name = "btnOUT";
            this.btnOUT.Size = new System.Drawing.Size(120, 40);
            this.btnOUT.TabIndex = 15;
            this.btnOUT.Text = "Check In";
            // 
            // frmShowRentRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 546);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnOUT);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmShowRentRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rent Record";
            this.Load += new System.EventHandler(this.frmShowRentRecord_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridRent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraVerticalGrid.VGridControl vGridBill;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraVerticalGrid.VGridControl vGridRent;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraVerticalGrid.VGridControl vGridVehicle;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraVerticalGrid.VGridControl vGridCustomer;
        private DsRecords dsRecords1;
        private DsRecords dsRecords2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNIC;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAddress1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPic;
        private DsRecords dsRecords3;
        private DsRecords dsRecords4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowchekoutID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDriver;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDriverNIC;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDriverName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDrivaerRate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDriverCharge;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStartDateTime;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowEndDatetime;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDays;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowHours;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLimitKm;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRatePerKm;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDecoRate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFuelLevel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFuelAmount;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRatePerExKm;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrandTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbilldate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillDue;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillPayed;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclNo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclMake;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlModel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowcurODO;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlNextService;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclInsExpDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlLicExpDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowImage1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnOUT;
    }
}