﻿namespace CarRentPro.Forms
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomer));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.btnClr = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.lblCustID = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress2 = new DevExpress.XtraEditors.MemoEdit();
            this.txtAddress1 = new DevExpress.XtraEditors.MemoEdit();
            this.txtName = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact1 = new DevExpress.XtraEditors.TextEdit();
            this.txtContact3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.picCustomer = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.chkblacklist = new DevExpress.XtraEditors.CheckEdit();
            this.txtNic = new DevExpress.XtraEditors.TextEdit();
            this.dtpLicExp = new System.Windows.Forms.DateTimePicker();
            this.txtLicNo = new DevExpress.XtraEditors.TextEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.btnDeleteDLIc = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteNIC = new DevExpress.XtraEditors.SimpleButton();
            this.picDLic = new DevExpress.XtraEditors.PictureEdit();
            this.picNIC = new DevExpress.XtraEditors.PictureEdit();
            this.btnGetDLicPic = new DevExpress.XtraEditors.SimpleButton();
            this.btnGetNICPic = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lstCustList = new System.Windows.Forms.ListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.appointmentsTableAdapter1 = new CarRentPro.carrentproDataSetTableAdapters.appointmentsTableAdapter();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkblacklist.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicNo.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picDLic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNIC.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1126, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 733);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1126, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 733);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1126, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 733);
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl1.Appearance.Options.UseBackColor = true;
            this.groupControl1.Controls.Add(this.xtraTabControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(416, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(710, 401);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Customer Deatils";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 20);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(706, 379);
            this.xtraTabControl1.TabIndex = 11;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.btnClr);
            this.xtraTabPage1.Controls.Add(this.btnDelete);
            this.xtraTabPage1.Controls.Add(this.btnUpdate);
            this.xtraTabPage1.Controls.Add(this.lblCustID);
            this.xtraTabPage1.Controls.Add(this.txtAddress2);
            this.xtraTabPage1.Controls.Add(this.txtAddress1);
            this.xtraTabPage1.Controls.Add(this.txtName);
            this.xtraTabPage1.Controls.Add(this.labelControl2);
            this.xtraTabPage1.Controls.Add(this.labelControl9);
            this.xtraTabPage1.Controls.Add(this.txtContact1);
            this.xtraTabPage1.Controls.Add(this.txtContact3);
            this.xtraTabPage1.Controls.Add(this.labelControl8);
            this.xtraTabPage1.Controls.Add(this.labelControl3);
            this.xtraTabPage1.Controls.Add(this.txtContact2);
            this.xtraTabPage1.Controls.Add(this.labelControl6);
            this.xtraTabPage1.Controls.Add(this.picCustomer);
            this.xtraTabPage1.Controls.Add(this.labelControl7);
            this.xtraTabPage1.Controls.Add(this.chkblacklist);
            this.xtraTabPage1.Controls.Add(this.txtNic);
            this.xtraTabPage1.Controls.Add(this.dtpLicExp);
            this.xtraTabPage1.Controls.Add(this.txtLicNo);
            this.xtraTabPage1.Controls.Add(this.btnSave);
            this.xtraTabPage1.Controls.Add(this.labelControl4);
            this.xtraTabPage1.Controls.Add(this.labelControl5);
            this.xtraTabPage1.Controls.Add(this.labelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(700, 351);
            this.xtraTabPage1.Text = "Customer Deatils";
            // 
            // btnClr
            // 
            this.btnClr.Location = new System.Drawing.Point(93, 9);
            this.btnClr.Name = "btnClr";
            this.btnClr.Size = new System.Drawing.Size(49, 23);
            this.btnClr.TabIndex = 20;
            this.btnClr.Text = "Clear";
            this.btnClr.Click += new System.EventHandler(this.btnClr_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.ImageOptions.Image")));
            this.btnDelete.ImageOptions.ImageUri.Uri = "Save";
            this.btnDelete.Location = new System.Drawing.Point(563, 294);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(88, 39);
            this.btnDelete.TabIndex = 19;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Appearance.Options.UseFont = true;
            this.btnUpdate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.ImageOptions.Image")));
            this.btnUpdate.ImageOptions.ImageUri.Uri = "Save";
            this.btnUpdate.Location = new System.Drawing.Point(454, 294);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(88, 39);
            this.btnUpdate.TabIndex = 18;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblCustID
            // 
            this.lblCustID.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustID.Appearance.Options.UseFont = true;
            this.lblCustID.Location = new System.Drawing.Point(320, 41);
            this.lblCustID.Name = "lblCustID";
            this.lblCustID.Size = new System.Drawing.Size(11, 14);
            this.lblCustID.TabIndex = 17;
            this.lblCustID.Text = "ID";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(93, 170);
            this.txtAddress2.MenuManager = this.barManager1;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Properties.LinesCount = 3;
            this.txtAddress2.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAddress2.Size = new System.Drawing.Size(374, 50);
            this.txtAddress2.TabIndex = 16;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(93, 109);
            this.txtAddress1.MenuManager = this.barManager1;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Properties.LinesCount = 3;
            this.txtAddress1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAddress1.Size = new System.Drawing.Size(374, 55);
            this.txtAddress1.TabIndex = 15;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(93, 68);
            this.txtName.MenuManager = this.barManager1;
            this.txtName.Name = "txtName";
            this.txtName.Properties.LinesCount = 2;
            this.txtName.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtName.Size = new System.Drawing.Size(374, 32);
            this.txtName.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(11, 69);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 14);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Name :";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(11, 307);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(55, 14);
            this.labelControl9.TabIndex = 9;
            this.labelControl9.Text = "Contact 03:";
            // 
            // txtContact1
            // 
            this.txtContact1.Location = new System.Drawing.Point(93, 252);
            this.txtContact1.Name = "txtContact1";
            this.txtContact1.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContact1.Properties.Appearance.Options.UseFont = true;
            this.txtContact1.Size = new System.Drawing.Size(202, 20);
            this.txtContact1.TabIndex = 1;
            // 
            // txtContact3
            // 
            this.txtContact3.Location = new System.Drawing.Point(93, 304);
            this.txtContact3.Name = "txtContact3";
            this.txtContact3.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContact3.Properties.Appearance.Options.UseFont = true;
            this.txtContact3.Size = new System.Drawing.Size(202, 20);
            this.txtContact3.TabIndex = 10;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(11, 281);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(55, 14);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Contact 02:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(11, 111);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 14);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Address 1 :";
            // 
            // txtContact2
            // 
            this.txtContact2.Location = new System.Drawing.Point(93, 278);
            this.txtContact2.Name = "txtContact2";
            this.txtContact2.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContact2.Properties.Appearance.Options.UseFont = true;
            this.txtContact2.Size = new System.Drawing.Size(202, 20);
            this.txtContact2.TabIndex = 8;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(301, 232);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(42, 14);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Ex: Date";
            // 
            // picCustomer
            // 
            this.picCustomer.Cursor = System.Windows.Forms.Cursors.Default;
            this.picCustomer.EditValue = global::CarRentPro.Properties.Resources.user;
            this.picCustomer.Location = new System.Drawing.Point(478, 41);
            this.picCustomer.MenuManager = this.barManager1;
            this.picCustomer.Name = "picCustomer";
            this.picCustomer.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picCustomer.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picCustomer.Properties.ZoomAccelerationFactor = 1D;
            this.picCustomer.Size = new System.Drawing.Size(212, 205);
            this.picCustomer.TabIndex = 3;
            this.picCustomer.EditValueChanged += new System.EventHandler(this.pictureEdit1_EditValueChanged);
            this.picCustomer.DoubleClick += new System.EventHandler(this.pictureEdit1_DoubleClick);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(11, 255);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(55, 14);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Contact 01:";
            // 
            // chkblacklist
            // 
            this.chkblacklist.Location = new System.Drawing.Point(478, 253);
            this.chkblacklist.MenuManager = this.barManager1;
            this.chkblacklist.Name = "chkblacklist";
            this.chkblacklist.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkblacklist.Properties.Appearance.Options.UseFont = true;
            this.chkblacklist.Properties.Caption = "Black List";
            this.chkblacklist.Size = new System.Drawing.Size(75, 19);
            this.chkblacklist.TabIndex = 6;
            // 
            // txtNic
            // 
            this.txtNic.Location = new System.Drawing.Point(93, 38);
            this.txtNic.MenuManager = this.barManager1;
            this.txtNic.Name = "txtNic";
            this.txtNic.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNic.Properties.Appearance.Options.UseFont = true;
            this.txtNic.Size = new System.Drawing.Size(202, 20);
            this.txtNic.TabIndex = 1;
            // 
            // dtpLicExp
            // 
            this.dtpLicExp.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLicExp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLicExp.Location = new System.Drawing.Point(349, 229);
            this.dtpLicExp.Name = "dtpLicExp";
            this.dtpLicExp.Size = new System.Drawing.Size(118, 22);
            this.dtpLicExp.TabIndex = 2;
            // 
            // txtLicNo
            // 
            this.txtLicNo.Location = new System.Drawing.Point(93, 226);
            this.txtLicNo.Name = "txtLicNo";
            this.txtLicNo.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLicNo.Properties.Appearance.Options.UseFont = true;
            this.txtLicNo.Size = new System.Drawing.Size(202, 20);
            this.txtLicNo.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.ImageOptions.ImageUri.Uri = "Save";
            this.btnSave.Location = new System.Drawing.Point(349, 294);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(88, 39);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(11, 172);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(54, 14);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Address 2 :";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(11, 229);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(78, 14);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Driving  Lic No :";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(11, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(23, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "NIC :";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.btnDeleteDLIc);
            this.xtraTabPage2.Controls.Add(this.btnDeleteNIC);
            this.xtraTabPage2.Controls.Add(this.picDLic);
            this.xtraTabPage2.Controls.Add(this.picNIC);
            this.xtraTabPage2.Controls.Add(this.btnGetDLicPic);
            this.xtraTabPage2.Controls.Add(this.btnGetNICPic);
            this.xtraTabPage2.Controls.Add(this.simpleButton4);
            this.xtraTabPage2.Controls.Add(this.simpleButton5);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(700, 351);
            this.xtraTabPage2.Text = "Document ";
            // 
            // btnDeleteDLIc
            // 
            this.btnDeleteDLIc.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnDeleteDLIc.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteDLIc.ImageOptions.Image")));
            this.btnDeleteDLIc.Location = new System.Drawing.Point(525, 37);
            this.btnDeleteDLIc.Name = "btnDeleteDLIc";
            this.btnDeleteDLIc.Size = new System.Drawing.Size(36, 32);
            this.btnDeleteDLIc.TabIndex = 56;
            this.btnDeleteDLIc.Click += new System.EventHandler(this.btnDeleteDLIc_Click);
            // 
            // btnDeleteNIC
            // 
            this.btnDeleteNIC.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnDeleteNIC.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteNIC.ImageOptions.Image")));
            this.btnDeleteNIC.Location = new System.Drawing.Point(161, 36);
            this.btnDeleteNIC.Name = "btnDeleteNIC";
            this.btnDeleteNIC.Size = new System.Drawing.Size(36, 32);
            this.btnDeleteNIC.TabIndex = 55;
            this.btnDeleteNIC.Click += new System.EventHandler(this.btnDeleteNIC_Click);
            // 
            // picDLic
            // 
            this.picDLic.Cursor = System.Windows.Forms.Cursors.Default;
            this.picDLic.EditValue = global::CarRentPro.Properties.Resources.drlicno;
            this.picDLic.Location = new System.Drawing.Point(365, 70);
            this.picDLic.Name = "picDLic";
            this.picDLic.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("picDLic.Properties.InitialImage")));
            this.picDLic.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picDLic.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picDLic.Properties.ZoomAccelerationFactor = 1D;
            this.picDLic.Size = new System.Drawing.Size(330, 268);
            this.picDLic.TabIndex = 54;
            // 
            // picNIC
            // 
            this.picNIC.Cursor = System.Windows.Forms.Cursors.Default;
            this.picNIC.EditValue = global::CarRentPro.Properties.Resources.nic;
            this.picNIC.Location = new System.Drawing.Point(14, 70);
            this.picNIC.Name = "picNIC";
            this.picNIC.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("picNIC.Properties.InitialImage")));
            this.picNIC.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picNIC.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picNIC.Properties.ZoomAccelerationFactor = 1D;
            this.picNIC.Size = new System.Drawing.Size(331, 268);
            this.picNIC.TabIndex = 53;
            // 
            // btnGetDLicPic
            // 
            this.btnGetDLicPic.Location = new System.Drawing.Point(483, 41);
            this.btnGetDLicPic.Name = "btnGetDLicPic";
            this.btnGetDLicPic.Size = new System.Drawing.Size(36, 22);
            this.btnGetDLicPic.TabIndex = 46;
            this.btnGetDLicPic.Text = "Set";
            this.btnGetDLicPic.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // btnGetNICPic
            // 
            this.btnGetNICPic.Location = new System.Drawing.Point(119, 42);
            this.btnGetNICPic.Name = "btnGetNICPic";
            this.btnGetNICPic.Size = new System.Drawing.Size(36, 22);
            this.btnGetNICPic.TabIndex = 45;
            this.btnGetNICPic.Text = "Set";
            this.btnGetNICPic.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(341, 3);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(38, 42);
            this.simpleButton4.TabIndex = 47;
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.ImageOptions.Image")));
            this.simpleButton5.Location = new System.Drawing.Point(524, 32);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(36, 42);
            this.simpleButton5.TabIndex = 48;
            this.simpleButton5.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupControl1);
            this.panel1.Controls.Add(this.groupControl3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1126, 401);
            this.panel1.TabIndex = 9;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lstCustList);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(416, 401);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "Customer List";
            // 
            // lstCustList
            // 
            this.lstCustList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader2,
            this.columnHeader3});
            this.lstCustList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstCustList.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCustList.FullRowSelect = true;
            this.lstCustList.Location = new System.Drawing.Point(2, 20);
            this.lstCustList.MultiSelect = false;
            this.lstCustList.Name = "lstCustList";
            this.lstCustList.Size = new System.Drawing.Size(412, 379);
            this.lstCustList.TabIndex = 17;
            this.lstCustList.UseCompatibleStateImageBehavior = false;
            this.lstCustList.View = System.Windows.Forms.View.Details;
            this.lstCustList.SelectedIndexChanged += new System.EventHandler(this.lstCustList_SelectedIndexChanged);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "NIC";
            this.columnHeader11.Width = 87;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 189;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Contact";
            this.columnHeader3.Width = 129;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupControl2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 401);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1126, 332);
            this.panel2.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.listView1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1126, 332);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Customer History";
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(2, 20);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1122, 310);
            this.listView1.TabIndex = 17;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Vehicle No";
            this.columnHeader12.Width = 82;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Res/ChkOut";
            this.columnHeader13.Width = 87;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Start";
            this.columnHeader14.Width = 57;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "End";
            this.columnHeader15.Width = 43;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Dis.Limit";
            this.columnHeader16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Days";
            this.columnHeader17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader17.Width = 52;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Rate";
            this.columnHeader18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Rental";
            this.columnHeader19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Driver Charges";
            this.columnHeader20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Extra Charges";
            this.columnHeader21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Total";
            this.columnHeader22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // appointmentsTableAdapter1
            // 
            this.appointmentsTableAdapter1.ClearBeforeFill = true;
            // 
            // ofd
            // 
            this.ofd.FileName = "openFileDialog1";
            // 
            // frmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 733);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmCustomer";
            this.Activated += new System.EventHandler(this.frmCustomer_Activated);
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkblacklist.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicNo.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picDLic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNIC.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpLicExp;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtLicNo;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNic;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.PictureEdit picCustomer;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtContact1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.CheckEdit chkblacklist;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtContact3;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtContact2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.ListView lstCustList;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private carrentproDataSetTableAdapters.appointmentsTableAdapter appointmentsTableAdapter1;
        private DevExpress.XtraEditors.SimpleButton btnGetDLicPic;
        private DevExpress.XtraEditors.SimpleButton btnGetNICPic;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private System.Windows.Forms.OpenFileDialog ofd;
        private DevExpress.XtraEditors.PictureEdit picDLic;
        private DevExpress.XtraEditors.PictureEdit picNIC;
        private DevExpress.XtraEditors.MemoEdit txtAddress2;
        private DevExpress.XtraEditors.MemoEdit txtAddress1;
        private DevExpress.XtraEditors.MemoEdit txtName;
        private DevExpress.XtraEditors.LabelControl lblCustID;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnUpdate;
        private DevExpress.XtraEditors.SimpleButton btnDeleteDLIc;
        private DevExpress.XtraEditors.SimpleButton btnDeleteNIC;
        private DevExpress.XtraEditors.SimpleButton btnClr;
    }
}