﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace CarRentPro.Forms
{
    public partial class frmShowRentRecord : DevExpress.XtraEditors.XtraForm
    {
        int apID;
        public frmShowRentRecord(int apid)
        {
            InitializeComponent();
            apID = apid;
        }

        private void frmShowRentRecord_Load(object sender, EventArgs e)
        {
            DsRecordsTableAdapters.DTrentDataTableAdapter da = new DsRecordsTableAdapters.DTrentDataTableAdapter();
            da.Fill(dsRecords1.DTrentData, apID);
            da.Fill(dsRecords2.DTrentData, apID);
            da.Fill(dsRecords3.DTrentData, apID);
            da.Fill(dsRecords4.DTrentData, apID);
            vGridCustomer.Refresh();
            vGridBill.Refresh();
            vGridRent.Refresh();
            vGridVehicle.Refresh();

            txtNote.Text = this.dsRecords1.DTrentData[0].outNote.ToString();
        }
    }
}