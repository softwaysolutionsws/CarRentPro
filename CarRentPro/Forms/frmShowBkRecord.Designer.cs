﻿namespace CarRentPro.Forms
{
    partial class frmShowBkRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmShowBkRecord));
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.vGridcustromer = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords1 = new CarRentPro.DsRecords();
            this.rowNIC = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAddress1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowContact3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPic = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.vGridVehicleDtl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords3 = new CarRentPro.DsRecords();
            this.rowvhclNo = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclMake = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlModel = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlOdoReading = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlNextService = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowImage1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvhclInsExpDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowvchlLicExpDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.dTbkDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.vGridBooking = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords2 = new CarRentPro.DsRecords();
            this.rowBkID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkDays = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkHrs = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkRate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkRental = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkWithDriver = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkDrRate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkDriverCharges = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkDecorCharges = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbkTotalCharge = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.vGridPaymentdlt = new DevExpress.XtraVerticalGrid.VGridControl();
            this.dsRecords4 = new CarRentPro.DsRecords();
            this.rowbillID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbilldate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillTotal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillPayed = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowbillDue = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.btnOUT = new DevExpress.XtraEditors.SimpleButton();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.dTbkDataTableAdapter = new CarRentPro.DsRecordsTableAdapters.DTbkDataTableAdapter();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridcustromer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicleDtl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTbkDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridBooking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridPaymentdlt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.vGridcustromer);
            this.groupControl3.Location = new System.Drawing.Point(3, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(312, 239);
            this.groupControl3.TabIndex = 6;
            this.groupControl3.Text = "Customer Details";
            // 
            // vGridcustromer
            // 
            this.vGridcustromer.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(57)))));
            this.vGridcustromer.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.vGridcustromer.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridcustromer.Appearance.Category.ForeColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.Category.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.Category.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.Category.Options.UseFont = true;
            this.vGridcustromer.Appearance.Category.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.vGridcustromer.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(139)))), ((int)(((byte)(48)))));
            this.vGridcustromer.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.vGridcustromer.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.Empty.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(193)))), ((int)(((byte)(74)))));
            this.vGridcustromer.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(193)))), ((int)(((byte)(74)))));
            this.vGridcustromer.Appearance.ExpandButton.ForeColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridcustromer.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(249)))), ((int)(((byte)(236)))));
            this.vGridcustromer.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(114)))), ((int)(((byte)(50)))));
            this.vGridcustromer.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(114)))), ((int)(((byte)(50)))));
            this.vGridcustromer.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(158)))), ((int)(((byte)(64)))));
            this.vGridcustromer.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(231)))), ((int)(((byte)(177)))));
            this.vGridcustromer.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.vGridcustromer.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(247)))), ((int)(((byte)(230)))));
            this.vGridcustromer.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridcustromer.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.vGridcustromer.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.vGridcustromer.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(166)))), ((int)(((byte)(37)))));
            this.vGridcustromer.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridcustromer.DataMember = "DTbkData";
            this.vGridcustromer.DataSource = this.dsRecords1;
            this.vGridcustromer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridcustromer.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridcustromer.Location = new System.Drawing.Point(2, 20);
            this.vGridcustromer.Name = "vGridcustromer";
            this.vGridcustromer.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowNIC,
            this.rowFName,
            this.rowAddress1,
            this.rowContact1,
            this.rowContact2,
            this.rowContact3,
            this.rowPic});
            this.vGridcustromer.Size = new System.Drawing.Size(308, 217);
            this.vGridcustromer.TabIndex = 0;
            // 
            // dsRecords1
            // 
            this.dsRecords1.DataSetName = "DsRecords";
            this.dsRecords1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowNIC
            // 
            this.rowNIC.Name = "rowNIC";
            this.rowNIC.Properties.AllowEdit = false;
            this.rowNIC.Properties.Caption = "NIC";
            this.rowNIC.Properties.FieldName = "NIC";
            // 
            // rowFName
            // 
            this.rowFName.Name = "rowFName";
            this.rowFName.Properties.AllowEdit = false;
            this.rowFName.Properties.Caption = "Name";
            this.rowFName.Properties.FieldName = "FName";
            // 
            // rowAddress1
            // 
            this.rowAddress1.Height = 50;
            this.rowAddress1.Name = "rowAddress1";
            this.rowAddress1.Properties.AllowEdit = false;
            this.rowAddress1.Properties.Caption = "Address";
            this.rowAddress1.Properties.FieldName = "Address1";
            this.rowAddress1.Properties.ReadOnly = false;
            // 
            // rowContact1
            // 
            this.rowContact1.Name = "rowContact1";
            this.rowContact1.Properties.AllowEdit = false;
            this.rowContact1.Properties.Caption = "Contact 1";
            this.rowContact1.Properties.FieldName = "Contact1";
            // 
            // rowContact2
            // 
            this.rowContact2.Name = "rowContact2";
            this.rowContact2.Properties.AllowEdit = false;
            this.rowContact2.Properties.Caption = "Contact 2";
            this.rowContact2.Properties.FieldName = "Contact2";
            // 
            // rowContact3
            // 
            this.rowContact3.Name = "rowContact3";
            this.rowContact3.Properties.AllowEdit = false;
            this.rowContact3.Properties.Caption = "Contact 3";
            this.rowContact3.Properties.FieldName = "Contact3";
            // 
            // rowPic
            // 
            this.rowPic.Height = 74;
            this.rowPic.Name = "rowPic";
            this.rowPic.Properties.AllowEdit = false;
            this.rowPic.Properties.Caption = "Photo";
            this.rowPic.Properties.FieldName = "Pic";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.vGridVehicleDtl);
            this.groupControl1.Location = new System.Drawing.Point(319, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(328, 280);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Vehicle Deatils";
            // 
            // vGridVehicleDtl
            // 
            this.vGridVehicleDtl.Appearance.Category.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.vGridVehicleDtl.Appearance.Category.BorderColor = System.Drawing.Color.Gray;
            this.vGridVehicleDtl.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridVehicleDtl.Appearance.Category.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicleDtl.Appearance.Category.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.Category.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.Category.Options.UseFont = true;
            this.vGridVehicleDtl.Appearance.Category.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(215)))), ((int)(((byte)(180)))));
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(215)))), ((int)(((byte)(180)))));
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(250)))), ((int)(((byte)(160)))));
            this.vGridVehicleDtl.Appearance.Empty.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(225)))), ((int)(((byte)(198)))));
            this.vGridVehicleDtl.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(225)))), ((int)(((byte)(198)))));
            this.vGridVehicleDtl.Appearance.ExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicleDtl.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.vGridVehicleDtl.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(239)))), ((int)(((byte)(187)))));
            this.vGridVehicleDtl.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.SeaGreen;
            this.vGridVehicleDtl.Appearance.FocusedRow.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.vGridVehicleDtl.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridVehicleDtl.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.vGridVehicleDtl.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.FocusedRow.Options.UseFont = true;
            this.vGridVehicleDtl.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray;
            this.vGridVehicleDtl.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.vGridVehicleDtl.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.vGridVehicleDtl.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.HideSelectionRow.Options.UseFont = true;
            this.vGridVehicleDtl.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.HorzLine.BackColor = System.Drawing.Color.Gray;
            this.vGridVehicleDtl.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.RecordValue.BackColor = System.Drawing.Color.White;
            this.vGridVehicleDtl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicleDtl.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.MediumAquamarine;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Options.UseFont = true;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.VertLine.BackColor = System.Drawing.Color.Gray;
            this.vGridVehicleDtl.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridVehicleDtl.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.vGridVehicleDtl.DataMember = "DTbkData";
            this.vGridVehicleDtl.DataSource = this.dsRecords3;
            this.vGridVehicleDtl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridVehicleDtl.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridVehicleDtl.Location = new System.Drawing.Point(2, 20);
            this.vGridVehicleDtl.Name = "vGridVehicleDtl";
            this.vGridVehicleDtl.RowHeaderWidth = 124;
            this.vGridVehicleDtl.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowvhclNo,
            this.rowvhclMake,
            this.rowvhclType,
            this.rowvchlModel,
            this.rowvchlOdoReading,
            this.rowvchlNextService,
            this.rowImage1,
            this.rowvhclInsExpDate,
            this.rowvchlLicExpDate});
            this.vGridVehicleDtl.Size = new System.Drawing.Size(324, 258);
            this.vGridVehicleDtl.TabIndex = 0;
            // 
            // dsRecords3
            // 
            this.dsRecords3.DataSetName = "DsRecords";
            this.dsRecords3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowvhclNo
            // 
            this.rowvhclNo.Name = "rowvhclNo";
            this.rowvhclNo.Properties.Caption = "Vehicle No";
            this.rowvhclNo.Properties.FieldName = "vhclNo";
            // 
            // rowvhclMake
            // 
            this.rowvhclMake.Name = "rowvhclMake";
            this.rowvhclMake.Properties.Caption = "Make";
            this.rowvhclMake.Properties.FieldName = "vhclMake";
            // 
            // rowvhclType
            // 
            this.rowvhclType.Name = "rowvhclType";
            this.rowvhclType.Properties.Caption = "Type";
            this.rowvhclType.Properties.FieldName = "vhclType";
            // 
            // rowvchlModel
            // 
            this.rowvchlModel.Name = "rowvchlModel";
            this.rowvchlModel.Properties.Caption = "Model";
            this.rowvchlModel.Properties.FieldName = "vchlModel";
            // 
            // rowvchlOdoReading
            // 
            this.rowvchlOdoReading.Name = "rowvchlOdoReading";
            this.rowvchlOdoReading.Properties.Caption = "ODO Reading";
            this.rowvchlOdoReading.Properties.FieldName = "vchlOdoReading";
            // 
            // rowvchlNextService
            // 
            this.rowvchlNextService.Name = "rowvchlNextService";
            this.rowvchlNextService.Properties.Caption = "Next Service";
            this.rowvchlNextService.Properties.FieldName = "vchlNextService";
            // 
            // rowImage1
            // 
            this.rowImage1.Height = 99;
            this.rowImage1.Name = "rowImage1";
            this.rowImage1.Properties.Caption = "Photo";
            this.rowImage1.Properties.FieldName = "Image1";
            // 
            // rowvhclInsExpDate
            // 
            this.rowvhclInsExpDate.Name = "rowvhclInsExpDate";
            this.rowvhclInsExpDate.Properties.Caption = "Ins Exp Date";
            this.rowvhclInsExpDate.Properties.FieldName = "vhclInsExpDate";
            // 
            // rowvchlLicExpDate
            // 
            this.rowvchlLicExpDate.Name = "rowvchlLicExpDate";
            this.rowvchlLicExpDate.Properties.Caption = "Lic Exp Date";
            this.rowvchlLicExpDate.Properties.FieldName = "vchlLicExpDate";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.vGridBooking);
            this.groupControl4.Location = new System.Drawing.Point(5, 247);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(308, 263);
            this.groupControl4.TabIndex = 9;
            this.groupControl4.Text = "Booking Details";
            // 
            // vGridBooking
            // 
            this.vGridBooking.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.vGridBooking.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.vGridBooking.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridBooking.Appearance.Category.ForeColor = System.Drawing.Color.Black;
            this.vGridBooking.Appearance.Category.Options.UseBackColor = true;
            this.vGridBooking.Appearance.Category.Options.UseBorderColor = true;
            this.vGridBooking.Appearance.Category.Options.UseFont = true;
            this.vGridBooking.Appearance.Category.Options.UseForeColor = true;
            this.vGridBooking.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.vGridBooking.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(174)))), ((int)(((byte)(66)))));
            this.vGridBooking.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridBooking.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridBooking.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.vGridBooking.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridBooking.Appearance.Empty.Options.UseBackColor = true;
            this.vGridBooking.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(192)))), ((int)(((byte)(114)))));
            this.vGridBooking.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(192)))), ((int)(((byte)(114)))));
            this.vGridBooking.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridBooking.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridBooking.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridBooking.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridBooking.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridBooking.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridBooking.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.vGridBooking.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridBooking.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.vGridBooking.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.vGridBooking.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridBooking.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridBooking.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gainsboro;
            this.vGridBooking.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.vGridBooking.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridBooking.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridBooking.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.vGridBooking.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridBooking.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.vGridBooking.Appearance.RecordValue.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.vGridBooking.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridBooking.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridBooking.Appearance.RecordValue.Options.UseBorderColor = true;
            this.vGridBooking.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridBooking.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(136)))));
            this.vGridBooking.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(95)))));
            this.vGridBooking.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.vGridBooking.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridBooking.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridBooking.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridBooking.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(175)))), ((int)(((byte)(175)))));
            this.vGridBooking.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridBooking.DataMember = "DTbkData";
            this.vGridBooking.DataSource = this.dsRecords2;
            this.vGridBooking.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridBooking.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridBooking.Location = new System.Drawing.Point(2, 20);
            this.vGridBooking.Name = "vGridBooking";
            this.vGridBooking.RowHeaderWidth = 127;
            this.vGridBooking.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowBkID,
            this.rowbkDate,
            this.rowbkStart,
            this.rowbkEnd,
            this.rowbkDays,
            this.rowbkHrs,
            this.rowbkRate,
            this.rowbkRental,
            this.rowbkWithDriver,
            this.rowbkDrRate,
            this.rowbkDriverCharges,
            this.rowbkDecorCharges,
            this.rowbkTotalCharge});
            this.vGridBooking.Size = new System.Drawing.Size(304, 241);
            this.vGridBooking.TabIndex = 0;
            // 
            // dsRecords2
            // 
            this.dsRecords2.DataSetName = "DsRecords";
            this.dsRecords2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowBkID
            // 
            this.rowBkID.Name = "rowBkID";
            this.rowBkID.Properties.Caption = "Booking ID";
            this.rowBkID.Properties.FieldName = "bkID";
            // 
            // rowbkDate
            // 
            this.rowbkDate.Name = "rowbkDate";
            this.rowbkDate.Properties.Caption = "Date";
            this.rowbkDate.Properties.FieldName = "bkDate";
            // 
            // rowbkStart
            // 
            this.rowbkStart.Name = "rowbkStart";
            this.rowbkStart.Properties.Caption = "Start";
            this.rowbkStart.Properties.FieldName = "bkStart";
            // 
            // rowbkEnd
            // 
            this.rowbkEnd.Name = "rowbkEnd";
            this.rowbkEnd.Properties.Caption = "End";
            this.rowbkEnd.Properties.FieldName = "bkEnd";
            // 
            // rowbkDays
            // 
            this.rowbkDays.Name = "rowbkDays";
            this.rowbkDays.Properties.Caption = "Days";
            this.rowbkDays.Properties.FieldName = "bkDays";
            // 
            // rowbkHrs
            // 
            this.rowbkHrs.Name = "rowbkHrs";
            this.rowbkHrs.Properties.Caption = "Hrs";
            this.rowbkHrs.Properties.FieldName = "bkHrs";
            // 
            // rowbkRate
            // 
            this.rowbkRate.Name = "rowbkRate";
            this.rowbkRate.Properties.Caption = "Rate";
            this.rowbkRate.Properties.FieldName = "bkRate";
            // 
            // rowbkRental
            // 
            this.rowbkRental.Name = "rowbkRental";
            this.rowbkRental.Properties.Caption = "Rental";
            this.rowbkRental.Properties.FieldName = "bkRental";
            // 
            // rowbkWithDriver
            // 
            this.rowbkWithDriver.Name = "rowbkWithDriver";
            this.rowbkWithDriver.Properties.Caption = "Driver";
            this.rowbkWithDriver.Properties.FieldName = "bkWithDriver";
            // 
            // rowbkDrRate
            // 
            this.rowbkDrRate.Name = "rowbkDrRate";
            this.rowbkDrRate.Properties.Caption = "Driver Rate";
            this.rowbkDrRate.Properties.FieldName = "bkDrRate";
            // 
            // rowbkDriverCharges
            // 
            this.rowbkDriverCharges.Name = "rowbkDriverCharges";
            this.rowbkDriverCharges.Properties.Caption = "Driver Charges";
            this.rowbkDriverCharges.Properties.FieldName = "bkDriverCharges";
            // 
            // rowbkDecorCharges
            // 
            this.rowbkDecorCharges.Name = "rowbkDecorCharges";
            this.rowbkDecorCharges.Properties.Caption = "Decor Charges";
            this.rowbkDecorCharges.Properties.FieldName = "bkDecorCharges";
            // 
            // rowbkTotalCharge
            // 
            this.rowbkTotalCharge.Name = "rowbkTotalCharge";
            this.rowbkTotalCharge.Properties.Caption = "Total Charge";
            this.rowbkTotalCharge.Properties.FieldName = "bkTotalCharge";
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.vGridPaymentdlt);
            this.groupControl5.Location = new System.Drawing.Point(319, 288);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(328, 118);
            this.groupControl5.TabIndex = 9;
            this.groupControl5.Text = "Bill deatils";
            // 
            // vGridPaymentdlt
            // 
            this.vGridPaymentdlt.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(214)))), ((int)(((byte)(115)))));
            this.vGridPaymentdlt.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridPaymentdlt.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridPaymentdlt.Appearance.Category.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.Category.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.Category.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.Category.Options.UseFont = true;
            this.vGridPaymentdlt.Appearance.Category.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.vGridPaymentdlt.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridPaymentdlt.Appearance.Empty.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(139)))));
            this.vGridPaymentdlt.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(139)))));
            this.vGridPaymentdlt.Appearance.ExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridPaymentdlt.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(251)))), ((int)(((byte)(193)))));
            this.vGridPaymentdlt.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(152)))), ((int)(((byte)(49)))));
            this.vGridPaymentdlt.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(167)))), ((int)(((byte)(62)))));
            this.vGridPaymentdlt.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridPaymentdlt.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(176)))), ((int)(((byte)(84)))));
            this.vGridPaymentdlt.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.White;
            this.vGridPaymentdlt.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridPaymentdlt.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(249)))), ((int)(((byte)(173)))));
            this.vGridPaymentdlt.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(229)))), ((int)(((byte)(128)))));
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(194)))), ((int)(((byte)(102)))));
            this.vGridPaymentdlt.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridPaymentdlt.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.vGridPaymentdlt.DataMember = "DTbkData";
            this.vGridPaymentdlt.DataSource = this.dsRecords4;
            this.vGridPaymentdlt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridPaymentdlt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridPaymentdlt.Location = new System.Drawing.Point(2, 20);
            this.vGridPaymentdlt.Name = "vGridPaymentdlt";
            this.vGridPaymentdlt.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowbillID,
            this.rowbilldate,
            this.rowbillTotal,
            this.rowbillPayed,
            this.rowbillDue});
            this.vGridPaymentdlt.Size = new System.Drawing.Size(324, 96);
            this.vGridPaymentdlt.TabIndex = 0;
            // 
            // dsRecords4
            // 
            this.dsRecords4.DataSetName = "DsRecords";
            this.dsRecords4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rowbillID
            // 
            this.rowbillID.Name = "rowbillID";
            this.rowbillID.Properties.Caption = "Bill ID";
            this.rowbillID.Properties.FieldName = "billID";
            // 
            // rowbilldate
            // 
            this.rowbilldate.Name = "rowbilldate";
            this.rowbilldate.Properties.Caption = "Bill date";
            this.rowbilldate.Properties.FieldName = "billdate";
            // 
            // rowbillTotal
            // 
            this.rowbillTotal.Name = "rowbillTotal";
            this.rowbillTotal.Properties.Caption = "Bill Total";
            this.rowbillTotal.Properties.FieldName = "billTotal";
            // 
            // rowbillPayed
            // 
            this.rowbillPayed.Name = "rowbillPayed";
            this.rowbillPayed.Properties.Caption = "Paid";
            this.rowbillPayed.Properties.FieldName = "billPayed";
            // 
            // rowbillDue
            // 
            this.rowbillDue.Name = "rowbillDue";
            this.rowbillDue.Properties.Caption = "Due Balance";
            this.rowbillDue.Properties.FieldName = "billDue";
            // 
            // btnOUT
            // 
            this.btnOUT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnOUT.ImageOptions.Image")));
            this.btnOUT.Location = new System.Drawing.Point(675, 118);
            this.btnOUT.Name = "btnOUT";
            this.btnOUT.Size = new System.Drawing.Size(120, 40);
            this.btnOUT.TabIndex = 11;
            this.btnOUT.Text = "Rent OUT";
            this.btnOUT.Click += new System.EventHandler(this.btnOUT_Click);
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(321, 410);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(320, 96);
            this.txtNote.TabIndex = 12;
            // 
            // dTbkDataTableAdapter
            // 
            this.dTbkDataTableAdapter.ClearBeforeFill = true;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(675, 247);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(120, 40);
            this.simpleButton1.TabIndex = 13;
            this.simpleButton1.Text = "Cancel";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(675, 183);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(120, 40);
            this.simpleButton2.TabIndex = 14;
            this.simpleButton2.Text = "Pay";
            // 
            // frmShowBkRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 511);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.btnOUT);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmShowBkRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Booking Record";
            this.Load += new System.EventHandler(this.frmShowRecord_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridcustromer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicleDtl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dTbkDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridBooking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridPaymentdlt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRecords4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraVerticalGrid.VGridControl vGridcustromer;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraVerticalGrid.VGridControl vGridVehicleDtl;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraVerticalGrid.VGridControl vGridBooking;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraVerticalGrid.VGridControl vGridPaymentdlt;
        private DsRecords dsRecords1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNIC;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAddress1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowContact3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPic;
        private System.Windows.Forms.BindingSource dTbkDataBindingSource;
        private DsRecordsTableAdapters.DTbkDataTableAdapter dTbkDataTableAdapter;
        private DsRecords dsRecords3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclNo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclMake;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlModel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlOdoReading;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlNextService;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowImage1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvhclInsExpDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowvchlLicExpDate;
        private DsRecords dsRecords2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkEnd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkDays;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkRate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkDrRate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkHrs;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkDecorCharges;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkDriverCharges;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkRental;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkTotalCharge;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbkWithDriver;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowBkID;
        private DevExpress.XtraEditors.SimpleButton btnOUT;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DsRecords dsRecords4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbilldate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillDue;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowbillPayed;
    }
}