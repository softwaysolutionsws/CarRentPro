﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using FoxLearn.License;

namespace CarRentPro.Forms
{
    public partial class frmActivation : DevExpress.XtraEditors.XtraForm
    {
        public frmActivation()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnActivation_Click(object sender, EventArgs e)
        {
            KeyManager kM = new KeyManager(txtID.Text);
            string ActivationKey = txtActivationKey.Text;
            if (kM.ValidKey(ref ActivationKey))
            {
                KeyValuesClass kv = new KeyValuesClass();
                if (kM.DisassembleKey(ActivationKey, ref kv))
                {
                    LicenseInfo lic = new LicenseInfo();
                    lic.ProductKey = ActivationKey;
                    lic.FullName = "Softway Solution";
                    if (kv.Type == LicenseType.TRIAL)
                    {
                        lic.Day = kv.Expiration.Day;
                        lic.Month = kv.Expiration.Month;
                        lic.Year = kv.Expiration.Year;

                    }
                    kM.SaveSuretyFile(string.Format(@"{0}\KEY.key", Application.StartupPath), lic);
                    MessageBox.Show("You have been successfuly registerd", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("YourActivation Key is invalid  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
        }

        private void frmActivation_Load(object sender, EventArgs e)
        {
            txtID.Text = ComputerInfo.GetComputerId();
        }
    }
}