﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.IO;
using System.Globalization;

namespace CarRentPro.Forms
{
    public partial class frmbooking : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand comm;
        string Billno;
        string bkID;
        bool isNew = true;
        double Gtot = 0;

        public int GetResourceID(string Vno)
        {
            int id = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT `Id` FROM `resources` WHERE `Description`='" + Vno + "'";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    id = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return id;
        }
        private void PrientCR(string billNo)
        {
            try
            {
                string sql = @" SELECT
                                            `tblbills`.`billTotal`
                                            , `tblbills`.`billDue`
                                            , `tblbills`.`billPayed`
                                            , `tblbooking`.`bkVhclNo`
                                            , `tblbooking`.`bkStart`
                                            , `tblbooking`.`bkEnd`
                                            , `tblbooking`.`bkDays`
                                            , `tblbooking`.`bkRate`
                                            , `tblbooking`.`bkDecorCharges`
                                            , `tblbooking`.`bkDriverCharges`
                                            , `tblbooking`.`bilID`
                                            , `tblcustomer`.`FName`
                                            , `tblcustomer`.`Address1`
                                            , `tblcustomer`.`Contact1`
                                            , `tblcustomer`.`Contact2`
                                            , `tblcustomer`.`Contact3`
                                        FROM
                                            `carrentpro`.`tblbooking`
                                            INNER JOIN `carrentpro`.`tblbills` 
                                                ON (`tblbooking`.`bilID` = `tblbills`.`billID`)
                                            INNER JOIN `carrentpro`.`tblcustomer` 
                                                ON (`tblbills`.`billCstNic` = `tblcustomer`.`NIC`)
                                          WHERE   (`tblbooking`.`bilID` ='" + billNo + "')";
                //where  `tblbooking`.`bilID` = 'BIL10003'";        //       '" + Billno + "' ";
                rptBookingBill rpt = new rptBookingBill();
                MySqlDataAdapter adapter = new MySqlDataAdapter(sql, con);
                DataSet Ds = new DataSet();
                // adapter.Fill(Ds, "DataSetCReport");
                adapter.Fill(Ds, "tblbills");
                adapter.Fill(Ds, "tblbooking");
                adapter.Fill(Ds, "tblcustomer");
                rpt.SetDataSource(Ds);
                frmRecord frm = new frmRecord();
                frm.crystalReportViewer1.ReportSource = rpt;

                // ************** quick Print CRPrint Report without wizad*************

                // rpt.PrintToPrinter(1, true, 0, 0);

                //******************************
                frm.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public int GetLastID(string Item)
        {
            int lastID = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT " + Item + " FROM tbllastid";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    lastID = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return lastID + 1;
        }
        public void UpdateID(string Item, int NewID)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                int intnewid = NewID;
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tbllastid` SET " + Item.Trim() + "='" + intnewid + "' WHERE clmnID=1";
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                comm.Dispose();
            }
        }
        public void AddPayment(string billID, DateTime payTime, string payMeth, string payName, string payRef, string chqDate, double payAmount)
        {
            string payID = string.Format("PAY{0}", this.GetLastID("`tblPayments`").ToString());
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    string sql = "INSERT INTO `tblpayments` (`payID`,`payBillID`,`payDate`,`PayMethod`,`payName`,`payRef`,`payChqDate`,`PayAmount`) VALUES (@payID,@payBillID,@payDate,@PayMethod,@payName,@payRef,@payChqDate,@PayAmount)";
                    comm = new MySqlCommand(sql, con);
                    comm.Parameters.AddWithValue("@payID", payID);
                    comm.Parameters.AddWithValue("@payBillID", billID);
                    comm.Parameters.AddWithValue("@payDate", payTime);
                    comm.Parameters.AddWithValue("@PayMethod", payMeth);
                    comm.Parameters.AddWithValue("@payName", payName);
                    comm.Parameters.AddWithValue("@payRef", payRef);
                    comm.Parameters.AddWithValue("@payChqDate", null);
                    comm.Parameters.AddWithValue("@PayAmount", payAmount);
                    con.Open();
                    comm.ExecuteNonQuery();
                    this.UpdateID("`tblPayments`", this.GetLastID("`tblPayments`"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void clrRent()
        {
            chkDriver.Checked = false;
            txtDriverRate.Text = "";
            txtHrs.Text = "";
            txtDays.Text = "";
            txtDayRate.Text = "";
        }
        public void clrHire()
        {
            txtPick.Text = "";
            txtDrop.Text = "";
            txtTrip.Text = "";
            txtHireCharge.Text = "";
        }
        public void clrCommon()
        {
            cmbVehicleList.SelectedIndex = -1;
            txtModel.Text = "";
            imageSlider1.Images.Clear();
            dateNavigator1.SelectedRanges.Clear();
            dtpEnd.Text = "";
            dtpStart.Text = "";
            tpStart.Text = "";
            tpEnd.Text = "";
            chkDecor.Checked = false;
            txtExCharges.Text = "";

        }
        public frmbooking()
        {
            InitializeComponent();
        }
        public TimeSpan CountTime()
        {
            DateTime strt = dtpStart.Value.AddHours(DateTime.Parse(tpStart.Text.Trim()).Hour).AddMinutes(DateTime.Parse(tpStart.Text.Trim()).Minute);
            DateTime end = dtpEnd.Value.AddHours(DateTime.Parse(tpEnd.Text.Trim()).Hour).AddMinutes(DateTime.Parse(tpEnd.Text.Trim()).Minute);
            TimeSpan time = end - strt;
            return time;

        }
        public void loadVhclDetails(string VhclNo)
        {
            #region Load Text Boxes
            string slctID = VhclNo;
            imageSlider1.Images.Clear();
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {

                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `vhclsupID`,`vhclNo`,`vhclMake`,`vhclType`,`vchlModel`,`vhclDelDate`,`vchlOdoReading`,`vchlNextService`,`vhclInsNo`,`vhclInsExpDate`,`vhclLicNo`,`vchlLicExpDate`,`vhclMadeYear`,`vchlId`,`Image1`, `Image2`, `Image3`,`Image4`,`vhclKmLimit`,`vhclDayRate` FROM `tblvehicle` WHERE `vhclNo` = '" + slctID + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                     /*
                        lblVhclID.Text = rdr[13].ToString();
                        txtVhclNo.Text = rdr[1].ToString();
                        txtVhclMake.Text = rdr[2].ToString();
                        txtVhclType.Text = rdr[3].ToString();
                        txtVhclModel.Text = rdr[4].ToString();
                        dtpDelDate.Text = rdr[5].ToString();
                        txtVhclCurOdo.Text = rdr[6].ToString();
                        txtVhclNxtSrvice.Text = rdr[7].ToString();
                        txtVhclInsNo.Text = rdr[8].ToString();
                        dtpInsExpDate.Text = rdr[9].ToString();
                        txtVhclLicNo.Text = rdr[10].ToString();
                        dtpLicExpDate.Text = rdr[11].ToString();
                        txtVhclMYeay.Text = rdr[12].ToString();

                        txtDisLimit.Text = rdr[18].ToString();
                        txtCurODO.Text = rdr[6].ToString();
                         */
                        txtModel.Text = rdr[4].ToString();
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[14]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[15]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[16]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[17]));

                        if (radioGroup1.EditValue=="rent")
                        {
                            txtDayRate.Text = rdr[19].ToString();
                        }



                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }
            }
            #endregion
        }
        public Image ByteArray2Image(byte[] Barry)
        {
            MemoryStream ms = new MemoryStream(Barry);
            Image retImage = Image.FromStream(ms);
            return retImage;
        }
        public bool loadCustDetails(string CustNic)
        {
            bool available = false;
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = @"SELECT 
                                                `ID`,
                                                `NIC`,
                                                `FName`,
                                                `Address1`,
                                                `Address2`,
                                                `LicNo`,
                                                `ExDate`,
                                                `Contact1`,
                                                `Contact2`,
                                                `Contact3`,
                                                `Pic`,
                                                `BlackLIsted`,
                                                `picID`,
                                                `picDLic`
                                        FROM `tblcustomer` 
                                        WHERE `NIC` = '" + CustNic + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        lblCstID.Text = rdr[0].ToString();
                        txtNIC.Text = rdr[1].ToString();
                        txtFName.Text = rdr[2].ToString();
                        txtAddress1.Text = rdr[3].ToString();
                        txtAddress2.Text = rdr[4].ToString();
                        //txtLIcNo.Text = rdr[5].ToString();
                        //dtpLicExp.Text = rdr[6].ToString();
                        txtContact1.Text = rdr[7].ToString();
                        txtContact2.Text = rdr[8].ToString();
                        txtContact3.Text = rdr[9].ToString();
                        picCustomer.Images.Add(this.ByteArray2Image((byte[])rdr[10]));
                        if ((bool)rdr[11])
                        {
                            lblIsBlackListed.Text = "Black Listed";
                        }
                        else
                        {
                            lblIsBlackListed.Text = "";
                        }
                        available = true;
                       
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }

            }
            return available;
        }
        private void AddCst_Click(object sender, EventArgs e)
        {
            frmCustomer frm = new frmCustomer();
            frm.Show();
        }
        private void txtNIC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                if (!this.loadCustDetails(txtNIC.Text.Trim()))
                {
                    frmCustomer frm = new frmCustomer(txtNIC.Text.Trim());
                    frm.ShowDialog();
                    this.loadCustDetails(txtNIC.Text.Trim());
                }
            }
        }
        private void frmbooking_Load(object sender, EventArgs e)
        {
            txtDriverRate.Enabled = false;
            txtExCharges.Enabled = false;
            imageSlider1.Images.Clear();
            #region Load Combos
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT vhclNo FROM tblvehicle";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        cmbVehicleList.Properties.Items.Add(rdr[0].ToString());
                    }

                    comm.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            #endregion
            txtNIC.Focus();
            radioGroup1.SelectedIndex = 0;
        }
        private void cmbVehicleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbVehicleList.SelectedIndex != -1)
            {
                this.loadVhclDetails(cmbVehicleList.Text.Trim());
            }
        }
        private void chkDriver_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDriver.Checked)
            {
                txtDriverRate.Enabled = true;
                txtDriverRate.Focus();
            }
            else
            {
                txtDriverRate.Enabled = false;
            }
        }
        private void chkExharges_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDecor.Checked)
            {
                txtExCharges.Enabled = true;
                txtExCharges.Focus();
            }
            else
            {
                txtExCharges.Enabled = false;
            }
        }
        private void dateNavigator1_SelectionChanged(object sender, EventArgs e)
        {
            dtpStart.Value = dateNavigator1.SelectionStart;
            dtpEnd.Value = dateNavigator1.SelectionEnd.AddDays(-1);
            tpStart.Text = "";
            tpEnd.Text = "";
            tpStart.Focus();
        }
        private void tpStart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                try
                {
                    DateTime dateTime = DateTime.ParseExact(tpStart.Text.Trim(), "HHmm", CultureInfo.InvariantCulture);
                    tpStart.Text = dateTime.ToShortTimeString();
                    tpEnd.Text = "";
                    tpEnd.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    tpStart.Text = "";
                    tpStart.Focus();
                }
                
            }
        }
        private void tpEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radioGroup1.EditValue.ToString()=="rent")
                {
                    try
                    {
                        DateTime dateTime = DateTime.ParseExact(tpEnd.Text.Trim(), "HHmm", CultureInfo.InvariantCulture);
                        tpEnd.Text = dateTime.ToShortTimeString();
                        txtDayRate.Focus();

                        txtHrs.Text = this.CountTime().Hours.ToString();
                        txtDays.Text = this.CountTime().Days.ToString();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        tpEnd.Text = "";
                        tpEnd.Focus();
                    }
                }
                else
                {
                    try
                    {
                        DateTime dateTime = DateTime.ParseExact(tpEnd.Text.Trim(), "HHmm", CultureInfo.InvariantCulture);
                        tpEnd.Text = dateTime.ToShortTimeString();
                        txtPick.Focus();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                
            }
        }
        private void txtDayRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                if (MessageBox.Show("Add appintment now","Confirm",MessageBoxButtons.YesNo)==DialogResult.Yes)
                {
                    if (radioGroup1.EditValue.ToString() == "rent")
                    {
                        double Driverrate;
                        double ExCarges;
                        string withDriver;
                        string note;
                        #region Validate
                        try
                        {

                            if (lblCstID.Text == "")
                            {
                                MessageBox.Show("Please Add Customer");
                                return;
                            }
                            if (cmbVehicleList.SelectedIndex == -1)
                            {
                                MessageBox.Show("Please select vehicle");
                                return;
                            }
                            if (chkDriver.Checked)
                            {
                                withDriver = "Yes";
                                if (!double.TryParse(txtDriverRate.Text.Trim(), out Driverrate))
                                {
                                    MessageBox.Show("Invalit entry for driver rate");
                                    txtDriverRate.Text = "";
                                    txtDriverRate.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                withDriver = "No";
                                Driverrate = 0;
                            }
                            if (chkDecor.Checked)
                            {
                                if (!double.TryParse(txtExCharges.Text.Trim(), out ExCarges))
                                {
                                    MessageBox.Show("Invalit entry for Decor Charges");
                                    txtExCharges.Text = "";
                                    txtExCharges.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                ExCarges = 0;
                            }
                            if (memoEdit1.Text == "")
                            {
                                note = "None";
                            }
                            else
                            {
                                note = memoEdit1.Text;
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }
                        #endregion
                        #region Addto List
                        try
                        {
                            ListViewItem itm = new ListViewItem(cmbVehicleList.SelectedText.Trim());
                            itm.SubItems.Add(txtModel.Text.Trim());
                            itm.SubItems.Add(dtpStart.Value.Add(DateTime.Parse(tpStart.Text.Trim()).TimeOfDay).ToString());
                            itm.SubItems.Add(dtpEnd.Value.Add(DateTime.Parse(tpEnd.Text.Trim()).TimeOfDay).ToString());
                            itm.SubItems.Add(txtDays.Text.Trim());
                            itm.SubItems.Add(txtHrs.Text.Trim());
                            itm.SubItems.Add(withDriver);
                            itm.SubItems.Add(AppSett.FormatCurrency(txtDayRate.Text.Trim()));
                            itm.SubItems.Add(AppSett.FormatCurrency(Driverrate.ToString()));
                            itm.SubItems.Add(AppSett.FormatCurrency(ExCarges.ToString()));

                            double ratePhr = double.Parse(txtDayRate.Text.Trim()) / 24;
                            double rental = ((double.Parse(txtDays.Text.Trim()) * 24) + (double.Parse(txtHrs.Text.Trim()))) * ratePhr;

                            double DCharge;
                            if (Driverrate==0)
                            {
                                DCharge = 0;
                            }
                            else
                            {
                                double ratePhrD = Driverrate / 24;
                                DCharge = ((double.Parse(txtDays.Text.Trim()) * 24) + (double.Parse(txtHrs.Text.Trim()))) * ratePhrD;
                            }

                            double tot = rental + DCharge;

                            itm.SubItems.Add(AppSett.FormatCurrency(rental.ToString()));
                            itm.SubItems.Add(AppSett.FormatCurrency(DCharge.ToString()));
                            itm.SubItems.Add(AppSett.FormatCurrency(tot.ToString()));
                            itm.SubItems.Add(note);

                            lstRent.Items.Add(itm);

                            this.clrRent();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }

                        #endregion
                        #region Cal Total
                        foreach (ListViewItem item in lstRent.Items)
                        {
                            Gtot += double.Parse(item.SubItems[12].Text);
                        }
                        lblGrandTotal.Text = AppSett.FormatCurrency(Gtot.ToString());
                        #endregion
                        this.clrCommon();
                        this.clrHire();
                        this.clrRent();
                    }
                }
                else
                {
                    return;
                }
                
            }
        }
        private void btnPay_Click(object sender, EventArgs e)
        {
            double tot = double.Parse(lblGrandTotal.Text);
            double due = double.Parse(lblDue.Text);

            SetPayEventArgs.TotAmount = tot;
            SetPayEventArgs.DuePayment = due;

            Billno = string.Format("BIL{0}", this.GetLastID("`tblBills`").ToString());

            frmPay paynow = new frmPay();
            paynow.PayDone += paynow_PayDone;

            if (paynow.ShowDialog()==DialogResult.OK)
            {
                foreach (ListViewItem item in lstRent.Items)
                {
                    #region Add to Appointmant bk
                    int resID = this.GetResourceID(item.SubItems[0].Text);
                    int apID = int.Parse(this.GetLastID("`tblappointment`").ToString());
                    try
                    {
                        using (con = new MySqlConnection(AppSett.CS))
                        {
                            string sql = "INSERT INTO `appointments` (`UniqeID`,`Type`,`StartDate`,`EndDate`,`AllDay`,`Subject`,`Location`,`Description`,`Status`,`Lable`,`ResourceID`) VALUES (@UniqeID,@Type,@StartDate,@EndDate,@AllDay,@Subject,@Location,@Description,@Status,@Lable,@ResourceID)";
                            comm = new MySqlCommand(sql, con);
                            comm.Parameters.AddWithValue("@UniqeID", apID);
                            comm.Parameters.AddWithValue("@Type", 0);
                            comm.Parameters.AddWithValue("@StartDate", DateTime.Parse(item.SubItems[2].Text));
                            comm.Parameters.AddWithValue("@EndDate", DateTime.Parse(item.SubItems[3].Text));
                            comm.Parameters.AddWithValue("@AllDay", 0);
                            comm.Parameters.AddWithValue("@Subject", txtFName.Text.Trim());
                            comm.Parameters.AddWithValue("@Location", txtContact1.Text.Trim());
                            comm.Parameters.AddWithValue("@Description", item.SubItems[0].Text);
                            comm.Parameters.AddWithValue("@Status", 0);
                            comm.Parameters.AddWithValue("@Lable", 3);
                            comm.Parameters.AddWithValue("@ResourceID", resID);
                            con.Open();
                            comm.ExecuteNonQuery();
                            this.UpdateID("`tblappointment`", this.GetLastID("`tblappointment`"));
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    #endregion
                    #region Add to Booking
                    try
                    {
                        bool withdriver = false;
                        string bkID = string.Format("BK{0}", this.GetLastID("`tblBooking`").ToString());
                        using (con = new MySqlConnection(AppSett.CS))
                        {
                            string sql = @"INSERT INTO `tblbooking` 
                                            (`bkID`,`bkDate`,`bkCstNIC`,
                                                `bkVhclNo`,`bkStart`,`bkEnd`,
                                                `bkDays`,`bkHrs`,`bkDecorCharges`,
                                                `bkDriverCharges`,`bkRental`,
                                                `bkTotalCharge`,`apID`,`bilID`,
                                                `bkRate`,`bkDrRate`,`bkWithDriver`,
                                                `bkNote`,`bkStatus`)
                                    VALUES 
                                           (@bkID,@bkDate,@bkCstNIC,
                                                @bkVhclNo,@bkStart,@bkEnd,
                                                @bkDays,@bkHrs,@bkDecorCharges,
                                                @bkDriverCharges,@bkRental,
                                                @bkTotalCharge,@apID,@bilID,
                                                @bkRate,@bkDrRate,@bkWithDriver,
                                                @bkNote,@bkStatus)";
                            comm = new MySqlCommand(sql, con);
                            comm.Parameters.AddWithValue("@bkID",bkID);
                            comm.Parameters.AddWithValue("@bkDate",DateTime.Now);
                            comm.Parameters.AddWithValue("@bkCstNIC",txtNIC.Text.Trim());
                            comm.Parameters.AddWithValue("@bkVhclNo", item.SubItems[0].Text);
                            comm.Parameters.AddWithValue("@bkStart",DateTime.Parse(item.SubItems[2].Text));
                            comm.Parameters.AddWithValue("@bkEnd",DateTime.Parse(item.SubItems[3].Text));
                            comm.Parameters.AddWithValue("@bkDays",int.Parse(item.SubItems[4].Text));
                            comm.Parameters.AddWithValue("@bkHrs",int.Parse(item.SubItems[5].Text));
                            comm.Parameters.AddWithValue("@bkDecorCharges",double.Parse(item.SubItems[9].Text));
                            comm.Parameters.AddWithValue("@bkDriverCharges",double.Parse(item.SubItems[11].Text));
                            comm.Parameters.AddWithValue("@bkRental",double.Parse(item.SubItems[10].Text));
                            comm.Parameters.AddWithValue("@bkTotalCharge",double.Parse(item.SubItems[12].Text));
                            comm.Parameters.AddWithValue("@apID",apID);
                            comm.Parameters.AddWithValue("@bilID",Billno);
                            comm.Parameters.AddWithValue("@bkRate",double.Parse(item.SubItems[7].Text));
                            comm.Parameters.AddWithValue("@bkDrRate",double.Parse(item.SubItems[8].Text));
                            comm.Parameters.AddWithValue("@bkStatus", "active");

                            if ((item.SubItems[7].Text)=="Yes")
                            {
                                withdriver = true;
                            }
                            else
                            {
                                withdriver = false;
                            }
                            comm.Parameters.AddWithValue("@bkWithDriver", withdriver);
                            comm.Parameters.AddWithValue("@bkNote", item.SubItems[13].Text);
                            
                            con.Open();
                            comm.ExecuteNonQuery();
                            this.UpdateID("`tblBooking`", this.GetLastID("`tblBooking`"));
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    #endregion
                }
                foreach (ListViewItem item in lstHire.Items)
                {
                    #region Add to Appointmant res
                    int resID = this.GetResourceID(item.SubItems[0].Text);
                    int apID = int.Parse(this.GetLastID("`tblappointment`").ToString());
                    try
                    {
                        using (con = new MySqlConnection(AppSett.CS))
                        {
                            string sql = "INSERT INTO `appointments` (`UniqeID`,`Type`,`StartDate`,`EndDate`,`AllDay`,`Subject`,`Location`,`Description`,`Status`,`Lable`,`ResourceID`) VALUES (@UniqeID,@Type,@StartDate,@EndDate,@AllDay,@Subject,@Location,@Description,@Status,@Lable,@ResourceID)";
                            comm = new MySqlCommand(sql, con);
                            comm.Parameters.AddWithValue("@UniqeID", apID);
                            comm.Parameters.AddWithValue("@Type", 0);
                            comm.Parameters.AddWithValue("@StartDate", DateTime.Parse(item.SubItems[2].Text));
                            comm.Parameters.AddWithValue("@EndDate", DateTime.Parse(item.SubItems[3].Text));
                            comm.Parameters.AddWithValue("@AllDay", 0);
                            comm.Parameters.AddWithValue("@Subject", txtFName.Text.Trim());
                            comm.Parameters.AddWithValue("@Location", txtContact1.Text.Trim());
                            comm.Parameters.AddWithValue("@Description", item.SubItems[0].Text);
                            comm.Parameters.AddWithValue("@Status", 0);
                            comm.Parameters.AddWithValue("@Lable", 1);
                            comm.Parameters.AddWithValue("@ResourceID", resID);
                            con.Open();
                            comm.ExecuteNonQuery();
                            this.UpdateID("`tblappointment`", this.GetLastID("`tblappointment`"));
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    #endregion
                    #region Add to reservation
                    try
                    {
                        string resvID = string.Format("RES{0}", this.GetLastID("`tblreservation`").ToString());
                        using (con = new MySqlConnection(AppSett.CS))
                        {
                            string sql = @"INSERT INTO `tblreservation`
                                            (`resID`,`resDate`,`resCstNIC`,
                                                `resVhclNo`,`resVhclModel`,`resStart`,
                                                `resEnd`,`resPick`,`resDrop`,
                                                `resTrip`,`resDecor`,`resHire`,
                                                `resTotal`,`resNote`,`apID`,
                                                `billID`,`resStatus`)
                                    VALUES 
                                           (@resID,@resDate,@resCstNIC,
                                                @resVhclNo,@resVhclModel,@resStart,
                                                @resEnd,@resPick,@resDrop,
                                                @resTrip,@resDecor,@resHire,
                                                @resTotal,@resNote,@apID,
                                                @billID,@resStatus)";
                            comm = new MySqlCommand(sql, con);
                            comm.Parameters.AddWithValue("@resID", resvID);
                            comm.Parameters.AddWithValue("@resDate", DateTime.Now);
                            comm.Parameters.AddWithValue("@resCstNIC", txtNIC.Text.Trim());
                            comm.Parameters.AddWithValue("@resVhclNo", item.SubItems[0].Text);
                            comm.Parameters.AddWithValue("@resVhclModel", item.SubItems[1].Text);
                            comm.Parameters.AddWithValue("@resStart", DateTime.Parse(item.SubItems[2].Text));
                            comm.Parameters.AddWithValue("@resEnd", DateTime.Parse(item.SubItems[3].Text));
                            comm.Parameters.AddWithValue("@resPick", item.SubItems[4].Text);
                            comm.Parameters.AddWithValue("@resDrop", item.SubItems[5].Text);
                            comm.Parameters.AddWithValue("@resTrip", int.Parse(item.SubItems[6].Text));
                            comm.Parameters.AddWithValue("@resDecor", double.Parse(item.SubItems[7].Text));
                            comm.Parameters.AddWithValue("@resHire", double.Parse(item.SubItems[8].Text));
                            comm.Parameters.AddWithValue("@resTotal", double.Parse(item.SubItems[9].Text));
                            comm.Parameters.AddWithValue("@resNote", item.SubItems[10].Text);
                            comm.Parameters.AddWithValue("@apID", apID);
                            comm.Parameters.AddWithValue("@billID", Billno);
                            comm.Parameters.AddWithValue("@resStatus", "active");

                            con.Open();
                            comm.ExecuteNonQuery();
                            this.UpdateID("`tblreservation`", this.GetLastID("`tblreservation`"));

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    #endregion
                }
                this.UpdateID("`tblBills`", this.GetLastID("`tblBills`"));
                MessageBox.Show("Data Saved");

                PrientCR(Billno);
            }
            this.Dispose();
        }
        private void paynow_PayDone(object sender, PayDoneEventArgs e)
        {
            this.AddPayment(Billno, e.PayTime, e.PayMethod, e.CardName, e.CardRef, e.ChqDate, e.Payment);
            #region Add 2 Bill
            if (isNew)
            {
                try
                {
                    using (con = new MySqlConnection(AppSett.CS))
                    {
                        string sql = @"INSERT INTO `tblbills` 
                                    (`billID`,`billdate`,`billCstNic`,`billTotal`,`billDue`,`billNote`,`billPayed`)
                                 VALUES  
                                    (@billID,@billdate,@billCstNic,@billTotal,@billDue,@billNote,@billPayed)";
                        comm = new MySqlCommand(sql, con);
                        comm.Parameters.AddWithValue("@billID", Billno);
                        comm.Parameters.AddWithValue("@billCstNic", txtNIC.Text.Trim());
                        comm.Parameters.AddWithValue("@billdate", DateTime.Now);
                        comm.Parameters.AddWithValue("@billTotal", double.Parse(lblGrandTotal.Text));
                        comm.Parameters.AddWithValue("@billDue", e.DuePayment);
                        comm.Parameters.AddWithValue("@billNote", null);
                        comm.Parameters.AddWithValue("@billPayed", (e.Payment));
                        con.Open();
                        comm.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = "UPDATE `tblbills` SET `billDue`=@billDue,`billNote`=@billNote WHERE `billID`=@billID";
                        comm.Parameters.AddWithValue("@billID", Billno);
                        comm.Parameters.AddWithValue("@billDue", e.DuePayment);
                        comm.Parameters.AddWithValue("@billNote", null);
                        comm.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            #endregion
        }
        private void lblGrandTotal_TextChanged(object sender, EventArgs e)
        {
            double GrandTot = double.Parse(lblGrandTotal.Text);
            double Paid = double.Parse(lblPaid.Text);
            lblDue.Text = AppSett.FormatCurrency((GrandTot - Paid).ToString());
        }
        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioGroup1.EditValue.ToString() == "rent")
            {
                grpHire.Enabled = false;
                grpRent.Enabled = true;
            }
            else if (radioGroup1.EditValue.ToString()=="hire")
            {
                grpHire.Enabled = true;
                grpRent.Enabled = false;
            }
        }
        private void txtHireCharge_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter)
            {
                if (MessageBox.Show("Add appintment now","Confirm",MessageBoxButtons.YesNo)==DialogResult.Yes)
                {
                    if (radioGroup1.EditValue.ToString() == "hire")
                    {
                        string VehNo;
                        string Model;
                        string Pick;
                        string Drop;
                        int Trip;
                        double Decor;
                        double Hire;
                        double Tot;
                        string Note;
                        DateTime start;
                        DateTime end;

                        if (!DateTime.TryParse(dtpStart.Value.Add(DateTime.Parse(tpStart.Text.Trim()).TimeOfDay).ToString(), out start))
                        {
                            MessageBox.Show("Invalid start");
                        }
                        if (!DateTime.TryParse(dtpEnd.Value.Add(DateTime.Parse(tpEnd.Text.Trim()).TimeOfDay).ToString(), out end))
                        {
                            MessageBox.Show("Invalid end");
                        }

                        #region Validate
                        try
                        {
                            if (lblCstID.Text == "")
                            {
                                MessageBox.Show("Please Add Customer");
                                return;
                            }
                            if (cmbVehicleList.SelectedIndex == -1)
                            {
                                MessageBox.Show("Please select vehicle");
                                return;
                            }
                            else
                            {
                                VehNo = cmbVehicleList.SelectedText.ToString();
                                Model = txtModel.Text.Trim();
                            }
                            if (txtPick.Text.Trim() == "")
                            {
                                MessageBox.Show("Enter Pick Location");
                                txtPick.Focus();
                                return;
                            }
                            else
                            {
                                Pick = txtPick.Text.Trim();
                            }
                            if (txtDrop.Text.Trim() == "")
                            {
                                MessageBox.Show("Enter Drop Location");
                                txtDrop.Focus();
                                return;
                            }
                            else
                            {
                                Drop = txtDrop.Text.Trim();
                            }
                            if (!int.TryParse(txtTrip.Text.Trim(), out Trip))
                            {
                                if (MessageBox.Show("Invalid Input /nSet Trip Distance as 0 km", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    Trip = 0;
                                }
                                else
                                {
                                    txtTrip.Focus();
                                    return;
                                }
                            }
                            if (chkDecor.Checked)
                            {
                                if (!double.TryParse(txtExCharges.Text.Trim(), out Decor))
                                {
                                    MessageBox.Show("Please enter valid Amount for Decoration charges");
                                    txtExCharges.Text = "";
                                    txtExCharges.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                Decor = 0;
                            }
                            if (!double.TryParse(txtHireCharge.Text.Trim(), out Hire))
                            {
                                MessageBox.Show("Please enter valid Amount for hire charges");
                                txtHireCharge.Text = "";
                                txtHireCharge.Focus();
                                return;
                            }

                            Tot = Decor + Hire;
                            if (memoEdit1.Text == "")
                            {
                                Note = "No Note";
                            }
                            else
                            {
                                Note = memoEdit1.Text;
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }
                        #endregion
                        #region Add to List
                        try
                        {
                            ListViewItem itm = new ListViewItem(VehNo);
                            itm.SubItems.Add(Model);
                            itm.SubItems.Add(start.ToString());
                            itm.SubItems.Add(end.ToString());
                            itm.SubItems.Add(Pick);
                            itm.SubItems.Add(Drop);
                            itm.SubItems.Add(Trip.ToString());
                            itm.SubItems.Add(AppSett.FormatCurrency(Decor.ToString()));
                            itm.SubItems.Add(AppSett.FormatCurrency(Hire.ToString()));
                            itm.SubItems.Add(AppSett.FormatCurrency(Tot.ToString()));
                            itm.SubItems.Add(Note);
                            lstHire.Items.Add(itm);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        #endregion
                        #region Cal Total
                        try
                        {
                            foreach (ListViewItem item in lstHire.Items)
                            {
                                Gtot += double.Parse(item.SubItems[9].Text);
                            }
                            lblGrandTotal.Text = AppSett.FormatCurrency(Gtot.ToString());
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        #endregion
                        this.clrCommon();
                        this.clrHire();
                        this.clrRent();
                    }
                }
                else
                {
                    return;
                }
                
            }
        }
        private void txtDriverRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                txtDayRate.Focus();
            }
        }
        private void txtExCharges_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radioGroup1.SelectedIndex == 0)
                {
                    txtDayRate.Focus();
                }
                else if (radioGroup1.SelectedIndex == 1)
                {
                    txtHireCharge.Focus();
                }
            }
        }
        private void memoEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                if (radioGroup1.SelectedIndex == 0)
                {
                    txtDayRate.Focus();
                }
                else if (radioGroup1.SelectedIndex == 1)
                {
                    txtHireCharge.Focus();
                }
            }
            
        }
        private void txtPick_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                txtDrop.Focus();
            }
        }
        private void txtDrop_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTrip.Focus();
            }
        }
        private void txtTrip_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtHireCharge.Focus();
            }
        }
        private void lstRent_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Delete)
            {
                if (lstRent.SelectedItems.Count>0 && MessageBox.Show("Do you want to delete selected bookings","Confirm",MessageBoxButtons.YesNo)==DialogResult.Yes)
                {
                    foreach (ListViewItem item in lstRent.SelectedItems)
                    {
                        lstRent.Items.Remove(item);
                    }
                }
            }
        }
        private void lstHire_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (lstHire.SelectedItems.Count > 0 && MessageBox.Show("Do you want to delete selected bookings", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    foreach (ListViewItem item in lstHire.SelectedItems)
                    {
                        lstHire.Items.Remove(item);
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PrientCR("BIL10003");
        }
    }
}