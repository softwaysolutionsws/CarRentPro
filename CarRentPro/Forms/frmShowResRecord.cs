﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;


namespace CarRentPro.Forms
{
    public partial class frmShowResRecord : DevExpress.XtraEditors.XtraForm
    {
        int apID;
        string bilID;
        string resID;
        MySqlConnection con;
        MySqlCommand comm;
        public frmShowResRecord(int apid)
        {
            InitializeComponent();
            apID = apid;
        }

        private void frmShowResRecord_Load(object sender, EventArgs e)
        {
            try
            {
                DsRecordsTableAdapters.DTresDataTableAdapter da = new DsRecordsTableAdapters.DTresDataTableAdapter();
                da.Fill(dsRecords1.DTresData, apID);
                da.Fill(dsRecords2.DTresData, apID);
                da.Fill(dsRecords3.DTresData, apID);
                da.Fill(dsRecords4.DTresData, apID);
                vGridCustomer.Refresh();
                vGridBill.Refresh();
                vGridRes.Refresh();
                vGridVehicle.Refresh();

                bilID = vGridBill.GetCellValue(vGridBill.Rows[0], 0).ToString();
                resID = vGridRes.GetCellValue(vGridRes.Rows[0], 0).ToString();

                txtNote.Text = this.dsRecords1.DTresData[0].resNote.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void btnOUT_Click(object sender, EventArgs e)
        {
            frmCheckOut frm = new frmCheckOut(false, bilID, resID, apID);
            frm.ShowDialog();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete this Appiontment", "COnfirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = "DELETE FROM `appointments` WHERE `UniqeID`='" + apID + "'";
                        comm.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    this.Dispose();
                }

                using (con = new MySqlConnection(AppSett.CS))
                {
                    try
                    {
                        con.Open();
                        comm = new MySqlCommand();
                        comm.Connection = con;
                        comm.CommandText = @"UPDATE `tblreservation` 
                                                        SET 
                                                            resStatus=@resStatus
                                                        WHERE
                                                            `resID`=@resID";

                        comm.Parameters.AddWithValue("@resStatus", "Canceled");
                        comm.Parameters.AddWithValue("@resID", resID);

                        comm.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            else
            {
                return;
            }
        }
    }
}