﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.IO;
namespace CarRentPro.Forms
{
    public partial class frmHair : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand comm;
        public frmHair()
        {
            InitializeComponent();
        }
        public void loadDriverDetails(string name)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `NIC`,`Name`,`Address`,`Cantact`,`DrivingLICNO`,`LICExDate`,`RarePerDay` FROM `tbldrivers` WHERE `Name` = '" + name + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        txtDName.Text = rdr[1].ToString();
                        txtDnic.Text = rdr[0].ToString();
                        txtDaddress.Text = rdr[2].ToString();
                        txtDcontact.Text = rdr[3].ToString();
                        txtDlic.Text = rdr[4].ToString();
                        dtpDlicExp.Value = DateTime.Parse(rdr[5].ToString());
                        txtDrate.Text = rdr[6].ToString();
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }
            }
        }
        private void ClrVhcl()
        {
            txtVhclNo.Text = "";
            txtVhclMake.Text = "";
            txtVhclType.Text = "";
            txtVhclModel.Text = "";
            dtpDelDate.Text = "";
            txtVhclNxtSrvice.Text = "";
            txtVhclInsNo.Text = "";
            txtVhclLicNo.Text = "";
            txtVhclMYeay.Text = "";
        }
        public void loadVhclDetails(string VhclNo)
        {
            #region Load Text Boxes
            this.ClrVhcl();
            string slctID = VhclNo;
            imageSlider1.Images.Clear();
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {

                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `vhclsupID`,`vhclNo`,`vhclMake`,`vhclType`,`vchlModel`,`vhclDelDate`,`vchlOdoReading`,`vchlNextService`,`vhclInsNo`,`vhclInsExpDate`,`vhclLicNo`,`vchlLicExpDate`,`vhclMadeYear`,`vchlId`,`Image1`, `Image2`, `Image3`,`Image4`,`vhclKmLimit` FROM `tblvehicle` WHERE `vhclNo` = '" + slctID + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        lblVhclID.Text = rdr[13].ToString();
                        txtVhclNo.Text = rdr[1].ToString();
                        txtVhclMake.Text = rdr[2].ToString();
                        txtVhclType.Text = rdr[3].ToString();
                        txtVhclModel.Text = rdr[4].ToString();
                        dtpDelDate.Text = rdr[5].ToString();
                        //picVhcl.Image = this.ByteArray2Image((byte[])rdr[6]);
                        txtVhclCurOdo.Text = rdr[6].ToString();
                        txtVhclNxtSrvice.Text = rdr[7].ToString();
                        txtVhclInsNo.Text = rdr[8].ToString();
                        dtpInsExpDate.Text = rdr[9].ToString();
                        txtVhclLicNo.Text = rdr[10].ToString();
                        dtpLicExpDate.Text = rdr[11].ToString();
                        txtVhclMYeay.Text = rdr[12].ToString();

                        //txtDisLimit.Text = rdr[18].ToString();
                        txtCurODO.Text = rdr[6].ToString();

                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[14]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[15]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[16]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[17]));



                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }
            }
            #endregion
        }
        public int GetLastID(string Item)
        {
            int lastID = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT " + Item + " FROM tbllastid";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    lastID = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return lastID + 1;
        }
        public void UpdateID(string Item, int NewID)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                int intnewid = NewID;
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tbllastid` SET " + Item.Trim() + "='" + intnewid + "' WHERE clmnID=1";
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                comm.Dispose();
            }
        }
        public bool loadCustDetails(string CustNic)
        {
            bool available = false;
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = @"SELECT 
                                                `ID`,
                                                `NIC`,
                                                `FName`,
                                                `Address1`,
                                                `Address2`,
                                                `LicNo`,
                                                `ExDate`,
                                                `Contact1`,
                                                `Contact2`,
                                                `Contact3`,
                                                `Pic`,
                                                `BlackLIsted`,
                                                `picID`,
                                                `picDLic`
                                        FROM `tblcustomer` 
                                        WHERE `NIC` = '" + CustNic + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        lblCstID.Text = rdr[0].ToString();
                        txtNIC.Text = rdr[1].ToString();
                        txtFName.Text = rdr[2].ToString();
                        txtAddress1.Text = rdr[3].ToString();
                        txtAddress2.Text = rdr[4].ToString();
                        txtLIcNo.Text = rdr[5].ToString();
                        dtpLicExp.Text = rdr[6].ToString();
                        txtContact1.Text = rdr[7].ToString();
                        txtContact2.Text = rdr[8].ToString();
                        txtContact3.Text = rdr[9].ToString();
                        PicCustomer.Image = this.ByteArray2Image((byte[])rdr[10]);
                        if ((bool)rdr[11])
                        {
                            lblIsBlackListed.Text = "Black Listed";
                        }
                        else
                        {
                            lblIsBlackListed.Text = "";
                        }
                        available = true;
                        //if (rdr[12].ToString() != "")
                        //{
                        //    picNIC.Image = this.ByteArray2Image((byte[])rdr[12]);

                        //}
                        //if (rdr[12].ToString() != "")
                        //{
                        //    picDLic.Image = this.ByteArray2Image((byte[])rdr[13]);

                        //}
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }

            }
            return available;
        }
        public Image ByteArray2Image(byte[] Barry)
        {
            MemoryStream ms = new MemoryStream(Barry);
            Image retImage = Image.FromStream(ms);
            return retImage;
        }
        private void txtNIC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!this.loadCustDetails(txtNIC.Text.Trim()))
                {
                    frmCustomer frm = new frmCustomer(txtNIC.Text.Trim());
                    frm.ShowDialog();
                    this.loadCustDetails(txtNIC.Text.Trim());
                }
            }
        }
        private void AddCst_Click(object sender, EventArgs e)
        {

        }
        private void frmHire_Load(object sender, EventArgs e)
        {
            tpEnd.Time = DateTime.Now;
            #region Load Combos
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT vhclNo FROM tblvehicle";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        cmbVehicleList.Properties.Items.Add(rdr[0].ToString());
                    }

                    comm.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT Name FROM tbldrivers";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        cmbDriverList.Properties.Items.Add(rdr[0].ToString());
                    }

                    comm.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion
            lblCstID.Text = "";
            cmbDriverList.Enabled = false;
            txtExCharges.Enabled = false;
            txtpetrolcharg.Enabled = false;
            lblIsBlackListed.Text = "";
            txtNIC.Focus();
        }
        private void cmbVehicleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbVehicleList.SelectedIndex != -1)
            {
                this.loadVhclDetails(cmbVehicleList.Text.Trim());
                txtCurODO.Focus();
            }
        }
        private void cmbDriverList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDriverList.SelectedIndex != -1)
            {
                this.loadDriverDetails(cmbDriverList.Text.Trim());
            }
        }
    }
}