﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using DevExpress.XtraGauges.Base;
using DevExpress.XtraGauges.Core.Primitive;
using DevExpress.XtraGauges.Core.Model;
using DevExpress.XtraGauges.Core.Base;

namespace CarRentPro.Forms
{
    public partial class frmCheckIn : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataAdapter DA;
        DataSet DS;
         MySqlDataReader rdr;
        int ODOReadingStart;
        int LimitKm;
        int FuelLevel;
        string vehicleNo;
         double VehRate;
         double ExKmRate;
         double subTotal;
         double granTotal;

         public frmCheckIn()
         {
             InitializeComponent();
         }
        private double Grandtotal (double subtol ,double due)
         {
             granTotal = subtol + due;
             return granTotal;
         }
        private double CalsubTotala(double AmountK,double Amountfuel,double Amountdriver,double Amountnight,double Amountlate,double Amountwaiting )
         {
            
                    subTotal =  AmountK + Amountfuel + Amountdriver + Amountnight + Amountlate + Amountwaiting ;
                 
               
             return subTotal; 
         }

        private void LoadEXcharge()
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                con.Open();
                string sql1 = " SELECT `vhclDayRate`, `vhclExChargeKm`, `vhclHireKm`, `vhclNightCrge`,`vchlOdoReading` FROM  `carrentpro`.`tblvehicle` where    `vhclNo` ='" + vehicleNo + "'";

                cmd = new MySqlCommand(sql1, con);
                rdr = cmd.ExecuteReader();
                while (rdr.Read() == true)
                {
                    VehRate = double.Parse(rdr["vhclDayRate"].ToString());
                    ExKmRate = double.Parse(rdr["vhclExChargeKm"].ToString());
                   

                    if (txtexdays.Text == "")
                    {
                        txtexdays.Text = "0";
                    }
                    else
                    {
                        lblexcharge.Text = (double.Parse(txtexdays.Text) * VehRate).ToString();
                        lblExchargeHours.Text = AppSett.FormatCurrency((VehRate / 24 * double.Parse(txtexhours.Text)).ToString());
                        if (txtExKM.Text !="")
                        {
                            txtAmountExKM.Text = AppSett.FormatCurrency((double.Parse(txtExKM.Text) * ExKmRate).ToString());
                        }
                        else
                        {
                            txtExKM.Text = "0";
                        }
                        

                    }
                }
            }

        }
        private void LoadData(string chkID, string vehID)
        {
            try
            {
                this.dataTable1TableAdapter.Fill(this.checkinDataSet.DataTable1, chkID);
                vGridControl1.DataSource = this.checkinDataSet.DataTable1;
                vGridControl1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void frmCheckIn_Load(object sender, EventArgs e)
        {
            txtagreementNo.Focus();
        }

        private void txtagreementNo_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void txtagreementNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadData(txtagreementNo.Text, vehicleNo);

                
                dtpDateNow.Value = DateTime.Now;
                tmtimeNow.Time = DateTime.Now;
                txtodoreading.Focus();

                
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    string sql = "SELECT * FROM carrentpro.tblchekout WHERE `billID` ='" + txtagreementNo.Text + "' ";
                    cmd = new MySqlCommand(sql, con);
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            double DrivaerRate = double.Parse(rdr["DrivaerRate"].ToString());
                            DateTime StartDateTime = DateTime.Parse(rdr["StartDateTime"].ToString());
                            DateTime EndDatetime = DateTime.Parse(rdr["EndDatetime"].ToString());
                            int Days = int.Parse(rdr["Days"].ToString());
                            int Hours = int.Parse(rdr["Hours"].ToString());
                            LimitKm = int.Parse(rdr["LimitKm"].ToString());
                            ExKmRate = double.Parse(rdr["RatePerKm"].ToString());
                            double DecoRate = double.Parse(rdr["DecoRate"].ToString());
                            FuelLevel = int.Parse(rdr["FuelLevel"].ToString());
                            double FuelAmount = double.Parse(rdr["FuelAmount"].ToString());
                            double GrandTotal = double.Parse(rdr["GrandTotal"].ToString());
                            double PayedAmount = double.Parse(rdr["PayedAmount"].ToString());
                            double DueAmount = double.Parse(rdr["DueAmount"].ToString());
                            ODOReadingStart = int.Parse(rdr["curODO"].ToString());
                            vehicleNo = rdr["VehicleNo"].ToString();

                            txtRateKM.Text = ExKmRate.ToString();
                            DateTime Sd = StartDateTime;
                            DateTime Ed = DateTime.Now;

                            TimeSpan t = Ed - Sd;
                            // int  NrOfDays =Convert.ToInt32( t);
                            txttotaldays.Text = t.Days.ToString();
                            txtexhours.Text = t.Hours.ToString();

                            txtexdays.Text =  (int.Parse(txttotaldays.Text) - Days).ToString(); 



                        }
                    }
                    rdr.Dispose();
                   string sql1 = @"SELECT
                                                `billID`
                                                , `billTotal`
                                                , `billPayed`
                                                , `billDue`
                                 FROM
                                                `carrentpro`.`tblbills`WHERE `billID` ='" + txtagreementNo.Text + "' ";
                    cmd = new MySqlCommand(sql1, con);
                  
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            double total = double.Parse(rdr["billTotal"].ToString());
                            double Payed = double.Parse(rdr["billPayed"].ToString());
                            double Due = double.Parse(rdr["billDue"].ToString());
                            lblPayed.Text =AppSett.FormatCurrency( Payed.ToString());
                            lbldueBlance.Text =AppSett.FormatCurrency( Due.ToString());
                        }

                        
                    }
                    lblGrandTotal.Text = Grandtotal(double.Parse(lblsubTotal.Text),double.Parse( lbldueBlance.Text)).ToString();
                   
                }
               // LoadEXcharge();
            }
                
        }

        private void txtagreementNo_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  MessageBox.Show("Hello World");

        }
        void CalculateGaugeValue(IGaugeContainer container, IConvertibleScaleEx scale, MouseEventArgs e)
        {
            BasePrimitiveHitInfo hi = container.CalcHitInfo(e.Location);
            if (hi.Element != null && !hi.Element.IsComposite)
            {
                PointF modelPt = MathHelper.PointToModelPoint(scale as IElement<IRenderableElement>, new PointF(e.X, e.Y));
                float percent = scale.PointToPercent(modelPt);
                scale.Value = scale.PercentToValue(percent);
                int FULL = Convert.ToInt32(scale.Value);
                if (txtfuelwastage.Text != "")
                {
                    txtfuelwastage.Text = (FuelLevel - FULL).ToString();
                    
                }
               
               

            }
        }
        void ChangeCursor(IGaugeContainer container, MouseEventArgs e)
        {
            BasePrimitiveHitInfo hi = container.CalcHitInfo(e.Location);
            Cursor cursor = (hi.Element != null && !hi.Element.IsComposite) ? Cursors.Hand : Cursors.Default;
            if (((Control)container).Cursor != cursor)
                ((Control)container).Cursor = cursor;
        }
        private void gaugeControl1_MouseMove(object sender, MouseEventArgs e)
        {
           
        }

        private void txtodoreading_EditValueChanged(object sender, EventArgs e)
        {
           if (txtodoreading.Text != ""  )
            {
                int totalKm = int.Parse(txtodoreading.Text) - ODOReadingStart;
                int ExKm = totalKm - LimitKm;
                txttotalKM.Text = totalKm.ToString();
                txtExKM.Text = ExKm.ToString();
                double amountExkm = ExKmRate * double.Parse(txtExKM.Text);
                txtAmountExKM.Text = AppSett.FormatCurrency(amountExkm.ToString());
               lblsubTotal.Text = AppSett.FormatCurrency( CalsubTotala(double.Parse(txtAmountExKM.Text), double.Parse(txtAomuntExfuel.Text), double.Parse(txtDriverExCharges.Text), double.Parse(txtNightCharEX.Text), double.Parse(txtlatechrgEX.Text), double.Parse(txtwatingchrgEX.Text)).ToString());
               lblGrandTotal.Text = AppSett.FormatCurrency(Grandtotal(double.Parse(lblsubTotal.Text), double.Parse(lbldueBlance.Text)).ToString());
            }


        }

        private void gaugeControl1_MouseMove_1(object sender, MouseEventArgs e)
        {
            ChangeCursor(gaugeControl1 as IGaugeContainer, e);
            if (e.Button == MouseButtons.Left)
            {
                CalculateGaugeValue(gaugeControl1 as IGaugeContainer, arcScaleComponent1, e);

            }
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if( textEdit1.Text != "")
            {
                arcScaleComponent1.Value = int.Parse(textEdit1.Text);
                txtfuelwastage.Text =( FuelLevel - int.Parse(textEdit1.Text)).ToString();

            }
            else
            {
                textEdit1.Focus();
            }
        }

        private void btncalculate_Click(object sender, EventArgs e)
        {
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                   
                    con.Open();
                    string sql = " SELECT `vhclDayRate`, `vhclExChargeKm`, `vhclHireKm`, `vhclNightCrge` FROM  `carrentpro`.`tblvehicle` where    `vhclNo` ='" + vehicleNo + "'";
                                           
                    cmd = new MySqlCommand(sql, con);
                    rdr = cmd.ExecuteReader();
                    while(rdr.Read() == true)
                    {
                       VehRate = double.Parse(rdr["vhclDayRate"].ToString());
                        ExKmRate = double.Parse(rdr["vhclExChargeKm"].ToString());

                        if (txtexdays.Text == "")
                        {
                            txtexdays.Text = "0";
                        }
                        else
                        {
                            lblexcharge.Text = (double.Parse(txtexdays.Text) * VehRate).ToString();
                            lblExchargeHours.Text =AppSett.FormatCurrency( (VehRate / 24 *double.Parse( txtexhours.Text)).ToString());
                            //lblExKmcharge.Text = AppSett.FormatCurrency((double.Parse(txtExKM.Text) * ExKmRate).ToString());

                        }
                      
                    }

                }
            }
            catch(Exception ex)
            {

            }
        }

        private void txtodoreading_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                textEdit1.Focus();
            }
        }

        private void txtamountFule_EditValueChanged(object sender, EventArgs e)
        {
            if (txtamountFule.Text != "" )
            {
                txtAomuntExfuel.Text = AppSett.FormatCurrency( ( double.Parse(txtamountFule.Text) * double.Parse(txtfuelwastage.Text)).ToString());
                 lblsubTotal.Text = AppSett.FormatCurrency( CalsubTotala(double.Parse(txtAmountExKM.Text), double.Parse(txtAomuntExfuel.Text), double.Parse(txtDriverExCharges.Text), double.Parse(txtNightCharEX.Text), double.Parse(txtlatechrgEX.Text), double.Parse(txtwatingchrgEX.Text)).ToString());
                lblGrandTotal.Text = AppSett.FormatCurrency( Grandtotal(double.Parse(lblsubTotal.Text), double.Parse(lbldueBlance.Text)).ToString());
            }

        }

        private void textEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                txtamountFule.Focus();
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void txtRateKM_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void txtRateKM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtamountFule.Text != "")
                {
                    //lblExKmcharge.Text = AppSett.FormatCurrency((double.Parse(txtExKM.Text) * double.Parse(txtRateKM.Text)).ToString());
                    textEdit1.Focus();
                }
            }
            
        }

        private void chkExharges_CheckedChanged(object sender, EventArgs e)
        {
            if (chkExharges.Checked == true )
            {
                txtDriverExCharges.Enabled = true;
                txtDriverExCharges.Focus();
            }
            else
            {
                txtDriverExCharges.Enabled = false;
                txtDriverExCharges.Text = "0";
            }
        }

        private void chkFuelAmt_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFuelAmt.Checked == true)
            {
                txtNightCharEX.Enabled = true;
                txtNightCharEX.Focus();
            }
            else
            {
                txtNightCharEX.Enabled = false;
                txtNightCharEX.Text = "0";
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit2.Checked == true)
            {
                txtlatechrgEX.Enabled = true;
                txtlatechrgEX.Focus();
            }
            else
            {
                txtlatechrgEX.Enabled = false;
                txtlatechrgEX.Text = "0";
            }
            
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit1.Checked == true)
            {
                txtwatingchrgEX.Enabled = true;
                txtwatingchrgEX.Focus();
            }
            else
            {
                txtwatingchrgEX.Enabled = false;
                txtwatingchrgEX.Text = "0";
            }

            
        }

        private void txtexdays_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txtDriverExCharges_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txtDriverExCharges_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtDriverExCharges.Text = AppSett.FormatCurrency(txtDriverExCharges.Text);
                 lblsubTotal.Text = AppSett.FormatCurrency( CalsubTotala(double.Parse(txtAmountExKM.Text), double.Parse(txtAomuntExfuel.Text), double.Parse(txtDriverExCharges.Text), double.Parse(txtNightCharEX.Text), double.Parse(txtlatechrgEX.Text), double.Parse(txtwatingchrgEX.Text)).ToString());
                lblGrandTotal.Text = AppSett.FormatCurrency(Grandtotal(double.Parse(lblsubTotal.Text), double.Parse(lbldueBlance.Text)).ToString());
            }
        }

        private void txtNightCharEX_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtNightCharEX.Text = AppSett.FormatCurrency(txtNightCharEX.Text);
                 lblsubTotal.Text = AppSett.FormatCurrency( CalsubTotala(double.Parse(txtAmountExKM.Text), double.Parse(txtAomuntExfuel.Text), double.Parse(txtDriverExCharges.Text), double.Parse(txtNightCharEX.Text), double.Parse(txtlatechrgEX.Text), double.Parse(txtwatingchrgEX.Text)).ToString());
                lblGrandTotal.Text = AppSett.FormatCurrency(Grandtotal(double.Parse(lblsubTotal.Text), double.Parse(lbldueBlance.Text)).ToString());
            }
        }

        private void txtlatechrgEX_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtlatechrgEX.Text = AppSett.FormatCurrency(txtlatechrgEX.Text);
                 lblsubTotal.Text = AppSett.FormatCurrency( CalsubTotala(double.Parse(txtAmountExKM.Text), double.Parse(txtAomuntExfuel.Text), double.Parse(txtDriverExCharges.Text), double.Parse(txtNightCharEX.Text), double.Parse(txtlatechrgEX.Text), double.Parse(txtwatingchrgEX.Text)).ToString());
                lblGrandTotal.Text = AppSett.FormatCurrency(Grandtotal(double.Parse(lblsubTotal.Text), double.Parse(lbldueBlance.Text)).ToString());
            }
        }

        private void txtwatingchrgEX_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtwatingchrgEX.Text = AppSett.FormatCurrency(txtwatingchrgEX.Text);
                lblsubTotal.Text = AppSett.FormatCurrency(CalsubTotala(double.Parse(txtAmountExKM.Text), double.Parse(txtAomuntExfuel.Text), double.Parse(txtDriverExCharges.Text), double.Parse(txtNightCharEX.Text), double.Parse(txtlatechrgEX.Text), double.Parse(txtwatingchrgEX.Text)).ToString());
                lblGrandTotal.Text = AppSett.FormatCurrency(Grandtotal(double.Parse(lblsubTotal.Text), double.Parse(lbldueBlance.Text)).ToString());
                txtdiscount.Focus();
            }
         }

        private void txtNightCharEX_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txtlatechrgEX_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txtwatingchrgEX_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            
            switch (this.xtraTabControl1.SelectedTabPageIndex)
            {
                case 1:
                    MessageBox.Show("page no : " + this.xtraTabControl1.SelectedTabPageIndex);

                    break;
                case 0 :
                    MessageBox.Show("page no : " + this.xtraTabControl1.SelectedTabPageIndex);

                    break;
            }


        }

        private void txtdiscount_EditValueChanged(object sender, EventArgs e)
        {
            double Billtotal =  double.Parse( lblGrandTotal.Text) - double.Parse(txtdiscount.Text);
            lblBilltotal.Text =   AppSett.FormatCurrency( Billtotal.ToString());
        }
    }
}