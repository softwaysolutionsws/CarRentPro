﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoxLearn.License;

using System.Management;

namespace CarRentPro.Forms
{
    public partial class frmActivationkey : Form
    {
        public frmActivationkey()
        {
            InitializeComponent();
        }
        private void getProductID()
        {
            StringBuilder sb = new StringBuilder();
            ManagementClass diskDrives = new ManagementClass("win32_LogicalDisk");
            ManagementObjectCollection disks = diskDrives.GetInstances();
            foreach (ManagementObject disk in disks)
            {
                if (sb.Length > 0)
                    sb.Append(",");
                string serial = Convert.ToString(disk["volumeserialnumber"]);
                if (!string.IsNullOrEmpty(serial))
                    sb.Append(serial);

                txtProducetKey.Text = serial;
            }
        }
        private void frmActivation_Load(object sender, EventArgs e)
        {
            getProductID();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            //frmLoging frm = new frmLoging();
           // frm.Hide();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            KeyManager km = new KeyManager(txtProducetKey.Text);
            string Activationkey = txtActivationKey.Text;
            if (km.ValidKey(ref Activationkey))
            {
                KeyValuesClass kv = new KeyValuesClass();
                if(km.DisassembleKey(Activationkey,ref kv))
                {
                    LicenseInfo lic = new LicenseInfo();
                    lic.ProductKey = Activationkey;
                    lic.FullName = "Softway solution";
                    if (kv.Type == LicenseType.TRIAL)
                    {
                        lic.Day = kv.Expiration.Day;
                        lic.Month = kv.Expiration.Month;
                        lic.Year = kv.Expiration.Year;
                    }
                    km.SaveSuretyFile(string.Format(@"{0}\KEY.key", Application.StartupPath), lic);
                    MessageBox.Show("You have been sucessfuly registerd","Message",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    this.Hide();
                    frmsplash frm = new frmsplash();
                    frm.ShowDialog();
                    
                  }
                else
                {
                    MessageBox.Show("You Activation Key is invalid", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
        }
    }
}
