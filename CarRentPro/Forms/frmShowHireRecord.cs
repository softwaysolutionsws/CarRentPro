﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace CarRentPro.Forms
{
    public partial class frmShowHireRecord : DevExpress.XtraEditors.XtraForm
    {
        int apID;
        public frmShowHireRecord(int apid)
        {
            InitializeComponent();
            apID = apid;
        }

        private void frmShowHireRecord_Load(object sender, EventArgs e)
        {
            DsRecordsTableAdapters.DThireDataTableAdapter da = new DsRecordsTableAdapters.DThireDataTableAdapter();
            da.Fill(dsRecords1.DThireData, apID);
            da.Fill(dsRecords2.DThireData, apID);
            da.Fill(dsRecords3.DThireData, apID);
            da.Fill(dsRecords4.DThireData, apID);
            vGridCustomer.Refresh();
            vGridBill.Refresh();
            vGridRent.Refresh();
            vGridVehicle.Refresh();

            txtNote.Text = this.dsRecords1.DThireData[0].outNote.ToString();
        }
    }
}