﻿namespace CarRentPro.Forms
{
    partial class frmbooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmbooking));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lstHire = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.lstRent = new System.Windows.Forms.ListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.txtExCharges = new DevExpress.XtraEditors.TextEdit();
            this.chkDecor = new DevExpress.XtraEditors.CheckEdit();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.grpHire = new DevExpress.XtraEditors.GroupControl();
            this.txtHireCharge = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtTrip = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtDrop = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtPick = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.grpRent = new DevExpress.XtraEditors.GroupControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.txtDays = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtHrs = new DevExpress.XtraEditors.TextEdit();
            this.txtDayRate = new DevExpress.XtraEditors.TextEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.chkDriver = new DevExpress.XtraEditors.CheckEdit();
            this.txtDriverRate = new DevExpress.XtraEditors.TextEdit();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.txtModel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.cmbVehicleList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tpStart = new DevExpress.XtraEditors.TextEdit();
            this.tpEnd = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.button1 = new System.Windows.Forms.Button();
            this.picCustomer = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.lblPaid = new DevExpress.XtraEditors.LabelControl();
            this.lblIsBlackListed = new DevExpress.XtraEditors.LabelControl();
            this.lblDue = new DevExpress.XtraEditors.LabelControl();
            this.lblCstID = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.Model = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact3 = new DevExpress.XtraEditors.TextEdit();
            this.lblGrandTotal = new DevExpress.XtraEditors.LabelControl();
            this.txtContact2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.AddCst = new DevExpress.XtraEditors.SimpleButton();
            this.txtContact1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtFName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtNIC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress1 = new DevExpress.XtraEditors.MemoEdit();
            this.txtAddress2 = new DevExpress.XtraEditors.MemoEdit();
            this.imageSlider1 = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.btnPay = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExCharges.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDecor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpHire)).BeginInit();
            this.grpHire.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHireCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPick.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpRent)).BeginInit();
            this.grpRent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHrs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDriver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVehicleList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1201, 670);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lstHire);
            this.panel2.Controls.Add(this.panelControl2);
            this.panel2.Controls.Add(this.panelControl3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1201, 670);
            this.panel2.TabIndex = 0;
            // 
            // lstHire
            // 
            this.lstHire.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader6,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader24,
            this.columnHeader27});
            this.lstHire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstHire.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstHire.FullRowSelect = true;
            this.lstHire.GridLines = true;
            this.lstHire.Location = new System.Drawing.Point(0, 534);
            this.lstHire.Name = "lstHire";
            this.lstHire.Size = new System.Drawing.Size(1201, 136);
            this.lstHire.TabIndex = 17;
            this.lstHire.UseCompatibleStateImageBehavior = false;
            this.lstHire.View = System.Windows.Forms.View.Details;
            this.lstHire.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstHire_KeyDown);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Vehicle NO";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Model";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 80;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Start time";
            this.columnHeader20.Width = 141;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "End Time";
            this.columnHeader21.Width = 97;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Pick @";
            this.columnHeader15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader15.Width = 179;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Drop @";
            this.columnHeader16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader16.Width = 149;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Trip (km)";
            this.columnHeader17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader17.Width = 89;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Decoration";
            this.columnHeader18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader18.Width = 100;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Hire (Rs.)";
            this.columnHeader19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader19.Width = 79;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Total";
            this.columnHeader24.Width = 86;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Note";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.lstRent);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 388);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1201, 146);
            this.panelControl2.TabIndex = 5;
            // 
            // lstRent
            // 
            this.lstRent.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader2,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader13,
            this.columnHeader25,
            this.columnHeader5,
            this.columnHeader10,
            this.columnHeader12,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader14,
            this.columnHeader26});
            this.lstRent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstRent.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstRent.FullRowSelect = true;
            this.lstRent.GridLines = true;
            this.lstRent.Location = new System.Drawing.Point(2, 2);
            this.lstRent.Name = "lstRent";
            this.lstRent.Size = new System.Drawing.Size(1197, 142);
            this.lstRent.TabIndex = 16;
            this.lstRent.UseCompatibleStateImageBehavior = false;
            this.lstRent.View = System.Windows.Forms.View.Details;
            this.lstRent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstRent_KeyDown);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Vehicle NO";
            this.columnHeader11.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Model";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 80;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "From";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader7.Width = 99;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "To";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 93;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Days";
            this.columnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader9.Width = 69;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Hours";
            this.columnHeader13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Driver";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Rate";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 61;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Driver Rate";
            this.columnHeader10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader10.Width = 100;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Decoration";
            this.columnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader12.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Rental";
            this.columnHeader3.Width = 72;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Drv Charges";
            this.columnHeader4.Width = 101;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Total";
            this.columnHeader14.Width = 108;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Note";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.panelControl5);
            this.panelControl3.Controls.Add(this.panelControl4);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1201, 388);
            this.panelControl3.TabIndex = 4;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.groupControl3);
            this.panelControl5.Controls.Add(this.dtpStart);
            this.panelControl5.Controls.Add(this.dtpEnd);
            this.panelControl5.Controls.Add(this.grpHire);
            this.panelControl5.Controls.Add(this.grpRent);
            this.panelControl5.Controls.Add(this.radioGroup1);
            this.panelControl5.Controls.Add(this.labelControl22);
            this.panelControl5.Controls.Add(this.dateNavigator1);
            this.panelControl5.Controls.Add(this.imageSlider1);
            this.panelControl5.Controls.Add(this.txtModel);
            this.panelControl5.Controls.Add(this.labelControl32);
            this.panelControl5.Controls.Add(this.cmbVehicleList);
            this.panelControl5.Controls.Add(this.tpStart);
            this.panelControl5.Controls.Add(this.tpEnd);
            this.panelControl5.Controls.Add(this.labelControl21);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(413, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(786, 384);
            this.panelControl5.TabIndex = 5;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.labelControl10);
            this.groupControl3.Controls.Add(this.memoEdit1);
            this.groupControl3.Controls.Add(this.txtExCharges);
            this.groupControl3.Controls.Add(this.chkDecor);
            this.groupControl3.Location = new System.Drawing.Point(493, 248);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(282, 132);
            this.groupControl3.TabIndex = 120;
            this.groupControl3.Text = "Additional Details";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(2, 52);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(30, 14);
            this.labelControl10.TabIndex = 123;
            this.labelControl10.Text = "Note :";
            // 
            // memoEdit1
            // 
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.memoEdit1.Location = new System.Drawing.Point(2, 68);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.MaxLength = 255;
            this.memoEdit1.Size = new System.Drawing.Size(278, 62);
            this.memoEdit1.TabIndex = 113;
            this.memoEdit1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.memoEdit1_KeyDown);
            // 
            // txtExCharges
            // 
            this.txtExCharges.Location = new System.Drawing.Point(134, 27);
            this.txtExCharges.Name = "txtExCharges";
            this.txtExCharges.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtExCharges.Size = new System.Drawing.Size(88, 20);
            this.txtExCharges.TabIndex = 99;
            this.txtExCharges.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtExCharges_KeyDown);
            // 
            // chkDecor
            // 
            this.chkDecor.Location = new System.Drawing.Point(5, 25);
            this.chkDecor.Name = "chkDecor";
            this.chkDecor.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.chkDecor.Properties.Appearance.Options.UseFont = true;
            this.chkDecor.Properties.Caption = "Decor Charges (Rs.)";
            this.chkDecor.Size = new System.Drawing.Size(123, 19);
            this.chkDecor.TabIndex = 98;
            this.chkDecor.CheckedChanged += new System.EventHandler(this.chkExharges_CheckedChanged);
            // 
            // dtpStart
            // 
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStart.Location = new System.Drawing.Point(314, 221);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(82, 21);
            this.dtpStart.TabIndex = 103;
            // 
            // dtpEnd
            // 
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEnd.Location = new System.Drawing.Point(540, 222);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(77, 21);
            this.dtpEnd.TabIndex = 104;
            // 
            // grpHire
            // 
            this.grpHire.Controls.Add(this.txtHireCharge);
            this.grpHire.Controls.Add(this.labelControl9);
            this.grpHire.Controls.Add(this.txtTrip);
            this.grpHire.Controls.Add(this.labelControl8);
            this.grpHire.Controls.Add(this.txtDrop);
            this.grpHire.Controls.Add(this.labelControl6);
            this.grpHire.Controls.Add(this.txtPick);
            this.grpHire.Controls.Add(this.labelControl5);
            this.grpHire.Location = new System.Drawing.Point(248, 247);
            this.grpHire.Name = "grpHire";
            this.grpHire.Size = new System.Drawing.Size(240, 132);
            this.grpHire.TabIndex = 119;
            this.grpHire.Text = "Hire";
            // 
            // txtHireCharge
            // 
            this.txtHireCharge.Location = new System.Drawing.Point(90, 102);
            this.txtHireCharge.Name = "txtHireCharge";
            this.txtHireCharge.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHireCharge.Properties.Appearance.Options.UseFont = true;
            this.txtHireCharge.Size = new System.Drawing.Size(145, 20);
            this.txtHireCharge.TabIndex = 122;
            this.txtHireCharge.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHireCharge_KeyDown);
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(6, 105);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 14);
            this.labelControl9.TabIndex = 121;
            this.labelControl9.Text = "Charge (Rs.)";
            // 
            // txtTrip
            // 
            this.txtTrip.Location = new System.Drawing.Point(90, 76);
            this.txtTrip.Name = "txtTrip";
            this.txtTrip.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrip.Properties.Appearance.Options.UseFont = true;
            this.txtTrip.Size = new System.Drawing.Size(145, 20);
            this.txtTrip.TabIndex = 120;
            this.txtTrip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTrip_KeyDown);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(6, 79);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 14);
            this.labelControl8.TabIndex = 119;
            this.labelControl8.Text = "Distance :";
            // 
            // txtDrop
            // 
            this.txtDrop.Location = new System.Drawing.Point(66, 50);
            this.txtDrop.Name = "txtDrop";
            this.txtDrop.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDrop.Properties.Appearance.Options.UseFont = true;
            this.txtDrop.Size = new System.Drawing.Size(169, 20);
            this.txtDrop.TabIndex = 118;
            this.txtDrop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDrop_KeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(6, 53);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(44, 14);
            this.labelControl6.TabIndex = 117;
            this.labelControl6.Text = "Drop @ :";
            // 
            // txtPick
            // 
            this.txtPick.Location = new System.Drawing.Point(66, 24);
            this.txtPick.Name = "txtPick";
            this.txtPick.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPick.Properties.Appearance.Options.UseFont = true;
            this.txtPick.Size = new System.Drawing.Size(169, 20);
            this.txtPick.TabIndex = 116;
            this.txtPick.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPick_KeyDown);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(6, 27);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(52, 14);
            this.labelControl5.TabIndex = 115;
            this.labelControl5.Text = "Pick up @:";
            // 
            // grpRent
            // 
            this.grpRent.Controls.Add(this.labelControl23);
            this.grpRent.Controls.Add(this.txtDays);
            this.grpRent.Controls.Add(this.labelControl24);
            this.grpRent.Controls.Add(this.txtHrs);
            this.grpRent.Controls.Add(this.txtDayRate);
            this.grpRent.Controls.Add(this.labelControl45);
            this.grpRent.Controls.Add(this.chkDriver);
            this.grpRent.Controls.Add(this.txtDriverRate);
            this.grpRent.Location = new System.Drawing.Point(6, 248);
            this.grpRent.Name = "grpRent";
            this.grpRent.Size = new System.Drawing.Size(236, 130);
            this.grpRent.TabIndex = 48;
            this.grpRent.Text = "Rent";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.Location = new System.Drawing.Point(102, 34);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(25, 14);
            this.labelControl23.TabIndex = 102;
            this.labelControl23.Text = "Days";
            // 
            // txtDays
            // 
            this.txtDays.Location = new System.Drawing.Point(159, 29);
            this.txtDays.Name = "txtDays";
            this.txtDays.Size = new System.Drawing.Size(49, 20);
            this.txtDays.TabIndex = 107;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Location = new System.Drawing.Point(10, 33);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(49, 14);
            this.labelControl24.TabIndex = 111;
            this.labelControl24.Text = "Rate (Rs.)";
            // 
            // txtHrs
            // 
            this.txtHrs.Location = new System.Drawing.Point(159, 53);
            this.txtHrs.Name = "txtHrs";
            this.txtHrs.Size = new System.Drawing.Size(48, 20);
            this.txtHrs.TabIndex = 108;
            // 
            // txtDayRate
            // 
            this.txtDayRate.Location = new System.Drawing.Point(7, 52);
            this.txtDayRate.Name = "txtDayRate";
            this.txtDayRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDayRate.Size = new System.Drawing.Size(75, 20);
            this.txtDayRate.TabIndex = 110;
            this.txtDayRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDayRate_KeyDown);
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.Location = new System.Drawing.Point(102, 56);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(29, 14);
            this.labelControl45.TabIndex = 109;
            this.labelControl45.Text = "Hours";
            // 
            // chkDriver
            // 
            this.chkDriver.Location = new System.Drawing.Point(7, 91);
            this.chkDriver.Name = "chkDriver";
            this.chkDriver.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.chkDriver.Properties.Appearance.Options.UseFont = true;
            this.chkDriver.Properties.Caption = "Driver Rate (Rs.)";
            this.chkDriver.Size = new System.Drawing.Size(123, 19);
            this.chkDriver.TabIndex = 94;
            this.chkDriver.CheckedChanged += new System.EventHandler(this.chkDriver_CheckedChanged);
            // 
            // txtDriverRate
            // 
            this.txtDriverRate.Location = new System.Drawing.Point(137, 91);
            this.txtDriverRate.Name = "txtDriverRate";
            this.txtDriverRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDriverRate.Size = new System.Drawing.Size(75, 20);
            this.txtDriverRate.TabIndex = 95;
            this.txtDriverRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDriverRate_KeyDown);
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(30, 10);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioGroup1.Properties.Appearance.Options.UseFont = true;
            this.radioGroup1.Properties.Columns = 2;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("rent", "Rent"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("hire", "Hire")});
            this.radioGroup1.Size = new System.Drawing.Size(243, 34);
            this.radioGroup1.TabIndex = 114;
            this.radioGroup1.SelectedIndexChanged += new System.EventHandler(this.radioGroup1_SelectedIndexChanged);
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Location = new System.Drawing.Point(516, 225);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(18, 14);
            this.labelControl22.TabIndex = 101;
            this.labelControl22.Text = "End";
            // 
            // dateNavigator1
            // 
            this.dateNavigator1.CalendarAppearance.DayCellSelected.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.dateNavigator1.CalendarAppearance.DayCellSelected.Options.UseFont = true;
            this.dateNavigator1.CalendarAppearance.DayCellSpecial.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.dateNavigator1.CalendarAppearance.DayCellSpecial.Options.UseFont = true;
            this.dateNavigator1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateNavigator1.Cursor = System.Windows.Forms.Cursors.Default;
            this.dateNavigator1.DateTime = new System.DateTime(2018, 1, 15, 0, 0, 0, 0);
            this.dateNavigator1.EditValue = new System.DateTime(2018, 1, 15, 0, 0, 0, 0);
            this.dateNavigator1.FirstDayOfWeek = System.DayOfWeek.Sunday;
            this.dateNavigator1.HighlightHolidays = false;
            this.dateNavigator1.Location = new System.Drawing.Point(289, 10);
            this.dateNavigator1.Name = "dateNavigator1";
            this.dateNavigator1.ShowFooter = false;
            this.dateNavigator1.ShowHeader = false;
            this.dateNavigator1.ShowTodayButton = false;
            this.dateNavigator1.ShowWeekNumbers = false;
            this.dateNavigator1.Size = new System.Drawing.Size(484, 205);
            this.dateNavigator1.TabIndex = 63;
            this.dateNavigator1.SelectionChanged += new System.EventHandler(this.dateNavigator1_SelectionChanged);
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(205, 50);
            this.txtModel.Name = "txtModel";
            this.txtModel.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModel.Properties.Appearance.Options.UseFont = true;
            this.txtModel.Size = new System.Drawing.Size(64, 20);
            this.txtModel.TabIndex = 49;
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl32.Appearance.Options.UseFont = true;
            this.labelControl32.Location = new System.Drawing.Point(30, 53);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(59, 14);
            this.labelControl32.TabIndex = 96;
            this.labelControl32.Text = "Vehicle No :";
            // 
            // cmbVehicleList
            // 
            this.cmbVehicleList.Location = new System.Drawing.Point(95, 50);
            this.cmbVehicleList.Name = "cmbVehicleList";
            this.cmbVehicleList.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVehicleList.Properties.Appearance.Options.UseFont = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVehicleList.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cmbVehicleList.Properties.AutoComplete = false;
            this.cmbVehicleList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbVehicleList.Properties.DropDownRows = 20;
            this.cmbVehicleList.Properties.Sorted = true;
            this.cmbVehicleList.Size = new System.Drawing.Size(104, 20);
            this.cmbVehicleList.TabIndex = 97;
            this.cmbVehicleList.SelectedIndexChanged += new System.EventHandler(this.cmbVehicleList_SelectedIndexChanged);
            // 
            // tpStart
            // 
            this.tpStart.EditValue = "";
            this.tpStart.Location = new System.Drawing.Point(402, 221);
            this.tpStart.Name = "tpStart";
            this.tpStart.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.tpStart.Properties.Mask.EditMask = "t";
            this.tpStart.Properties.MaxLength = 4;
            this.tpStart.Size = new System.Drawing.Size(57, 20);
            this.tpStart.TabIndex = 105;
            this.tpStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tpStart_KeyDown);
            // 
            // tpEnd
            // 
            this.tpEnd.EditValue = "";
            this.tpEnd.Location = new System.Drawing.Point(623, 222);
            this.tpEnd.Name = "tpEnd";
            this.tpEnd.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.tpEnd.Properties.Mask.EditMask = "t";
            this.tpEnd.Properties.MaxLength = 4;
            this.tpEnd.Size = new System.Drawing.Size(57, 20);
            this.tpEnd.TabIndex = 112;
            this.tpEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tpEnd_KeyDown);
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Location = new System.Drawing.Point(284, 224);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(24, 14);
            this.labelControl21.TabIndex = 100;
            this.labelControl21.Text = "Start";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.button1);
            this.panelControl4.Controls.Add(this.picCustomer);
            this.panelControl4.Controls.Add(this.btnPay);
            this.panelControl4.Controls.Add(this.lblPaid);
            this.panelControl4.Controls.Add(this.lblIsBlackListed);
            this.panelControl4.Controls.Add(this.lblDue);
            this.panelControl4.Controls.Add(this.lblCstID);
            this.panelControl4.Controls.Add(this.labelControl29);
            this.panelControl4.Controls.Add(this.labelControl35);
            this.panelControl4.Controls.Add(this.labelControl28);
            this.panelControl4.Controls.Add(this.Model);
            this.panelControl4.Controls.Add(this.labelControl25);
            this.panelControl4.Controls.Add(this.txtContact3);
            this.panelControl4.Controls.Add(this.lblGrandTotal);
            this.panelControl4.Controls.Add(this.txtContact2);
            this.panelControl4.Controls.Add(this.labelControl34);
            this.panelControl4.Controls.Add(this.AddCst);
            this.panelControl4.Controls.Add(this.txtContact1);
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.labelControl4);
            this.panelControl4.Controls.Add(this.labelControl3);
            this.panelControl4.Controls.Add(this.txtFName);
            this.panelControl4.Controls.Add(this.labelControl2);
            this.panelControl4.Controls.Add(this.txtNIC);
            this.panelControl4.Controls.Add(this.labelControl1);
            this.panelControl4.Controls.Add(this.txtAddress1);
            this.panelControl4.Controls.Add(this.txtAddress2);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(411, 384);
            this.panelControl4.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(135, 317);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 43);
            this.button1.TabIndex = 101;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // picCustomer
            // 
            this.picCustomer.Cursor = System.Windows.Forms.Cursors.Default;
            this.picCustomer.Location = new System.Drawing.Point(254, 73);
            this.picCustomer.Name = "picCustomer";
            this.picCustomer.Size = new System.Drawing.Size(145, 135);
            this.picCustomer.TabIndex = 100;
            this.picCustomer.Text = "imageSlider2";
            // 
            // lblPaid
            // 
            this.lblPaid.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaid.Appearance.Options.UseFont = true;
            this.lblPaid.Appearance.Options.UseTextOptions = true;
            this.lblPaid.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblPaid.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPaid.Location = new System.Drawing.Point(153, 314);
            this.lblPaid.Name = "lblPaid";
            this.lblPaid.Size = new System.Drawing.Size(112, 19);
            this.lblPaid.TabIndex = 98;
            this.lblPaid.Text = "0.00";
            // 
            // lblIsBlackListed
            // 
            this.lblIsBlackListed.Appearance.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsBlackListed.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblIsBlackListed.Appearance.Options.UseFont = true;
            this.lblIsBlackListed.Appearance.Options.UseForeColor = true;
            this.lblIsBlackListed.Location = new System.Drawing.Point(254, 224);
            this.lblIsBlackListed.Name = "lblIsBlackListed";
            this.lblIsBlackListed.Size = new System.Drawing.Size(140, 29);
            this.lblIsBlackListed.TabIndex = 47;
            this.lblIsBlackListed.Text = " Is Black Listed";
            // 
            // lblDue
            // 
            this.lblDue.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDue.Appearance.Options.UseFont = true;
            this.lblDue.Appearance.Options.UseTextOptions = true;
            this.lblDue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDue.Location = new System.Drawing.Point(153, 341);
            this.lblDue.Name = "lblDue";
            this.lblDue.Size = new System.Drawing.Size(112, 19);
            this.lblDue.TabIndex = 97;
            this.lblDue.Text = "0.00";
            // 
            // lblCstID
            // 
            this.lblCstID.Location = new System.Drawing.Point(339, 21);
            this.lblCstID.Name = "lblCstID";
            this.lblCstID.Size = new System.Drawing.Size(60, 13);
            this.lblCstID.TabIndex = 6;
            this.lblCstID.Text = "Customer ID";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.Location = new System.Drawing.Point(10, 341);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(109, 19);
            this.labelControl29.TabIndex = 96;
            this.labelControl29.Text = "Due Payment";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Appearance.Options.UseFont = true;
            this.labelControl35.Location = new System.Drawing.Point(12, 233);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(55, 14);
            this.labelControl35.TabIndex = 45;
            this.labelControl35.Text = "Contact 03:";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.Options.UseFont = true;
            this.labelControl28.Location = new System.Drawing.Point(10, 314);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(36, 19);
            this.labelControl28.TabIndex = 95;
            this.labelControl28.Text = "Paid";
            // 
            // Model
            // 
            this.Model.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Model.Appearance.Options.UseFont = true;
            this.Model.Location = new System.Drawing.Point(313, 283);
            this.Model.Name = "Model";
            this.Model.Size = new System.Drawing.Size(37, 14);
            this.Model.TabIndex = 48;
            this.Model.Text = "Model :";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.Location = new System.Drawing.Point(10, 282);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(97, 19);
            this.labelControl25.TabIndex = 94;
            this.labelControl25.Text = "Grand Total";
            // 
            // txtContact3
            // 
            this.txtContact3.Location = new System.Drawing.Point(76, 230);
            this.txtContact3.Name = "txtContact3";
            this.txtContact3.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContact3.Properties.Appearance.Options.UseFont = true;
            this.txtContact3.Size = new System.Drawing.Size(167, 20);
            this.txtContact3.TabIndex = 46;
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandTotal.Appearance.Options.UseFont = true;
            this.lblGrandTotal.Appearance.Options.UseTextOptions = true;
            this.lblGrandTotal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblGrandTotal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGrandTotal.Location = new System.Drawing.Point(153, 282);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(112, 19);
            this.lblGrandTotal.TabIndex = 93;
            this.lblGrandTotal.Text = "0.00";
            this.lblGrandTotal.TextChanged += new System.EventHandler(this.lblGrandTotal_TextChanged);
            // 
            // txtContact2
            // 
            this.txtContact2.Location = new System.Drawing.Point(76, 204);
            this.txtContact2.Name = "txtContact2";
            this.txtContact2.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContact2.Properties.Appearance.Options.UseFont = true;
            this.txtContact2.Size = new System.Drawing.Size(167, 20);
            this.txtContact2.TabIndex = 44;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Appearance.Options.UseFont = true;
            this.labelControl34.Location = new System.Drawing.Point(13, 207);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(58, 14);
            this.labelControl34.TabIndex = 43;
            this.labelControl34.Text = "Contact 02 :";
            // 
            // AddCst
            // 
            this.AddCst.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddCst.Appearance.Options.UseFont = true;
            this.AddCst.Location = new System.Drawing.Point(245, 16);
            this.AddCst.Name = "AddCst";
            this.AddCst.Size = new System.Drawing.Size(87, 23);
            this.AddCst.TabIndex = 42;
            this.AddCst.Text = "Add Customer";
            // 
            // txtContact1
            // 
            this.txtContact1.Location = new System.Drawing.Point(76, 177);
            this.txtContact1.Name = "txtContact1";
            this.txtContact1.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContact1.Properties.Appearance.Options.UseFont = true;
            this.txtContact1.Size = new System.Drawing.Size(167, 20);
            this.txtContact1.TabIndex = 37;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(12, 180);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(58, 14);
            this.labelControl7.TabIndex = 32;
            this.labelControl7.Text = "Contact 01 :";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(14, 129);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(54, 14);
            this.labelControl4.TabIndex = 35;
            this.labelControl4.Text = "Address 2 :";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(12, 73);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 14);
            this.labelControl3.TabIndex = 34;
            this.labelControl3.Text = "Address 1 :";
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(75, 42);
            this.txtFName.Name = "txtFName";
            this.txtFName.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFName.Properties.Appearance.Options.UseFont = true;
            this.txtFName.Size = new System.Drawing.Size(330, 20);
            this.txtFName.TabIndex = 41;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(12, 45);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 14);
            this.labelControl2.TabIndex = 33;
            this.labelControl2.Text = "Name :";
            // 
            // txtNIC
            // 
            this.txtNIC.Location = new System.Drawing.Point(77, 16);
            this.txtNIC.Name = "txtNIC";
            this.txtNIC.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNIC.Properties.Appearance.Options.UseFont = true;
            this.txtNIC.Size = new System.Drawing.Size(162, 20);
            this.txtNIC.TabIndex = 1;
            this.txtNIC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNIC_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(14, 21);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(23, 14);
            this.labelControl1.TabIndex = 36;
            this.labelControl1.Text = "NIC :";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(75, 70);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress1.Properties.Appearance.Options.UseFont = true;
            this.txtAddress1.Size = new System.Drawing.Size(168, 50);
            this.txtAddress1.TabIndex = 40;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(74, 126);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress2.Properties.Appearance.Options.UseFont = true;
            this.txtAddress2.Size = new System.Drawing.Size(169, 45);
            this.txtAddress2.TabIndex = 39;
            // 
            // imageSlider1
            // 
            this.imageSlider1.CurrentImageIndex = 0;
            this.imageSlider1.Cursor = System.Windows.Forms.Cursors.Default;
            this.imageSlider1.Images.Add(((System.Drawing.Image)(resources.GetObject("imageSlider1.Images"))));
            this.imageSlider1.Images.Add(((System.Drawing.Image)(resources.GetObject("imageSlider1.Images1"))));
            this.imageSlider1.Images.Add(((System.Drawing.Image)(resources.GetObject("imageSlider1.Images2"))));
            this.imageSlider1.Location = new System.Drawing.Point(30, 76);
            this.imageSlider1.Name = "imageSlider1";
            this.imageSlider1.Size = new System.Drawing.Size(243, 165);
            this.imageSlider1.TabIndex = 93;
            this.imageSlider1.Text = "imageSlider1";
            // 
            // btnPay
            // 
            this.btnPay.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPay.ImageOptions.Image")));
            this.btnPay.Location = new System.Drawing.Point(293, 315);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(101, 44);
            this.btnPay.TabIndex = 99;
            this.btnPay.Text = "Proceed";
            this.btnPay.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // frmbooking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1201, 670);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmbooking";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmbooking";
            this.Load += new System.EventHandler(this.frmbooking_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExCharges.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDecor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpHire)).EndInit();
            this.grpHire.ResumeLayout(false);
            this.grpHire.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHireCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDrop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPick.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpRent)).EndInit();
            this.grpRent.ResumeLayout(false);
            this.grpRent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHrs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDriver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVehicleList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.ListView lstRent;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.TextEdit txtExCharges;
        private DevExpress.XtraEditors.CheckEdit chkDecor;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private DevExpress.XtraEditors.GroupControl grpHire;
        private DevExpress.XtraEditors.TextEdit txtHireCharge;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtTrip;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtDrop;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtPick;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.GroupControl grpRent;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit txtDays;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtHrs;
        private DevExpress.XtraEditors.TextEdit txtDayRate;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.CheckEdit chkDriver;
        private DevExpress.XtraEditors.TextEdit txtDriverRate;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private DevExpress.XtraEditors.TextEdit txtModel;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.ComboBoxEdit cmbVehicleList;
        private DevExpress.XtraEditors.TextEdit tpStart;
        private DevExpress.XtraEditors.TextEdit tpEnd;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.Controls.ImageSlider imageSlider1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton btnPay;
        private DevExpress.XtraEditors.LabelControl lblPaid;
        private DevExpress.XtraEditors.LabelControl lblIsBlackListed;
        private DevExpress.XtraEditors.LabelControl lblDue;
        private DevExpress.XtraEditors.LabelControl lblCstID;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl Model;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtContact3;
        private DevExpress.XtraEditors.LabelControl lblGrandTotal;
        private DevExpress.XtraEditors.TextEdit txtContact2;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.SimpleButton AddCst;
        private DevExpress.XtraEditors.TextEdit txtContact1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNIC;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.Controls.ImageSlider picCustomer;
        private DevExpress.XtraEditors.TextEdit txtFName;
        private DevExpress.XtraEditors.MemoEdit txtAddress1;
        private DevExpress.XtraEditors.MemoEdit txtAddress2;
        private System.Windows.Forms.ListView lstHire;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.Button button1;
    }
}