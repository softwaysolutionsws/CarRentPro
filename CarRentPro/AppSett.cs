﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CarRentPro
{
    class AppSett
    {
        internal static Image capturedImage;
                                    //server=192.168.8.101;user id=root;password=softway@12345;persistsecurityinfo=True;database=carrentpro
        //internal static string CS = "server=127.0.0.1;user id=root;password=softway@12345;database=carrentpro";
        internal static string CS = Properties.Settings.Default.carrentproConnectionString;
        public static String FormatCurrency(string currencyTXT)
        {
            
          
            string s = currencyTXT.Trim();
            String Fromated = "0.00";
            if (s!="")
            {
                Double AddAmount = Double.Parse(s);
                int lenth;

                int index = s.IndexOf(".");

                lenth = s.Length;
                int TXTm = lenth - (index);

                if (index <= 0)
                {
                    index = 0;
                    lenth = 0;
                    Fromated = AddAmount.ToString() + ".00";

                }
                else
                {

                    if (TXTm == 3)
                    {

                        Fromated = currencyTXT.ToString();
                    }
                    else if (TXTm == 2)
                    {
                        Fromated = AddAmount.ToString() + "0";
                    }

                    else if (TXTm >= 4)
                    {
                        double xx = Double.Parse(currencyTXT);
                        Fromated = Math.Round(xx, 2, MidpointRounding.AwayFromZero).ToString();
                    }
                    else
                    {

                        Fromated = AddAmount.ToString() + ".00";
                    }
                }
            }
            return Fromated;


            
        }

    }
  
        
}
