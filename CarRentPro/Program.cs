﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.LookAndFeel;

namespace CarRentPro
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //UserLookAndFeel.Default.SkinName = "DevExpress Dark Style";
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Forms.frmMain());
            //Application.Run(new Forms.frmActivationkey());
            // Application.Run(new frmLoging());
            //Application.Run(new Forms.frmsplash());
            //Application.Run(new Forms.frmVehList());
        }
    }
}
