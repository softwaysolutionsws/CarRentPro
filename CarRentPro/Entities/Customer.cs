﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentPro.Entities
{
    class Customer
    {
        private string _cstID;
        private string _cstName;
        private string _cstNic;
        private string _cstLicNo;
        private string _cstTel;
        private string _cstAddress1;
        private string _cstAddress2;
        private DateTime _cstLicExpDate;
        private string _cstnote;
        private string _imgID;

        public string ImgID
        {
            get { return _imgID; }
            set { _imgID = value; }
        }

        public string Cstnote
        {
            get { return _cstnote; }
            set { _cstnote = value; }
        }
        public DateTime CstLicExpDate
        {
            get { return _cstLicExpDate; }
            set { _cstLicExpDate = value; }
        }

        public string CstPresentAddress
        {
            get { return _cstAddress2; }
            set { _cstAddress2 = value; }
        }

        public string CstAddress
        {
            get { return _cstAddress1; }
            set { _cstAddress1 = value; }
        }

        public string CstTel
        {
            get { return _cstTel; }
            set { _cstTel = value; }
        }

        public string CstLicNo
        {
            get { return _cstLicNo; }
            set { _cstLicNo = value; }
        }

        public string CstNic
        {
            get { return _cstNic; }
            set { _cstNic = value; }
        }

        public string CstID
        {
            get { return _cstID; }
            set { _cstID = value; }
        }

        public string CstName
        {
            get { return _cstName; }
            set { _cstName = value; }
        }
    }
}
