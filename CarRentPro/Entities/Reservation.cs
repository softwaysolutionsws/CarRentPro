﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentPro.Entities
{
    class Reservation
    {
        private string _resID;
        private string _resCstID;
        private string _resVhclID;
        private DateTime _resPicupDate;
        private DateTime _resReturnDate;
        private double _ResRntlRate;
        private double _ResRntldriverCharge;
        private bool _ResRntlwithDriver;
        private double _ResRntlDecCharge;

        public double ResRntlDecCharge
        {
            get { return _ResRntlDecCharge; }
            set { _ResRntlDecCharge = value; }
        }


        public bool ResRntlwithDriver
        {
            get { return _ResRntlwithDriver; }
            set { _ResRntlwithDriver = value; }
        }


        public double ResRntldriverCharge
        {
            get { return _ResRntldriverCharge; }
            set { _ResRntldriverCharge = value; }
        }

        public double ResRntlRate
        {
            get { return _ResRntlRate; }
            set { _ResRntlRate = value; }
        }
        

        public DateTime ResReturnDate
        {
            get { return _resReturnDate; }
            set { _resReturnDate = value; }
        }

        public DateTime ResPicupDate
        {
            get { return _resPicupDate; }
            set { _resPicupDate = value; }
        }

        public string ResVhclID
        {
            get { return _resVhclID; }
            set { _resVhclID = value; }
        }

        public string ResCstID
        {
            get { return _resCstID; }
            set { _resCstID = value; }
        }

        public string ResID
        {
            get { return _resID; }
            set { _resID = value; }
        }
    }
}
